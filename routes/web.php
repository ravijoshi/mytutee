<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();



/*
  |--------------------------------------------------------------------------
  | Web Routes for Login
  |--------------------------------------------------------------------------
 */

// route to show the login form 
Route::get('/', ('LoginController@showLogin'));
Route::get('login', ('LoginController@showLogin'))->name("login");
// route to process the login
Route::post('login',  'LoginController@doLogin')->name("login");
Route::get('forgot-password',  'LoginController@forgotPassword')->name('forgotPassword');

Route::POST('forgot-password',  'UserController@forgotPwd')->name('forgotpassword');
Route::POST('resetpassword',  'LoginController@resetPasswordPost')->name('resetPasswordPost');
Route::get('reset-password/{token?}',  'LoginController@resetPassword')->name('resetpassword');

//cron job url start
Route::get('children-arrive',  'CronjobController@childrenArrivedAtSchool')->name('childrenArrivedAtSchool');
Route::get('expired-free-trial',  'CronjobController@expiredFreeTrailPack')->name('expiredFreeTrailPack');
Route::get('expired-sbuscription',  'CronjobController@expiredSubscriptionPack')->name('expiredFreeTrailPack');
Route::get('sbuscription-remind',  'CronjobController@sendReminderNotification')->name('sendReminderNotification');
Route::get('start-trip-remind',  'CronjobController@startTripReminderNotification')->name('startTripReminderNotification');
Route::get('sh_start-trip-remind',  'CronjobController@startSHTripReminderNotification')->name('startSHTripReminderNotification');
Route::get('driver-earning',  'CronjobController@saveDriverMonthlyEarning')->name('saveDriverMonthlyEarning');
Route::get('end-sh-trip-remind',  'CronjobController@endSHTripNotification')->name('endSHTripNotification');
Route::get('end-hs-trip-remind',  'CronjobController@endHSTripNotification')->name('endHSTripNotification');
Route::get('end-trip',  'CronjobController@endTrip')->name('endTrip');
//cron job url end

// cms page start
Route::get('help',  'PageController@parentHelp')->name('parentHelp');
Route::get('terms-conditions',  'PageController@parentTerms')->name('parentTerms');
Route::get('terms-conditions-driver',  'PageController@driverTerms')->name('driverTerms');
Route::get('help-driver',  'PageController@driverHelp')->name('driverHelp');
// cms page end


/********** After login **********/
Route::group(['middleware' => 'webToken'], function () {
	// route to process the logout
	Route::get('logout',  'LoginController@doLogout')->name('logout');

	Route::get('dashboard',  'LoginController@showDashboard')->name('dashboard');
	Route::get('holisticview',  'HolisticController@index')->name('holisticview');

	Route::get('profile',  'UserController@index')->name('profile');
	Route::get('edit-profile',  'UserController@edit')->name('profile.edit');
	Route::post('update-profile',  'UserController@update')->name('user.update');
	Route::get('change-password',  'UserController@changePassword')->name('user.changePassword');
	Route::post('change-pwd',  'UserController@changePasswordPost')->name('user.changePwd');

	// List of users who have not completed the Registration
	Route::get('registerationPending',  'UserController@registerationPending')->name('user.registerationPending');
	Route::post('registerationPending-list',  'UserController@registerationPendingList')->name('user.registerationPendingList');
	Route::post('registerationPending-delete/{id?}',  'UserController@forcedelete')->name('user.forcedelete');
	
	// route School
	Route::get('schools', 'SchoolController@schoolList')->name('schools');
	Route::post('getSchoolList','SchoolController@getSchoolList')->name('getSchoolList');
	Route::get('schools/edit/{id?}','SchoolController@edit')->name('schools/edit');
	Route::get('delete/{id?}', 'SchoolController@delete')->name('schools/delete');
	Route::get('schools/add', 'SchoolController@create')->name('schools/add');
	Route::post('addschoolinfo', 'SchoolController@addschool')->name('addschoolinfo');
	Route::post('changestatus/{id?}',  'SchoolController@ChangeStatus')->name('changestatus');
	Route::get('school/import',  'SchoolController@importForm')->name('importForm');
	Route::post('school-import',  'SchoolController@import')->name('import');
	Route::get('school-export',  'SchoolController@export')->name('export');
	Route::post('school-export-search',  'SchoolController@exportSearch')->name('school.export');
	Route::get('school-search',  'SchoolController@search')->name('school.search');
	Route::post('school-searchList',  'SchoolController@getSearchList')->name('school.search.list');
	
	// Driver routes
	Route::get('drivers',  'DriverController@index')->name('drivers');
	Route::post('getDriverList','DriverController@getDriverList')->name('getDriverList');
	Route::get('driver/edit/{id?}',  'DriverController@edit')->name('driver.edit');
	Route::get('driver/add',  'DriverController@create')->name('driver.add');
	Route::post('driver/save',  'DriverController@save')->name('driver.save');
	Route::post('driver/delete/{id?}',  'DriverController@delete')->name('driver.delete');
	Route::post('driver/hard-delete/{id?}',  'DriverController@forcedelete')->name('driver.hard.delete');
	Route::post('driver/restore/{id?}',  'DriverController@restore')->name('driver.restore');
	Route::post('driver/changestatus/{id?}',  'DriverController@changeStatus')->name('driver.changestatus');

	// Parent routes
	Route::get('parents',  'ParentController@index')->name('parents');
	Route::post('parent/list','ParentController@getParentList')->name('parent.list');
	Route::get('parent/edit/{id?}',  'ParentController@edit')->name('parent.edit');
	Route::get('parent/add',  'ParentController@create')->name('parent.add');
	Route::post('parent/save',  'ParentController@save')->name('parent.save');
	Route::post('parent/delete/{id?}',  'ParentController@delete')->name('parent.delete');
	Route::post('parent/hard-delete/{id?}',  'ParentController@forcedelete')->name('parent.hard_delete');
	Route::post('parent/restore/{id?}',  'ParentController@restore')->name('parent.restore');
	Route::post('parent/changestatus/{id?}',  'ParentController@changeStatus')->name('parent.changestatus');

	//Children routes
	Route::get('children/{parentId?}',  'ChildrenController@index')->name('children');
	Route::post('children-list/{parentId?}','ChildrenController@getChildrenList')->name('children.list');
	Route::get('children-edit/{id?}',  'ChildrenController@edit')->name('children.edit');
	Route::get('children-add/{parentId?}',  'ChildrenController@create')->name('children.add');
	Route::post('children-save',  'ChildrenController@save')->name('children.save');
	Route::post('children-delete/{id?}',  'ChildrenController@delete')->name('children.delete');
	Route::post('children-hard-delete/{id?}',  'ChildrenController@forcedelete')->name('children.hard_delete');
	Route::post('children-changestatus/{id?}',  'ChildrenController@changeStatus')->name('children.changestatus');

	// mailtemplate routes
	Route::get('mailtemplate',  'MailtemplateController@index')->name('mailtemplate');
	Route::post('mailtemplate/list','MailtemplateController@grid')->name('mailtemplate.list');
	Route::get('mailtemplate/add',  'MailtemplateController@create')->name('mailtemplate.add');
	Route::get('mailtemplate/edit/{id?}',  'MailtemplateController@edit')->name('mailtemplate.edit');
	Route::post('mailtemplate/save',  'MailtemplateController@save')->name('mailtemplate.save');
	Route::post('mailtemplate/delete/{id?}',  'MailtemplateController@delete')->name('mailtemplate.delete');
	Route::post('mailtemplate/changestatus/{id?}',  'MailtemplateController@changeStatus')->name('mailtemplate.changestatus');
	
	// system_config routes
	Route::get('system-config',  'SystemConfigController@index')->name('system_config');
	Route::post('system-config/list','SystemConfigController@grid')->name('system_config.list');
	Route::get('system-config/add',  'SystemConfigController@create')->name('system_config.add');
	Route::get('system-config/edit/{id?}',  'SystemConfigController@edit')->name('system_config.edit');
	Route::post('system-config/save',  'SystemConfigController@save')->name('system_config.save');
	Route::post('system-config/delete/{id?}',  'SystemConfigController@delete')->name('system_config.delete');
	Route::post('system-config/changestatus/{id?}',  'SystemConfigController@changeStatus')->name('system_config.changestatus');
	
	// page routes
	Route::get('pages',  'PageController@index')->name('page');
	Route::post('page/list','PageController@grid')->name('page.list');
	Route::get('page/add',  'PageController@create')->name('page.add');
	Route::get('page/edit/{id?}',  'PageController@edit')->name('page.edit');
	Route::post('page/save',  'PageController@save')->name('page.save');
	Route::post('page/delete/{id?}',  'PageController@delete')->name('page.delete');
	Route::post('page/changestatus/{id?}',  'PageController@changeStatus')->name('page.changestatus');
	
	// bank routes
	Route::get('banks',  'BankController@index')->name('bank');
	Route::post('bank/list','BankController@grid')->name('bank.list');
	Route::get('bank/add',  'BankController@create')->name('bank.add');
	Route::get('bank/edit/{id?}',  'BankController@edit')->name('bank.edit');
	Route::post('bank/save',  'BankController@save')->name('bank.save');
	Route::post('bank/delete/{id?}',  'BankController@delete')->name('bank.delete');
	Route::post('bank/changestatus/{id?}',  'BankController@changeStatus')->name('bank.changestatus');
	
	// package routes
	Route::get('packages',  'PackageController@index')->name('package');
	Route::post('package-list','PackageController@grid')->name('package.list');
	Route::get('package/add',  'PackageController@create')->name('package.add');
	Route::get('package/edit/{id?}',  'PackageController@edit')->name('package.edit');
	Route::post('package/save',  'PackageController@save')->name('package.save');
	Route::post('package/delete/{id?}',  'PackageController@delete')->name('package.delete');
	Route::post('package/changestatus/{id?}',  'PackageController@changeStatus')->name('package.changestatus');

	// route routes
	Route::get('routes-listing',  'RouteController@index')->name('route');
	Route::post('route/list','RouteController@grid')->name('route.list');
	Route::get('route/add',  'RouteController@create')->name('route.add');
	Route::get('route/edit/{id?}',  'RouteController@edit')->name('route.edit');
	Route::post('route/save',  'RouteController@save')->name('route.save');
	Route::post('route/delete/{id?}',  'RouteController@delete')->name('route.delete');
	Route::post('route/changestatus/{id?}',  'RouteController@changeStatus')->name('route.changestatus');

	// suggestion routes
	Route::get('suggestions',  'SuggestionController@index')->name('suggestion');
	Route::get('suggestion-view/{id?}',  'SuggestionController@view')->name('suggestion.view');
	Route::post('suggestion/list','SuggestionController@grid')->name('suggestion.list');
	Route::post('suggestion/changestatus/{id?}',  'SuggestionController@changeStatus')->name('suggestion.changestatus');
	Route::get('suggestion/reply/{id?}',  'SuggestionController@reply')->name('suggestion.reply');
	Route::post('suggestion-reply',  'SuggestionController@replyPost')->name('suggestion-reply');


	// Order routes
	Route::get('order',  'OrderController@index')->name('order');
	Route::post('order-list','OrderController@grid')->name('order.list');
	Route::get('order/add',  'OrderController@create')->name('order.add');
	Route::get('order/view/{id?}',  'OrderController@view')->name('order.view');
	Route::post('order/save',  'OrderController@save')->name('order.save');
	Route::post('order/delete/{id?}',  'OrderController@delete')->name('order.delete');
	Route::post('order/changestatus/{id?}',  'OrderController@changeStatus')->name('order.changestatus');
});
