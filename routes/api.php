<?php

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API v1 routes
Route::prefix('v1')/*->middleware(['LogAfterRequest'])*/->namespace('API')->group(function () {
    // Auth Controller    
    Route::post('/register', 'AuthController@register');
    Route::post('/googleSigning', 'AuthController@googleSigning');
    Route::post('/userSigningMobileUpdate', 'AuthController@userSigningMobileUpdate');
    Route::post('/login', 'AuthController@login');

    Route::post('/otpVerification', 'AuthController@otpVerification');
    Route::post('/forgotPassword', 'AuthController@forgotPassword');
    Route::post('/resetPassword', 'AuthController@resetPassword');
    // Route::get('/sendOtp', 'CommonController@sendOtp');

    // check authentication
    Route::middleware(['APIToken'])->group(function () {

        // Auth Controller
        Route::post('/logout', 'AuthController@postLogout');
        Route::post('/updateDeviceToken', 'AuthController@updateDeviceToken');
        Route::post('/sendPushNotificaion', 'AuthController@sendPushNotificaion');

        // User Controller 
        Route::get('/driverDashboard', 'UserController@driverdashboard');
        Route::get('/parentDashboard', 'UserController@parentDashboard');
        Route::post('/updateProfile', 'UserController@updateProfile');
        Route::post('/changePassword', 'UserController@changePassword');

        // Vehicle Controller
        Route::post('/alterVehicleDetails', 'VehicleController@alterVehicleDetails');
        Route::get('/getVehicle', 'VehicleController@getVehicle');
        Route::post('/trackVehicleLocation', 'VehicleController@trackVehicleLocation');
        Route::get('/getChildrenOnRoute/{route_id?}', 'VehicleController@getChildrenInVehicle');

        // Route Controller
        Route::get('/getRouteList', 'RouteController@getRouteList');
        Route::post('/getRouteDetails', 'RouteController@getRouteDetails');
        Route::post('/deleteRouteDetails', 'RouteController@deleteRouteDetails');
        Route::post('/addRouteDetail', 'RouteController@create');
        Route::post('/updateRouteDetail', 'RouteController@update');
        Route::post('/joinRoute', 'RouteController@joinRoute');

        // Child Controller
        Route::get('/getChildList', 'ChildController@getChildList');
        Route::post('/deleteChild', 'ChildController@deleteChild');
        Route::post('/alterChildDetails', 'ChildController@alterChildDetails');
        Route::post('/activateFreeTrial', 'ChildController@activateFreeTrial');
        Route::post('/childNotComing', 'ChildController@childNotComing');

        // Trip Controller
        Route::post('/tripStart', 'TripController@startTrip');
        Route::post('/tripEnd', 'TripController@endTrip');
        Route::post('/updateTripLocations', 'TripController@updateTripLocations');
        Route::post('/getTripHistory', 'TripController@getTripHistory');

        // School Controller 
        Route::get('/getSchoolList', 'SchoolController@getSchoolList');

        // Page Controller 
        Route::any('/contactus', 'PageController@contactus');

        // Suggestion Controller
        Route::post('/suggestions', 'SuggestionController@suggestions');
        
        // Bank Controller
        Route::get('/getBankDetail', 'BankController@getBankDetail');
        Route::post('/alterBankDetail', 'BankController@updateBankDetails');

        // Share rote code with parent
        Route::post('/shareRouteCodeWithParent', 'CommonController@shareRouteCodeToParent');
    });
});


//API v2 routes  subscription functioanlity added in v2
Route::namespace('API')/*->middleware(['LogAfterRequest'])*/->prefix('v2')->group(function () {
    Route::namespace('V2')->group(function () {
        // Auth Controller    
        Route::post('/register', 'AuthController@register');
        Route::post('/googleSigning', 'AuthController@googleSigning');
        Route::post('/userSigningMobileUpdate', 'AuthController@userSigningMobileUpdate');
        Route::post('/login', 'AuthController@login');

        Route::post('/otpVerification', 'AuthController@otpVerification');
        Route::post('/forgotPassword', 'AuthController@forgotPassword');
        Route::post('/resetPassword', 'AuthController@resetPassword');

        // check authentication
        Route::middleware(['APIToken'])->group(function () {
            // Auth Controller
            Route::post('/logout', 'AuthController@postLogout');
            Route::post('/updateDeviceToken', 'AuthController@updateDeviceToken');
            Route::post('/sendPushNotificaion', 'AuthController@sendPushNotificaion');

            // User Controller 
            Route::get('/driverDashboard', 'UserController@driverdashboard');
            Route::get('/parentDashboard', 'UserController@parentDashboard');
            Route::post('/updateProfile', 'UserController@updateProfile');
            Route::post('/changePassword', 'UserController@changePassword');

            // Vehicle Controller
            Route::post('/alterVehicleDetails', 'VehicleController@alterVehicleDetails');
            Route::get('/getVehicle', 'VehicleController@getVehicle');
            Route::post('/trackVehicleLocation', 'VehicleController@trackVehicleLocation');
            Route::get('/getChildrenOnRoute/{route_id?}', 'VehicleController@getChildrenInVehicle');

            // Route Controller
            Route::get('/getRouteList', 'RouteController@getRouteList');
            Route::post('/getRouteDetails', 'RouteController@getRouteDetails');
            Route::post('/deleteRouteDetails', 'RouteController@deleteRouteDetails');
            Route::post('/addRouteDetail', 'RouteController@create');
            Route::post('/updateRouteDetail', 'RouteController@update');
            Route::post('/joinRoute', 'RouteController@joinRoute');

            // Child Controller
            Route::get('/getChildList', 'ChildController@getChildList');
            Route::post('/deleteChild', 'ChildController@deleteChild');
            Route::post('/alterChildDetails', 'ChildController@alterChildDetails');
            Route::post('/activateFreeTrial', 'ChildController@activateFreeTrial');
            Route::post('/childNotComing', 'ChildController@childNotComing');

            // Trip Controller
            Route::post('/tripStart', 'TripController@startTrip');
            Route::post('/tripEnd', 'TripController@endTrip');
            Route::post('/updateTripLocations', 'TripController@updateTripLocations');
            Route::post('/getTripHistory', 'TripController@getTripHistory');
             Route::post('/checkTripIsOngoing', 'TripController@checkTripIsOngoing');

            // School Controller 
            Route::get('/getSchoolList', 'SchoolController@getSchoolList');

            // Page Controller 
            Route::any('/contactus', 'PageController@contactus');

            // Suggestion Controller
            Route::post('/suggestions', 'SuggestionController@suggestions');
            
            // Bank Controller
            Route::get('/getBankDetail', 'BankController@getBankDetail');
            Route::post('/alterBankDetail', 'BankController@updateBankDetails');

            // Share rote code with parent
            Route::post('/shareRouteCodeWithParent', 'CommonController@shareRouteCodeToParent');
            
            // Paytm routes
            Route::post('/generateChecksum', 'PaytmController@generateChecksum');
            Route::post('/verifyChecksum', 'PaytmController@verifyChecksum');
            
            // Settings routes
            Route::get('getSettings', 'SettingController@index');
            Route::post('updateSetting', 'SettingController@update');

        });
    });
});