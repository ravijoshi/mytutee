-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 31, 2018 at 01:43 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mytutee`
--

-- --------------------------------------------------------

--
-- Table structure for table `childs`
--

DROP TABLE IF EXISTS `childs`;
CREATE TABLE IF NOT EXISTS `childs` (
  `child_id` int(10) NOT NULL AUTO_INCREMENT,
  `child_fname` varchar(255) NOT NULL,
  `child_mname` varchar(255) NOT NULL,
  `child_lname` varchar(255) NOT NULL,
  `parent_id` int(10) NOT NULL,
  `school_id` int(10) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `pickup_lat` varchar(50) NOT NULL,
  `pickup_long` varchar(50) NOT NULL,
  `pickup_address` varchar(255) NOT NULL,
  `child_status` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `childs`
--

INSERT INTO `childs` (`child_id`, `child_fname`, `child_mname`, `child_lname`, `parent_id`, `school_id`, `driver_id`, `route_id`, `pickup_lat`, `pickup_long`, `pickup_address`, `child_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sanket', 'Hardik', 'Patel', 14, 3, 13, 1, '23.0371775', '72.5292381', 'A/8, Himalaya Arcade, Block A, Opposite Vastrapur lake, Nehru Park, Mahavir Nagar society, Vastrapur, Ahmedabad, Gujarat 380015, India', 1, '2018-08-31 12:43:28', '2018-08-31 13:34:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mailtemplate`
--

DROP TABLE IF EXISTS `mailtemplate`;
CREATE TABLE IF NOT EXISTS `mailtemplate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `access_key` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive 1 = active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailtemplate`
--

INSERT INTO `mailtemplate` (`id`, `subject`, `access_key`, `content`, `cc`, `bcc`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '{{website_name}} : Forgot Password', 'forgot_password', '<p>{{website_name}}</p>\r\n\r\n<table cellspacing=\"0\" style=\"width:600px\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"height:83px; text-align:center\"><a href=\"{{logoUrl}}\" target=\"_blank\"><img alt=\"{{website_name}}\" src=\"{{emailLogo}}\" style=\"height:50px\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">Dear {{name}},</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">You have requested to change your password on {{website_name}}</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"text-align:center\"><a href=\"{{changeUrl}}\">CHANGE PASSWORD</a></td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">{{website_name}} Team.</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">\r\n			<hr /></td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"background-color:#f3f3f3\">If you believe you received this email in error, please reply to this email and let us know.<br />\r\n			Thank you.<br />\r\n			<br />\r\n			If you don&rsquo;t see correctly this email click this link in order to reset password:<br />\r\n			<a href=\"{{changeUrl}}\">{{changeUrl}}</a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'siddhraj.chauhan@plutustec.com', 'siddhraj.chauhan@plutustec.com', 1, '2018-08-28 09:21:57', '2018-08-28 10:40:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(10) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) NOT NULL,
  `access_key` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_status` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_name`, `access_key`, `page_content`, `page_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'contact us', '', '<div class=\"contact-address\">\r\n<h5>Contact Address</h5>\r\n\r\n<p>Dummy Address<br />\r\nLorem Ipsum Sit Amet<br />\r\nDummy Pin<br />\r\nDummy place<br />\r\nTelephone : 1-800-123-4567<br />\r\nEmail : info@dummy.com</p>\r\n</div>\r\n', 1, '2018-08-24 04:04:01', '2018-08-24 04:04:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
CREATE TABLE IF NOT EXISTS `routes` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_code` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `start_loc_lat` varchar(255) NOT NULL,
  `start_loc_long` varchar(255) NOT NULL,
  `start_address` text NOT NULL,
  `end_loc_lat` varchar(255) NOT NULL,
  `end_loc_long` varchar(255) NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  `route_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`route_id`, `route_code`, `driver_id`, `school_id`, `start_loc_lat`, `start_loc_long`, `start_address`, `end_loc_lat`, `end_loc_long`, `start_time`, `end_time`, `route_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'RO1DR13', 13, 3, '23.045026', '72.515289', 'Zodiac Square Building, Opp. Gurudwara, Sarkhej - Gandhinagar Highway, Bodakdev, Ahmedabad, Gujarat 380059, India', '23.051488', '72.516902', '1535718900', '1535723100', 1, '2018-08-31 07:08:30', '2018-08-31 08:06:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `route_details`
--

DROP TABLE IF EXISTS `route_details`;
CREATE TABLE IF NOT EXISTS `route_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `route_id` int(10) NOT NULL,
  `child_id` int(11) NOT NULL,
  `route_promo_code` varchar(50) NOT NULL,
  `route_code_available` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
CREATE TABLE IF NOT EXISTS `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) NOT NULL,
  `school_address` varchar(255) NOT NULL,
  `school_latitude` varchar(255) NOT NULL,
  `school_longitude` varchar(255) NOT NULL,
  `school_map_id` varchar(255) NOT NULL,
  `school_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `school_name`, `school_address`, `school_latitude`, `school_longitude`, `school_map_id`, `school_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Udgam School For Children', 'Opp. Sardar Patel Institute, Patel Society, Jai Ambe Nagar, Thaltej, Ahmedabad, Gujarat 380054', '23.051588', '72.519963', '11111111', 1, '2018-08-28 19:36:02', '2018-08-28 19:36:02', NULL),
(2, 'Aavishkar School', 'Ognaj - Vadsar Rd, Lapkaman, Ahmedabad, Gujarat 380060', '23.129779', '72.477462', '22222222', 1, '2018-08-28 19:36:02', '2018-08-28 19:36:02', NULL),
(3, 'Nirma Vidyavihar', 'Plot No. 390, Besides Galaxy Tower, Nr. The Grand Bhagawati Hotel, Bodakdev, Bodakdev, Ahmedabad, Gujarat 380054', '23.051488', '72.516902', '33333333', 1, '2018-08-28 21:38:05', '2018-08-28 21:38:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

DROP TABLE IF EXISTS `suggestions`;
CREATE TABLE IF NOT EXISTS `suggestions` (
  `suggestion_id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `suggestion_msg` text NOT NULL,
  `suggestion_stauts` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`suggestion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`suggestion_id`, `parent_id`, `suggestion_msg`, `suggestion_stauts`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 'nothing jshhsksxbxbxxkx sbdjddkdjdnd', 1, '2018-08-31 12:44:13', '2018-08-31 12:44:13', NULL),
(2, 14, 'High gfddghjksjagajsjshsgshsa', 1, '2018-08-31 12:44:22', '2018-08-31 12:44:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

DROP TABLE IF EXISTS `trips`;
CREATE TABLE IF NOT EXISTS `trips` (
  `trip_id` int(5) NOT NULL AUTO_INCREMENT,
  `driver_id` int(5) DEFAULT NULL,
  `route_id` int(5) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `trip_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`trip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`trip_id`, `driver_id`, `route_id`, `start_time`, `end_time`, `trip_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 13, 1, '2018-08-31 07:10:21', '2018-08-31 07:11:24', 1, '2018-08-31 07:10:21', '2018-08-31 07:11:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trip_details`
--

DROP TABLE IF EXISTS `trip_details`;
CREATE TABLE IF NOT EXISTS `trip_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `trip_id` int(10) NOT NULL,
  `trip_lat` varchar(255) NOT NULL,
  `trip_long` varchar(255) NOT NULL,
  `trip_time` datetime NOT NULL,
  `trip_accuracy` varchar(255) NOT NULL,
  `trip_bearing` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip_details`
--

INSERT INTO `trip_details` (`id`, `trip_id`, `trip_lat`, `trip_long`, `trip_time`, `trip_accuracy`, `trip_bearing`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '23.0507222', '72.5187029', '2018-08-31 12:40:23', '17.009', '0', '2018-08-31 12:40:31', '2018-08-31 12:40:31', NULL),
(2, 1, '23.0491183', '72.5165308', '2018-08-31 12:40:35', '100', '0', '2018-08-31 12:40:41', '2018-08-31 12:40:41', NULL),
(3, 1, '23.0489813', '72.5163269', '2018-08-31 12:40:47', '81.044', '0', '2018-08-31 12:40:52', '2018-08-31 12:40:52', NULL),
(4, 1, '23.0488231', '72.5160949', '2018-08-31 12:40:59', '56.768', '0', '2018-08-31 12:41:02', '2018-08-31 12:41:02', NULL),
(5, 1, '23.0507223', '72.5187032', '2018-08-31 12:41:11', '22.556', '0', '2018-08-31 12:41:13', '2018-08-31 12:41:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(252) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role` int(10) NOT NULL COMMENT '0=>''Admin'', 1=>''Driver'', 2=>"Parents"',
  `user_image` text COLLATE utf8mb4_unicode_ci,
  `user_image_url` text COLLATE utf8mb4_unicode_ci,
  `user_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=inactive,1=active,2=delete',
  `user_social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_otp_token` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_otp_timeout` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_otp_verification` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_fname`, `user_lname`, `email`, `password`, `user_phone`, `user_role`, `user_image`, `user_image_url`, `user_status`, `user_social_id`, `user_otp_token`, `user_otp_timeout`, `user_otp_verification`, `remember_token`, `api_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'user', 'admin@admin.com', NULL, '9879879879', 0, NULL, NULL, '1', NULL, NULL, '', '0', NULL, '', NULL, NULL, NULL),
(13, 'Plutus', 'Tech', 'plutus1202@gmail.com', NULL, '9879879879', 1, NULL, NULL, '1', '103341898491406823334', '123456', '1535726231', '1', NULL, 'RUpiNGx0RTNjR2FQakJ4STNpcEQyaWs2OXJJMjUxdnI5Z3ZyVGVlTTN0WUhHcWV1VmU1bUZ6NnBPdmt55b893ca25954d', '2018-08-31 07:07:05', '2018-08-31 07:33:19', NULL),
(14, 'Hardik', 'Patel', 'hardik.patel@plutustec.com', '$2y$10$kyANhJCt2XZDPNuIaovw..47TPGcRpah7mpzYj21.i9QnygGZyZR6', '9998887770', 2, 'no-user.png', 'https://lh4.googleusercontent.com/-uL7eQUw1AGs/AAAAAAAAAAI/AAAAAAAAAAw/I9WGG4NXKuQ/photo.jpg', '1', '117112281962789222254', '123456', '1535726532', '1', NULL, 'RUpiNGx0RTNjR2FQakJ4STNpcEQyaWs2OXJJMjUxdnI5Z3ZyVGVlTTN0WUhHcWV1VmU1bUZ6NnBPdmt55b893ca25954d', '2018-08-31 07:12:12', '2018-08-31 07:33:30', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_bank`
--

DROP TABLE IF EXISTS `user_bank`;
CREATE TABLE IF NOT EXISTS `user_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `ifsc_code` varchar(255) NOT NULL,
  `bank_address` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_tokens`
--

DROP TABLE IF EXISTS `user_tokens`;
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `api_token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `vehicle_owner_name` varchar(255) NOT NULL,
  `vehicle_capacity` int(11) NOT NULL DEFAULT '0',
  `vehicle_photo` text,
  `vehicle_reg_no` varchar(255) NOT NULL,
  `vehicle_school_id` varchar(255) NOT NULL,
  `vehicle_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vehicle_id`, `driver_id`, `vehicle_owner_name`, `vehicle_capacity`, `vehicle_photo`, `vehicle_reg_no`, `vehicle_school_id`, `vehicle_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 13, 'Kamal', 1, '1535719843.jpg', 'gh1gh1244', '888', 1, '2018-08-31 07:07:49', '2018-08-31 07:32:37', NULL),
(2, 13, 'Kamal', 10, 'no-vehicle.png', 'gh1gh1244', '007', 1, '2018-08-31 07:07:54', '2018-08-31 07:07:54', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
