-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2018 at 08:44 AM
-- Server version: 5.6.17
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mytutee`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `route_list`
--

CREATE TABLE IF NOT EXISTS `route_list` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_code` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `start_loc_lat` varchar(255) NOT NULL,
  `start_loc_long` varchar(255) NOT NULL,
  `end_loc_lat` varchar(255) NOT NULL,
  `end_loc_long` varchar(255) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `route_list`
--

INSERT INTO `route_list` (`route_id`, `route_code`, `driver_id`, `school_id`, `start_loc_lat`, `start_loc_long`, `end_loc_lat`, `end_loc_long`, `start_time`, `end_time`, `created_at`, `updated_at`) VALUES
(1, 'RCD220', 1, 1, '44.968046', '-94.420307', ' 44.33328 ', ' -89.132008', '02:00:00', '03:00:00', '2018-08-21 13:20:18', '2018-08-21 13:20:18'),
(2, 'RCD221', 1, 2, '44.968046', '-94.420307', ' 44.33328 ', ' -89.132008', '02:00:00', '03:00:00', '2018-08-21 13:20:22', '2018-08-21 13:20:22');

-- --------------------------------------------------------

--
-- Table structure for table `school_list`
--

CREATE TABLE IF NOT EXISTS `school_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) NOT NULL,
  `school_address` varchar(255) NOT NULL,
  `school_latitude` varchar(255) NOT NULL,
  `school_longitude` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `school_list`
--

INSERT INTO `school_list` (`id`, `school_name`, `school_address`, `school_latitude`, `school_longitude`, `created_at`, `updated_at`) VALUES
(1, 'First School Name', 'First School Address', '', '', '2018-08-21 13:18:35', '2018-08-21 13:18:35'),
(2, 'Sec School Name', 'Sec  School Address', '', '', '2018-08-21 13:18:37', '2018-08-21 13:18:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(252) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role` int(10) NOT NULL COMMENT '0=>''Admin'', 1=>''Driver'', 2=>"Parents"',
  `user_image` text COLLATE utf8mb4_unicode_ci,
  `user_image_url` text COLLATE utf8mb4_unicode_ci,
  `user_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=inactive,1=active,2=delete',
  `user_social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_otp_token` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_otp_timeout` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_otp_verification` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`user_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_fname`, `user_lname`, `user_email`, `user_password`, `user_phone`, `user_role`, `user_image`, `user_image_url`, `user_status`, `user_social_id`, `user_otp_token`, `user_otp_timeout`, `user_otp_verification`, `remember_token`, `api_token`, `created_at`, `updated_at`) VALUES
(1, 'Hardik', 'Patel', 'hardik.patel@plutustec.com', '$2y$10$3Q4JVfdwtdEKIfor8FQqvuvpRVxRJphh5FSQuKWEmxNZygoSKZHBG', '9998809886', 1, '1534918490.jpg', 'https://lh4.googleusercontent.com/-uL7eQUw1AGs/AAAAAAAAAAI/AAAAAAAAAAw/I9WGG4NXKuQ/photo.jpg', '1', '117112281962789222254', '123456', '1534925691', '1', NULL, 'aWwxSklkSjIxREpQc1RvaTg2bEVvOHF4bzZFZGY1SEZVUFZhYTRMZXp1QjdUbHVaaXFWY0FTeE9oMTFm5b7d0ead07f19', '2018-08-22 00:44:51', '2018-08-22 01:50:13');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE IF NOT EXISTS `vehicle` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `vehicle_owner_name` varchar(255) NOT NULL,
  `vehicle_capacity` int(11) NOT NULL DEFAULT '0',
  `vehicle_photo` text,
  `vehicle_reg_no` varchar(255) NOT NULL,
  `vehicle_school_id` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `vehicle`
--

INSERT INTO `vehicle` (`vehicle_id`, `driver_id`, `vehicle_owner_name`, `vehicle_capacity`, `vehicle_photo`, `vehicle_reg_no`, `vehicle_school_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hardik Patel', 10, '1534920817.jpg', 'gj18af3456', 'hfj', NULL, '2018-08-22 01:24:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
