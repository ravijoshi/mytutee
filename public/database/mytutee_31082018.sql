-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 29, 2018 at 03:08 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mytutee`
--

-- --------------------------------------------------------

--
-- Table structure for table `childs`
--

DROP TABLE IF EXISTS `childs`;
CREATE TABLE IF NOT EXISTS `childs` (
  `child_id` int(10) NOT NULL AUTO_INCREMENT,
  `child_fname` varchar(255) NOT NULL,
  `child_mname` varchar(255) NOT NULL,
  `child_lname` varchar(255) NOT NULL,
  `parent_id` int(10) NOT NULL,
  `school_id` int(10) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `child_status` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `childs`
--

INSERT INTO `childs` (`child_id`, `child_fname`, `child_mname`, `child_lname`, `parent_id`, `school_id`, `driver_id`, `route_id`, `child_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Child First Name', 'Child Middle Name', 'Child Last Name', 6, 1, 1, 1, 1, '2018-08-29 15:03:06', '2018-08-29 15:03:06', NULL),
(2, 'Child First Name', 'Child Middle Name', 'Child Last Name', 6, 1, 1, 1, 1, '2018-08-29 15:03:51', '2018-08-29 15:04:05', '2018-08-29 09:34:05'),
(3, 'Child First Name', 'Child Middle Name', 'Child Last Name', 6, 1, 1, 1, 1, '2018-08-29 15:04:08', '2018-08-29 15:04:19', '2018-08-29 09:34:19');

-- --------------------------------------------------------

--
-- Table structure for table `mailtemplate`
--

DROP TABLE IF EXISTS `mailtemplate`;
CREATE TABLE IF NOT EXISTS `mailtemplate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `access_key` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `cc` varchar(255) DEFAULT NULL,
  `bcc` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0 = inactive 1 = active',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mailtemplate`
--

INSERT INTO `mailtemplate` (`id`, `subject`, `access_key`, `content`, `cc`, `bcc`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '{{website_name}} : Forgot Password', 'forgot_password', '<p>{{website_name}}</p>\r\n\r\n<table cellspacing=\"0\" style=\"width:600px\">\r\n	<tbody>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"height:83px; text-align:center\"><a href=\"{{logoUrl}}\" target=\"_blank\"><img alt=\"{{website_name}}\" src=\"{{emailLogo}}\" style=\"height:50px\" /></a></td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">Dear {{name}},</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">You have requested to change your password on {{website_name}}</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"text-align:center\"><a href=\"{{changeUrl}}\">CHANGE PASSWORD</a></td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">{{website_name}} Team.</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\">\r\n			<hr /></td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"2\" style=\"background-color:#f3f3f3\">If you believe you received this email in error, please reply to this email and let us know.<br />\r\n			Thank you.<br />\r\n			<br />\r\n			If you don&rsquo;t see correctly this email click this link in order to reset password:<br />\r\n			<a href=\"{{changeUrl}}\">{{changeUrl}}</a></td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'siddhraj.chauhan@plutustec.com', 'siddhraj.chauhan@plutustec.com', 1, '2018-08-28 09:21:57', '2018-08-28 10:40:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(10) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) NOT NULL,
  `access_key` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_status` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_name`, `access_key`, `page_content`, `page_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'contact us', '', '<div class=\"contact-address\">\r\n<h5>Contact Address</h5>\r\n\r\n<p>Dummy Address<br />\r\nLorem Ipsum Sit Amet<br />\r\nDummy Pin<br />\r\nDummy place<br />\r\nTelephone : 1-800-123-4567<br />\r\nEmail : info@dummy.com</p>\r\n</div>\r\n', 1, '2018-08-24 04:04:01', '2018-08-24 04:04:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

DROP TABLE IF EXISTS `routes`;
CREATE TABLE IF NOT EXISTS `routes` (
  `route_id` int(11) NOT NULL AUTO_INCREMENT,
  `route_code` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `start_loc_lat` varchar(255) NOT NULL,
  `start_loc_long` varchar(255) NOT NULL,
  `start_address` text NOT NULL,
  `end_loc_lat` varchar(255) NOT NULL,
  `end_loc_long` varchar(255) NOT NULL,
  `start_time` varchar(50) NOT NULL,
  `end_time` varchar(50) NOT NULL,
  `route_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`route_id`, `route_code`, `driver_id`, `school_id`, `start_loc_lat`, `start_loc_long`, `start_address`, `end_loc_lat`, `end_loc_long`, `start_time`, `end_time`, `route_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'RO1DR1', 1, 1, '44.968046', '-94.420307', 'first route start location', '23.051588', '72.519963', '1535439600', '1535461200', 1, '2018-08-29 09:10:16', '2018-08-29 09:10:16', NULL),
(2, 'RO2DR1', 1, 2, '44.968046', '-94.420307', 'second route start location', '23.129779', '72.477462', '1535439700', '1535461300', 1, '2018-08-29 09:10:31', '2018-08-29 09:10:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `route_details`
--

DROP TABLE IF EXISTS `route_details`;
CREATE TABLE IF NOT EXISTS `route_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `route_id` int(10) NOT NULL,
  `child_id` int(11) NOT NULL,
  `route_promo_code` varchar(50) NOT NULL,
  `route_code_available` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

DROP TABLE IF EXISTS `schools`;
CREATE TABLE IF NOT EXISTS `schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_name` varchar(255) NOT NULL,
  `school_address` varchar(255) NOT NULL,
  `school_latitude` varchar(255) NOT NULL,
  `school_longitude` varchar(255) NOT NULL,
  `school_map_id` varchar(255) NOT NULL,
  `school_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schools`
--

INSERT INTO `schools` (`id`, `school_name`, `school_address`, `school_latitude`, `school_longitude`, `school_map_id`, `school_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Udgam School For Children', 'Opp. Sardar Patel Institute, Patel Society, Jai Ambe Nagar, Thaltej, Ahmedabad, Gujarat 380054', '23.051588', '72.519963', '11111111', 1, '2018-08-28 19:36:02', '2018-08-28 19:36:02', NULL),
(2, 'Aavishkar School', 'Ognaj - Vadsar Rd, Lapkaman, Ahmedabad, Gujarat 380060', '23.129779', '72.477462', '22222222', 1, '2018-08-28 19:36:02', '2018-08-28 19:36:02', NULL),
(3, 'Nirma Vidyavihar', 'Plot No. 390, Besides Galaxy Tower, Nr. The Grand Bhagawati Hotel, Bodakdev, Bodakdev, Ahmedabad, Gujarat 380054', '23.051488', '72.516902', '33333333', 1, '2018-08-28 21:38:05', '2018-08-28 21:38:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

DROP TABLE IF EXISTS `suggestions`;
CREATE TABLE IF NOT EXISTS `suggestions` (
  `suggestion_id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) NOT NULL,
  `suggestion_msg` text NOT NULL,
  `suggestion_stauts` int(10) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`suggestion_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`suggestion_id`, `parent_id`, `suggestion_msg`, `suggestion_stauts`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 6, 'driver is late today', 1, '2018-08-29 15:05:25', '2018-08-29 15:05:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trips`
--

DROP TABLE IF EXISTS `trips`;
CREATE TABLE IF NOT EXISTS `trips` (
  `trip_id` int(5) NOT NULL AUTO_INCREMENT,
  `driver_id` int(5) DEFAULT NULL,
  `route_id` int(5) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `trip_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`trip_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trips`
--

INSERT INTO `trips` (`trip_id`, `driver_id`, `route_id`, `start_time`, `end_time`, `trip_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, '2018-08-29 09:17:01', '2018-08-29 09:20:25', 1, '2018-08-29 09:17:01', '2018-08-29 09:20:25', NULL),
(2, 1, 1, '2018-08-29 09:21:22', '2018-08-29 09:21:33', 1, '2018-08-29 09:21:22', '2018-08-29 09:21:33', NULL),
(3, 1, 1, '2018-08-29 09:22:28', '2018-08-29 09:23:06', 1, '2018-08-29 09:22:28', '2018-08-29 09:23:06', NULL),
(4, 1, 1, '2018-08-29 09:22:51', '2018-08-29 09:23:08', 1, '2018-08-29 09:22:51', '2018-08-29 09:23:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trip_details`
--

DROP TABLE IF EXISTS `trip_details`;
CREATE TABLE IF NOT EXISTS `trip_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `trip_id` int(10) NOT NULL,
  `trip_lat` varchar(255) NOT NULL,
  `trip_long` varchar(255) NOT NULL,
  `trip_time` datetime NOT NULL,
  `trip_accuracy` varchar(255) NOT NULL,
  `trip_bearing` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip_details`
--

INSERT INTO `trip_details` (`id`, `trip_id`, `trip_lat`, `trip_long`, `trip_time`, `trip_accuracy`, `trip_bearing`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '12.34', '12.34', '2018-08-24 05:38:02', '12.34', '12.34', '2018-08-29 14:53:24', '2018-08-29 14:53:24', NULL),
(2, 1, '12.34', '13.34', '2018-08-24 05:38:50', '12.34', '12.34', '2018-08-29 14:53:24', '2018-08-29 14:53:24', NULL),
(3, 1, '12.34', '14.34', '2018-08-24 05:40:03', '12.34', '12.34', '2018-08-29 14:53:24', '2018-08-29 14:53:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(252) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role` int(10) NOT NULL COMMENT '0=>''Admin'', 1=>''Driver'', 2=>"Parents"',
  `user_image` text COLLATE utf8mb4_unicode_ci,
  `user_image_url` text COLLATE utf8mb4_unicode_ci,
  `user_status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0=inactive,1=active,2=delete',
  `user_social_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_otp_token` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_otp_timeout` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_otp_verification` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_fname`, `user_lname`, `email`, `password`, `user_phone`, `user_role`, `user_image`, `user_image_url`, `user_status`, `user_social_id`, `user_otp_token`, `user_otp_timeout`, `user_otp_verification`, `remember_token`, `api_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'test1', 'test2', 'test12@gmail.com', '$2y$10$ATbhcVcbf.vOXwhRWeDACu/EdH4no1HLGUQgZdxY9Hw7/L0QvVtW6', '88888888888', 1, '1535552509.png', NULL, '1', NULL, '123456', '1535559709', '1', NULL, 'TTE3M1FGYTBmYktSQ1g0eEdpSThpVVJPUTlhUmIzb0xGWHQxM0UyQWRXNFhjdUpVRzhzQTZtWUxBNVNJ5b86acdb517a8', '2018-08-29 08:51:49', '2018-08-29 08:55:31', NULL),
(2, 'test3', 'test4', 'test34@gmail.com', '$2y$10$aLe1b7t6SHJUAsNAKcGUbuClKZKVtJyY0Ss/VTHUYcrekPvufvDyq', '88888888888', 1, 'no-user.png', NULL, '1', NULL, '123456', '1535559757', '0', NULL, 'UFI0Qk9FQ3NyOXNZV3NZOTJUbE5XVTlLcDg5NHRHSFVrYzVsOHJXT2cxVG1lMDJnVlB1azhFRGxMM1hv5b86ac2cd2c8f', '2018-08-29 08:52:37', '2018-08-29 08:52:37', NULL),
(6, 'test05', 'test06', 'test56@gmail.com', NULL, '9999999999', 2, NULL, 'http://testcreative.co.uk/wp-content/uploads/2017/10/Test-Logo-Small-Black-transparent-1.png', '1', '103341898491406823334', '123456', '1535562091', '1', NULL, 'ck9CakdjZXBDV040dWZnQlFWTlprWFdGdXpPSlptc3drOGhsMDBkUVNjMnpVdWVtQmdSWUNnTzloN29I5b86b56998908', '2018-08-29 09:27:31', '2018-08-29 09:32:01', NULL),
(7, 'test05', 'test06', 'admin@gmail.com', NULL, '9999999999', 0, NULL, 'http://testcreative.co.uk/wp-content/uploads/2017/10/Test-Logo-Small-Black-transparent-1.png', '1', '103341898491406823334', '123456', '1535562091', '1', NULL, '', '2018-08-29 09:27:31', '2018-08-29 09:32:01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE IF NOT EXISTS `vehicles` (
  `vehicle_id` int(11) NOT NULL AUTO_INCREMENT,
  `driver_id` int(11) NOT NULL,
  `vehicle_owner_name` varchar(255) NOT NULL,
  `vehicle_capacity` int(11) NOT NULL DEFAULT '0',
  `vehicle_photo` text,
  `vehicle_reg_no` varchar(255) NOT NULL,
  `vehicle_school_id` varchar(255) NOT NULL,
  `vehicle_status` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`vehicle_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`vehicle_id`, `driver_id`, `vehicle_owner_name`, `vehicle_capacity`, `vehicle_photo`, `vehicle_reg_no`, `vehicle_school_id`, `vehicle_status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Kamal Sanghavi', 4, '1535552921.jpg', 'GJ-01 FV 1234', '001', 1, '2018-08-29 08:57:59', '2018-08-29 08:58:41', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
