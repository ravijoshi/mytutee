// alert msg hide after time out
jQuery(function() {
    setTimeout(function() {
		jQuery(".alert").fadeOut("slow");
    }, 5000);
});


jQuery(document).ajaxStart(function(){
    // setTimeout(function () {
        jQuery('#grid_processing').hide();
        jQuery('#default-loader').show();
    // },100);//console.log('shown');
});
jQuery(document).ajaxStop(function(){
	setTimeout(function () {
        jQuery("#default-loader").hide();
        //  console.log('hidden');
    },1000);

    
    if(jQuery(document).width() <= "414" && jQuery("a").children("span").hasClass('fa-edit') ){
        jQuery("a").children('span.fa-edit').parent('a').css({"margin-bottom": "2px"});
    }
});

// add table responsive class to data table.
jQuery(document).ready(function () {
    if(!jQuery(".table-striped").parent('div').hasClass("table-responsive")){
        jQuery(".table-striped").parent('div').addClass("table-responsive");
    }

});