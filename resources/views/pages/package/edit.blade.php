@extends('layouts.master')
@section('title', 'Edit Package')
@section('content')

<section class="content-header">
    <h1>Edit Package <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Package </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                
                {!! Form::model($package,array('route' => array('package.save'), 'id' => 'package-frm', 'method' => 'post')) !!}
                    {!! Form::hidden('id',$package->id, array("class"=>"form-control required","id"=> "id")) !!}
                    <div class="box-body">
                        <div class="form-group ">
                            {!! Form::label('title', 'Package Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('title',$package->title,array("class"=>"form-control required","placeholder"=> "Package Name", "maxlength" => 20)) !!}
                        </div>

                        <div class="form-group ">
                            {!! Form::label('duration', 'Package Duration (In Months)',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('duration',$package->duration,array("class"=>"form-control digits required","placeholder"=> "Package Duration (In Months)", "min" => "1", "max" => "12")) !!}
                        </div>

                        <div class="form-group ">
                            {!! Form::label('price', 'Price',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('price',$package->price,array("class"=>"form-control numbers required","placeholder"=> "Price", "min" => 0, "max" => 100000)) !!}
                        </div>

                        <div class="form-group ">
                            {!! Form::label('description', 'Description',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::textarea('description',$package->description,array("class"=>"form-control required","placeholder"=> "description","rows" => 3 , "cols" => 10)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('status', array(1 => 'Active' ,0 => 'Inactive'),$package->status) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('package')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#package-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

