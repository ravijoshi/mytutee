@extends('layouts.master')
@section('title', 'Add Children')
@section('content')
<section class="content-header">
    <h1>Add Children <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('parents')}}"><i class="fa fa-user"></i> {{ __("Parent Management")}}</a></li>
        <li class="active">Add Children </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Children</h3>
                </div>
               {!! Form::open( array('route' => array('children.save'), 'id' => 'children-frm', 'method' => 'post')) !!}
                    <input type="hidden" class="form-control" value="{{ $parentId }}" name="parent_id" >
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('school_id', 'School Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('school_id',\App\School::getSchoolOptions(), NULL, array("class"=>"form-control required","placeholder"=> "Select School")) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('route_id', 'Route Code',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('route_id',\App\Route::getRouteCodeOptions(), NULL, array("class"=>"form-control required","placeholder"=> "Select Route Code")) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('child_fname', 'First Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('child_fname',NULL, array("class"=>"form-control required","placeholder"=> "First Name","maxlength"=> 100)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('child_mname', 'Middle Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('child_mname',NULL, array("class"=>"form-control required","placeholder"=> "Middle Name","maxlength"=> 100)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('child_lname', 'Last Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('child_lname',NULL, array("class"=>"form-control required","placeholder"=> "Last Name","maxlength"=> 100)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('pickup_address', 'Pickup Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('pickup_address',NULL, array("class"=>"form-control required","placeholder"=> "Pickup Address")) !!}
                            {!! Form::hidden('pickup_lat',NULL, array("class"=>"form-control required","id"=>"pickup_lat","placeholder"=> "Pickup lat")) !!}
                            {!! Form::hidden('pickup_long',NULL, array("class"=>"form-control required","id"=>"pickup_long","placeholder"=> "Pickup long")) !!}
                        </div>
                        <div class="form-group">
                            <div id="map" style="height: 400px; "></div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('child_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('child_status', array(1 => 'Active', 0 => 'Inactive')) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('children', $parentId)}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery('#children-frm').validate( {
                        errorPlacement: function(error, element) {
                        if(element.attr("name") == "route_id") {
                            error.appendTo( element.parent("div") );
                          } else {
                            error.insertAfter(element);
                          }
                        }
                    });

                    jQuery('#route_id').select2({
                        selectOnClose: false,
                        allowClear: true,
                        placeholder: "Select Route Code"
                    });
                    jQuery('#school_id').select2({
                        selectOnClose: false,
                        allowClear: true,
                        placeholder: "Select School"
                    });
                    jQuery(window).keydown(function(event){
                        if(event.keyCode == 13) {
                            event.preventDefault();
                            return false;
                        }
                    });
                </script>
                <script>
                    function initialize() {
                        var latitude = <?php echo env('DEFAULT_LATITUDE', 23.022505)?>; 
                        var longitude = <?php echo env('DEFAULT_LONGITUDE',  72.57136209999999)?>; 
                        var myLatLng = {lat:  latitude, lng: longitude};
                        var geocoder = new google.maps.Geocoder();
                        var infowindow = new google.maps.InfoWindow();

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 14,
                            center: myLatLng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            draggable: true ,
                            // title: 'Hello World!'
                        });

                        var options = {
                          // types: ['address']
                         };

                        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('pickup_address'), options);
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            var place = autocomplete.getPlace();
                            document.getElementById('pickup_lat').value = place.geometry.location.lat();
                            document.getElementById('pickup_long').value = place.geometry.location.lng();
                            infowindow.setContent(jQuery("#pickup_address").val());
                            infowindow.open(map, marker);
                        });


                        geocoder.geocode({'latLng': myLatLng }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    jQuery('#pickup_lat').val(marker.getPosition().lat());
                                    jQuery('#pickup_long').val(marker.getPosition().lng());
                                    infowindow.setContent(results[0].formatted_address);
                                    infowindow.open(map, marker);
                                }
                            }
                        });

                        google.maps.event.addListener(marker, 'dragend', function (event) {
                            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {
                                        jQuery('#pickup_address').val(results[0].formatted_address);
                                        jQuery('#pickup_lat').val(marker.getPosition().lat());
                                        jQuery('#pickup_long').val(marker.getPosition().lng());
                                        infowindow.setContent(results[0].formatted_address);
                                        infowindow.open(map, marker);
                                    }
                                }
                            });
                        });
                        
                    }
                    jQuery(document).ready(function(){
                        google.maps.event.addDomListener(window, 'load', initialize);
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@endSection

