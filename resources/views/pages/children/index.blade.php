@extends('layouts.master')
@section('title', 'Children Management')
@section('content')
<section class="content-header">
    <h1>Children Management<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li><a href="{{route('parents')}}"><i class="fa fa-user"></i> {{ __("Parent Management")}}</a></li>
        <li class="active">{{ __("Children Management")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- alert -->
                @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Children  Management</h3>
                    <a href="{{route('children.add',$parentId)}}" class="pull-right btn btn-success ">+ Add New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="child-grid" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="">Full Name</th>
                                <th width="">Parent Name</th>
                                <th width="">Route Code</th>
                                <th width="">Pickup Address</th>
                                <th width="">Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('scripts')
<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript">
jQuery(document).ready(function () {
    var previousUrl = '{{str_replace(url('/'), '', url()->previous())}}';
    var stateSave =  false;
    if (previousUrl.indexOf("/children-edit/") >= 0){
        stateSave =  true;
    }
    var dtable = jQuery('#child-grid').DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "routeLength": 5,
        "stateSave":stateSave,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": "{{route('children.list',$parentId)}}",
            "dataType": "json",
            "type": "POST",
            "data": {'_token': "{{csrf_token()}}"}
        },
        initComplete: function() {
            var $searchInput = jQuery('div.dataTables_filter input');
            $searchInput.unbind();
            $searchInput.bind('keyup', function(e) {
                if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                    dtable.search( this.value ).draw();
                }else if(this.value.length == 0) {
                    dtable.search( this.value ).draw();
                }
            });
        },
        "columns": [
            {"data": "name"},
            {"data": "parent_name"},
            {"data": "route_code"},
            {"data": "pickup_address"},
            {"data": "child_status"},
            {"data": "action"}
        ],
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, -2 ]
            }
        ]
    });
});
function getDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to delete Child ("+title+")?");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('children.delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#child-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
            },
            complete: function(res){
            }
        });
    }
    else
    {
        return false;
    }
}

function hardDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to Permanently delete Children ("+title+")? All records related to parent ("+title+") also deleted.");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('children.hard_delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#child-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
            },
            complete: function(res){
            }
        });
    }
    else
    {
        return false;
    }
}

function ChangeStatus(id ,element) {
    var status = "{{route('children.changestatus')}}" + "/" + id;
    jQuery.ajax({
        type: "POST",
        "url": status,
        data: {_token: "{{csrf_token()}}"},
        dataType: 'json',
        success: function (response) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");

                    if(jQuery(element).hasClass("btn-success")){
                        jQuery(element).removeClass("btn-success").addClass("btn-danger");
                    }else if(jQuery(element).hasClass("btn-danger")){
                        jQuery(element).removeClass("btn-danger").addClass("btn-success");
                    }
                    jQuery(element).text(response.content);
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                // jQuery('#child-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
            },
            complete: function(res){
            }
    });
}
</script>
@stop
