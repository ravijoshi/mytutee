@extends('layouts.master')
@section('title', 'Edit Mailtemplate')
@section('content')
<section class="content-header">
    <h1>Edit Mailtemplate <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Mailtemplate </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                {!! Form::model($mailtemplate, array('route' => array('mailtemplate.save'), 'id' => 'mailtemplate-frm', 'method' => 'post')) !!}
                <input type="hidden" name="id" value="{{$mailtemplate->id}}" />
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('subject', 'Subject',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('subject',$mailtemplate->subject, array("class"=>"form-control required","placeholder"=> "Subject","maxlength"=> 30)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('access_key', 'Access Key',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('access_key',$mailtemplate->access_key, array("class"=>"form-control required","placeholder"=> "Access Key","maxlength"=> 30)) !!}
                            <div class="notes" style="color: red;font-weight: bold;">Do Not change this Access Key</div>
                        </div>
                        <?php /*<div class="form-group">
                            {!! Form::label('cc', 'CC',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::email('cc',$mailtemplate->cc, array("class"=>"form-control","placeholder"=> "CC","maxlength"=> 30)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('bcc', 'BCC',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::email('bcc',$mailtemplate->bcc, array("class"=>"form-control","placeholder"=> "BCC","maxlength"=> 30)) !!}
                        </div> */ ?>
                        <div class="form-group">
                            {!! Form::label('content', 'Content',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::textarea('content',$mailtemplate->content, array("class"=>"form-control required","id"=> "content","placeholder"=> "Content")) !!}
                        </div>
                       
                        <div class="form-group">
                            {!! Form::label('status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('status', array(1 => 'Active',0 => 'Inactive'),$mailtemplate->status) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('mailtemplate')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Update',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#mailtemplate-frm').validate();
                    });
                    jQuery(function () {
                        CKEDITOR.replace('content')
                    })
                </script>
            </div>
        </div>
    </div>
</section>
@stop

