@extends('layouts.master')
@section('title', 'School Management')
@section('content')
<section class="content-header">
    <h1>School Management<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li class="active">{{ __("School Management")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- alert -->
                @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">School Management</h3>
                    <a href="{{route('importForm')}}" class="pull-right btn btn-success" style="margin-left: 5px;"><i class="fa fa-cloud-upload"></i> Import</a>
                    <a href="{{route('school.search')}}" class="pull-right btn btn-success" style="margin-left: 5px;"><i class="fa fa-search"></i> Search School On Google</a>
                    <?php /*<a href="{{route('export')}}" class="pull-right btn btn-success" style="margin-left: 5px;"><i class="fa fa-cloud-download"></i> Export</a> */?>
                    <a href="{{route('schools/add')}}" class="pull-right btn btn-success ">+ Add New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="school-grid" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="30%">School Name</th>
                                <th width="20%">Phone Number</th>
                                <th width="30%">Address</th>
                                <th width="10%">Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('scripts')
<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript">
jQuery(document).ready(function () {
    var previousUrl = '{{str_replace(url('/'), '', url()->previous())}}';
    var stateSave =  false;
    if (previousUrl.indexOf("/schools/edit/") >= 0){
        stateSave =  true;
    }
    
    var dtable = jQuery('#school-grid').DataTable({
        "processing"    : true,
        "autoWidth"     : true, 
        "responsive"    : true,
        "serverSide"    : true,
        "routeLength"   : 5,
        "stateSave"     : stateSave,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": "{{route('getSchoolList')}}",
            "dataType": "json",
            "type": "POST",
            "data": {'_token': "{{csrf_token()}}"}
        },
        initComplete: function() {
            var $searchInput = jQuery('div.dataTables_filter input');
            $searchInput.unbind();
            $searchInput.bind('keyup', function(e) {
                if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                    dtable.search( this.value ).draw();
                }else if(this.value.length == 0) {
                    dtable.search( this.value ).draw();
                }
            });
        },
        "columns": [
            {"data": "school_name"},
            {"data": "school_phone"},
            {"data": "school_address"},
            {"data": "school_status"},
            {"data": "action"}
        ],
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, -2 ]
            }
        ]
    });
});
function getDelConfirmation(id, title) {
    var retVal = confirm( "Are you sure you want to delete School ("+title+")?");
    if (retVal == true) {
        jQuery.ajax({
            type: "GET",
            url: "{{route('schools/delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(3000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#school-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(3000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
            },
            complete: function(res){
            }
        });
    } else {
        return false;
    }
}


function ChangeStatus(id,element) {
    jQuery.ajax({
        type: "POST",
        "url": "{{route('changestatus')}}" + "/" + id,
        data: {_token: "{{csrf_token()}}"},
        success: function (response) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");

                    if(jQuery(element).hasClass("btn-success")){
                        jQuery(element).removeClass("btn-success").addClass("btn-danger");
                    }else if(jQuery(element).hasClass("btn-danger")){
                        jQuery(element).removeClass("btn-danger").addClass("btn-success");
                    }
                    jQuery(element).text(response.content);
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(3000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
            },
            error: function (err) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery('.ajaxAlert').fadeOut(3000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
            },
            complete: function(res){
            }
    });
}
</script>
@stop
