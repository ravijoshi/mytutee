@extends('layouts.master')
@section('title', 'Import School')
@section('content')
<section class="content-header">
    <h1>Import School<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Import School</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Import School</h3>
                </div>
                {!! Form::open( array('route' => array('import'), 'id' => 'import-frm', 'method' => 'post' ,'files' => true)) !!}
                    <div class="box-body">

                        <div class="form-group">
                            {!! Form::label('import_file', 'Select File',array("class" => "pr-1 form-control-label")) !!}
                            {{Form::file("import_file" , array("class" => "required " , "accept" =>".xls,.csv,.xlsx") )}}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('schools')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Import',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#import-frm').validate();
                        jQuery('#import-frm').submit(function (){
                            if( jQuery("#import_file").val() != ''){
                                jQuery('#default-loader').show();
                            } else {
                                return false;
                            }
                        });
                        
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

