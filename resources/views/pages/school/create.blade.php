@extends('layouts.master')
@section('title', 'Add School')
@section('content')
<section class="content-header">
    <h1>School<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">School</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                {!! Form::open(array('route' => array('addschoolinfo'), 'id' => 'schooldetails', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('school_name', 'School Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('school_name',null, array("class"=>"form-control required","placeholder"=> "School Name" ,"id"=>"sclname", "maxlength" => 100)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('school_phone', 'Phone Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('school_phone',null, array("class"=>"form-control required digits","placeholder"=> "Phone Number" ,"maxlength"=>10,"minlength"=>8)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('school_address', 'School Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::textarea('school_address',null,['class'=>'form-control required', 'rows' => 3, 'cols' => 40,"placeholder"=> "School Address"]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('school_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {{Form::select('school_status', array(1 => 'Active', 0 => 'Inactive' )) }}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('schools')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#schooldetails').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

