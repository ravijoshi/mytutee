<?php $i = 0;?>
@extends('layouts.master')
@section('title', 'Search School On Google')
@section('content')
<section class="content-header">
    <h1>Search School On Google<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li><a href="{{route('schools')}}"><i class="fa fa-dashboard"></i> {{ __("Manage School")}}</a></li>
        <li class="active">{{ __("Search School On Google")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12" id="searchBar">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search School On Google</h3>
                </div>
                <div class="box-body">
                    <div class="form-group common hs">
                        {!! Form::label('name', 'Enter City Name / Postal Code',array("class" => "pr-1 form-control-label required")) !!}
                        {!! Form::text('name',null,array("class"=>"form-control required","placeholder"=> "Enter City Name / Postal Code", "maxlength" => 255)) !!}
                    </div>
                </div>
                <div class="box-footer">
                    <button id="search-btn">Search</button>
                </div>
            </div>
        </div>

        <div class="col-xs-12" id="school-list">
            <form method="post" name="schoolList-frm" action="{{route('school.export')}}">
                 @csrf
                <div class="box">
                    <div class="box-header">
                    <!-- <h3 class="box-title">School Management</h3> -->
                    <button type="submit" class="pull-right btn btn-success exportButton" style="display: none;" ><i class="fa fa-cloud-download exportButton" ></i> Export</button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="schoolList-grid" class="table table-bordered table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th width="30%">School Name</th>
                                    <th width="70%">Address</th>
                                </tr>
                            </thead>
                            <tbody id="tableBody">
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@stop
@section('scripts')
<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#search-btn').click( function(){
        if(jQuery("#name").val() =='') {
            alert("Please enter city name.");
            jQuery("#name").focus();
            return false;
        } else {
            jQuery('#search-btn').prop('disabled', true);
            getSchoolList(jQuery("#name").val());
            return true;
        }
    });
    getSchoolList('');
});

function getSchoolList(address = ''){
    jQuery.ajax({
        type: "POST",
        url: "{{route('school.search.list')}}",
        data: {_token: "{{csrf_token()}}", "address" : address },
        dataType: 'json',
        success: function (response) {
            var dtable =  jQuery('#schoolList-grid').DataTable({
                "routeLength"  : 5,
                "pageLength"   : 50,
                "bDestroy"  : true,
                "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
                initComplete: function() {
                    var $searchInput = jQuery('div.dataTables_filter input');
                    $searchInput.unbind();
                    $searchInput.bind('keyup', function(e) {
                        if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                            dtable.search( this.value ).draw();
                        }else if(this.value.length == 0) {
                            dtable.search( this.value ).draw();
                        }
                    });
                },
                data: response.dataSet,
                columns: [
                    { "data" : "name"},
                    { "data" : "address"},
                ]
            });
            // jQuery('.ajaxAlert').show();
            jQuery(".ajaxAlert").removeClass("bg-success");
            jQuery(".ajaxAlert").removeClass("bg-danger");
            if(response.responseType =='success') {
                jQuery(".ajaxAlert").addClass("bg-success");
            } else {
                jQuery(".ajaxAlert").addClass("bg-danger");
            }

            if(response.isExport == 1) {
                jQuery(".exportButton").show();
            } else {
                jQuery(".exportButton").hide();
            }
            jQuery(".ajaxAlert #msg").text(response.message);
            jQuery('.ajaxAlert').fadeOut(5000);
            jQuery('.ajaxAlert .close').show(500);
            jQuery("html, body").animate({scrollTop: 0}, "fast");
        },
        error: function (err) {
            jQuery('.ajaxAlert').show();
            jQuery('.ajaxAlert').fadeOut(5000);
            jQuery('.ajaxAlert .close').show(500);
            jQuery(".ajaxAlert #msg").text(err);
            jQuery(".ajaxAlert").addClass("bg-danger");
        },
        complete: function(res){
            jQuery('#search-btn').prop("disabled", false);
        }
    });
 }
</script>
@stop