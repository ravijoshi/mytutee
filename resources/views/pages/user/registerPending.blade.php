@extends('layouts.master')
@section('title', 'User Registration Pending List')
@section('content')
<section class="content-header">
    <h1>User Registration Pending List<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Registration Pending List</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">User Registration Pending List</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="register-grid" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="">Name</th>
                                <th width="">Email Address</th>
                                <th width="">Phone No</th>
                                <th width="">User Type</th>
                                <th width="">Created On</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('scripts')
<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript">
jQuery(document).ready(function () {
    var dtable = jQuery('#register-grid').DataTable({
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "stateSave": true,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": "{{route('user.registerationPendingList')}}",
            "dataType": "json",
            "type": "POST",
            "data": {'_token': "{{csrf_token()}}"}
        },
        initComplete: function() {
            var $searchInput = jQuery('div.dataTables_filter input');
            $searchInput.unbind();
            $searchInput.bind('keyup', function(e) {
                if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                    dtable.search( this.value ).draw();
                }else if(this.value.length == 0) {
                    dtable.search( this.value ).draw();
                }
            });
        },
        "columns": [
            {"data": "user_name"},
            {"data": "email"},
            {"data": "user_phone"},
            {"data": "user_type"},
            {"data": "created_on"},
            {"data": "action"}
        ],
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, -1 ]
            }
        ]
    });
});
function getDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to permanently delete user details ("+title+")?");
    
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('user.forcedelete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#register-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
            },
            complete: function(res){
            }
        });
    } else {
        return false;
    }
}
</script>
@stop
