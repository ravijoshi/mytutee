@extends('layouts.master')
@section('title', 'Profile')
@section('content')
<section class="content-header">
    <h1>My Profile<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">My Profile</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">My Profile</h3>
                        <a href="{{route('profile.edit')}}" class="pull-right btn btn-success " style="margin-left: 5px;">Edit Profile</a>

                        <a href="{{route('user.changePassword')}}" class="pull-right btn btn-success ">Change Password</a>
                </div>
                
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('user_fname', 'First Name :',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::label($user->user_fname,NULL, array("class"=>"")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_lname', 'Last Name :',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::label($user->user_lname,NULL, array("class"=>"")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email Address :',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::label($user->email,null, array("class"=>"")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_phone-lbl', 'Phone Number :',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::label($user->user_phone,null, array("class"=>"")) !!}
                        </div>
                    </div>
                    
                
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#users-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

