@extends('layouts.master')
@section('title', 'Edit Profile')
@section('content')
<section class="content-header">
    <h1>Edit Profile<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Profile</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Profile</h3>
                </div>
                {!! Form::model($user, array('route' => array('user.update'), 'id' => 'users-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('user_fname', 'First Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_fname',$user->user_fname, array("class"=>"form-control required","placeholder"=> "First Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_lname', 'Last Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_lname',$user->user_lname, array("class"=>"form-control required","placeholder"=> "Last Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::email('email',null, array("class"=>"form-control required","placeholder"=> "Email Address","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_phone', 'Phone Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_phone',$user->user_phone, array("class"=>"form-control required digits","placeholder"=> "Phone Number","maxlength"=> 10)) !!}
                        </div>
                       
                        <?php /* <div class="form-group">
                            {!! Form::label('user_role', 'User Role',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_role', $roleOptions) !!}
                        </div> 
                        <div class="form-group">
                            {!! Form::label('user_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_status', array(0 => 'Inactive', 1 => 'Active'),$user->user_status) !!}
                        </div>*/ ?>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('profile')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit("Update Profile",array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#users-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

