@extends('layouts.master')
@section('title', 'Change Password')
@section('content')

<section class="content-header">
    <h1>Change Password <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Change Password </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Change Password</h3>
                </div>
                {!! Form::open(array('route' => array('user.changePwd'), 'id' => 'change-pwd-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('old_password', 'Current Password',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::password('old_password', array("class"=>"form-control required","placeholder"=> "Current Password","maxlength"=> 20)) !!}
                            
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'New Password',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::password('password', array("class"=>"form-control required","placeholder"=> "New Password","minlength"=> 6,"maxlength"=> 20)) !!}
                            
                        </div>
                        <div class="form-group">
                            {!! Form::label('confirm_password', 'Confirm Password',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::password('confirm_password', array("class"=>"form-control required","placeholder"=> "Confirm Password","minlength"=> 6,"maxlength"=> 20 , "equalto" =>"#password" )) !!}
                            
                        </div>
                        
                    </div>
                    <div class="box-footer">
                        <a href="{{route('profile')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Change Password',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#change-pwd-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@endSection

