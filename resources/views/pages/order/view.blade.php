@extends('layouts.master')
@section('title', 'Order Details')
@section('content')

<section class="content-header">
    <h1>Order Details <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Order Details </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Order Details</h3>
                </div>
                 
                {!! Form::model($order, array('id' => 'order-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group ">
                            {!! Form::label('parent_name', 'Parent Name : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('parent_name', isset($order->parent->parent_name) ? $order->parent->parent_name : "-",array("class" => "pr-1  label-value ")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('child_name', 'Child Name : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('child_name', isset($order->child->child_name) ? $order->child->child_name : "-",array("class" => "pr-1  label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('title', 'Package Name : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('title', isset($order->package->title) ? $order->package->title : "-",array("class" => "pr-1  label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('package_days', 'Package Days : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('package_days', isset($order->package_days) ? $order->package_days : "-",array("class" => "pr-1  label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('package_price', 'Price : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('package_price', isset($order->package_price) ? DEFAULT_CURRENCY_SYMBOL.$order->package_price : "-",array("class" => "pr-1  label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('type', 'Order Type : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('type', isset($order->type) ? $order->getPaymentTypeOptionsText($order->type) : "-",array("class" => " pr-1 label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('payment_method', 'Payment Method : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('payment_method', isset($order->payment_method) ? $order->getPaymentOptionsText($order->payment_method) : "-",array("class" => "pr-1  label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('payment_status', 'Payment Status : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('payment_status', isset($order->payment_status) ? $order->getStatusOptionsText($order->payment_status) : "-",array("class" => " pr-1 label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('created_at', 'Order Date : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('created_at', isset($order->created_at) ? \App\User::getDefaultFormatedDate($order->created_at,'',DEFAULT_TIMEZONE) : "-",array("class" => " label-value")) !!}
                        </div>
                        <div class="form-group ">
                            {!! Form::label('updated_at', 'Order Updated Date : ',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::label('updated_at', isset($order->updated_at) ? \App\User::getDefaultFormatedDate($order->updated_at,'',DEFAULT_TIMEZONE) : "-",array("class" => "pr-1 label-value")) !!}
                        </div>

                    </div>
                    <div class="box-footer">
                        <a href="{{route('order')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Back</a>
                        <!-- {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!} -->
                    </div>
                {{ Form::close() }}
               
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#order-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

