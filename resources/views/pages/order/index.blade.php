@extends('layouts.master')
@section('title', 'Order Management')
@section('content')
<section class="content-header">
    <h1>Order Management<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li class="active">{{ __("Order Management")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Order  Management</h3>
                    <!-- <a href="{{route('order.add')}}" class="pull-right btn btn-success ">+ Add New</a> -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="order-grid" class="table table-hover table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="">Parent Name</th>
                                <th width="">Child Name</th>
                                <th width="">Package Name</th>
                                <th width="">Price</th>
                                <th width="">Order Type</th>
                                <th width="10%">Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('scripts')


<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript">
jQuery(document).ready(function () {
    var previousUrl = '{{str_replace(url('/'), '', url()->previous())}}';
    var stateSave =  false;
    if (previousUrl.indexOf("/order/view/") >= 0){
        stateSave =  true;
    }
    var dtable = jQuery('#order-grid').DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "routeLength": 5,
        "stateSave": stateSave,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": "{{route('order.list')}}",
            "dataType": "json",
            "type": "POST",
            "data": {'_token': "{{csrf_token()}}"}
        },
        initComplete: function() {
            var $searchInput = jQuery('div.dataTables_filter input');
            $searchInput.unbind();
            $searchInput.bind('keyup', function(e) {
                if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                    dtable.search( this.value ).draw();
                }else if(this.value.length == 0) {
                    dtable.search( this.value ).draw();
                }
            });
        },
        "columns": [
            {"data": "parent_name"},
            {"data": "child_name"},
            {"data": "title"},
            {"data": "price"},
            {"data": "type"},
            {"data": "payment_status"},
            {"data": "action"}
        ],
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, -1 ]
            }
        ]
    });
});

</script>
@stop
