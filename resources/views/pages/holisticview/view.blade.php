@extends('layouts.master')
@section('title', 'Holistic View')
@section('content')
<section class="content-header">
    <h1>Holistic View<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li class="active">{{ __("Holistic View")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Holistic View</h3>
                    <?php /*if(count($results)):?>
                    	<a href="javascript:void(0);" onclick="history.back();" class="pull-right btn btn-success ">Back</a>
                    <?php endif;*/?>
                </div>
                <div class="box-body">
                    <table id="grid" class="table table-bordered table-striped table-responsive">
                        <thead>
                        	<?php if($view == 'route'):?>
	                            <tr>
	                                <th width="">Route Code</th>
	                                <th width="">Driver Name (HS)</th>
	                                <th width="">Driver Name (SH)</th>
	                                <th width="">School Name</th>
	                                <th width="">Start Location Address</th>
	                                <th width="">End Location Address</th>
	                                <th width="">Total Children</th>
	                            </tr>
                            <?php elseif($view == 'driver'):?>
                            	<tr>
	                                <th width="">Route Code</th>
	                                <th width="">Driver Name</th>
	                                <th width="">School Name</th>
	                                <th width="">Start Location Address</th>
	                                <th width="">End Location Address</th>
	                                <th width="">Total Children</th>
	                            </tr>
	                        <?php elseif($view == 'school'):?>
                            	<tr>
	                                <th width="">Route Code</th>
	                                <th width="">Driver Name (HS)</th>
	                                <th width="">Driver Name (SH)</th>
	                                <th width="">School Name</th>
	                                <th width="">Start Location Address</th>
	                                <th width="">End Location Address</th>
	                                <th width="">Total Children</th>
	                            </tr>
                            <?php elseif($view == 'children'):?>
                            	<tr>
	                                <th width="">Route Code</th>
	                                <th width="">Driver Name (HS)</th>
	                                <th width="">Driver Name (SH)</th>
	                                <th width="">Child Name</th>
	                                <th width="">Parent Name</th>
	                                <th width="">School Name</th>
	                            </tr>
                            <?php endif;?>
                        </thead>
                        <tbody>
                        	<?php if($view == 'route'):?>
                        		<?php if(isset($results->route_code) && !empty($results->route_code)):?>
		                            <tr>
		                                <td>
		                                	<?php if(isset($results->route_code) && !empty($results->route_code)):?>
		                                		<a title="View Route" href='{{route("holisticview").'?view=route&id='.$results->route_id}}'> {{$results->route_code}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($results->typeHs->driver->driver_name) && !empty($results->typeHs->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$results->typeHs->driver->id}}'>{{$results->typeHs->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($results->typeSh->driver->driver_name) && !empty($results->typeSh->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$results->typeSh->driver->id}}'>{{$results->typeSh->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($results->school->school_name) && !empty($results->school->school_name)):?>
		                                		<a title="View School" href='{{route("holisticview").'?view=school&id='.$results->school->id}}'>{{$results->school->school_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                <td>{{isset($results->typeHs->start_loc_address) ? $results->typeHs->start_loc_address : ''}}</td>
		                                <td>{{isset($results->typeHs->end_loc_address) ? $results->typeHs->end_loc_address : ''}}</td>
		                                <td>
		                                	<?php if(isset($results->routeChildren)):?>
		                                		<a title="View Children on Route" href='{{route("holisticview").'?view=children&id='.$results->route_id}}'>{{$results->routeChildren->count() ? $results->routeChildren->count() : "0"}}</a>
		                                	<?php endif;?>
		                                </td>
		                            </tr>
	                    		<?php else:?>
	                    			<tr>
	                    				<td colspan="7"> No Record Found.</td>
	                    			</tr>
	                    		<?php endif;?>        
                            <?php elseif($view == 'school'):?>
                            	<?php if(count($results)):?>
                            		<?php foreach ($results as $result) :?>
			                            <tr>
		                                <td>
		                                	<?php if(isset($result->route_code) && !empty($result->route_code)):?>
		                                		<a title="View Route" href='{{route("holisticview").'?view=route&id='.$result->route_id}}'>{{$result->route_code}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->typeHs->driver->driver_name) && !empty($result->typeHs->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$result->typeHs->driver->id}}'>{{$result->typeHs->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->typeSh->driver->driver_name) && !empty($result->typeSh->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$result->typeSh->driver->id}}'>{{$result->typeSh->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->school->school_name) && !empty($result->school->school_name)):?>
		                                		<a title="View School" href='{{route("holisticview").'?view=school&id='.$result->school->id}}'>{{$result->school->school_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>

		                                <td>{{isset($result->typeHs->start_loc_address) ? $result->typeHs->start_loc_address : ''}}</td>
		                                <td>{{isset($result->typeHs->end_loc_address) ? $result->typeHs->end_loc_address : ''}}</td>
		                                <td>
		                                	<?php if(isset($result->routeChildren)):?>
		                                		<a title="View Children on Route" href='{{route("holisticview").'?view=children&id='.$result->route_id}}'>{{$result->routeChildren->count() ? $result->routeChildren->count() : "0"}}</a>
		                                	<?php endif;?>
			                                </td>
			                            </tr>
		                        <?php endforeach;?>
	                    		<?php else:?>
	                    			<tr>
	                    				<td colspan="7"> No Record Found.</td>
	                    			</tr>
	                    		<?php endif;?>
                            <?php elseif($view == 'driver'):?>
                            	<?php if(count($results)):?>
                            		<?php foreach ($results as $result):?>
		                            <tr>
		                                <td>
		                                	<?php if(isset($result->route->route_code) && !empty($result->route->route_code)):?>
		                                		<a title="View Route" href='{{route("holisticview").'?view=route&id='.$result->route->route_id}}'>{{$result->route->route_code}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->driver->driver_name) && !empty($result->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$result->driver->id}}'>{{$result->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->route->school->school_name) && !empty($result->route->school->school_name)):?>
		                                		<a title="View School" href='{{route("holisticview").'?view=school&id='.$result->route->school->id}}'>{{$result->route->school->school_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>

		                                </td>
		                                <td>
		                                	{{isset($result->start_loc_address) ? $result->start_loc_address : ''}}
		                                </td>
		                                <td>
		                                	{{isset($result->end_loc_address) ? $result->end_loc_address : ''}}
		                                </td>
		                                <td>
		                                	<?php if(isset($result->route->routeChildren)):?>
		                                		<a title="View Children on Route" href='{{route("holisticview").'?view=children&id='.$result->route->route_id}}'>{{ $result->route->routeChildren->count() ? $result->route->routeChildren->count() : "0"}}</a>
		                                	<?php endif;?>
		                                </td>
		                                
		                            </tr>
			                        <?php endforeach;?>
	                    		<?php else:?>
	                    			<tr>
	                    				<td colspan="6"> No Record Found.</td>
	                    			</tr>
	                    		<?php endif;?>
                            <?php elseif($view == 'children'):?>
                            	<?php if(count($results)):?>
                            		<?php foreach ($results as $result):?>
		                            <tr>
		                                <td>
		                                	<?php if(isset($result->route->route_code) && !empty($result->route->route_code)):?>
		                                		<a title="View Route" href='{{route("holisticview").'?view=route&id='.$result->route->route_id}}'>{{$result->route->route_code}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->route->typeHs->driver->driver_name) && !empty($result->route->typeHs->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$result->route->typeHs->driver->id}}'>{{$result->route->typeHs->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>
		                                	<?php if(isset($result->route->typeSh->driver->driver_name) && !empty($result->route->typeSh->driver->driver_name)):?>
		                                		<a title="View Driver" href='{{route("holisticview").'?view=driver&id='.$result->route->typeSh->driver->id}}'>{{$result->route->typeSh->driver->driver_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                                <td>{{isset($result->child->fullname) ? $result->child->fullname : ''}}</td>
		                                <td>{{isset($result->child->parent->fullname) ? $result->child->parent->fullname : ''}}</td>
		                                <td>
		                                	<?php if(isset($result->child->school->school_name) && !empty($result->child->school->school_name)):?>
		                                		<a title="View School" href='{{route("holisticview").'?view=school&id='.$result->child->school->id}}'>{{$result->child->school->school_name}}</a>
		                                	<?php else:  ''?>
		                                	<?php endif;?>
		                                </td>
		                            </tr>
			                        <?php endforeach;?>
	                    		<?php else:?>
	                    			<tr>
	                    				<td colspan="6"> No Record Found.</td>
	                    			</tr>
	                    		<?php endif;?>      
                            <?php endif;?>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('scripts')
@endsection
