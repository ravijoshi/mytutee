@extends('layouts.master')
@section('title', 'Add Route')
@section('content')

<section class="content-header">
    <h1>Add Route <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Route </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                 
                {!! Form::open(array('route' => array('route.save'), 'id' => 'route-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        
                        <div class="form-group common hs">
                            {!! Form::label('school_id', 'School Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('school_id',$schoolOptions, null,array("class"=>"form-control hs-input required","placeholder"=> "School Name")) !!}
                        </div>
                        <div class="form-group common">
                            <div class=" common col-md-6">
                                {!! Form::label('start_time', 'Start Time (H-S)',array("class" => "pr-1 form-control-label required")) !!}
                                {!! Form::text('start_time',NULL, array("class"=>"form-control required","id"=> "start_time","placeholder"=> "Start Time (H-S)","readonly"=> "readonly")) !!}
                            </div>
                            <div class=" common col-md-6">
                                {!! Form::label('end_time', 'End Time (H-S)',array("class" => "pr-1 form-control-label required")) !!}
                                {!! Form::text('end_time',NULL , array("class"=>"form-control required","id"=> "end_time","placeholder"=> "End Time (H-S)","readonly"=> "readonly" , "onChange" =>" initSHStartTimePicker(this.value)")) !!}
                            </div>
                        </div>
                        <div class="form-group common">
                            <div class=" common col-md-6">
                                {!! Form::label('sh_start_time', 'Start Time (S-H)',array("class" => "pr-1 form-control-label required")) !!}
                                {!! Form::text('sh_start_time',NULL, array("class"=>"form-control required","id"=> "sh_start_time","placeholder"=> "Start Time (S-H)","readonly"=> "readonly" , "onChange" => "initSHEndTimePicker(this.value)")) !!}
                            </div>
                            <div class=" common col-md-6">
                                {!! Form::label('sh_end_time', 'End Time (S-H)',array("class" => "pr-1 form-control-label required")) !!}
                                {!! Form::text('sh_end_time',NULL , array("class"=>"form-control required","id"=> "sh_end_time","placeholder"=> "End Time (S-H)","readonly"=> "readonly" )) !!}
                            </div>
                        </div>
                        <div class="form-group common">
                            {!! Form::label('is_roud_trip', TYPE_ROUND_TRIP_TEXT,array("class" => "pr-1 form-control-label required")) !!}
                            {{Form::checkbox('is_roud_trip', IS_ROUND_TRIP_YES,IS_ROUND_TRIP_YES , array("class" => "checkbox "))}}
                        </div>
                        <div class="form-group common">
                            <div class=" common col-md-6">
                                {!! Form::label('driver_id', TYPE_HOME_TO_SCHOOL_TEXT,array("class" => "pr-1 form-control-label required")) !!}
                                {!! Form::select('driver_id', $driverOptions,null,array("class"=>"form-control  common-input required","placeholder"=> "Driver Name")) !!}
                            </div>
                            <div class=" common col-md-6">
                                {!! Form::label('sh_driver_id', TYPE_SCHOOL_TO_HOME_TEXT,array("class" => "pr-1 form-control-label required")) !!}
                                {!! Form::select('sh_driver_id', $driverOptions,null,array("class"=>"form-control required sh_driver_id","id"=> "sh_driver_id","placeholder"=> "Driver Name","disabled"=> "disabled")) !!}
                            </div>
                        </div>
                        
                        <div class="form-group hs common">
                            {!! Form::label('start_loc_address', 'Start Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('start_loc_address',null, array("class"=>"form-control hs-input required","id"=> "address","placeholder"=> "Start Address", "maxlength" => 255)) !!}
                            {!! Form::hidden('start_loc_lat',null, array("class"=>"form-control hs-input  ","id"=> "latitude","placeholder"=> "latitude")) !!}
                            {!! Form::hidden('start_loc_long',null, array("class"=>"form-control hs-input ","id"=> "longitude","placeholder"=> "longitude")) !!}
                            
                        </div>

                        <div class="form-group hs common">
                            <div id="map" style="height: 400px; "></div>
                        </div>
                       
                        <div class="form-group common hs">
                            {!! Form::label('route_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('route_status', array(1 => 'Active' ,0 => 'Inactive'),null,array('class' => "common-input hs-input")) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('route')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
               
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#route-frm').validate( {
                            errorPlacement: function(error, element) {
                            if(element.attr("name") == "driver_id" || element.attr("name") == "sh_driver_id" || element.attr("name") == "school_id" || element.attr("name") == "route_code") {
                                error.appendTo( element.parent("div") );
                              } else {
                                error.insertAfter(element);
                              }
                            }
                        });
                        
                        
                        jQuery("#is_roud_trip").click(function(){
                            var value = jQuery(this).is(":checked");
                            if(value == 1){
                                jQuery('#sh_driver_id').prop("disabled",true);
                                jQuery('.sh-input').removeClass('required');
                            } else {
                                jQuery('#sh_driver_id').prop("disabled",false);
                                jQuery('.sh-input').addClass('required');
                            }
                        });

                        jQuery("#driver_id").change(function(){
                            if(jQuery("#is_roud_trip").is(":checked")){
                                jQuery('#sh_driver_id').val(jQuery(this).val()).change();
                            }
                        });

                        jQuery(window).keydown(function(event){
                            if(event.keyCode == 13) {
                                event.preventDefault();
                                return false;
                            }
                        });
                        initEndTimePicker(0);
                        
                        jQuery('#start_time').timepicker({
                             "timeFormat": 'HH:mm',
                             "minTime" : 0,
                             onSelect: function(time) {
                               initEndTimePicker(time);
                             }
                        });
                        
                        jQuery('#driver_id').select2({
                            selectOnClose: false,
                            allowClear: true,
                            // minimumResultsForSearch: -1,
                            // tokenSeparators: [",", " "],
                            placeholder: "Select Driver"
                        });
                        jQuery('#sh_driver_id').select2({
                            selectOnClose: false,
                            allowClear: true,
                            // minimumResultsForSearch: -1,
                            // tokenSeparators: [",", " "],
                            placeholder: "Select Driver"
                        });
                        jQuery('#school_id').select2({
                            selectOnClose: false,
                            allowClear: true,
                            // minimumResultsForSearch: -1,
                            // tokenSeparators: [",", " "],
                            placeholder: "Select School"
                        });
                    });
                   
                    function initEndTimePicker(min_time = 0){
                        jQuery('#end_time').timepicker("destroy");
                        jQuery("#end_time").timepicker( {                    
                           "minTime": min_time,
                           "timeFormat": 'HH:mm',
                        });
                    }

                    function initSHStartTimePicker(min_time = 0){
                        jQuery('#sh_start_time').timepicker("destroy");
                        jQuery("#sh_start_time").timepicker( {                    
                           "minTime": min_time,
                           "timeFormat": 'HH:mm',
                        });
                    }
                    function initSHEndTimePicker(min_time = 0){
                        jQuery('#sh_end_time').timepicker("destroy");
                        jQuery("#sh_end_time").timepicker( {                    
                           "minTime": min_time,
                           "timeFormat": 'HH:mm',
                        });
                    }
                </script>
                 <script>
                    function initialize() {
                        var latitude = <?php echo env('DEFAULT_LATITUDE', 23.022505)?>; 
                        var longitude = <?php echo env('DEFAULT_LONGITUDE',  72.57136209999999)?>; 
                        var myLatLng = {lat:  latitude, lng: longitude};
                        var geocoder = new google.maps.Geocoder();
                        var infowindow = new google.maps.InfoWindow();

                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 14,
                            center: myLatLng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            draggable: true ,
                            // title: 'Hello World!'
                        });

                        var options = {
                          // types: ['address']
                         };

                        var autocomplete = new google.maps.places.Autocomplete(document.getElementById('address'), options);
                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            var place = autocomplete.getPlace();
                            document.getElementById('latitude').value = place.geometry.location.lat();
                            document.getElementById('longitude').value = place.geometry.location.lng();
                            infowindow.setContent(jQuery("#address").val());
                            infowindow.open(map, marker);
                        });


                        geocoder.geocode({'latLng': myLatLng }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    jQuery('#latitude').val(marker.getPosition().lat());
                                    jQuery('#longitude').val(marker.getPosition().lng());
                                    infowindow.setContent(results[0].formatted_address);
                                    infowindow.open(map, marker);
                                }
                            }
                        });

                        google.maps.event.addListener(marker, 'dragend', function (event) {
                            geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    if (results[0]) {
                                        jQuery('#address').val(results[0].formatted_address);
                                        jQuery('#latitude').val(marker.getPosition().lat());
                                        jQuery('#longitude').val(marker.getPosition().lng());
                                        infowindow.setContent(results[0].formatted_address);
                                        infowindow.open(map, marker);
                                    }
                                }
                            });
                        });
                        
                    }
                    jQuery(document).ready(function(){
                        google.maps.event.addDomListener(window, 'load', initialize);
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

