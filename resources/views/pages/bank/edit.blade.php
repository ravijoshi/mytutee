@extends('layouts.master')
@section('title', 'Edit Bank')
@section('content')

<section class="content-header">
    <h1>Edit Bank <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Bank </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                
                {!! Form::model($bank,array('route' => array('bank.save'), 'id' => 'bank-frm', 'method' => 'post')) !!}
                    {!! Form::hidden('id',$bank->id, array("class"=>"form-control required","id"=> "id")) !!}
                    <div class="box-body">
                        
                        
                        <div class="form-group common">
                            {!! Form::label('name', 'End Time',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('name',$bank->name , array("class"=>"form-control required","id"=> "name","placeholder"=> "Bank Name", "maxlength" => 255)) !!}
                        </div>
                        
                        <div class="form-group common hs">
                            {!! Form::label('status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('status', array(1 => 'Active' ,0 => 'Inactive'),$bank->status) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('bank')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#bank-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

