@extends('layouts.master')
@section('title', 'Add Bank')
@section('content')

<section class="content-header">
    <h1>Add Bank <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Bank </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                 
                {!! Form::open(array('route' => array('bank.save'), 'id' => 'bank-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        
                        <div class="form-group common hs">
                            {!! Form::label('name', 'Bank Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('name',null,array("class"=>"form-control required","placeholder"=> "Bank Name", "maxlength" => 255)) !!}
                        </div>
                        
                        <div class="form-group common hs">
                            {!! Form::label('status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('status', array(1 => 'Active' ,0 => 'Inactive'),null,array('class' => "common-input hs-input")) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('bank')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
               
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#bank-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

