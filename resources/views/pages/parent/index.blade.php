@extends('layouts.master')
@section('title', 'Parent Management')
@section('content')
<section class="content-header">
    <h1>Parent Management<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li class="active">{{ __("Parent Management")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- alert -->
                @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Parent  Management</h3>
                    <a href="{{route('parent.add')}}" class="pull-right btn btn-success ">+ Add New</a>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="parent-grid" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="">Name</th>
                                <th width="">Email Address</th>
                                <th width="">Phone No</th>
                                <th width="10%">Status</th>
                                <th width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('scripts')
<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript">
jQuery(document).ready(function () {
    var previousUrl = '{{str_replace(url('/'), '', url()->previous())}}';
    var stateSave =  false;
    if (previousUrl.indexOf("/parent/edit/") >= 0){
        stateSave =  true;
    }

    var dtable = jQuery('#parent-grid').DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "routeLength": 5,
        "stateSave": stateSave,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": "{{route('parent.list')}}",
            "dataType": "json",
            "type": "POST",
            "data": {'_token': "{{csrf_token()}}"}
        },
        initComplete: function() {
            var $searchInput = jQuery('div.dataTables_filter input');
            $searchInput.unbind();
            $searchInput.bind('keyup', function(e) {
                if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                    dtable.search( this.value ).draw();
                }else if(this.value.length == 0) {
                    dtable.search( this.value ).draw();
                }
            });
        },
        "columns": [
            {"data": "user_name"},
            {"data": "email"},
            {"data": "user_phone"},
            {"data": "user_status"},
            {"data": "action"}
        ],
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, -2 ]
            }
        ]
    });
});
function getDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to delete Parent ("+title+")?");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('parent.delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#parent-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
        });
    }
    else
    {
        return false;
    }
}

function hardDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to Permanently delete Parent ("+title+")? All records related to parent ("+title+") also deleted.");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('parent.hard_delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#parent-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
        });
    }
    else
    {
        return false;
    }
}

function restoreUser(id, title) {

    var retVal = confirm( "Are you sure you want to restore Parent ("+title+")?");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('parent.restore')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#parent-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
        });
    }
    else
    {
        return false;
    }
}

function ChangeStatus(id ,element) {
    var status = "{{route('parent.changestatus')}}" + "/" + id;
    jQuery.ajax({
        type: "POST",
        "url": status,
        data: {_token: "{{csrf_token()}}"},
        dataType: 'json',
        success: function (response) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");

                    if(jQuery(element).hasClass("btn-success")){
                        jQuery(element).removeClass("btn-success").addClass("btn-danger");
                    }else if(jQuery(element).hasClass("btn-danger")){
                        jQuery(element).removeClass("btn-danger").addClass("btn-success");
                    }
                    jQuery(element).text(response.content);
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                // jQuery('#parent-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
    });
}
</script>
@stop
