@extends('layouts.master')
@section('title', 'Add Parent User')
@section('content')

<section class="content-header">
    <h1>Add Parent User <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Parent User </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Parent User</h3>
                </div>
                {!! Form::open(array('route' => array('parent.save'), 'id' => 'parent-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('user_fname', 'First Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_fname',null, array("class"=>"form-control user_fname required","placeholder"=> "First Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_lname', 'Last Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_lname',null, array("class"=>"form-control user_lname required","placeholder"=> "Last Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::email('email',null, array("class"=>"form-control email required","placeholder"=> "Email Address","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_phone', 'Phone Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_phone',null, array("class"=>"form-control user_phone digits required","placeholder"=> "Phone Number","maxlength"=> 10,"minlength"=> 8)) !!}
                        </div>
                       
                        <div class="form-group">
                            {!! Form::label('nearby_distance', 'Notification Near By Distance',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('nearby_distance', \App\User::getDistanceOptionsWeb(),\App\User::DISTANCE_500,['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_status', array(1 => 'Active', 0 => 'Inactive')) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('parents')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#parent-frm').validate();
                    });

                    
                </script>
            </div>
        </div>
    </div>
</section>
@endSection

