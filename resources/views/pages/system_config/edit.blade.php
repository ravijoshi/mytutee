@extends('layouts.master')
@section('title', 'Edit System Config')
@section('content')
<section class="content-header">
    <h1>Edit System Config <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit System Config </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit</h3>
                </div>
                {!! Form::model($system_config, array('route' => array('system_config.save'), 'id' => 'system_config-frm', 'method' => 'post')) !!}
                <input type="hidden" name="id" value="{{$system_config->id}}" />
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('name',$system_config->name, array("class"=>"form-control required","placeholder"=> "Name","maxlength"=> 255)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('access_key', 'Access Key',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('access_key',$system_config->access_key, array("class"=>"form-control required","placeholder"=> "Access Key","maxlength"=> 255,"readonly"=> "readonly")) !!}
                            <div class="notes" style="color: red;font-weight: bold;">Do Not change this Access Key</div>
                        </div>
                       
                        <div class="form-group">
                            {!! Form::label('value', 'Value',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('value',$system_config->value, array("class"=>"form-control required","id"=> "value","placeholder"=> "Value")) !!}
                        </div>
                       
                        <div class="form-group" style="display: none !important;">
                            {!! Form::label('status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('status', array(1 => 'Active',0 => 'Inactive'),$system_config->status) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('system_config')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Update',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#system_config-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

