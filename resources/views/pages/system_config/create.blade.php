@extends('layouts.master')
@section('title', 'Add System Config')
@section('content')
<section class="content-header">
    <h1>Add System Config <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add System Config </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                {!! Form::open(array('route' => array('system_config.save'), 'id' => 'system_config-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('name',null, array("class"=>"form-control required","placeholder"=> "Name","maxlength"=> 255)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('access_key', 'Access Key',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('access_key',null, array("class"=>"form-control required","placeholder"=> "Access Key","maxlength"=> 255)) !!}
                            <div class="notes" style="color: red;font-weight: bold;">Do Not change this Access key.</div>
                        </div>
                       
                        <div class="form-group">
                            {!! Form::label('value', 'Value',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('value',null, array("class"=>"form-control required","id"=> "value","placeholder"=> "Value")) !!}
                        </div>
                       
                        <div class="form-group" style="display: none !important;">
                            {!! Form::label('status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('status', array(1 => 'Active', 0 => 'Inactive'), 1) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('system_config')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#system_config-frm').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

