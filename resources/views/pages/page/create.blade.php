@extends('layouts.master')
@section('title', 'Add Page')
@section('content')
<?php $userModel =  new \App\User();
$roleOptions = $userModel->getRoleOptions(); ?>
<section class="content-header">
    <h1>Add Page <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Page </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                {!! Form::open(array('route' => array('page.save'), 'id' => 'page-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('page_name', 'Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('page_name',null, array("class"=>"form-control required","placeholder"=> "Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('access_key', 'Access Key',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('access_key',null, array("class"=>"form-control required","placeholder"=> "Access Key","maxlength"=> 50)) !!}
                            <div class="notes" style="color: red;font-weight: bold;">Do Not change this Access Key</div>
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('page_content', 'Content',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::textarea('page_content',null, array("class"=>"form-control required","id"=> "page_content","placeholder"=> "Content")) !!}
                        </div>
                       
                        <div class="form-group">
                            {!! Form::label('page_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('page_status', array(1 => 'Active' ,0 => 'Inactive')) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('page')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#page-frm').validate();
                    });
                    jQuery(function () {
                        CKEDITOR.replace('page_content');
                        CKEDITOR.config.allowedContent = true;
                    })
                </script>
            </div>
        </div>
    </div>
</section>
@stop

