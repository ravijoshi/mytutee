@extends('layouts.master')
@section('title', 'Add Driver User')
@section('content')
<section class="content-header">
    <h1>Add Driver User <small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Driver User </li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Add</h3>
                </div>
                {!! Form::open(array('route' => array('driver.save'), 'id' => 'driver-frm', 'method' => 'post')) !!}
                    <div class="box-body">
                        <h3 class="box-title">Driver Detail</h3>
                        <div class="form-group">
                            {!! Form::label('user_fname', 'First Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_fname',null, array("class"=>"form-control required","placeholder"=> "First Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_lname', 'Last Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_lname',null, array("class"=>"form-control required","placeholder"=> "Last Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::email('email',null, array("class"=>"form-control required","placeholder"=> "Email Address","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_phone', 'Phone Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_phone',null, array("class"=>"form-control digits required","placeholder"=> "Phone Number","maxlength"=> 10,"minlength"=> 8)) !!}
                        </div>
                       
                        <?php /* <div class="form-group">
                            {!! Form::label('user_role', 'User Role',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_role', $roleOptions) !!}
                        </div> */ ?>
                        <div class="form-group">
                            {!! Form::label('user_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_status', array(1 => 'Active',0 => 'Inactive')) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('vehicle', 'Do You Have Vehicle Details ?',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::checkbox('vehicle', 1) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('bank', 'Do You Have Bank Details ?',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::checkbox('bank', 1) !!}
                        </div>
                    </div>

                    <div class="box-body vehicle">
                        <h3 class="box-title">Vehicle Details</h3>
                        <div class="form-group">
                            {!! Form::label('vehicle_school_id', 'School',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('vehicle_school_id', \App\School::getSchoolOptions(), NULL, array("class"=>"form-control vehicle_input ")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vehicle_owner_name', 'Owner Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('vehicle_owner_name',  NULL, array("class"=>"form-control vehicle_input ","placeholder"=> "Owner Name", "maxlength" => 30)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vehicle_reg_no', 'Registration No',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('vehicle_reg_no',  NULL, array("class"=>"form-control vehicle_input ","placeholder"=> "Registration No", "maxlength" => 20)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vehicle_capacity', 'Capacity',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('vehicle_capacity', NULL, array("class"=>"form-control digits vehicle_input","placeholder"=> "Capacity","maxlength"=> 3)) !!}
                        </div>
                    </div>
                    <div class="box-body bank">
                        <h3 class="box-title">Bank Details</h3>
                        <div class="form-group">
                            {!! Form::label('bank_id', 'Bank Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('bank_id',\App\Bank::getBankOptions(FALSE), NULL, array("class"=>"form-control bank_input ","placeholder"=> "Bank Name")) !!}
                        </div>
                         <div class="form-group">
                            {!! Form::label('account_number', 'Account Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('account_number',NULL, array("class"=>"form-control digits bank_input","placeholder"=> "Account Number", "maxlength" => 20)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ifsc_code', 'IFSC Code',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('ifsc_code',null, array("class"=>"form-control bank_input","placeholder"=> "IFSC Code","minlength"=> 11,"maxlength"=> 11)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('bank_address', 'Bank Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::textarea('bank_address',null, array("class"=>"form-control ","placeholder"=> "Bank Address","rows"=> 4, "cols" => 10)) !!}
                        </div>
                        
                    </div>
                    <div class="box-footer">
                        <a href="{{route('drivers')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Submit',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery(".bank").hide();
                        jQuery(".vehicle").hide();
                        jQuery('#driver-frm').validate();
                    
                        jQuery("#bank").change(function() {
                            if(jQuery(this).is(":checked")) {
                                jQuery(".bank").show();
                                jQuery(".bank_input").addClass("required");
                                // jQuery(this).attr("checked", returnVal);
                            } else {
                                jQuery(".bank").hide();
                                jQuery(".bank_input").removeClass("required");
                            }
                        });

                        jQuery("#vehicle").change(function() {
                            if(jQuery(this).is(":checked")) {
                                jQuery(".select2-container").css('width','');
                                jQuery(".select2-container").css('width','100%');
                                jQuery(".vehicle").show();
                                jQuery(".vehicle_input").addClass("required");
                                // jQuery(this).attr("checked", returnVal);
                            } else {
                                jQuery(".vehicle").hide();
                                jQuery(".vehicle_input").removeClass("required");
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

