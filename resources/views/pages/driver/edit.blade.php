@extends('layouts.master')
@section('title', 'Edit Driver User')
@section('content')
<section class="content-header">
    <h1>Edit Driver User<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Driver User</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
                    @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Driver User</h3>
                </div>
                {!! Form::model($driver, array('route' => array('driver.save'), 'id' => 'drivers-frm', 'method' => 'post')) !!}
                    {{ csrf_field() }}
                    <input type="hidden" class="form-control" value="{{ $driver->id }}" name="id" >
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('', 'Life Time Earning : '. DEFAULT_CURRENCY_SYMBOL .$earning,array("class" => "pr-1 form-control-label required")) !!}
                        </div><div class="form-group">
                            {!! Form::label('', 'Monthly Earning : '. DEFAULT_CURRENCY_SYMBOL .number_format($driver->getMothlyEarning($driver->id),2),array("class" => "pr-1 form-control-label required")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_fname', 'First Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_fname',$driver->user_fname, array("class"=>"form-control required","placeholder"=> "First Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_lname', 'Last Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_lname',$driver->user_lname, array("class"=>"form-control required","placeholder"=> "Last Name","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Email Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::email('email',null, array("class"=>"form-control required","placeholder"=> "Email Address","maxlength"=> 50)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('user_phone', 'Phone Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('user_phone',$driver->user_phone, array("class"=>"form-control required digits","placeholder"=> "Phone Number","maxlength"=> 10,"minlength"=> 8)) !!}
                        </div>
                       
                        <?php /* <div class="form-group">
                            {!! Form::label('user_role', 'User Role',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_role', $roleOptions) !!}
                        </div> */ ?>
                        <div class="form-group">
                            {!! Form::label('user_status', 'Status',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::select('user_status', array(1 => 'Active', 0 => 'Inactive'),$driver->user_status) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('vehicle', 'Do You Have Vehicle Details ?',array("class" => "pr-1 form-control-label")) !!}
                            <?php if(!empty($driver->vehicle)):?>
                                {!! Form::checkbox('vehicle', 1, $driver->vehicle ? true : false, [""]) !!}
                            <?php else:?>
                                {!! Form::checkbox('vehicle', 1, $driver->vehicle ? true : false) !!}
                            <?php endif;?>

                        </div>

                        <div class="form-group">
                            {!! Form::label('bank', 'Do You Have Bank Details ?',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::checkbox('bank', 1, $driver->bank ? true : false, []) !!}
                        </div>
                    </div>
                    
                    <div class="box-body vehicle">
                        <h3 class="box-title">Vehicle Details</h3>
                        <div class="form-group">
                            {!! Form::label('vehicle_school_id', 'School',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('vehicle_school_id', \App\School::getSchoolOptions(), $driver->vehicle ? $driver->vehicle->vehicle_school_id : NULL, array("class"=>"form-control vehicle_input ")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vehicle_owner_name', 'Owner Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('vehicle_owner_name', $driver->vehicle ? $driver->vehicle->vehicle_owner_name : NULL, array("class"=>"form-control vehicle_input ","placeholder"=> "Owner Name", "maxlength" => 20)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vehicle_reg_no', 'Registration No',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('vehicle_reg_no', $driver->vehicle ? $driver->vehicle->vehicle_reg_no : NULL, array("class"=>"form-control vehicle_input ","placeholder"=> "Registration No", "maxlength" => 20)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('vehicle_capacity', 'Capacity',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('vehicle_capacity',$driver->vehicle ? $driver->vehicle->vehicle_capacity : NULL, array("class"=>"form-control digits vehicle_input","placeholder"=> "Capacity","maxlength"=> 3)) !!}
                        </div>
                    </div>

                    <div class="box-body bank">
                        <h3 class="box-title">Bank Details</h3>
                        <div class="form-group">
                            {!! Form::label('bank_id', 'Bank Name',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::select('bank_id', \App\Bank::getBankOptions(FALSE), $driver->bank ? $driver->bank->bank_id : NULL, array("class"=>"form-control bank_input ","placeholder"=> "Bank Name")) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('account_number', 'Account Number',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('account_number',$driver->bank ? $driver->bank->account_number : NULL, array("class"=>"form-control digits bank_input","placeholder"=> "Account Number", "maxlength" => 20)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('ifsc_code', 'IFSC Code',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::text('ifsc_code',$driver->bank ? $driver->bank->ifsc_code : NULL, array("class"=>"form-control bank_input","placeholder"=> "IFSC Code","maxlength"=> 11)) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('bank_address', 'Bank Address',array("class" => "pr-1 form-control-label required")) !!}
                            {!! Form::textarea('bank_address',$driver->bank ? $driver->bank->bank_address : NULL, array("class"=>"form-control ","placeholder"=> "Bank Address","rows"=> 4, "cols" => 10)) !!}
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('drivers')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Update',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#drivers-frm').validate();
                        
                        jQuery(".select2-container").removeAttr('style');
                        <?php if(!empty($driver->bank)):?>
                            jQuery(".bank").show();
                            jQuery(".bank_input").addClass("required");
                        <?php else: ?>
                            jQuery(".bank").hide();
                        <?php endif?>
                        <?php if(!empty($driver->vehicle)):?>
                            jQuery(".vehicle").show();
                            jQuery(".vehicle_input").addClass("required");
                        <?php else: ?>
                            jQuery(".vehicle").hide();
                        <?php endif?>

                        jQuery('#vehicle_school_id').select2({
                            selectOnClose: false,
                            allowClear: true,
                            // minimumResultsForSearch: -1,
                            // tokenSeparators: [",", " "],
                            placeholder: "Select School"
                        });

                        jQuery("#vehicle").change(function() {
                            if(jQuery(this).is(":checked")) {
                                jQuery(".select2-container").css('width','');
                                jQuery(".select2-container").css('width','100%');
                                jQuery(".vehicle").show();
                                jQuery(".vehicle_input").addClass("required");
                                // jQuery(this).attr("checked", returnVal);
                            } else {
                                jQuery(".vehicle").hide();
                                jQuery(".vehicle_input").removeClass("required");
                            }
                        });
                        
                        jQuery("#bank").change(function() {
                            if(jQuery(this).is(":checked")) {
                                jQuery(".bank").show();
                                jQuery(".bank_input").addClass("required");
                                // jQuery(this).attr("checked", returnVal);
                            } else {
                                jQuery(".bank").hide();
                                jQuery(".bank_input").removeClass("required");
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

