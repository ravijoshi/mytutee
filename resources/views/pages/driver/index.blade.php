@extends('layouts.master')
@section('title', 'Driver Management')
@section('content')
<section class="content-header">
    <h1>Driver Management<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> {{ __("Home")}}</a></li>
        <li class="active">{{ __("Driver Management")}}</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- alert -->
                @include('includes.alert')
            <!-- alert -->
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Driver  Management</h3>
                    <a href="{{route('driver.add')}}" class="pull-right btn btn-success ">+ Add New</a>
                </div>
                <div class="box-body">
                    <table id="driver-grid" class="table table-bordered table-striped table-responsive">
                        <thead>
                            <tr>
                                <th width="">Name</th>
                                <th width="">Email Address</th>
                                <th width="">Phone No.</th>
                                <th width="">Life Time Earning</th>
                                <th width="10%">Status</th>
                                <th width="10%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('scripts')
<script src="{{asset('public/theme/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('public/theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function () {
    var previousUrl = '{{str_replace(url('/'), '', url()->previous())}}';
    var stateSave =  false;
    if ((previousUrl.indexOf("/driver/edit/") >= 0 )|| (previousUrl.indexOf("driver/restore/") >= 0)){
        stateSave =  true;
    }
    var dtable = jQuery('#driver-grid').DataTable({
        "processing": true,
        "responsive" : true,
        "serverSide": true,
        "routeLength": 5,
        "stateSave": stateSave,
        "lengthMenu": [[5, 10, 25, 50], [5, 10, 25, 50]],
        "ajax": {
            "url": "{{route('getDriverList')}}",
            "dataType": "json",
            "type": "POST",
            "data": {'_token': "{{csrf_token()}}"}
        },
        initComplete: function() {
            var $searchInput = jQuery('div.dataTables_filter input');
            $searchInput.unbind();
            $searchInput.bind('keyup', function(e) {
                if(this.value.length > {{ DATATABLE_MINIMUM_LENGTH_FOR_SEARCH }}) {
                    dtable.search( this.value ).draw();
                }else if(this.value.length == 0) {
                    dtable.search( this.value ).draw();
                }
            });
        },
        "columns": [
            {"data": "user_name"},
            {"data": "email"},
            {"data": "user_phone"},
            {"data": "earning"},
            {"data": "user_status"},
            {"data": "action"}
        ],
        aoColumnDefs: [
            {
                bSortable: false,
                aTargets: [ -1, -3 ]
            }
        ]
    });
});
function getDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to delete Driver ("+title+")?");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('driver.delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#driver-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
        });
    } else {
        return false;
    }
}

function hardDelConfirmation(id, title) {

    var retVal = confirm( "Are you sure you want to Permanently delete Driver ("+title+")? All records related to driver ("+title+") also deleted.");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('driver.hard.delete')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#driver-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
        });
    } else {
        return false;
    }
}
function restoreUser(id, title) {

    var retVal = confirm( "Are you sure you want to restore Driver ("+title+")?");
    if (retVal == true) {
        jQuery.ajax({
            type: "POST",
            url: "{{route('driver.restore')}}" + "/" + id,
            data: {_token: "{{csrf_token()}}"},
            dataType: 'json',
            success: function (response) {
                jQuery('.ajaxAlert').show();
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                jQuery('#driver-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').show();
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
        });
    } else {
        return false;
    }
}
function ChangeStatus(id, element) {
    var status = "{{route('driver.changestatus')}}" + "/" + id;
    jQuery.ajax({
        type: "POST",
        "url": status,
        data: {_token: "{{csrf_token()}}"},
        dataType: 'json',
        success: function (response) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery(".ajaxAlert").removeClass("bg-success");
                jQuery(".ajaxAlert").removeClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").removeClass("fa-warning");
                
                if(response.responseType =='success'){
                    jQuery(".ajaxAlert").addClass("bg-success");
                    jQuery(".ajaxAlertIcon").addClass("fa-check");
                    if(jQuery(element).hasClass("btn-success")){
                        jQuery(element).removeClass("btn-success").addClass("btn-danger");
                    }else if(jQuery(element).hasClass("btn-danger")){
                        jQuery(element).removeClass("btn-danger").addClass("btn-success");
                    }
                    jQuery(element).text(response.content);
                } else {
                    jQuery(".ajaxAlert").addClass("bg-danger");
                    jQuery(".ajaxAlertIcon").addClass("fa-warning");
                }
                jQuery(".ajaxAlert #msg").text(response.message);
                
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery("html, body").animate({scrollTop: 0}, "fast");
                // jQuery('#driver-grid').DataTable().ajax.reload();
            },
            error: function (err) {
                jQuery('.ajaxAlert').css('display','block');
                jQuery('.ajaxAlert').fadeOut(5000);
                jQuery('.ajaxAlert .close').show(500);
                jQuery(".ajaxAlert #msg").text(err);
                jQuery(".ajaxAlert").addClass("bg-danger");
                jQuery(".ajaxAlertIcon").removeClass("fa-check");
                jQuery(".ajaxAlertIcon").addClass("fa-warning");
            },
            complete: function(res){
            }
    });
}
</script>
@stop
