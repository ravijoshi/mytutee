<!DOCTYPE html>
<html>
    <head>
        <title>{{ env('APP_NAME', 'My Tutee') }} - Login</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{url('/')}}"><img src ="{{asset('public/logo.png') }}" class="login-page-logo" /></b></a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Welcome to Sign in</p>
                {!! Form::open(array('route' => array('login'), 'id' => 'myLoginform', 'method' => 'post', "class" =>"clearfix")) !!}
                   <!-- alert -->
                        @include('includes.alert')
                    <!-- alert -->
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        {!! Form::email('email',null, array("class"=>"form-control required","placeholder"=> "Email","maxlength"=> 30)) !!}
                    </div>
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        {!! Form::password('password',array("class"=>"form-control required","placeholder"=> "Password","maxlength"=> 30)) !!}
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <a href="{{route('forgotPassword')}}">I forgot my password</a><br>
                        </div>
                        <div class="col-xs-4">
                            {!! Form::submit('Sign In',array("class" =>"btn btn-primary btn-block btn-flat")) !!}
                        </div>
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#myLoginform').validate();
                    });
                </script>

            </div>
        </div>
        
        <script src="{{asset('public/theme/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
        jQuery(function () {
            jQuery('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
        </script>
    </body>
</html>
