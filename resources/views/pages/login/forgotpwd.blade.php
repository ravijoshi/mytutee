<!DOCTYPE html>
<html>
    <head>
        <title>{{ env('APP_NAME', 'My Tutee') }} - Forgot Password</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page">
        
        <div class="login-box">
            <div class="login-logo">
                <a href="">Forgot your password?</a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Please enter the email address.</p>
                {!! Form::open(array('route' => array('forgotpassword'), 'id' => 'myForgotform', 'method' => 'post', "class" =>"clearfix")) !!}
                     <!-- alert -->
                        @include('includes.alert')
                    <!-- alert -->
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        {!! Form::email('email',null, array("class"=>"form-control required","placeholder"=> "Email","maxlength"=> 30)) !!}
                    </div>

                    <div class="row">
                        <div class="col-xs-8">
                            <a href="{{route('login')}}">Back to login</a><br>
                        </div>
                        <div class="col-xs-4">
                            {!! Form::submit('Submit',array("class" =>"btn btn-primary btn-block btn-flat")) !!}
                        </div>
                    </div>
                
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#myForgotform').validate();
                    });
                </script>

            </div>
        </div>
    </body>
</html>
