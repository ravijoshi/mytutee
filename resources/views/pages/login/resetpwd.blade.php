<!DOCTYPE html>
<html>
    <head>
        <title>{{ env('APP_NAME', 'My Tutee') }} - Reset Password</title>
        @include('includes.head')
    </head>
    <body class="hold-transition login-page">
        
        <div class="login-box">
            <div class="login-logo">
                <a href="">Reset your password</a>
            </div>
            

            <div class="login-box-body">
                <p class="login-box-msg">Please enter new password.</p>
                {!! Form::open(array('route' => array('resetPasswordPost'), 'id' => 'reset-password-frm', 'method' => 'post', "class" =>"clearfix")) !!}
                    <!-- alert -->
                        @include('includes.alert')
                    <!-- alert -->
                    <input type="hidden" name="forgot_token" value="{{$token}}">
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        {!! Form::password('password', array("class"=>"form-control required","placeholder"=> "Password","maxlength"=> 30 , "minlength"=> 6, "id"=> "password" )) !!}
                    </div>
                    
                    <div class="form-group has-feedback">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        {!! Form::password('confirm_password', array("class"=>"form-control required","placeholder"=> "Confirm Password","maxlength"=> 30, "minlength"=> 6 , "equalto" =>"#password")) !!}
                    </div>

                    <div class="row">
                        <div class="col-xs-4">
                            <a href="{{route('login')}}">Back to login</a><br>
                        </div>
                        <div class="col-xs-8">
                            {!! Form::submit('Reset Password',array("class" =>"btn btn-primary btn-block btn-flat")) !!}
                        </div>
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#reset-password-frm').validate();
                    });
                </script>

            </div>
        </div>
    </body>
</html>
