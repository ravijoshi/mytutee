@extends('layouts.master')
@section('title', 'Suggestions Reply')
@section('content')
<section class="content-header">
    <h1>Suggestions Reply<small></small></h1>
    <ol class="breadcrumb">
        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Suggestions Reply</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
        <!-- alert -->
            @include('includes.alert')
        <!-- alert -->
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-envelope"></i>
                    <h3 class="box-title">Suggestions Reply</h3>
                </div>
                {!! Form::model($suggestion, array('route' => array('suggestion-reply'), 'id' => 'suggestions', 'method' => 'post')) !!}
                    {{ csrf_field() }}


                    {!! Form::hidden('email',$suggestion->parent->email,array("" => "")) !!}
                    {!! Form::hidden('id',$suggestion->suggestion_id,array("" => "")) !!}
                    <div class="box-body">
                        <div class="form-group">

                            {!! Form::label('suggestion_by', 'Suggestion By',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::text('school_name',$suggestion->parent->user_fname ." ".$suggestion->parent->user_lname, array("class"=>"form-control required" ,"disabled" => "disabled","placeholder"=> "Suggestion By")) !!}
                           
                        </div>
                        <div class="form-group">
                            {!! Form::label('suggestion_msg', 'Suggestion Message',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::textarea('suggestion_msg',$suggestion->suggestion_msg ,['class'=>'form-control required', 'rows' => 10, 'cols' => 40,"disabled" => "disabled","placeholder"=> "Suggestion Message"]) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('reply', 'Reply',array("class" => "pr-1 form-control-label")) !!}
                            {!! Form::textarea('reply',null ,['class'=>'form-control required', 'rows' => 3, 'cols' => 40,"placeholder"=> "Reply"]) !!}

                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{route('suggestion')}}"  class='btn btn-primary' ><span class='fa fa-cancel'></span>Cancel</a>
                        {!! Form::submit('Reply',array("class" =>"btn btn-primary")) !!}
                    </div>
                {{ Form::close() }}
                <script type="text/javascript">
                    jQuery(document).ready(function() {
                        jQuery('#suggestions').validate();
                    });
                </script>
            </div>
        </div>
    </div>
</section>
@stop

