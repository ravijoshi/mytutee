<!DOCTYPE html>
<html>
    <head>
        
        <title>{{ env('APP_NAME', 'My Tutee') }} - @yield('title')</title>

        @include('includes.head')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- Navbar Links-->
            @include('includes.header')
            <!-- /.navbar -->
            <!-- Main Sidebar Container -->
            <!-- Sidebar -->
            @include('includes.sidebar')
            <!-- /.sidebar -->
            <!-- Content Wrapper. Contains page content -->

            <div class="content-wrapper">
                @yield('content')
            </div>
            @include('includes.footer')
            @yield('scripts')
    </body>
</html>