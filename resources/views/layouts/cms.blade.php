<!DOCTYPE html>
<html>
    <head>
        <title>{{ env('APP_NAME', 'My Tutee') }} - @yield('title')</title>
        @include('includes.head')
        <link rel="stylesheet" href="{{asset('public/css/cms.css') }}">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="privacy-policy">
            <div class="logo-section">
                <h1 class="title">{{$title}}</h1>
                <img src="{{asset('public/images/logo.png') }}" class="logo" alt="Logo"/>
            </div>
            @yield('content')
            <div class="logo-section">
                <strong>Copyright &copy; {{date("Y") -1 .'-'.date("Y")}}. {{ env('APP_NAME', 'My Tutee') }}.</strong>
                <img src="{{asset('public/images/logo.png') }}" class="logo" alt="Logo"/>
            </div>
        </div>
    </body>
</html>