<!DOCTYPE html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @include('includes.header')
            <!-- Left side column. contains the logo and sidebar -->
            @include('includes.sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>{{ $totalSchool }}</h3>
                                    <p>School Management </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-university"></i>
                                </div>
                                <a href="{{ route('schools')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>{{ $totalDriverUser }}</sup></h3>
                                    <p>Driver Management</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{ route('drivers')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>{{ $totalParentUser }}</h3>
                                    <p> Parent Management </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{ route('parents')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>{{number_format($totalPayment,2) }}</h3>
                                    <p> Total Payment To Driver </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-inr"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer"><i class="fa fa-align-justify"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-blue">
                                <div class="inner">
                                    <h3>{{number_format($totalEarning,2) }}</h3>
                                    <p> Gross Earning </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-inr"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer"> <i class="fa fa-align-justify"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xs-6">
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3>{{number_format($totalEarning - $totalPayment,2) }}</h3>
                                    <p> Net Earning </p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-inr"></i>
                                </div>
                                <a href="javascript:void(0)" class="small-box-footer"> <i class="fa fa-align-justify"></i></a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            @include('includes.footer')
    </body>
</html>