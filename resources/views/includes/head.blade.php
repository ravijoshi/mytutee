        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="{{asset('public/logo.png') }}" type="image/x-icon"/>

        <title>My Tutee | Dashboard</title>
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{asset('public/theme/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('public/theme/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{asset('public/theme/bower_components/Ionicons/css/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('public/theme/dist/css/AdminLTE.min.css') }}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset('public/theme/dist/css/skins/_all-skins.min.css') }}">
        <?php /*Morris chart -->
        <?php //<link rel="stylesheet" href="{{asset('public/theme/bower_components/morris.js/morris.css') }}"> ?>
        <!-- jvectormap -->
        <!-- <link rel="stylesheet" href="{{asset('public/theme/bower_components/jvectormap/jquery-jvectormap.css') }}"> -
        */?>
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{asset('public/css/timepicker/jquery-ui-timepicker-addon.css') }}">
        
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{asset('public/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
        <link rel="stylesheet" href="{{asset('public/theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
        
        <link rel="stylesheet" href="{{asset('public/css/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{asset('public/css/select2.css') }}">
        <link rel="stylesheet" href="{{asset('public/theme/custom.css') }}">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <script src="{{asset('public/js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{asset('public/js/jquery.validate.min.js') }}"></script>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Bootstrap 3.3.7 -->
        <script src="{{asset('public/theme/dist/js/custom.js') }}"></script>
        
        <script src="{{asset('public/js/select2.min.js') }}"></script>
        <script src="{{asset('public/theme/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{asset('public/theme/bower_components/ckeditor/ckeditor.js') }}"></script>
        <!-- ./wrapper -->
        <!-- jQuery 3 -->
        <?php /*<!-- <script src="{{asset('public/theme/bower_components/jquery/dist/jquery.min.js') }}"></script> - */?>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{asset('public/theme/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <?php  /*Bootstrap 3.3.7 -->
        <!-- <script src="{{asset('public/theme/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script> -->
        <!-- Morris.js charts -->
        <!-- <script src="{{asset('public/theme/bower_components/raphael/raphael.min.js') }}"></script> -->
        <!-- <script src="{{asset('public/theme/bower_components/morris.js/morris.min.js') }}"></script> -->
        <!-- Sparkline -->
        <!-- <script src="{{asset('public/theme/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script> -->
        <!-- jvectormap -->
        <!-- <script src="{{asset('public/theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script> -->
        <!-- <script src="{{asset('public/theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> -->
        <!-- jQuery Knob Chart -->
        <!-- <script src="{{asset('public/theme/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script> -->
        <!-- daterangepicker 
        */?>
        <script src="{{asset('public/theme/bower_components/moment/min/moment.min.js') }}"></script>
        
        <!-- datepicker -->
        
        <script src="{{asset('public/js/timepicker/jquery-ui-timepicker-addon.js') }}"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{asset('public/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
        <!-- Slimscroll -->
        <script src="{{asset('public/theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
        <!-- FastClick -->
        <!-- <script src="{{asset('public/theme/bower_components/fastclick/lib/fastclick.js') }}"></script> -->
        <!-- AdminLTE App -->
        <script src="{{asset('public/theme/dist/js/adminlte.min.js') }}"></script>
        <?php /* AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- <script src="{{asset('public/theme/dist/js/pages/dashboard.js') }}"></script> -->
        <!-- AdminLTE for demo purposes -->
        <!-- <script src="{{asset('public/theme/dist/js/demo.js') }}"></script> */?>

        <?php  if(in_array(Route::currentRouteName(),['route.edit','route.add','children.edit','children.add'])):?>
            <script async defer src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&callback=initialize&key={{ env('GOOGLE_API_KEY', '') }}" type="text/javascript"></script>
        <?php endif;?>

        