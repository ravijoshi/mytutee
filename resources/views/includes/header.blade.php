         <header class="main-header">
                <a href="{{route('dashboard')}}" class="logo">
                    <span class="logo-mini"><img src ="{{asset('public/logo.png') }}" /></span>
                    <span class="logo-lg"><img src ="{{asset('public/logo.png') }}" /></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">{{ Auth::user()->user_fname. " " . Auth::user()->user_lname }}</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Tasks: style can be found in dropdown.less -->
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- <img src="{{asset('public/theme/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"> -->
                                    <span class="hidden-xs">{{ Auth::user()->user_fname. " " . Auth::user()->user_lname }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <?php /* <li class="user-header">
                                        <img src="{{asset('public/theme/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">

                                        <p>
                                            Alexander Pierce - Web Developer
                                            <small>Member since Nov. 2012</small>
                                        </p>
                                    </li> 
                                    <!-- Menu Body -->
                                    <li class="user-body">
                                        <div class="row">
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Followers</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Sales</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <a href="#">Friends</a>
                                            </div>
                                        </div>
                                        <!-- /.row -->
                                    </li>*/?>
                                    <!-- Menu Footer-->
                                    <li class="">
                                        <li class="">
                                            <a href="{{route('profile')}}" class="btn btn-default btn-flat">
                                                <i class="fa fa-user"></i> <span>{{ __("My Profile")}}</span>
                                                <span class="pull-right-container">
                                                </span>
                                            </a>
                                        </li>
                                        <li class="">
                                            <a href="{{route('logout')}}" class="btn btn-default btn-flat">
                                                <i class="fa fa-sign-out"></i> <span>{{ __("Sign out")}}</span>
                                                <span class="pull-right-container">
                                                </span>
                                            </a>
                                        </li>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>