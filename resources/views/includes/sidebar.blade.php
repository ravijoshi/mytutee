<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!--        <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{asset('public/theme/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>Alexander Pierce</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>-->
        <ul class="sidebar-menu" data-widget="tree">
            <li <?php if(Route::currentRouteName() == 'dashboard'):?> class=" dashboard active" <?php else :?> class=" dashboard" <?php endif;?>>
                <a href="{{route('dashboard')}}">
                    <i class="fa fa-dashboard"></i> <span>{{ __("Dashboard")}}</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['schools','schools/edit','schools/add','schools/delete'])):?> class="schools active" <?php endif;?>>
                <a href="{{route('schools')}}">
                    <i class="fa fa-university"></i> <span>{{ __("School Management")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['drivers','driver.edit','driver.add','driver.delete'])):?> class="driver active" <?php endif;?>>
                <a href="{{route('drivers')}}">
                    <i class="fa fa-car"></i> <span>{{ __("Driver Management")}}</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['parents','parent.edit','parent.add','parent.delete'])):?> class="parent active" <?php endif;?>>
                <a href="{{route('parents')}}">
                    <i class="fa fa-users"></i> <span> {{ __("Parent Management")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['user.registerationPending','user.registerationPendingList','user.forcedelete',])):?> class="registerationPending active" <?php endif;?>>
                <a href="{{route('user.registerationPending')}}">
                    <i class="fa fa-users"></i> <span> {{ __("User Registration Pending List")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['package','package.edit','package.add','package.delete'])):?> class="package active" <?php endif;?>>
                <a href="{{route('package')}}">
                    <i class="fa fa-tags"></i> <span> {{ __("Package Management")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>

            <li <?php  if(in_array(Route::currentRouteName(),['bank','bank.edit','bank.add','bank.delete'])):?> class="bank active" <?php endif;?>>
                <a href="{{route('bank')}}">
                    <i class="fa fa-money"></i> <span> {{ __("Bank Management")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>

            <li <?php  if(in_array(Route::currentRouteName(),['route','route.edit','route.add','route.delete'])):?> class="route active" <?php endif;?>>
                <a href="{{route('route')}}">
                    <i class="fa fa-road"></i> <span> {{ __("Route Management")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['order','order.edit','order.add','order.delete'])):?> class="order active" <?php endif;?>>
                <a href="{{route('order')}}">
                    <i class="fa fa-shopping-cart"></i> <span> {{ __("Order Management")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['suggestion','suggestion.changestatus'])):?> class="suggestion active" <?php endif;?>>
                <a href="{{route('suggestion')}}">
                    <i class="fa fa-envelope-o"></i> <span> {{ __("Suggestions")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>

            <li <?php  if(in_array(Route::currentRouteName(),['system_config','system_config.edit','system_config.add','system_config.delete'])):?> class="system_config active" <?php endif;?>>
                <a href="{{route('system_config')}}">
                    <i class="fa fa-cog"></i> <span> {{ __("Manage System Configs")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>


             <li <?php  if(in_array(Route::currentRouteName(),['page','page.edit','page.add','page.delete'])):?> class="page active" <?php endif;?>>
                <a href="{{route('page')}}">
                    <i class="fa fa-file"></i> <span> {{ __("Pages")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <li <?php  if(in_array(Route::currentRouteName(),['mailtemplate','mailtemplate.edit','mailtemplate.add','mailtemplate.delete'])):?> class="mailtemplate active" <?php endif;?>>
                <a href="{{route('mailtemplate')}}">
                    <i class="fa fa-envelope-open"></i> <span> {{ __("Mail Templates")}} </span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li>
            <?php /*<li>
                <a href="{{url('logout')}}">
                    <i class="fa fa-sign-out"></i> <span>{{ __("Logout")}}</span>
                    <span class="pull-right-container">
                    </span>
                </a>
            </li> */ ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>