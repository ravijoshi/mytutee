            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <!-- <b>Version</b> 2.4.0 -->
                </div>
                <strong>Copyright &copy; {{date("Y") -1 .' - '.date("Y")}} <a href="#">{{ env('APP_NAME', 'My Tutee') }}</a>.</strong> All rights reserved.
            </footer>
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async></script>
        
        