<div id="default-loader" style="display: none;">
    <img src="{{asset('public/loader.gif') }}">
</div>
@if ($message = Session::get('success'))
    <div class="alert bg-success" role="alert">
        <em class="fa fa-lg icon fa-check">&nbsp;</em> 
        {{ __($message) }}
        <a href="#" class="pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a>
    </div>
@endif
@if ($message = Session::get('error'))
    <div class="alert bg-danger" role="alert">
        <em class="fa fa-lg fa-warning">&nbsp;</em> 
        {{ __($message) }}
        <a href="#" class="pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a>
    </div>
@endif

@if ($errors->any())
    <div class="alert bg-danger" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> 
            @foreach ($errors->all() as $error)
                {{ __($error) }}</br>
            @endforeach
        <a href="#" class="pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a>
    </div>
@endif
<div class="alert ajaxAlert"  role="ajaxAlert" style="display: none;">
    <em class="ajaxAlertIcon fa fa-lg icon fa-check">&nbsp;</em> 
    <span class="msg" id="msg"></span>
    <a href="#" class="pull-right" data-dismiss="ajaxAlert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a>
</div>