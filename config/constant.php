<?php

define("UNAUTHORIZE_ACCESS", "Unauthorized access.");
define("EMAIL_NOT_FOUND", "This email is not registered with us");
define("USER_NOT_FOUND", "User not found");
define("FROM_EMAIL", "donotreplay@mytutee.com");
define("FROM_EMAIL_TITLE", "My Tutee");
define("VEHICLE_NOT_FOUND", "Vehicle not found");
define("RECORD_NOT_FOUND", "Record not found");
define("ROUTE_NOT_FOUND", "Route not found");
define("SCHOOL_NOT_FOUND", "School not found");
define("TRIP_NOT_FOUND", "Trip not found");
define("TRIP_DETAILS_NOT_FOUND", "Trip Details not found");
define("CHILD_NOT_FOUND", "Child not found");
define("INVALID_USER_ROLE", "User Role not valid");
define("USER_IMAGE_FOLDER", "users");
define("VEHICLE_IMAGE_FOLDER", "vehicles");
define("DEFAULT_TIME_ZONE", "GMT");
define("BANK_FOUND", "Bank details found successfully.");
define("BANK_ERROR", "Bank details not found.");
define("USER_BANK_UPDATED", "User Bank details updated successfully.");
define("USER_BANK_CEATED", "User Bank details created successfully.");
define("DEFAULT_CURRENCY_SYMBOL", "&#8377;");
define("SMS_GATEWAY_API_KEY", "212bbe88-c57e-11e8-a895-0200cd936042");
define("OTP_VERIFICATION_TYPE_REGISTRATION", 1);
define("OTP_VERIFICATION_TYPE_LOGIN", 2);
define("OTP_VERIFICATION_TYPE_FORGOT_PASSWORD", 3);


// FCM Notification Details
define("FCM_SERVER_KEY", "AAAAAyVtERs:APA91bEoDaoYoauovskHmp416DBHcoMj-bJkDsmO6-Z_nLBK9boj4_NLby8DacCrbAJpfhgjDnBAbuWUzXTl6zkDq8uEgk9uEI1nFZTqll8afSGWIzW9PccBmjl6pJCCMH_rk3ZwiHhr");

// define("FCM_SERVER_KEY", "AAAAMDi-Lg8:APA91bHHd-BAPiFA4_i2dH4aF4G-Q89AgEs8gJ7Xycuz5jhYCMTSlL8Fh-Xb0pU5GcRISjJRKHUobXs0teMJauT4UnHSzu217RPE8tb0uJdg8qFGQZmOQE8I0IcBnXCPJHq4nW0ZHRcK");
define("TRIP_STARTED", 0);
define("VEHICLE_NEARBY", 1);
define("TRIP_ENDED", 2);
define("CHILD_NOT_COMING", 3);
define("CHILD_ARRIVE_SCHOOL", 4);
define("CHILD_ADDED_ON_ROUTE", 5);
define("SUBSCRIPTION_REMINDER", 6);
define("TRIP_START_REMINDER", 7);
define("DRIVER_TRIP_END_REMINDER", 8);


define("VEHICLE_NEARBY_DISTANCE", 1000);  // in meters
define("VEHICLE_NEARBY_SCHOOL", 200);  // in meters
define("CHILDREN_ARRIVE_AT_SCHOOL", 5);  // in minute
define("VEHICLE_NEARBY_NOTIFICATION_MINUTE_LENGTH", 15);  // in minute
define("DATATABLE_MINIMUM_LENGTH_FOR_SEARCH", 3); // character length

define("TYPE_HOME_TO_SCHOOL", 0);
define("TYPE_SCHOOL_TO_HOME", 1);

define("TRIP_TYPE_HOME_SCHOOL", 0);
define("TRIP_TYPE_SCHOOL_HOME", 1);
define("TRIP_TYPE_ROUND", 2);

define("IS_ROUND_TRIP_YES", 1);
define("IS_ROUND_TRIP_NO", 0);
define("DEFAULT_FREE_TRIL_DAYS", 30);

define("IS_NOTIFICATION_SENT_TRUE", 1);
define("IS_NOTIFICATION_SENT_FALSE", 0);

define("IS_NEARBY_SCHOOL_NO", 0);
define("IS_NEARBY_SCHOOL_YES", 1);

define("TYPE_HOME_TO_SCHOOL_TEXT", "Home to Study");
define("TYPE_SCHOOL_TO_HOME_TEXT", "Study to Home");
define("TYPE_ROUND_TRIP_TEXT", "Round Trip");

define("ROUTE_TYPE_HOME_TO_SCHOOL", "HS");
define("ROUTE_TYPE_SCHOOL_TO_HOME", "SH");

define("FREE_TRIL_AVAILABLE_NO", 0);
define("FREE_TRIL_AVAILABLE_YES", 1);
define("SUBSCRIPTION_MONTH_DAYS", 30); // day value for subscription 1 month 
define("DRIVER_EARNING_IN_PERCENTAGE", 10); // DRIVER_EARNING_IN_PERCENTAGE per month 

define("IS_FREE_TRIL_ACTIVATED_NO", 0);
define("IS_FREE_TRIL_ACTIVATED_YES", 1);
define("IS_FREE_TRIL_ACTIVATED_EXPIRED", 2);
define("IS_SUBSCRIBED_NO", 0);
define("IS_SUBSCRIBED_YES", 1);
define("IS_SUBSCRIBED_EXPIRED", 2);
define("SUBSCRIPTION_EXPIRED_REMIND_BEFORE_DAYS", 10);
define("TRIP_START_REMIND_BEFORE_MIN", 5);
define("DEFAULT_TIMEZONE", "Asia/Kolkata");
define("ACCOUNT_STATUS_INACTIVE", "Your account status inactive. Please contact admin for more details.");

define("DELETE_OLD_DEVICE_TOKEN", 7);
define("DELETE_OLD_NOTIFICATION", 20);
define("END_ONGOIN_TRIP_DURATION", 60); //in mins