<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'schools';
    protected $primaryKey = 'id';

    public $timestamps = true;
    protected $_schoolList = [];
   
    protected $fillable = [
        'school_name', 'school_address', 'school_latitude', 'school_longitude', 'school_map_id','school_phone','school_status'
    ];

    protected static function boot() {
        parent::boot();


        /*static::deleting(function($schools) {
            $schools->value()->delete();
        });*/

        /*self::restoring(function ($schools) {
           $schools->value()->restore();
        });*/
    }

    public function getLatLong ($address)
    {
        $data =[];
        $url = "https://maps.google.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false&key=". env('GOOGLE_API_KEY', '')."";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response);
        
        $data['lat']  = NULL;
        $data['long'] = NULL;

        if($response->status == "OK") {
            $data['lat']  = $response->results[0]->geometry->location->lat;
            $data['long'] = $response->results[0]->geometry->location->lng;
        }
        return $data;
    }

    // this function is used to import school from google
    public function getSchoolList ($address, $nextPageToken = '')
    {
        $data =[];
        $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?input=".urlencode($address)."&type=school";
        
        if($nextPageToken != ""){
            $url =  "https://maps.googleapis.com/maps/api/place/textsearch/json?pagetoken=".$nextPageToken."&type=school";
        }
        
        $url .=  "&key=".env('GOOGLE_API_KEY', '')."";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response, true);
        if($response['status'] == "OK") {
            if(count($response['results']) > 0){
                foreach ($response['results'] as $result) {
                    $data['name'] =  $result['name'];
                    $data['address']    =  $result['formatted_address'];
                    $data['lat']        =  $result['geometry']['location']['lat'];
                    $data['long']       =  $result['geometry']['location']['lng'];
                    $data['phone_no']       =  "";
                    array_push($this->_schoolList ,$data);
                }
            }
            
            if(isset($response['next_page_token']) && $response['next_page_token'] != "") {
                sleep(15);
                $this->getSchoolList($address, $response['next_page_token']);
            }
            ksort($this->_schoolList);
        } 
        return $this->_schoolList;
    }

    public static function getSchoolOptions( $flag = true) {
        if($flag) {
            $options = School::where("school_status", 1)->pluck("school_name", "id");
            $options[''] = "Select School";
            return $options;
        }
        return School::select("id","school_name")->where("school_status", 1)->get();
    }
}
