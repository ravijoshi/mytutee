<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';
    protected $primaryKey = 'page_id';

    public $timestamps = true;

    protected $fillable = [
        'page_name','access_key','page_content', 'page_status'
    ];
}
