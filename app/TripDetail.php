<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripDetail extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trip_details';
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
   public function calculateDistance($currentLatLong = "0.0,0.0", $destinationLatLong = "0.0,0.0") {
        $data = [];
        $data['distance_text'] = "";
        $data['distance_value'] = 0;

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$currentLatLong."&destinations=".$destinationLatLong."&mode=driving&key=". env('GOOGLE_API_KEY', '');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($response);
        
        if(isset($response->status) && !empty($response->status) && $response->status == "OK") {
            if(isset($response->rows[0]->elements) && $response->rows[0]->elements){
                $data['distance_text'] = isset($response->rows[0]->elements[0]->duration->text) ? $response->rows[0]->elements[0]->duration->text :
                  "";
                $data['distance_value'] = isset($response->rows[0]->elements[0]->duration->value) ? $response->rows[0]->elements[0]->duration->value : 0;
            }
        }
        return $data;
   }

}
