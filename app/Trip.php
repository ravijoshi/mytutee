<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User, App\SystemConfig, App\Child, App\CronjobLog;

class Trip extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'trips';
    protected $primaryKey = 'trip_id';

    public $timestamps = true;
    protected $fillable = [
        'trip_type', 'driver_id', 'route_id', 'start_time', 'end_time', 'trip_status', 'is_notification_sent', 'is_nearby_school', 'nearby_school_at'
    ];

   protected static function boot() {
        parent::boot();

        static::deleting(function($trip) {
            if ($trip->isForceDeleting()) {
                $trip->tripdetails()->forceDelete();
            } else {
                $trip->tripdetails()->delete();
            }
        }); 

        /* self::restoring(function ($trip) {
          $trip->tripdetails()->restore();
          }); */
    }

    
    public function route() {
        return $this->hasOne('App\Route', 'route_id','route_id');
    }

    public function routeDetail() {
        return $this->hasMany('App\RouteDetail', 'route_id','route_id')->where("child_id", "!=",0);
    }

    public function driverDetail() {
        return $this->hasOne('App\User', 'id','driver_id')->select("*", \DB::raw('CONCAT(users.user_fname," ",users.user_lname) AS driver_name'));
    }

    public function tripdetails() {
        return $this->hasMany('App\TripDetail', 'trip_id', 'trip_id');
    }

    public function timeZoneConvert($fromTime, $fromTimezone, $toTimezone, $format) {
        $from = new DateTimeZone($fromTimezone);
        $to = new DateTimeZone($toTimezone);
        $orgTime = new DateTime($fromTime, $from);
        $toTime = new DateTime($orgTime->format("c"));
        $toTime->setTimezone($to);
        return $toTime->format($format);
    }

    public function findVehicleIsNearBy($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return ($miles * 1.609344 * 1000);
    }

    // this function is used to end trip which end time exceded
    public static function endTrip($trip) {
        if(!empty($trip) && isset($trip->trip_id)) {
            Trip::where("trip_id", $trip->trip_id)->update(["trip_status" => 1,"end_time" => date("Y-m-d H:i:s")]);
        }
    }


    public static function endTripBySystem($routeType = ROUTE_TYPE_HOME_TO_SCHOOL){
        // \DB::enableQueryLog();

        $defaultMins = END_ONGOIN_TRIP_DURATION;
        $tripDuration =  SystemConfig::where("access_key", "end_ongoing_trip_duration")->first();
        if(!empty($tripDuration) && isset($tripDuration->id) ){
            $defaultMins = (int) $tripDuration->value ? $tripDuration->value : CHILDREN_ARRIVE_AT_SCHOOL;
        }

        $systemConfig =  SystemConfig::where("access_key", "admin_time_zone")->first();
        $timezone =  isset($systemConfig->value) ? $systemConfig->value : DEFAULT_TIMEZONE;
        $currentTime =  User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", $timezone);
        $UTCHours = "+00:00";
        $timezoneHoursDiff = "+".User::getDateDiffInFormat(User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", "UTC"), $currentTime,"%H:%I");

        $tripModels =  Trip::select(
                "trip_id",
                "route_code",
                \DB::raw("trips.route_id"),
                \DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(trips.start_time,'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i') as trip_start_time"),
                \DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(route_types.end_time,'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i') as route_end_time"),
                \DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_ADD(trips.start_time, INTERVAL (".$defaultMins." + TIMESTAMPDIFF(MINUTE,route_types.start_time,route_types.end_time )) MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i') as trip_start_time_PlusTripDifferencePlusBuffer"),
                \DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_ADD(trips.start_time, INTERVAL ".$defaultMins." MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i') as admincurrentTime")
            )
            ->join("routes" ,function($join) {
                $join->on("routes.route_id","=","trips.route_id");
                $join->whereNull("routes.deleted_at");
            })
            ->join("route_types" ,function($join) {
                $join->on("route_types.route_id","=","trips.route_id");
                $join->whereNull("route_types.deleted_at");
            })
            ->where("trip_status", 0)
            ->where("route_types.route_type", $routeType)
            ->where(
                \DB::raw("DATE_FORMAT(
                    TIME(
                        CONVERT_TZ(
                            DATE_ADD(trips.start_time, INTERVAL (".$defaultMins." + TIMESTAMPDIFF(MINUTE,route_types.start_time,route_types.end_time )) MINUTE),
                            '".$UTCHours."','".$timezoneHoursDiff."')
                        ),'%H:%i')") ,
                '<=', $currentTime)
            ->get();
        // echo "<pre>";
        // $query = \DB::getQueryLog();
        // print_r(end($query));
        if($tripModels->count()){
            $cronLabel =  "endTrip HS : ";
            if($routeType == ROUTE_TYPE_SCHOOL_TO_HOME)
                $cronLabel =  "endTrip SH : ";
            
            foreach ($tripModels as $trip) {
                self::endTrip($trip);
                self::sendEndTripBySystemNotificationToParent($trip);
                $cronjobLog = new CronjobLog();
                $cronjobLog->notification_id =  0;
                $cronjobLog->title = $cronLabel.json_encode($trip->toArray());
                $cronjobLog->save();
            }
        }
    }

    public static function sendEndTripBySystemNotificationToParent($tripModel)
    {
        $childList = Child::where('route_id', $tripModel->route_id)->with(['parent' => function ($query) {
                        $query->with('userToken');
                    }])->get();
        
        if($childList->count()) {
            foreach ($childList as $key => $childModel) {
                $deviceTokens = [];
                if (!empty($childModel->parent) && !empty($childModel->parent->userToken) && $childModel->parent->userToken->count()) {
                    foreach ($childModel->parent->userToken as $key => $token) {
                        if ($token->device_token != '')
                            $deviceTokens[] = $token->device_token;
                    }
                }
                $data = [
                    'msg_type' => TRIP_ENDED,
                    'msg_text' => 'Trip Ended By System.',
                    'child_id' => strval($childModel->child_id),
                    'child_fname' => $childModel->child_fname,
                    'child_mname' => $childModel->child_mname,
                    'child_lname' => $childModel->child_lname,
                    'ended_by' => "System",
                    'pickup_lat' => floatval($childModel->pickup_lat),
                    'pickup_long' => floatval($childModel->pickup_long),
                    'route_code' => $tripModel->route_code,
                ];
                $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);

                if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                    $tripEndFlag = 1;
                } else {
                    $tripEndFlag = 0;
                }
                $updatedNotification = NotificationDetail::where( ['child_id' => $childModel->child_id, 'trip_id' => $tripModel->trip_id])->first();
                
                if (isset($updatedNotification->id) && !empty($updatedNotification->id)) {
                    $updatedNotification->trip_end = $tripEndFlag;
                    $updatedNotification->notification_data = $updatedNotification->notification_data." End trip by System :".json_encode($data);

                    if(!$tripEndFlag) {
                        if(!empty($updatedNotification->fcm_response)){
                            $updatedNotification->fcm_response = $updatedNotification->fcm_response. " End trip by system = ". json_encode($sendPushNotification);
                        } else {
                            $updatedNotification->fcm_response = json_encode($sendPushNotification);
                        }
                    }
                    $updatedNotification->save();
                }
            }
        }
    }
}
