<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CronjobLog extends Model {

    use SoftDeletes;
    
    protected $table = 'cronjob_log';
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'title'
    ];

}
