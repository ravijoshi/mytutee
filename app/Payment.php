<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model  {
    use SoftDeletes;
    
    protected $table = 'payment';
    protected $primaryKey = 'id';
    
    protected $fillable = [
        'parent_id', 'child_id', 'package_id', 'package_days', 'package_price', 'start_date', 'end_date', 'type', 'payment_method','payment_status',
        'additional_info','created_by','updated_by','deleted_by'
    ];

    const PAYMENT_TYPE_SBUSCRIBED   = 1;
    const PAYMENT_TYPE_UNSBUSCRIBED = 2;

    const PAYMENT_TYPE_SBUSCRIBED_TEXT      = "Subscribed";
    const PAYMENT_TYPE_UNSBUSCRIBED_TEXT    = "Unsubscribed";

    const PAYMENT_METHOD_PAYTM = 1;
    const PAYMENT_METHOD_PAYTM_TEXT = "Paytm";

    const PAYMENT_STATUS_PENDING    = 1;
    const PAYMENT_STATUS_INPROCESS  = 2;
    const PAYMENT_STATUS_PAID       = 3;
    const PAYMENT_STATUS_CANCEL     = 4;
    const PAYMENT_STATUS_OTHER      = 5;
    
    const PAYMENT_STATUS_PENDING_TEXT   = "Pending";
    const PAYMENT_STATUS_INPROCESS_TEXT = "In Process";
    const PAYMENT_STATUS_PAID_TEXT      = "Paid";
    const PAYMENT_STATUS_CANCEL_TEXT    = "Cancel";
    const PAYMENT_STATUS_OTHER_TEXT     = "Other";


    protected static function boot() {
        parent::boot();
        /*static::deleting(function($user) {
            $user->driver()->delete();
        });*/

        /* self::restoring(function ($user) {
          $user->driver()->restore();
          }); */
    }

    public function parent() {
        return $this->hasOne("\App\User", "id","parent_id")->select("*", \DB::raw("CONCAT(user_fname, ' ',user_lname) as parent_name"));
    }

    public function child() {
        return $this->hasOne("\App\Child", "child_id", "child_id")->select("*", \DB::raw("CONCAT(child_fname, ' ',child_mname, ' ',child_lname) as child_name"));
    }

    public function package() {
        return $this->hasOne("\App\Package", "id", "package_id");
    }

    public function getStatusOptions() {
        return [
            self::PAYMENT_STATUS_PENDING => self::PAYMENT_STATUS_PENDING_TEXT,
            self::PAYMENT_STATUS_INPROCESS => self::PAYMENT_STATUS_INPROCESS_TEXT,
            self::PAYMENT_STATUS_PAID => self::PAYMENT_STATUS_PAID_TEXT,
            self::PAYMENT_STATUS_CANCEL => self::PAYMENT_STATUS_CANCEL_TEXT,
            self::PAYMENT_STATUS_OTHER => self::PAYMENT_STATUS_OTHER_TEXT
        ];
    }

    public function getStatusOptionsText($status =  PAYMENT_STATUS_PENDING) {
        $options =  $this->getStatusOptions();

        return !empty($status) ? $options[$status] : PAYMENT_STATUS_PENDING_TEXT;
    }

    public function getPaymentOptions() {
        return [
            self::PAYMENT_METHOD_PAYTM => self::PAYMENT_METHOD_PAYTM_TEXT,
        ];
    }

    public function getPaymentOptionsText($methos =  PAYMENT_METHOD_PAYTM) {
        $options =  $this->getPaymentOptions();

        return !empty($methos) ? $options[$methos] : PAYMENT_METHOD_PAYTM_TEXT;
    }

    public function getPaymentTypeOptions() {
        return [
            self::PAYMENT_TYPE_SBUSCRIBED => self::PAYMENT_TYPE_SBUSCRIBED_TEXT,
            self::PAYMENT_TYPE_UNSBUSCRIBED => self::PAYMENT_TYPE_UNSBUSCRIBED_TEXT,
        ];
    }

    public function getPaymentTypeOptionsText($paymentType =  PAYMENT_TYPE_SBUSCRIBED) {
        $options =  $this->getPaymentTypeOptions();

        return !empty($paymentType) ? $options[$paymentType] : PAYMENT_TYPE_SBUSCRIBED;
    }
}