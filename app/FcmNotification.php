<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kawankoding\Fcm\Fcm;

class FcmNotification extends Model {

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    public static function sendPushNotification($recipients, $data) {
        $fcmEndpoint = 'https://fcm.googleapis.com/fcm/send';

        $fields = [
            'content-available' => true,
            'priority' => 'high',
            'data' => $data,
        ];
        $fields['registration_ids'] = $recipients;

        $serverKey = config('laravel-fcm.server_key');

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type:application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmEndpoint);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        return $result;
    }

}
