<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\RouteDetail, App\User;

class NotificationDetail extends Model {

    use Notifiable,
        SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';
    protected $primaryKey = 'id';

    public $timestamps = true;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    public function parent() {
        return $this->hasOne('App\User', 'id', 'parent_id');
    }

    public function route() {
        return $this->hasOne('App\Route', 'route_id', 'route_id');
    }

    public function child() {
        return $this->belongsTo('App\Child', 'child_id', 'child_id');
    }

    protected static function boot() {
        parent::boot();


        static::deleting(function($route) {
            $route->details()->delete();
        });

        static::created(function($notification) {
           self::deleteOldData();
        });
        /* self::restoring(function ($schools) {
          $schools->value()->restore();
          }); */
    }

    public static function deleteOldData(){
        $prevDate =  User::subDays(DELETE_OLD_NOTIFICATION);// today - 20 days.
        NotificationDetail::where("created_at", "<" , $prevDate)->forcedelete();
    }

}
