<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RouteDetail extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'route_details';
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    public function child() {
       return $this->belongsTo('App\Child', 'child_id', 'child_id')->select("*" , \DB::raw("CONCAT(child_fname, ' ',child_mname, ' ',child_lname) AS fullname"));
    }

    public function subscribedChildren() {
       return $this->belongsTo('App\Child', 'child_id', 'child_id')->where("is_subscribed",IS_SUBSCRIBED_YES)->select("*" , \DB::raw("CONCAT(child_fname, ' ',child_mname, ' ',child_lname) AS fullname"));
    }

    public function route() {
       return $this->hasOne('App\Route', 'route_id', 'route_id');
    }

    public function routeTypeHs() {
       return $this->hasOne('App\RouteType', 'route_id', 'route_id')->where("route_type",ROUTE_TYPE_HOME_TO_SCHOOL);
    }

}
