<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';
    protected $primaryKey = 'id';

    protected $fillable = [
        'type', 'url', 'method', 'ip_address', 'header', 'request', 'response', 'status_code', 'additional_info'
    ];

    const LOG_TYPE_API_REQUEST = "API_REQUEST";
    const LOG_TYPE_API_RESPONSE = "API_RESPONSE";
}
