<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail, App\UserBank, App\Child, Carbon\Carbon, App\SystemConfig;

class User extends Authenticatable {

    use Notifiable, SoftDeletes;

    const USER_ROLE_ADMIN = 0;
    const USER_ROLE_DRIVER = 1;
    const USER_ROLE_PARENT = 2;
    const USER_ROLE_ADMIN_TEXT = "Admin";
    const USER_ROLE_DRIVER_TEXT = "Driver";
    const USER_ROLE_PARENT_TEXT = "Parent";
    
    const USER_OTP_VERIFIED_TRUE = "1";
    const USER_OTP_VERIFIED_FALSE = "0";
    
    const USER_REGISTRATION_OTP_TEMPLATE = "REGISTRATION";
    const USER_RESET_PASSWORD_OTP_TEMPLATE = "FORGOT_PASSWORD";


    const DISTANCE_100 = 100;
    const DISTANCE_200 = 200;
    const DISTANCE_300 = 300;
    const DISTANCE_400 = 400;
    const DISTANCE_500 = 500;
    const DISTANCE_1000 = 1000;

    const DISTANCE_100_TEXT = "100 M";
    const DISTANCE_200_TEXT = "200 M";
    const DISTANCE_300_TEXT = "300 M";
    const DISTANCE_400_TEXT = "400 M";
    const DISTANCE_500_TEXT = "500 M";
    const DISTANCE_1000_TEXT = "1 KM";

    protected  $_apiKey;
    protected  $_appUrl;
    protected  $_phoneNo;
    protected  $_otp;
    protected  $_template;
    protected  $_sessionId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_fname', 'user_lname', 'email', 'password', 'user_phone', 'user_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($user) {
            if($user->isForceDeleting()) {
                $user->bank()->forcedelete();
                $user->children()->forcedelete();
                $user->vehicle()->forcedelete();
                $user->userToken()->forcedelete();
                $user->userEarning()->forcedelete();
            } else {
                $user->bank()->delete();
                $user->children()->delete();
                $user->vehicle()->delete();
                $user->userToken()->delete();
                $user->userEarning()->delete();
            }
        });

        self::restoring(function ($user) {
            $user->bank()->restore();
            $user->children()->restore();
            $user->vehicle()->restore();
        }); 
    }

    public function getApiKey(){
        
        $this->_apiKey =  SMS_GATEWAY_API_KEY;
        
        $config =  SystemConfig::where("access_key", "SMS_GATEWAY_API_KEY")->first();
        
        if(isset($config->value) && !empty($config->value)){
            $this->_apiKey =  $config->value;
        }
        return $this;
    }
    
    public function getAppUrl( $flag =  true){
        
        if($flag)
            $this->_appUrl = "https://2factor.in/API/V1/".$this->_apiKey."/SMS/".$this->_phoneNo."/".$this->_otp."/".$this->_template; 
        else 
            $this->_appUrl = "https://2factor.in/API/V1/".$this->_apiKey."/SMS/VERIFY/".$this->_sessionId."/".$this->_otp;

        return $this; 
    }

    public function setPhoneNo($phoneNo){
        $this->_phoneNo =  $phoneNo;
        return $this;
    }

    public function setOTP($otp){
        $this->_otp =  $otp;
        return $this;
    }

    public function setTemplate($template = ""){
        $this->_template =  self::USER_REGISTRATION_OTP_TEMPLATE;
        if(!empty($template))
            $this->_template =  $template;
        
        return $this;
    }

    public function setSessionId($sessionId){
        $this->_sessionId =  $sessionId;
        return $this;
    }
    
    public function generateOTP(){
        return str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
    }

    public function generateOTPTimeOut(){
        return time() + 60 * 60 * 2; // valid for 2 hours
    }

    // send otp 
    public function sendOTP($flag =  true){
        $this->getApiKey();
        $this->getAppUrl($flag);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->_appUrl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded"
            ),
        ));
        
        $response = curl_exec($curl);
        return json_decode($response,true);
    }

    public function bank() {
        return $this->hasOne("\App\UserBank", "user_id", "id")->select('id','bank_id','user_id','account_number','ifsc_code','bank_address');
    }

    public function vehicle() {
        return $this->hasOne("\App\Vehicle", "driver_id", "id");
    }

    public function userToken() {
        return $this->hasMany("\App\UserToken", "user_id", "id");
    }

    public function userRouteHs() {
        return $this->hasMany("\App\RouteType", "driver_id", "id")->where("route_type", ROUTE_TYPE_HOME_TO_SCHOOL);
    }

    public function children() {
        return $this->hasMany("\App\Child", "parent_id", "id");
    }

    public function userEarning() {
        return $this->hasMany("\App\UserEarning", "user_id", "id");
    }

    public function getUserEarning() {
        return $this->hasOne("\App\UserEarning", "user_id", "id")->select("user_id", \DB::raw('CONCAT("'.DEFAULT_CURRENCY_SYMBOL.'"," ",SUM(amount)) as totalEarning'))->where(['type' => UserEarning::EARNING_TYPE_CREDIT]);
    }

    public function lifeTimeEarning() {
        return UserEarning::where(["type" => UserEarning::EARNING_TYPE_CREDIT, "user_id" =>$this->id])->sum("amount");
    }

    public function getRoleOptions() {
        return [
            self::USER_ROLE_ADMIN => self::USER_ROLE_ADMIN_TEXT,
            self::USER_ROLE_DRIVER => self::USER_ROLE_DRIVER_TEXT,
            self::USER_ROLE_PARENT => self::USER_ROLE_PARENT_TEXT
        ];
    }

    public function sendForgotPasswordMail($data) {

        Mail::send('email.mailcontent', ['data' => $data], function ($m) use ($data) {
            $m->from($data['from'], $data['from_title']);

            $m->to($data['to'], $data['full_name'])->subject($data['subject']);
        });

        if (count(Mail::failures()) > 0) {
            foreach (Mail::failures() as $errors) {
                return false;
            }
        }
        return true;
    }

    // this function is used to get start of day like  1970-01-01 00:00:00
    public static function getStartOfDay() {
        return Carbon::today()->startOfDay()->toDateTimeString();
    }

    // this function is used to get start of day like  1970-01-01 23:59:59
    public static function getEndOfDay() {
        return Carbon::today()->endOfDay()->toDateTimeString();
    }

    // this function is used to get current datetime
    public static function getCurrentDateTime() {
        return Carbon::now()->toDateTimeString();
    }

    // this function is used to get current date
    public static function getCurrentDate() {
        return Carbon::now()->toDateString();
    }
    // this function is used to add days in current date time
    public static function addDays($day = 0, $date = NULL) {
        $dateObj =  Carbon::now();

        if(!empty($date)){
            $dateObj =  Carbon::parse($date);
        }
        return $dateObj->addDays($day)->toDateTimeString();
    }

    // this function is used to sub days in current date time
    public static function subDays($day = 0, $date = NULL) {
        $dateObj =  Carbon::now();

        if(!empty($date)){
            $dateObj =  Carbon::parse($date);
        }
        return $dateObj->subDays($day)->toDateTimeString();
    }

    // this function is used to get date difference in days
    public static function getDateDiffInDays($startDate = NULL, $endDate = NULL) {
        if(empty($startDate))
            $startDate =  self::getCurrentDateTime();
        if(empty($endDate))
            $endDate =  self::getCurrentDateTime();

        $startDate =  Carbon::parse($startDate);
        $endDate =  Carbon::parse($endDate);
        return $endDate->diffInDays($startDate);
    }

    // this function is used to get date difference in hours
    public static function getDateDiffInMinutes($startDate = NULL, $endDate = NULL) {
        if(empty($startDate))
            $startDate =  self::getCurrentDateTime();
        if(empty($endDate))
            $endDate =  self::getCurrentDateTime();

        $startDate =  Carbon::parse($startDate);
        $endDate =  Carbon::parse($endDate);
        return $endDate->diffInMinutes($startDate);
    }

     // this function is used to sub days in current date time
    public static function getDateDiffInFormat($startDate = NULL, $endDate = NULL, $format = "%H:%I:%S") {
        if(empty($startDate))
            $startDate =  self::getCurrentDateTime();
        if(empty($endDate))
            $endDate =  self::getCurrentDateTime();

        $startDate =  Carbon::parse($startDate);
        $endDate =  Carbon::parse($endDate);

        return $endDate->diff($startDate)->format($format);
    }

    // this function is used to convert utc date to given timezone
    public static function getDateTimeFromTimezone($date = NULL , $timezone =  "asia/kolkata") {
        if(empty($date)) { // set default UTC date time
            $date =  date("Y-m-d H:i:s");
        }

        return Carbon::parse($date, "UTC")->setTimezone($timezone)->format('Y-m-d H:i:s');
    }

    // this function is used to convert utc date to given timezone
    public static function getTimeFromTimezone($time = NULL, $format =  "H:i:s" , $timezone =  "asia/kolkata") {
        if(empty($time)) { // set default UTC time time
            $time =  date("H:i:s");
        }
        return Carbon::parse($time, "UTC")->setTimezone($timezone)->format($format);
    }

    // this function is used to convert utc date to given timezone
    public static function getDateFromTimezone($date = NULL , $timezone =  "asia/kolkata") {
        if(empty($date)) { // set default UTC date time
            $date =  date("Y-m-d");
        }
        return Carbon::parse($date, "UTC")->setTimezone($timezone)->format('Y-m-d');
    }

    // get last date time of current month
    public static function getCurrentMonthLastDateTime() {
        return  Carbon::now()->endOfMonth()->toDateTimeString();
    }

    // get last date  of current month
    public static function getCurrentMonthLastDate() {
        return  Carbon::now()->endOfMonth()->toDateString();
    }
    
    // get previous month last date time of current month
    public static function getPreviousMonthLastDateTime() {
        return Carbon::now()->subMonth()->endOfMonth()->toDateTimeString();
    }

    // pass driver user id to get monthly total earning. pass route id to get perticular routes earning.
    public function getMothlyEarning($userId, $routeId = "") {
        $total =  0;
        $percentage =  DRIVER_EARNING_IN_PERCENTAGE;
        
        $config =  SystemConfig::where("access_key", "driver_earning_per_month_percentage")->first();
        if(isset($config->value) && $config->value) {
            $percentage =  $config->value;
        }

        $query =  Child::with(["driverHs", "route", "payment"]);
        
        $query->where( function($select) use ($userId, $routeId){
            $select->where('is_subscribed', IS_SUBSCRIBED_YES);
            $select->where('subscribed_upto', '>=', $this->getStartOfDay());
            
            // get Perticular routes children
            if(!empty($routeId))
                $select->where('route_id', $routeId);

            $select->whereHas('driverHs', function($q) use ($userId) {
                $q->where('driver_id', $userId );
            });
        });
        
        $children =  $query->get();
        
        if($children->count()) {
            foreach ($children as $child) {
                
                if(isset($child->payment->package_days) && !empty($child->payment->package_days)) {
                    $suscriptionMonths =  ($child->payment->package_days / SUBSCRIPTION_MONTH_DAYS);
                    $perMonthAmount =  round($child->payment->package_price / $suscriptionMonths);
                    $total = $total + (($perMonthAmount * $percentage) / 100);
                }
            }
        }
        return (float) number_format($total, 2);
    }

    // this function is used to convert utc date default format 
    public static function getDefaultFormatedDate($date = NULL,$format = "Y F d h:i:s a" , $timezone =  "asia/kolkata") {
        if(empty($date)) { // set default UTC date time
            $date =  date("Y-m-d");
        }
        if(empty($format)) { // set default UTC date time
            $format = "Y F d h:i:s A";
        }
        return Carbon::parse($date, "UTC")->setTimezone($timezone)->format($format);
    }


    public static function getDistanceOptions(){
        return [
            [
                "label" =>  self::DISTANCE_100_TEXT,
                "value" =>  self::DISTANCE_100
            ],
            [
                "label" =>  self::DISTANCE_200_TEXT,
                "value" =>  self::DISTANCE_200
            ],
            [
                "label" =>  self::DISTANCE_300_TEXT,
                "value" =>  self::DISTANCE_300
            ],
            [
                "label" =>  self::DISTANCE_400_TEXT,
                "value" =>  self::DISTANCE_400
            ],
            [
                "label" =>  self::DISTANCE_500_TEXT,
                "value" =>  self::DISTANCE_500
            ],
            [
                "label" =>  self::DISTANCE_1000_TEXT,
                "value" =>  self::DISTANCE_1000
            ]
        ];
    }
    public static function getDistanceOptionsWeb(){
        return [
            self::DISTANCE_100 =>  self::DISTANCE_100_TEXT,
            self::DISTANCE_200 =>  self::DISTANCE_200_TEXT,
            self::DISTANCE_300 =>  self::DISTANCE_300_TEXT,
            self::DISTANCE_400 =>  self::DISTANCE_400_TEXT,
            self::DISTANCE_500 =>  self::DISTANCE_500_TEXT,
            self::DISTANCE_1000 =>  self::DISTANCE_1000_TEXT,
        ];
    }
}
