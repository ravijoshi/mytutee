<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model  {
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vehicles';
    protected $primaryKey = 'vehicle_id';
    
    protected $fillable = [
        'driver_id', 'vehicle_owner_name', 'vehicle_capacity', 'vehicle_photo', 'vehicle_reg_no', 'vehicle_school_id', 'vehicle_status'
    ];

    protected static function boot() {
        parent::boot();
        /*static::deleting(function($user) {
            $user->driver()->delete();
        });*/

        /* self::restoring(function ($user) {
          $user->driver()->restore();
          }); */
    }

    public function driver() {
        return $this->hasOne("\App\User", "id","driver_id");
    }

    public function school() {
        return $this->hasOne("\App\School", "vehicle_school_id", "id");
    }
}