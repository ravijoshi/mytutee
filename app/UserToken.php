<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class UserToken extends Model {

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_tokens';
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
//    public function school() {
//        return $this->belongsTo('App\School', 'school_id');
//    }

    protected static function boot() {
        parent::boot();
        /* static::deleting(function($schools) {
          $schools->value()->delete();
          }); */

        /* self::restoring(function ($schools) {
          $schools->value()->restore();
          }); */
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    // this function is used to hard delete record
    public function deleteOldData() {
        $prevDate =  User::subDays(DELETE_OLD_DEVICE_TOKEN);
        UserToken::withTrashed()->where("deleted_at", "<=" , $prevDate)->forcedelete();
        
    }

    // this function is used to soft delete same device token record from the db
    public function deleteOldDeviceToken() {
        UserToken::where("device_token", "=" , $this->device_token)->where("id", "!=" , $this->id)->delete();
    }
}
