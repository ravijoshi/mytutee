<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mail;

class Mailtemplate extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mailtemplate';
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'subject','access_key', 'content', 'cc', 'bcc','status'
    ];

}
