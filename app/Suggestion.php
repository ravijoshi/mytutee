<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Suggestion extends Model {
    
    use SoftDeletes;
    
    protected $table = 'suggestions';
    protected $primaryKey = 'suggestion_id';

    public $timestamps = true;

    protected $fillable = [
        'parent_id','suggestion_msg', 'suggestion_stauts'
    ];


    public function parent() {
        return $this->belongsTo("\App\User","parent_id","id");
    }
}
