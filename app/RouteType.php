<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RouteType extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'route_types';
    protected $primaryKey = 'id';

    protected $fillable = [
        'route_id', 'driver_id', 'route_type', 'start_time', 'end_time', 'start_loc_lat', 'start_loc_long', 'start_loc_address', 'end_loc_lat', 'end_loc_long', 'end_loc_address'
    ];

   
    protected $hidden = [
        'deleted_at'
    ];

    public function driver() {
        return $this->hasOne('App\User', 'id', 'driver_id')->select("*", \DB::raw('CONCAT(users.user_fname," ",users.user_lname) AS driver_name'));
    }

    public function vehicle() {
        return $this->hasOne("\App\Vehicle", "driver_id", "driver_id");
    }


    public function children() {
        return $this->hasMany('App\Child', 'route_id', 'route_id')->select("child_id","parent_id","route_id","pickup_lat","pickup_long","pickup_address","free_trial_activate", "free_trial_days","activate_date",\DB::raw("concat(child_fname,' ',child_mname,' ' ,child_lname) as fullname"));
    }

    // This function is used to get total children in route 
    public function routeChildren() {
        return $this->hasMany('App\RouteDetail', 'route_id', 'route_id')->where("child_id", "!=", 0);
    }

    public function route() {
       return $this->hasOne('App\Route', 'route_id','route_id');
    }

    public function saveRouteType($route, $data,  $flag = true)
    {
            
        if($flag)
            $routeType = RouteType::where(["route_id" => $route->route_id, "route_type" => ROUTE_TYPE_HOME_TO_SCHOOL ])->first();
        else 
            $routeType = RouteType::where(["route_id" => $route->route_id, "route_type" => ROUTE_TYPE_SCHOOL_TO_HOME ])->first();


        if(empty($routeType)) {
            $routeType = new RouteType();
        }


        $routeType->route_id    =  $route->route_id;
        $routeType->start_time  =  date("Y-m-d H:i:s" ,strtotime($data['start_time'])) ;
        $routeType->end_time    =  date("Y-m-d H:i:s" ,strtotime($data['end_time'])) ;
        

        if($flag) {
            $routeType->driver_id   =  $data['driver_id'];
            $routeType->route_type  =  ROUTE_TYPE_HOME_TO_SCHOOL;
            $routeType->start_loc_lat       =  !empty($data['start_loc_lat']) ? $data['start_loc_lat'] : NULL; 
            $routeType->start_loc_long      =  !empty($data['start_loc_long']) ? $data['start_loc_long'] : NULL;
            $routeType->start_loc_address   =  !empty($data['start_loc_address']) ? $data['start_loc_address'] : NULL;
            $routeType->end_loc_lat     =  !empty($data['end_loc_lat']) ? $data['end_loc_lat'] : NULL;
            $routeType->end_loc_long    =  !empty($data['end_loc_long']) ? $data['end_loc_long'] : NULL;
            $routeType->end_loc_address =  !empty($data['end_loc_address']) ? $data['end_loc_address'] : NULL;
            
        } else {
            $routeType->driver_id   =  $data['sh_driver_id'];
            $routeType->route_type  =  ROUTE_TYPE_SCHOOL_TO_HOME;
            $routeType->start_loc_lat       =  !empty($data['end_loc_lat']) ? $data['end_loc_lat'] : NULL;
            $routeType->start_loc_long      =  !empty($data['end_loc_long']) ? $data['end_loc_long'] : NULL;
            $routeType->start_loc_address   =  !empty($data['end_loc_address']) ? $data['end_loc_address'] : NULL;
            $routeType->end_loc_lat     =  !empty($data['start_loc_lat']) ? $data['start_loc_lat'] : NULL; 
            $routeType->end_loc_long    =  !empty($data['start_loc_long']) ? $data['start_loc_long'] : NULL;
            $routeType->end_loc_address =  !empty($data['start_loc_address']) ? $data['start_loc_address'] : NULL;
        }

        $routeType->save();
    }

}
