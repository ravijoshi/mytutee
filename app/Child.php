<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User, App\RouteDetail, App\Payment, Auth;

class Child extends Model {

    use Notifiable,
        SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'childs';
    protected $primaryKey = 'child_id';

    
    protected $appends = ['parent_name', 'parent_mobile_no', "fullname", "subscription_upto", "subscribed", "is_free_trial"];
    protected $hidden = ['parent'];

    protected $fillable = [
        'child_fname', 'child_mname', 'child_lname', 'parent_id', 'school_id', 'driver_id', 'route_id', 'pickup_lat', 'pickup_long', 'pickup_address', 'child_status', 'free_trial_activate', 'activate_date', 'is_subscribed', 'subscription_days', 'subscribed_at', 'subscribed_upto'
    ];

    const FREE_TRIAL_STATUS_NOT_ACTIVATED = 0;
    const FREE_TRIAL_STATUS_ACTIVATED = 1;
    const FREE_TRIAL_STATUS_EXPIRED = 2;

    protected static function boot() {
        parent::boot();
        static::deleting(function($child) {
            if($child->isForceDeleting()) {
                $child->notifications()->forcedelete();
                $child->routeDetail()->update(['child_id'=> 0]);
            } else {
                $child->notifications()->delete();
                $child->routeDetail()->update(['child_id'=> 0]);
            }
        });

        self::restoring(function ($child) {
            $child->notifications()->restore();
        }); 
    }
    
    public function school() {
        return $this->hasOne('App\School', 'id', 'school_id');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'driver_id');
    }

    public function parent() {
        return $this->hasOne('App\User', 'id','parent_id')->select("*",\DB::raw("CONCAT(user_fname, ' ',user_lname) AS fullname"));
    }

    public function notifications() {
        return $this->belongsTo('App\NotificationDetail', 'child_id', 'child_id');
    }

    public function route() {
        return $this->hasOne('App\Route', 'route_id', 'route_id');
    }

    public function payment() {
        return $this->hasOne('App\Payment', 'child_id', 'child_id')->where(["type" => Payment::PAYMENT_TYPE_SBUSCRIBED, "payment_status" =>Payment::PAYMENT_STATUS_PAID])->latest();
    }

    public function driverHs() {
        return $this->hasOne('App\RouteType', 'route_id', 'route_id')->where("route_type",ROUTE_TYPE_HOME_TO_SCHOOL);
    }

    public function routeDetail() {
        return $this->hasOne('App\RouteDetail', 'child_id', 'child_id');
    }

    // This function append subscription flag value in response
    protected function getSubscribedAttribute(){
        $isSubscribed = IS_SUBSCRIBED_NO;
        
        if($this->free_trial_activate == IS_FREE_TRIL_ACTIVATED_YES){
            $isSubscribed = IS_SUBSCRIBED_YES;
        } else if($this->free_trial_activate == IS_FREE_TRIL_ACTIVATED_EXPIRED && $this->is_subscribed == IS_SUBSCRIBED_NO){
            $isSubscribed = IS_SUBSCRIBED_EXPIRED;
        } else if($this->is_subscribed == IS_SUBSCRIBED_YES){
            $isSubscribed = IS_SUBSCRIBED_YES;
        } else if($this->is_subscribed == IS_SUBSCRIBED_EXPIRED){
            $isSubscribed = IS_SUBSCRIBED_EXPIRED;
        }
        return $isSubscribed;
    }

    // This function append subscription upto value in response
    protected function getSubscriptionUptoAttribute(){
        $subscribedUpto = User::subDays(1);
        
        if($this->free_trial_activate == IS_FREE_TRIL_ACTIVATED_YES){
            $subscribedUpto = User::addDays($this->free_trial_days, $this->activate_date);
        } else if($this->free_trial_activate == IS_FREE_TRIL_ACTIVATED_EXPIRED && $this->is_subscribed == IS_SUBSCRIBED_NO){
            $subscribedUpto = User::addDays($this->free_trial_days, $this->activate_date);
        } else if($this->is_subscribed == IS_SUBSCRIBED_YES){
            $subscribedUpto = $this->subscribed_upto;
        } else if($this->is_subscribed == IS_SUBSCRIBED_EXPIRED){
            $subscribedUpto = $this->subscribed_upto;
        }

        return (int) !empty($subscribedUpto) ? strtotime($subscribedUpto) : 0;
    }

    // This function append is_free_trial boolean value.
    protected function getIsFreeTrialAttribute(){
        $flag =  FALSE; 
        if($this->free_trial_activate == IS_FREE_TRIL_ACTIVATED_YES){
            $flag = TRUE;
        }
        return $flag;
    }

    protected function getParentNameAttribute(){
        if(isset($this->parent->user_fname) && !empty($this->parent->user_fname))
            return $this->parent->user_fname ." ". $this->parent->user_lname;
        return NULL;
    }

    protected function getParentMobileNoAttribute(){
        if(isset($this->parent->user_phone) && !empty($this->parent->user_phone))
            return $this->parent->user_phone;
        return NULL;
    }

    protected function getFullnameAttribute(){
        return $this->child_fname ." ".$this->child_mname." ".$this->child_lname;
    }

    public function saveChild($childModel,  $routeModel , $request){
        $childModel->child_fname    = isset($request->fname) ? $request->fname : "";
        $childModel->child_mname    = isset($request->mname) ? $request->mname : "";
        $childModel->child_lname    = isset($request->lname) ? $request->lname : "";
        $childModel->pickup_lat     = isset($request->pickup_lat) ? $request->pickup_lat : "";
        $childModel->pickup_long    = isset($request->pickup_long) ? $request->pickup_long : "";
        $childModel->pickup_address = isset($request->pickup_address) ? $request->pickup_address : "";
        $childModel->school_id      = $routeModel->school_id;
        $childModel->driver_id      = 0;
        $childModel->parent_id      = $request->parent_id;
        $childModel->route_id       = $routeModel->route_id;
        $childModel->route_id       = $routeModel->route_id;
        
        if(isset($request->child_status) && $request->child_status)
            $childModel->child_status =  $request->child_status;
        
        $childModel->save();
        
        // update child id in route details.
        $updateRouteDetail = RouteDetail::where(['child_id' => $childModel->child_id])->update(['child_id'=> 0]);
        $routeDetail = RouteDetail::where(['child_id' => 0, "route_id" =>$routeModel->route_id  ])->first();
        if(isset($routeDetail->route_id) && $routeDetail->route_id){
            $routeDetail->child_id =  $childModel->child_id;
            $routeDetail->save();
        }

        return $childModel;
    }


    // send notification to driver
    public function childAddedOnRoute($childModel, $routeModel) {
        
        if(isset($routeModel->typeHs->driver) && $routeModel->typeHs->driver->userToken->count()) {
            $deviceToken = [];
            foreach ($routeModel->typeHs->driver->userToken as $userToken) {
                $deviceToken[] = $userToken->device_token;
            }
            // send notification if driver device token found.
            if(count($deviceToken)){
                $data = [
                    'msg_type' => CHILD_ADDED_ON_ROUTE,
                    'child_id' => strval($childModel->child_id),
                    'child_fname' => $childModel->child_fname,
                    'child_mname' => $childModel->child_mname,
                    'child_lname' => $childModel->child_lname,
                    'route_code'  => isset($routeModel->route_code) ? $routeModel->route_code : "",
                    'driver_name' => isset($routeModel->typeHs->driver->id) ? $routeModel->typeHs->driver->driver_name : "",
                ];

                $sendPushNotification = FcmNotification::sendPushNotification($deviceToken, $data);
                $notifications = new NotificationDetail;
                $notifications->driver_id = isset($routeModel->typeHs->driver->id) ? $routeModel->typeHs->driver->id : 0;
                $notifications->route_id  = $routeModel->route_id;
                $notifications->child_id  = $childModel->child_id;
                $notifications->parent_id = $childModel->parent_id;
                $notifications->trip_id = 0;
                $notifications->trip_nearby = 1;
                $notifications->trip_end = 1;
                
                if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                    $notifications->trip_start = 1; // notification sent
                } else {
                    $notifications->trip_start = 0; // notification not sent
                }
                $notifications->save();
            }
        }
    }
}
