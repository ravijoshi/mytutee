<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBank extends Model {

    use SoftDeletes;
    
    protected $table = 'user_bank';
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $appends = ['bank_name'];
    protected $hidden = ['details'];

    protected $fillable = [
        'user_id','bank_id', 'ifsc_code', 'bank_address','status'
    ];


    public function details() {
        return $this->hasOne("\App\Bank", "id", "bank_id")->select('id','name','status');
    }

    public function getBankNameAttribute(){
    	if(isset($this->details->name) && !empty($this->details->name))
    		return $this->details->name;

    	return null;
    }
}
