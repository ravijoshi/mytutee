<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SystemConfig extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'system_config';
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'name','access_key', 'value', 'type','status'
    ];

}
