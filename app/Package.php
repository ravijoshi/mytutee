<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model {

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'packege';
    protected $primaryKey = 'id';

    public $timestamps = true;
    protected $fillable = [
        'title', 'description', 'duration', 'price', 'status'
    ];

   protected static function boot() {
        parent::boot();

        static::deleting(function($package) {
            if ($package->isForceDeleting()) {
                // $package->scbscribedChildren()->forceDelete();
            } else {
                // $package->scbscribedChildren()->delete();
            }
        }); 

        /* self::restoring(function ($package) {
          $package->scbscribedChildren()->restore();
          }); */
    }

    public function scbscribedChildren() {
        return $this->hasMany('App\Child', 'package_id', 'id');
    }


    public function getPackages(){
       return Package::where("id" ,"!=", 1)->where("status", 1)->select('id','title','description','price', \DB::raw("IF(description IS NOT NULL, description, '') AS description"))->orderBy("duration")->get();
    }

    public static function getPackageOptions(){
       return Package::where("status", 1)->pluck('title','id');
    }
}
