<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\RouteDetail , App\RouteType;

class Route extends Model {

    use Notifiable,
        SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routes';
    protected $primaryKey = 'route_id';

    protected $fillable = [
        'route_code', 'type', 'school_id', 'end_loc_lat', 'end_loc_long', 'route_status'
    ];

    protected $appends = ['route_type'];

    protected $hidden = [
        'deleted_at'
    ];

    protected static function boot() {
        parent::boot();

        static::deleting(function($route) {
            if ($route->isForceDeleting()) {
                $route->details()->forceDelete();
                $route->type()->forceDelete();
            } else {
                $route->details()->delete();
                $route->type()->delete();
            }
        }); 

        /* self::restoring(function ($schools) {
          $schools->value()->restore();
          }); */
    }

    public static function getRouteCodeOptions(){
        $options =  Route::where("route_status", 1)->pluck("route_code","route_id");
        // $options[''] =  'Select Route Code';
        return $options;
    }

    // This function is used to get route school details.
    public function school() {
        return $this->hasOne('App\School', 'id', 'school_id');
    }

    // This function is used to get route driver details 
    public function user() {
        return $this->hasOne('App\User', 'id', 'driver_id');
    }

    // This function is used to get route child details 
    public function child() {
        return $this->hasMany('App\Child', 'route_id');
    }

    // This function is used to get route details 
    public function details() {
        return $this->hasOne('App\RouteDetail', 'route_id', 'route_id');
    }

    // This function is used to get total children in route 
    public function routeChildren() {
        return $this->hasMany('App\RouteDetail', 'route_id', 'route_id')->where("child_id", "!=", 0);
    }
    
    // This function is used to both (HS AND SH) route details
    public function type() {
        return $this->hasMany('App\RouteType', 'route_id', 'route_id');
    }

    // This function is used to Driver route details
    public function driverRoute() {
        return $this->hasOne('App\RouteType', 'route_id', 'route_id');
    }

    // This function is used to get school to home route details
    public function typeSh() {
        return $this->hasOne('App\RouteType', 'route_id', 'route_id')->where("route_type" , ROUTE_TYPE_SCHOOL_TO_HOME )->select("*", \DB::raw('UNIX_TIMESTAMP(start_time) as sh_start_time'),\DB::raw('UNIX_TIMESTAMP(end_time) as sh_end_time'));
    }

    // This function is used to get Home to school route details
    public function typeHs() {
        return $this->hasOne('App\RouteType', 'route_id', 'route_id')->where("route_type" , ROUTE_TYPE_HOME_TO_SCHOOL );
    }

    // This function is used to get last trip details
    public function trip() {
        return $this->hasOne('App\Trip', 'route_id', 'route_id')->latest();
    }

    public function getRouteTypeAttribute(){
        $routeType = TRIP_TYPE_ROUND;
        if($this->is_roud_trip == IS_ROUND_TRIP_NO){
            $routeType = TRIP_TYPE_HOME_SCHOOL;

            if(isset($this->driverRoute->route_type) && !empty($this->driverRoute->route_type)){
                if($this->driverRoute->route_type != ROUTE_TYPE_HOME_TO_SCHOOL)
                    $routeType = TRIP_TYPE_SCHOOL_HOME;
                else if(isset($this->typeHs->route_type) && !empty($this->typeHs->route_type)){
                    
                    if($this->typeHs->route_type != ROUTE_TYPE_HOME_TO_SCHOOL)
                        $routeType = TRIP_TYPE_SCHOOL_HOME;
                    
                } else if(isset($this->typeSh->route_type) && !empty($this->typeSh->route_type)){
                    
                    if($this->typeSh->route_type != ROUTE_TYPE_HOME_TO_SCHOOL)
                        $routeType = TRIP_TYPE_SCHOOL_HOME;
                }
            } else if(isset($this->typeHs->route_type) && !empty($this->typeHs->route_type)){
              
                if($this->typeHs->route_type != ROUTE_TYPE_HOME_TO_SCHOOL)
                    $routeType = TRIP_TYPE_SCHOOL_HOME;

            } else if(isset($this->typeSh->route_type) && !empty($this->typeSh->route_type)){
                
                if($this->typeSh->route_type != ROUTE_TYPE_HOME_TO_SCHOOL)
                    $routeType = TRIP_TYPE_SCHOOL_HOME;
            }
        }
        return $routeType;
    }
}
