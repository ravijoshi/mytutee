<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model {

    use SoftDeletes;
    
    protected $table = 'bank';
    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'name', 'status'
    ];
    


    public static function getBankOptions( $flag = true) {
        if(!$flag){
        	return Bank::select("id","name")->where("status", 1)->pluck("name", "id");
        }
        return Bank::select("id","name")->where("status", 1)->get();
    }
}
