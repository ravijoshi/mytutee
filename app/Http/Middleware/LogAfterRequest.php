<?php 
namespace App\Http\Middleware;
use Illuminate\Support\Facades\Log;
use Monolog\Logger, Carbon\Carbon;
use Monolog\Handler\StreamHandler;
use App\Log as RequestLog;

class LogAfterRequest {

	public function handle($request, \Closure  $next)
	{
		$this->log($request);
		return $next($request);
	}

	public function terminate($request, $response)
	{	
		$this->log($request, $response);
	}

	protected function log($request , $response = '') {
		
		$logModel = new RequestLog();

		$logModel->type =  RequestLog::LOG_TYPE_API_REQUEST;
		$logModel->url =  $request->fullUrl();
		$logModel->method =  $request->getMethod();
		$logModel->ip_address =  $request->getClientIp();
		$logModel->header =  json_encode($request->header());
		$logModel->request =  count($request->all()) ? json_encode($request->all()) : NULL;
		$logModel->request_time =  time();

		$logModel->additional_info =  json_encode(['Url' => $request->fullUrl(), 'Method' => $request->getMethod(), 'IP Address' => $request->getClientIp(),  'header' => $request->header(), 'request' => $request->all()]);

		if(!empty($response)) {
			$logModel->type =  RequestLog::LOG_TYPE_API_RESPONSE;
			$logModel->status_code =  $response->getStatusCode();
			$logModel->response =  (string)($response->getContent());
			$logModel->additional_info =  json_encode(['Url' => $request->fullUrl(), 'Method' => $request->getMethod(), 'IP Address' => $request->getClientIp(),  'header' => $request->header(), 'request' => $request->all(), 'Status Code' => $response->getStatusCode(),'response' => $response->getContent() ]);
		}
		$logModel->save();
	}

}