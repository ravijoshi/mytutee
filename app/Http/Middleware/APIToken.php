<?php
namespace App\Http\Middleware;
use Closure, Auth, Response,Exception;

class APIToken {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        try{
            if (Auth::guard("api")->check()) {
                if(Auth::guard("api")->user()->user_status != 1 ){
                    throw new Exception(ACCOUNT_STATUS_INACTIVE);
                }
                return $next($request);
            } else {
                return Response::json(["success" => false, "msg" => UNAUTHORIZE_ACCESS, "result" => (object) []]);
            }
        } catch (Exception $e) {
            $routes = \Route::getCurrentRoute()->getActionName();
            $routeWithMethod = explode("@",last(explode("\\",$routes)));
            $actionName = $routeWithMethod[1];
            $arr = ['driverdashboard','getRouteList','getSchoolList'];
            $result = [];
            if(!in_array($actionName, $arr)){
                $result = (object)[];
            }
            return Response::json(["success" => false, "msg" => $e->getMessage(), "result" => $result ]);
        }
    }
}
