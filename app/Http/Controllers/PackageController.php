<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Redirect;
use App\Package;

class PackageController extends Controller
{
    public function index() {
        return view('pages.package.index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'title',
            1 => 'price',
            2 => 'status',
            3 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        $select = Package::select("*", DB::raw('CONCAT("'.DEFAULT_CURRENCY_SYMBOL.' ",price) AS price'))->orderBy($order, $dir);
        
        if (!empty($search) ) {
            $select = $select->where(function($q)use($search) {
                $q->orWhere('title', 'LIKE', "%{$search}%");
                $q->orWhere('price', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $packages =  $select->offset($start)->limit($limit)->get();

        $data = array();
        if (!empty($packages)) {
            
            foreach ($packages as $_package) {
                $Editlink = route('package.edit', $_package->id);
                
                $nestedData['title'] = $_package->title;
                $nestedData['price'] = $_package->price ;
                
                $viewElement = $_package->id.',"'.$_package->title.'"';
                $nestedData['action'] ='';
                
                $disabled =  "disabled='disabled'";
                if($_package->id != 1){
                    $disabled =  "";
                    $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>  <button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";
                }

                if ($_package->status == "1") {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_package->id, this);' ".$disabled.">Active</button>";
                } else {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_package->id, this);' ".$disabled.">Inactive</button>";
                }

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function create() {
        return View('pages.package.create');
    }

    public function edit($id ="") {
    	if(empty($id) || !isset($id)) {
            return redirect()->route("package")->with("error","invalid Package id.");
        }

        $package = Package::find($id);
        if(empty($package))
        {
            return redirect()->route("package")->with("error","Package not found.");   
        }
        return view('pages.package.edit', compact('package'));
    }

    public function save(Request $request) {

        $id = $request->input('id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'title' => 'required',
                'duration' => 'required',
                'price' => 'required',
                'description' => 'nullable',
                'status' => 'required|in:0,1',
                'id' => 'required',
            ]);
        } else {
             $input = $request->validate([
                'title' => 'required',
                'duration' => 'required',
                'price' => 'required',
                'description' => 'nullable',
                'status' => 'required|in:0,1',
            ]);
         }
        
        if (isset($input['id']) && !empty($input['id'])) {

            $package = Package::find($input['id']);
            if(empty($package)) {
                return redirect()->route("package")->with("error","Package not found.");                
            }
            $message = "Package updated Sucessfully";
        } else {
            $package = new Package();
            $message = "Package added Sucessfully";
        }
        
        $package->title = $request->title;
        $package->duration = $request->duration;
        $package->price = $request->price;
        $package->description = $request->description;
        $package->status = $request->status;
        $package->save();

        return redirect()->route("package")->with("success",$message);
    }


    public function delete($id = "") {
        $message = "Package deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid package id.";
            $responseType = "failure";
        }
        $package = Package::find($id);
        if(empty($package)) {
            $message = "Package not found.";
            $responseType = "failure";
        } else{
            $package->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Package id.";
            $responseType = "failure";
        } else {
            $package = Package::find($id);

            if(empty($package)) {
                $message = "Package not found.";
                $responseType = "failure";
            } else {
                if ($package->status == 1) {
                    $package->status = 0;
                    $content = "Inactive";
                } else {
                    $package->status = 1;
                }
                $package->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" =>$content,
        ]);
    }
}
