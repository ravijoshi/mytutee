<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect;
use App\SystemConfig;

class SystemConfigController extends Controller
{
    public function index() {
        return View('pages/system_config/index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'name',
            1 => 'access_key',
            2 => 'access_key',
            3 => 'status',
            4 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = SystemConfig::orderBy($order, $dir);

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->where('name', 'LIKE', "%{$search}%");
                $q->orWhere('access_key', 'LIKE', "%{$search}%");
                $q->orWhere('value', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $systemConfig =  $select->offset($start)->limit($limit)->get();
        

        $data = array();
        if (!empty($systemConfig)) {
            
            foreach ($systemConfig as $_systemConfig) {
                $Editlink = route('system_config.edit', $_systemConfig->id);
                
                $nestedData['name'] = $_systemConfig->name;
                $nestedData['access_key'] = $_systemConfig->access_key;
                $nestedData['value'] = $_systemConfig->value;
                
                if ($_systemConfig->status == "1") {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_systemConfig->id,this);'>Active</button>";
                } else {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_systemConfig->id,this);'>Inactive</button>";
                }

                $viewElement = $_systemConfig->id.',"'.$_systemConfig->name.'"';
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>"; /*"<button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";*/
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create() {
        return View('pages/system_config/create');
    }

    public function edit($id ="") {
    	if(empty($id) || !isset($id)) {
            return redirect()->route("system_config")->with("error","invalid System Config id.");
        }

        $system_config = SystemConfig::find($id);
        if(empty($system_config))
        {
            return redirect()->route("system_config")->with("error","System Config not found.");   
        }
        return view('pages.system_config.edit', compact('system_config'));
    }

    public function save(Request $request) {

        $id = $request->input('id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'name' => 'required',
                'access_key' => 'required|unique:system_config,access_key,'.$id.',id,deleted_at,NULL',
                'value' => 'required',
                'status' => 'required|in:0,1',
                'id' => 'required',
            ]);
        } else {
             $input = $request->validate([
                'name' => 'required',
                'access_key' => 'required|unique:system_config,access_key,null,id,deleted_at,NULL',
                'value' => 'required',
                'status' => 'required|in:0,1',
            ]);
         }

        if (isset($input['id']) && !empty($input['id'])) {

            $system_config = SystemConfig::find($input['id']);
            if(empty($system_config)) {
                return redirect()->route("system_config")->with("error","System Config not found.");                
            }
            $message = "System Config updated Sucessfully";
        } else {
            $system_config = new SystemConfig();
            $message = "System Config added Sucessfully";
        }
        
        $system_config->access_key = $input["access_key"];
        $system_config->name =  $input["name"];
        $system_config->value = $input["value"];
        $system_config->status = $input["status"];
        $system_config->save();

        return redirect()->route("system_config")->with("success",$message);
    }


    public function delete($id = "") {
        $message = "System Config deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid system config id.";
            $responseType = "failure";
        }

        $system_config = SystemConfig::find($id);
        
        if(empty($system_config))
        {
            $message = "System Config not found.";
            $responseType = "failure";
        } else{

            $system_config->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid System Config id.";
            $responseType = "failure";
        } else {
            $system_config = SystemConfig::find($id);

            if(empty($system_config)) {
                $message = "System Config not found.";
                $responseType = "failure";
            } else {
                if ($system_config->status == 1) {
                    $system_config->status = 0;
                    $content =  "Inactive";
                } else {
                    $system_config->status = 1;
                }
                $system_config->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" => $content
        ]);
    }
    
}
