<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator, Hash, Redirect, DB;
use App\User, App\Child, App\Route, App\RouteDetail;

class ChildrenController extends BaseController {


    public function index($parentId = '') {
        if(empty($parentId)){
            return redirect()->route('parents')->with('error',"Invalid parent id.");
        }
        return view('pages/children/index',["parentId"=>$parentId]);
    }

    public function getChildrenList($id = '', Request $request) {
        $columns = array(
            0 => 'name',
            1 => 'parent_name',
            2 => 'route_code',
            3 => 'pickup_address',
            4 => 'child_status',
            5 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = Child::/*withTrashed()->*/where('parent_id',$id)->orderBy($order, $dir)
                ->select("*",
                DB::raw('CONCAT(child_fname," " ,child_lname) AS name'), 
                DB::raw("IF(childs.deleted_at IS NOT NULL,1,0) as hard_delete"),
                DB::raw('CONCAT(U.user_fname," ",U.user_lname) AS parent_name'),
                "R.route_code"
            );

        $select->leftJoin('users AS U', function($join)
        {
            $join->on('childs.parent_id', '=', 'U.id');
            $join->where('U.user_role', '=', User::USER_ROLE_PARENT);
        });

        $select->leftJoin('routes as R', 'R.route_id', '=', 'childs.route_id');
        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->orWhere(DB::raw('CONCAT(child_fname," " ,child_lname)'), 'LIKE', "%{$search}%");
                $q->orWhere(DB::raw('CONCAT(U.user_fname," ",U.user_lname)'), 'LIKE', "%{$search}%");
                $q->orWhere('pickup_address', 'LIKE', "%{$search}%");
                $q->orWhere('R.route_code', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $childrens =  $select->offset($start)->limit($limit)->get();

        $data = array();
        if (!empty($childrens)) {
            
            foreach ($childrens as $_children) {
                $Editlink = route('children.edit', $_children->child_id);
                
                $nestedData['name'] = $_children->name;
                $nestedData['parent_name'] = $_children->parent_name;
                $nestedData['route_code'] =  "<a title='View Route' href='".route("holisticview")."?view=route&id=".$_children->route_id."'> ".$_children->route_code." </a>";
                $nestedData['pickup_address'] = $_children->pickup_address;
                
                if ($_children->child_status == "1") {
                    $nestedData['child_status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_children->child_id, this);'>Active</button>";
                } else {
                    $nestedData['child_status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_children->child_id, this);'>Inactive</button>";
                }

                $viewElement = $_children->child_id.',"'.$_children->name.'"';
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>";
               /* if($_children->hard_delete == 1){
                    $nestedData['action'] .= " <button type='button' class='btn btn-warning btn-sm' title='Hard Delete' onclick='hardDelConfirmation(" . $viewElement . ");'> <i class='fa fa-trash'></i></button>";
                } else {*/
                    $nestedData['action'] .= " <button type='button' class='btn btn-danger btn-sm' title='Delete' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";
                // }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function edit($id = "") {
        if(empty($id) || !isset($id)) {
            return redirect()->route("children")->with("error","invalid children id.");
        }

        $children = Child::where(["child_id" => $id ])->first();
        if(empty($children)) {
            return redirect()->route("children")->with("error","children not found.");   
        }
        return view('pages.children.edit', compact('children'));
    }

    public function delete($id = "") {
        $message = "children deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid children id.";
            $responseType = "failure";
        }

        $childrenUser = Child::where(["child_id" => $id ])->first();
        
        if(empty($childrenUser)) {
            $message = "children not found.";
            $responseType = "failure";
        } else {
            $childrenUser->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function forcedelete($id = "") {
        $message = "children deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid children id.";
            $responseType = "failure";
        }

        $childrenUser = Child::withTrashed()->where(["child_id" => $id ])->first();
        
        if(empty($childrenUser)) {
            $message = "children not found.";
            $responseType = "failure";
        } else{
            $childrenUser->forcedelete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function create($parentId = '') {
        if(empty($parentId)){
            return redirect()->route('parents')->with('error',"Invalid parent id.");
        }
        return view('pages/children/create',['parentId' => $parentId]);
    }

    public function save(Request $request) {
        $validationMessage =  ["child_fname.required" => "The first name field is required.","child_mname.required" => "The middle name field is required.","child_lname.required" => "The last name field is required."];
        $id = $request->input('child_id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'child_fname' => 'required',
                'child_mname' => 'required',
                'child_lname' => 'required',
                'parent_id' => 'required',
                'school_id' => 'required',
                'route_id' => 'required',
                'pickup_address' => 'required',
                'pickup_lat' => 'required',
                'pickup_long' => 'required',
                'child_status' => 'required|in:0,1',
                'child_id' => 'required',
            ],$validationMessage);
        } else {
             $input = $request->validate([
                'child_fname' => 'required',
                'child_mname' => 'required',
                'child_lname' => 'required',
                'parent_id' => 'required',
                'school_id' => 'required',
                'route_id' => 'required',
                'pickup_address' => 'required',
                'pickup_lat' => 'required',
                'pickup_long' => 'required',
                'child_status' => 'required|in:0,1',
            ],$validationMessage);
         }

        $routeExist = Route::where('route_id', strtoupper($request->route_id))->first();
        $isRouteUpdated =  false;
        if (isset($input['child_id']) && !empty($input['child_id'])) {

            $childrenUser = Child::find($input['child_id']);
            if(empty($childrenUser)) {
                return redirect()->route("childrens")->with("error","children details not found.");                
            }
            if($childrenUser->route_id !=  $input['route_id']){
                // check route have remain capacity.
                $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" =>$input["route_id"]  ])->get();
                if(empty($routeCapacity) && !$routeCapacity->count()){
                    return back()->with("error","The route code doesn't have more space available.");
                }
                $isRouteUpdated =  true;
            }
            $message = "children details updated Sucessfully";
        } else {
            $childrenUser = new Child();
            $message = "children details added Sucessfully";
            // check route have remain capacity.
            $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" =>$input["route_id"]  ])->get();
            if(empty($routeCapacity) && !$routeCapacity->count()){
                return back()->with("error","The route code doesn't have more space available.");
            }
            $isRouteUpdated =  true;
        }
        
        $childrenUser->parent_id    = $input["parent_id"];
        $childrenUser->school_id    = $input["school_id"];
        $childrenUser->route_id     = $input["route_id"];
        $childrenUser->child_fname  = $input["child_fname"];
        $childrenUser->child_mname  = $input["child_mname"];
        $childrenUser->child_lname  = $input["child_lname"];
        $childrenUser->pickup_address = $input["pickup_address"];
        $childrenUser->pickup_lat   = $input["pickup_lat"];
        $childrenUser->pickup_long  = $input["pickup_long"];
        $childrenUser->child_status = $input["child_status"];
        $childrenUser->save();

        // update child id in route details.
        $updateRouteDetail = RouteDetail::where(['child_id' => $childrenUser->child_id])->update(['child_id'=> 0]);
        $routeDetail = RouteDetail::where(['child_id' => 0, "route_id" =>$input["route_id"]  ])->first();
        if(isset($routeDetail->route_id) && $routeDetail->route_id){
            $routeDetail->child_id =  $childrenUser->child_id;
            $routeDetail->save();
        }

        if($isRouteUpdated) {
            // send notification to driver.
            $childrenUser->childAddedOnRoute($childrenUser, $routeExist);
        }
        return redirect()->route("children",$childrenUser->parent_id)->with("success",$message);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content =  "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid children user id.";
            $responseType = "failure";
        } else {
            $childrenUser = Child::withTrashed()->find($id);

            if(empty($childrenUser)) {
                $message = "children user not found.";
                $responseType = "failure";
            } else {
                if ($childrenUser->child_status == "1") {
                    $childrenUser->child_status = "0";
                    $content = "Inactive";
                } else {
                    $childrenUser->child_status = "1";
                }
                $childrenUser->save();
            }

        }
        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" => $content
        ]);
    }

}