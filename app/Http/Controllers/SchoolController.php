<?php
namespace App\Http\Controllers;

// if(in_array(Route::currentRouteName(),['import','export','school.search.list']))
ini_set('max_execution_time', 300);

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Validator, Auth, Hash, Redirect, DB, Excel;
use App\School;

class SchoolController extends BaseController {

   protected $_flag = true;

    public function schoolList() {
        return View('pages/school/index');
    }

    public function getSchoolList(Request $request) {
        $columns = array(
            0 => 'school_name',
            1 => 'school_phone',
            2 => 'school_address',
            3 => 'status',
            4 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = School::orderBy($order, $dir);

        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->orWhere('school_name', 'LIKE', "%{$search}%");
                $q->orWhere('school_phone', 'LIKE', "%{$search}%");
                $q->orWhere('school_address', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $schools =  $select->offset($start)->limit($limit)->get();
        
        $data = array();
        if (!empty($schools)) {
            
            foreach ($schools as $_school) {
                $Editlink = route('schools/edit', $_school->id);
                
                $nestedData['school_name'] = "<a  title='View School' href='".route("holisticview")."?view=school&id=".$_school->id."'> ".$_school->school_name." </a>";
                $nestedData['school_phone'] = $_school->school_phone;
                $nestedData['school_address'] = $_school->school_address;
                if ($_school->school_status == 1) {
                    $nestedData['school_status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_school->id,this);'>Active</button>";
                } else {
                    $nestedData['school_status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_school->id,this);'>Inactive</button>";
                }
                $viewElement = $_school->id.',"'.$_school->school_name.'"';
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a> <button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->json($json_data);
    }

    public function edit($id = "") {
        if(empty($id) || !isset($id)) {
            return redirect()->route("schools")->with("error","invalid school id.");
        }

        $schooldetail = School::find($id);
        if(empty($schooldetail)) {
            return redirect()->route("schools")->with("error","School not found.");   
        }
        return view('pages.school.edit', compact('schooldetail'));
    }

    public function delete($id = "") {
        $message = "School deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid school id.";
            $responseType = "failure";
        }

        $schooldetail = School::find($id);
        
        if(empty($schooldetail))
        {
            $message = "School not found.";
            $responseType = "failure";
        } else{

            $schooldetail->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function create() {
        return view('pages/school/create');
    }

    public function addschool(Request $request) {

        $input = $request->validate([
            'school_name' => 'required',
            'school_phone' => 'required',
            'school_address' => 'required',
            'school_status' => 'required|in:0,1',
            'id' => 'nullable',
        ]);

        if (isset($input['id']) && !empty($input['id'])) {

            $schoolModel = School::find($input['id']);
            if(empty($schoolModel))
            {
                return redirect()->route("schools")->with("error","School not found.");                
            }
            $message = "School updated Sucessfully.";
        } else {
            $schoolModel = new School();
            $message = "School added Sucessfully.";
        }

        $latlng =  $schoolModel->getLatLong($input["school_address"]);
            
        $schoolModel->school_name = $input["school_name"];
        $schoolModel->school_phone = $input["school_phone"];
        $schoolModel->school_address = $input["school_address"];
        $schoolModel->school_status = $input["school_status"];
        
        $schoolModel->school_latitude = !empty($latlng["lat"]) ? $latlng["lat"] : NULL;
        $schoolModel->school_longitude = !empty($latlng["long"]) ? $latlng["long"] : NULL;

        $schoolModel->save();
        return redirect()->route("schools")->with("success",$message);
    }

    public function ChangeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";

        if(empty($id) || !isset($id)) {
            $message = "Invalid school id.";
            $responseType = "failure";
        } else {
            $schoolModel = School::find($id);
            if(empty($schoolModel)) {
                $message = "School not found.";
                $responseType = "failure";
            } else {
                if ($schoolModel->school_status == 1) {
                    $schoolModel->school_status = 0;
                    $content = "Inactive";
                } else {
                    $schoolModel->school_status = 1;
                }
                $schoolModel->save();
            }
        }
        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content"  => $content
        ]);
    }

    public function importForm() {
        return view('pages/school/import');
    }

    // this function is used to import school
    public function import(Request $request) {
        $request->validate([
            'import_file' => 'required'
        ]);
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();
        $fileHeaders = $data->getHeading();
        $dbColumn = ['name','address','phone_no'];
        if(!array_diff($dbColumn, $fileHeaders)){
            Excel::filter('chunk')->load($path)->chunk(50, function($results)
            {
                if($results->count()){
                    $schoolModel = new School();
                    foreach ($results as $key => $value) {
                            if(isset($value->lat) && isset($value->long) && !empty($value->lat) && !empty($value->long))
                                $latlng =  ["lat" =>  $value->lat, "long" =>  $value->long];
                            else
                                $latlng =  $schoolModel->getLatLong($value->address);

                            $postData[] = [
                                'school_name' => $value->name, 
                                'school_address' => $value->address,
                                'school_phone' => !empty($value->phone_no) ? $value->phone_no : "",
                                'school_status' => 1,
                                'school_latitude' => !empty($latlng["lat"]) ? $latlng["lat"] : NULL,
                                'school_longitude' => !empty($latlng["long"]) ? $latlng["long"] : NULL,
                            ];
                    }
         
                    if(!empty($postData)){
                        School::insert($postData);
                    }
                }
            });

        return redirect()->route("schools")->with("success", "School imported Sucessfully.");
        
        } else {
            return back()->with('error', 'Invalid column.');
        }
    }

    // this function is used to export school
    public function export() {
        $data = School::select(
                            DB::raw("school_name as Name"), 
                            DB::raw("school_phone as Phone Number"), 
                            DB::raw("school_address as Address"),
                            DB::raw("school_latitude as Latitude"),
                            DB::raw("school_longitude as Longitude"),
                            DB::raw(" IF(school_status = 1 , 'Active', 'Inactive') AS Status")
                        )->get()->toArray();
        
        $type = "xlsx";
        return Excel::create('schools', function($excel) use ($data) {
            $excel->sheet('school', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
        return redirect()->route("schools")->with("success","School export Sucessfully.");
    }

    // this function is used to export school from google
    public function exportSearch(Request $request) {
        $type = "xlsx";
        $data =  session()->get('schoolList');
        return Excel::create('school', function($excel) use ($data) {
            $excel->sheet('school', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
        return redirect()->route("schools")->with("success","School export Sucessfully.");
    }

    // This function is used to display google search form
    public function search() {
        return view('pages/school/search');
    }

    public function getSearchList(Request $request) {
        $responseType =  "success";
        $message =  "School list found successfully.";
        $schoolList = [];
        $flag =  0;
        if(!isset($request->address) || empty($request->address)){
            $responseType =  "success";
            $message =  "School list found successfully.";
            /*if(session()->has('schoolList')) {
                $schoolList =  session()->get('schoolList');
                if(count($schoolList)) {
                    $flag =  1;
                } 
            }*/
        } else {
            $schoolModel = new School();
            $schoolList =  $schoolModel->getSchoolList($request->address);
            if(count($schoolList)) {
                $flag =  1;
            } 
            session()->forget('schoolList');
            session()->put('schoolList', $schoolList);
        }

        return response()->json([
            "responseType" => $responseType,
            "message" => $message,
            "isExport"  => $flag,
            "dataSet"  => $schoolList,
        ]);
    }
}