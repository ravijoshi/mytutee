<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Route, App\SystemConfig, App\User, App\Child, App\Trip, App\TripDetail, App\NotificationDetail, App\FcmNotification;
use Response, Auth,Validator, Exception;
use Illuminate\Support\Facades\DB;

class TripController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Start Trip API
     */
    public function startTrip(Request $request) {
        $isSuccess = true;
        $message = "Trip Started";
        $ArrResponse = (object) [];
        try{
            $rules = [
                'route_id'  => 'required',
                'trip_type' => 'required',
                'lat'       => 'required|gt:0.99',
                'long'      => 'required|gt:0.99',
                'time'      => 'required',
                'accuracy'  => 'required',
                'bearing'   => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message =  "It seems like your location is disabled, retry after enabling it.";
                $isSuccess = false;
            } else {
                
                $amCheckRouteExist = Route::find($request->route_id);
                if (isset($amCheckRouteExist->route_id) && !empty($amCheckRouteExist->route_id)) {
                    $amGetCurrentUser = Auth::guard("api")->user();
                    $amCheckTripIsOngoing = Trip::where(['route_id' => $amCheckRouteExist->route_id, 'trip_status' => 0])->first();
                    
                    // return false if already any trip start for the given route code.
                    if (isset($amCheckTripIsOngoing->trip_id) && $amCheckTripIsOngoing->trip_id) {
                        $message =  "Trip is ongoing for the route (".$amCheckRouteExist->route_code.").";
                        $isSuccess = false;
                    } else {

                        $trips = new Trip;
                        $trips->trip_type =  TRIP_TYPE_HOME_SCHOOL;
                        
                        if(isset($request->trip_type) &&  $request->trip_type &&  $request->trip_type == TRIP_TYPE_SCHOOL_HOME){
                            $trips->trip_type =  TRIP_TYPE_SCHOOL_HOME;
                        }
                        
                        $trips->driver_id = $amGetCurrentUser->id;
                        $trips->route_id = $request->route_id;
                        $trips->start_time = now();

                        if ($trips->save()) {
                            $tripDetails = new TripDetail;
                            $tripDetails->trip_id = $trips->trip_id;
                            if ($request->lat == '0.0' || $request->long == '0.0') {
                                $tripDetails->trip_lat = $amCheckRouteExist->start_loc_lat;
                                $tripDetails->trip_long = $amCheckRouteExist->start_loc_long;
                                $tripDetails->trip_time = strtotime($trips->created_at);
                            } else {
                                $tripDetails->trip_lat = $request->lat ? $request->lat : 0.0;
                                $tripDetails->trip_long = $request->long ? $request->long : 0.0;
                                $tripDetails->trip_time = $request->time;
                            }
                            $tripDetails->save();

                            // save trip distance to school start
                            $currentTripModle = Trip::where('trip_id', $trips->trip_id)->with(
                                [
                                    'route' => function ($select) {
                                        $select->with(['typeHs','typeSh']);
                                }]
                            )->first();

                            // calaculate distance using Distance Matrix API
                            $currentLatLong = $tripDetails->trip_lat.",".$tripDetails->trip_long;
                            $destinationLatLong = "0.0,0.0";

                            if($trips->trip_type == TRIP_TYPE_HOME_SCHOOL && isset($currentTripModle->route->typeHs->end_loc_lat) && $currentTripModle->route->typeHs->end_loc_lat && isset($currentTripModle->route->typeHs->end_loc_long) && $currentTripModle->route->typeHs->end_loc_long) {

                                $destinationLatLong = $currentTripModle->route->typeHs->end_loc_lat.",".$currentTripModle->route->typeHs->end_loc_long;

                            } else if ($trips->trip_type == TRIP_TYPE_SCHOOL_HOME && isset($currentTripModle->route->typeSh->end_loc_lat) && $currentTripModle->route->typeSh->end_loc_lat && isset($currentTripModle->route->typeSh->end_loc_long) && $currentTripModle->route->typeSh->end_loc_long) {
                                 $destinationLatLong = $currentTripModle->route->typeSh->end_loc_lat.",".$currentTripModle->route->typeSh->end_loc_long;
                            }
                            
                            $distanceData =  $tripDetails->calculateDistance($currentLatLong, $destinationLatLong);

                            $tripDetails->time_to_reach_school =  $distanceData['distance_value'];
                            $tripDetails->save();
                            // calaculate distance using Distance Matrix API
                            // save trip distance to school end

                            $amGetChildLists = Child::where('route_id', $amCheckRouteExist->route_id)->with(['route', 'parent' => function ($query) {
                                            $query->with('userToken');
                                        }])->get();
                            if ($amGetChildLists->count()) {
                                
                                foreach ($amGetChildLists as $key => $amGetChildList) {
                                    $deviceTokens = [];
                                    if (isset($amGetChildList->parent->UserToken) && $amGetChildList->parent->UserToken->count()) {
                                        foreach ($amGetChildList->parent->UserToken as $key => $data) {
                                            if ( isset($data->device_token) && $data->device_token != '')
                                                $deviceTokens[] = $data->device_token;
                                        }
                                    }

                                    $data = [
                                        'msg_type' => TRIP_STARTED,
                                        'msg_text' => 'Trip Started',
                                        'child_id' => strval($amGetChildList->child_id),
                                        'child_fname' => $amGetChildList->child_fname,
                                        'child_mname' => $amGetChildList->child_mname,
                                        'child_lname' => $amGetChildList->child_lname,
                                        'pickup_lat' => floatval($amGetChildList->pickup_lat),
                                        'pickup_long' => floatval($amGetChildList->pickup_long),
                                        'route_code' => $amGetChildList->route->route_code,
                                    ];
                                    $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                                    $notifications = new NotificationDetail;
                                    $notifications->driver_id = $amGetCurrentUser->id;
                                    $notifications->route_id = $amGetChildList->route->route_id;
                                    $notifications->child_id = $amGetChildList->child_id;
                                    $notifications->parent_id = $amGetChildList->parent->id;
                                    $notifications->trip_id = $trips->trip_id;
                                    $notifications->notification_data = json_encode($data);
                                    
                                    if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                                        $notifications->trip_start = 1; // notification sent
                                    } else {
                                        $notifications->trip_start = 0; // notification not sent
                                        $notifications->fcm_response = json_encode($sendPushNotification);
                                    }
                                    $notifications->save();
                                }
                            }
                            $ArrResponse->trip_id = $trips->trip_id;
                        } else {
                            $isSuccess = false;
                            $message = "Something went wrong";
                        }
                    }
                } else {
                    $isSuccess = false;
                    $message = ROUTE_NOT_FOUND;
                }
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * End Trip API
     */
    public function endTrip(Request $request) {

        $isSuccess = false;
        $message = TRIP_NOT_FOUND;
        $ArrResponse = (object) [];
        try{
            $trips = Trip::where('trip_id', $request->trip_id)->first();
            if (isset($trips->trip_id) && !empty($trips->trip_id)) {
                $isSuccess = true;
                $message = "Trip Ended";
                if($trips->trip_status != 1) {
                    $trips->end_time = now();
                    $trips->trip_status = 1;
                    $trips->save();
                    
                    // update all offline data remain at the time of end trip
                    if(isset($request->data) && count($request->data)) {
                        foreach ($request->data as $_data) {
                            $lastTripDetails = TripDetail::where('trip_id', $trips->trip_id)->where('trip_time', $_data['time'])->first();
                            if(empty($lastTripDetails))
                            {
                                $modelTripDetail = new TripDetail;
                                $modelTripDetail->trip_id = $trips->trip_id;
                                $modelTripDetail->trip_lat = (float) $_data['lat'];
                                $modelTripDetail->trip_long = (float) $_data['long'];
                                $modelTripDetail->trip_time = $_data['time'];
                                $modelTripDetail->trip_accuracy = $_data['accuracy'];
                                $modelTripDetail->trip_bearing = $_data['bearing'];
                                $modelTripDetail->save();
                            }
                        }
                    }
                    // save trip distance to school start
                    $currentTripModle = Trip::where('trip_id', $trips->trip_id)->with(
                                        [
                                            'route' => function ($select) {
                                                $select->with(['typeHs','typeSh']);
                                        }]
                                    )->first();

                    // calaculate distance using Distance Matrix API
                    $tripDetails = TripDetail::where('trip_id', $trips->trip_id)->latest()->first();
                    $currentLatLong = $tripDetails->trip_lat.",".$tripDetails->trip_long;
                    $destinationLatLong = "0.0,0.0";

                    if($currentTripModle->trip_type == TRIP_TYPE_HOME_SCHOOL && isset($currentTripModle->route->typeHs->end_loc_lat) && $currentTripModle->route->typeHs->end_loc_lat && isset($currentTripModle->route->typeHs->end_loc_long) && $currentTripModle->route->typeHs->end_loc_long) {
                        $destinationLatLong = $currentTripModle->route->typeHs->end_loc_lat.",".$currentTripModle->route->typeHs->end_loc_long;
                    } else if($currentTripModle->trip_type == TRIP_TYPE_SCHOOL_HOME && isset($currentTripModle->route->typeSh->end_loc_lat) && $currentTripModle->route->typeSh->end_loc_lat && isset($currentTripModle->route->typeSh->end_loc_long) && $currentTripModle->route->typeSh->end_loc_long) {
                         $destinationLatLong = $currentTripModle->route->typeSh->end_loc_lat.",".$currentTripModle->route->typeSh->end_loc_long;
                    }
                   
                    $distanceData =  $tripDetails->calculateDistance($currentLatLong, $destinationLatLong);
                    $tripDetails->time_to_reach_school =  $distanceData['distance_value'];
                    $tripDetails->save();
                    // calaculate distance using Distance Matrix API
                    // save trip distance to school end
                    if (isset($trips->trip_id) && !empty($trips->trip_id)) {
                        $ArrResponse->trip_id = $trips->trip_id;

                        $amGetChildLists = Child::where('route_id', $trips->route_id)->with(['route', 'parent' => function ($query) {
                                        $query->with('userToken');
                                    }])->get();
                        if ($amGetChildLists->count()) {
                            foreach ($amGetChildLists as $key => $amGetChildList) {
                                $deviceTokens = [];
                                if (!empty($amGetChildList->parent) && !empty($amGetChildList->parent->UserToken) && $amGetChildList->parent->UserToken->count()) {
                                    foreach ($amGetChildList->parent->UserToken as $key => $token) {
                                        if ($token->device_token != '')
                                            $deviceTokens[] = $token->device_token;
                                    }
                                }
                                $data = [
                                    'msg_type' => TRIP_ENDED,
                                    'msg_text' => 'Trip Ended',
                                    'child_id' => strval($amGetChildList->child_id),
                                    'child_fname' => $amGetChildList->child_fname,
                                    'child_mname' => $amGetChildList->child_mname,
                                    'child_lname' => $amGetChildList->child_lname,
                                    'ended_by' => "Driver",
                                    'pickup_lat' => floatval($amGetChildList->pickup_lat),
                                    'pickup_long' => floatval($amGetChildList->pickup_long),
                                    'route_code' => $amGetChildList->route->route_code,
                                ];
                                $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);

                                if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                                    $tripEndFlag = 1;
                                } else {
                                    $tripEndFlag = 0;
                                }
                                $updatedNotification = NotificationDetail::where( ['child_id' => $amGetChildList->child_id, 'trip_id' => $trips->trip_id])->first();
                                
                                if (isset($updatedNotification->id) && !empty($updatedNotification->id)) {
                                    $updatedNotification->trip_end = $tripEndFlag;
                                    $updatedNotification->notification_data = $updatedNotification->notification_data." End trip DATA :".json_encode($data);

                                    if(!$tripEndFlag) {
                                        if(!empty($updatedNotification->fcm_response)){
                                            $updatedNotification->fcm_response = $updatedNotification->fcm_response. " End trip = ". json_encode($sendPushNotification);
                                        } else {
                                            $updatedNotification->fcm_response = json_encode($sendPushNotification);
                                        }
                                    }
                                    $updatedNotification->save();
                                }
                            }
                        }

                    } else {
                        $isSuccess = false;
                        $message = "Something went wrong";
                    }
                }
            } else {
                $isSuccess = false;
                $message = TRIP_NOT_FOUND;
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Update Trip Location API
     */
    public function updateTripLocations(Request $request) {
        $isSuccess = true;
        $message = "";
        $result =  [];
        try {
            $tripId = $request->trip_id;
            $tripDetails = $request->data;

            if (count($tripDetails)) {
                $amGetTripDetails = Trip::where('trip_id', $tripId)->with(
                        [
                            'route' => function ($select) {
                                $select->with(['typeHs','typeSh']);
                            }/*,
                            'routeDetail' => function($select){
                                $select->with([
                                    'child' => function ($select) {
                                        $select->with([
                                            'parent' => function ($query) {
                                                $query->with('userToken');
                                            }
                                        ]);
                                    }
                                ]);
                            }*/
                        ]
                    )->first();

                if (isset($amGetTripDetails->trip_id) && !empty($amGetTripDetails->trip_id)) {
                    if($amGetTripDetails->trip_status == 1 )
                        throw new Exception("TRIP_ENDED");
                        
                    foreach ($tripDetails as $tripDetail) {
                        $amGetLastTripDetails = TripDetail::where('trip_id', $tripId)->where('trip_time', $tripDetail['time'])->first();
                        if (empty($amGetLastTripDetails)) {
                            $modelTripDetail = new TripDetail;
                            $modelTripDetail->trip_id = $tripId;
                            $modelTripDetail->trip_lat = floatval($tripDetail['lat']);
                            $modelTripDetail->trip_long = floatval($tripDetail['long']);
                            $modelTripDetail->trip_time = $tripDetail['time'];
                            $modelTripDetail->trip_accuracy = $tripDetail['accuracy'];
                            $modelTripDetail->trip_bearing = $tripDetail['bearing'];
                            $tripDetailsSave = $modelTripDetail->save();
                        }
                    }
                    $getLastTrackingLocation = TripDetail::where('trip_id', $tripId)->latest()->first();

                    if (isset($getLastTrackingLocation->trip_id) && $getLastTrackingLocation->trip_id) {

                        // calaculate distance using Distance Matrix API
                        $currentLatLong = $getLastTrackingLocation->trip_lat.",".$getLastTrackingLocation->trip_long;
                        $destinationLatLong = "0.0,0.0";


                        if($amGetTripDetails->trip_type == TRIP_TYPE_HOME_SCHOOL && isset($amGetTripDetails->route->typeHs->end_loc_lat) && $amGetTripDetails->route->typeHs->end_loc_lat && isset($amGetTripDetails->route->typeHs->end_loc_long) && $amGetTripDetails->route->typeHs->end_loc_long) {

                            $destinationLatLong = $amGetTripDetails->route->typeHs->end_loc_lat.",".$amGetTripDetails->route->typeHs->end_loc_long;

                        }else if($amGetTripDetails->trip_type == TRIP_TYPE_SCHOOL_HOME && isset($amGetTripDetails->route->typeSh->end_loc_lat) && $amGetTripDetails->route->typeSh->end_loc_lat && isset($amGetTripDetails->route->typeSh->end_loc_long) && $amGetTripDetails->route->typeSh->end_loc_long){
                             $destinationLatLong = $amGetTripDetails->route->typeSh->end_loc_lat.",".$amGetTripDetails->route->typeSh->end_loc_long;
                        }

                        $distanceData =  $getLastTrackingLocation->calculateDistance($currentLatLong, $destinationLatLong);
                        $getLastTrackingLocation->time_to_reach_school =  $distanceData['distance_value'];
                        $getLastTrackingLocation->save();
                        // calaculate distance using Distance Matrix API

                        // check vehicle near school location start
                        $tripModel =  Trip::where('trip_id', $amGetTripDetails->trip_id)->first();
                        if($amGetTripDetails->is_nearby_school == IS_NEARBY_SCHOOL_NO && isset($amGetTripDetails->route->school->school_latitude) && !empty($amGetTripDetails->route->school->school_latitude)) {

                            $nearBySchool =  SystemConfig::where("access_key", "VEHICLE_NEARBY_SCHOOL")->first();

                            $distance = $tripModel->findVehicleIsNearBy($amGetTripDetails->route->school->school_latitude, $amGetTripDetails->route->school->school_longitude, $getLastTrackingLocation->trip_lat, $getLastTrackingLocation->trip_long);

                            $defaultSchoolDistance = VEHICLE_NEARBY_SCHOOL;

                            if(!empty($nearBySchool) && isset($nearBySchool->id) ){
                                $defaultSchoolDistance = (int) $nearBySchool->value ? $nearBySchool->value : VEHICLE_NEARBY_SCHOOL;
                            }

                            if ($distance <= $defaultSchoolDistance) {
                                $tripModel->is_nearby_school = IS_NEARBY_SCHOOL_YES;
                                $tripModel->nearby_school_at = date("Y-m-d H:i:s");
                                $tripModel->save();
                            }
                        }
                        // check vehicle near school location end

                        $amGetChildLists = NotificationDetail::where('route_id', $amGetTripDetails->route->route_id)
                                ->where('trip_id', $tripId)
                                ->where('trip_nearby', '!=', 1)
                                ->with(['route', 'child', 'parent' => function ($query) {
                                        $query->with('userToken');
                                    }])
                                ->get();
                        if ($amGetChildLists->count()) {
                            foreach ($amGetChildLists as $key => $amGetChildList) {
                                $deviceTokens = [];
                                if ($amGetChildList->parent->UserToken->count()) {
                                    foreach ($amGetChildList->parent->UserToken as $key => $data) {
                                        if ($data->device_token != '')
                                            $deviceTokens[] = $data->device_token;
                                    }
                                }

                                
                                $tripLength =  SystemConfig::where("access_key", "VEHICLE_NEARBY_HOME_NOTIFICATION")->first();

                                $data = [
                                    'msg_type' => VEHICLE_NEARBY,
                                    'msg_text' => 'Vehicle Nearby',
                                    'child_id' => strval($amGetChildList->child->child_id),
                                    'child_fname'   => $amGetChildList->child->child_fname,
                                    'child_mname'   => $amGetChildList->child->child_mname,
                                    'child_lname'   => $amGetChildList->child->child_lname,
                                    'pickup_lat'    => floatval($amGetChildList->child->pickup_lat),
                                    'pickup_long'   => floatval($amGetChildList->child->pickup_long),
                                    'route_code'    => $amGetChildList->route->route_code,
                                    'trip_type'     => (int) $amGetTripDetails->trip_type,
                                    'time'          => (int) time(),
                                    'trip_length'   => (int) (isset($tripLength->value) ? $tripLength->value : VEHICLE_NEARBY_NOTIFICATION_MINUTE_LENGTH),
                                ];
                                $childLat = $amGetChildList->child->pickup_lat;
                                $childLong = $amGetChildList->child->pickup_long;
                                $vehicleLat = $getLastTrackingLocation->trip_lat;
                                $vehicleLong = $getLastTrackingLocation->trip_long;

                                
                                $findVehicleIsNearBy = $tripModel->findVehicleIsNearBy($childLat, $childLong, $vehicleLat, $vehicleLong);
                                
                               /* $systemConfig =  SystemConfig::where("access_key", "VEHICLE_NEARBY_DISTANCE")->first();
                                
                                $defaultDistance = VEHICLE_NEARBY_DISTANCE;
                                if(!empty($systemConfig) && isset($systemConfig->id) ){
                                    $defaultDistance = (int) $systemConfig->value ? $systemConfig->value : VEHICLE_NEARBY_DISTANCE;
                                }*/
                                $defaultDistance =  $amGetChildList->parent->nearby_distance;
                                if ($findVehicleIsNearBy < $defaultDistance) {

                                    $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                                    if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                                        $tripEndFlag = 1;
                                    } else {
                                        $tripEndFlag = 0;
                                    }

                                    $updatedNotification = NotificationDetail::where( ['child_id' => $amGetChildList->child_id, 'trip_id' => $tripId])->first();
                                    
                                    if (isset($updatedNotification->id) && !empty($updatedNotification->id)) {
                                        $updatedNotification->trip_nearby = $tripEndFlag;
                                        $updatedNotification->save();
                                    }
                                }
                            }
                        }

                        $amResponse = [
                            "lat" => floatval($getLastTrackingLocation->trip_lat),
                            "long" => floatval($getLastTrackingLocation->trip_long),
                            "time" => intval($getLastTrackingLocation->trip_time),
                            "accuracy" => floatval($getLastTrackingLocation->trip_accuracy),
                            'bearing' => floatval($getLastTrackingLocation->trip_bearing),
                        ];
                        $result['is_tracking_available'] = true;
                        $result['location'] = $amResponse;
                    } else {
                        $isSuccess = true;
                        $message = TRIP_DETAILS_NOT_FOUND;
                        $result['is_tracking_available'] = true;
                        $result['location'] = [
                            "lat" => floatval(0.0),
                            "long" => floatval(0.0),
                            "time" => intval(time()),
                            "accuracy" => floatval(0.0),
                            'bearing' => floatval(0.0),
                        ];
                    }
                } else {
                    $isSuccess = false;
                    $message = TRIP_NOT_FOUND;
                    $result =  (object)[];
                }
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $isSuccess = false;
            $result =  (object)[];
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

    /**
     * Get Trip History API
     */
    public function getTripHistory(Request $request) {
        $isSuccess = true;
        $message = 'Trip History Details';
        $ArrResponse = [];
        $result = [];
        try{
            $trips = new Trip;
            $tripFirstDate = date('Y-m-d H:i:s', $request->datestamp_start);
            $tripSecondDate = date('Y-m-d H:i:s', $request->datestamp_end);
            $userTimezone = $request->time_zone;

            $amCheckChildExist = Child::where('child_id', $request->child_id)->where("child_status" ,1)
                ->with(
                    [
                        'route', 
                        'user',
                        'school'
                     ])->first();


            if (isset($amCheckChildExist->child_id) && $amCheckChildExist->child_id ) {

                $amCheckTripDetails = Trip::where('route_id', $amCheckChildExist->route->route_id)->where("trip_status",1)
                                ->whereBetween('created_at', [$tripFirstDate, $tripSecondDate])
                                ->with(
                                    [
                                        'tripdetails',
                                        'driverDetail'
                                    ])->get();

                if (($amCheckTripDetails->count())) {
                    foreach ($amCheckTripDetails as $key1 => $getCheckTripDetail) {
                        
                        foreach ($getCheckTripDetail->tripdetails as $key => $amCheckTripDetail) {
                            
                            $ArrResponse[$key1][$key]['lat'] = floatval($amCheckTripDetail->trip_lat);
                            $ArrResponse[$key1][$key]['long'] = floatval($amCheckTripDetail->trip_long);
                            if (!empty($amCheckTripDetail->trip_time)) {
                                $ArrResponse[$key1][$key]['time'] = $trips->timeZoneConvert(date('Y-m-d H:i:s', $amCheckTripDetail->trip_time), DEFAULT_TIME_ZONE, $userTimezone, $format = "h:i:s A");
                            } else {
                                $ArrResponse[$key1][$key]['time'] = '';
                            }
                        }
                        $result[$key1]['trip_id'] = $getCheckTripDetail->trip_id;
                        $result[$key1]['driver_name'] = isset($getCheckTripDetail->driverDetail->driver_name) ? $getCheckTripDetail->driverDetail->driver_name : '';
                        $result[$key1]['driver_phone'] = isset($getCheckTripDetail->driverDetail->user_phone) ? $getCheckTripDetail->driverDetail->user_phone : '';

                        $result[$key1]['school_lat'] = isset($amCheckChildExist->school->school_latitude) ? floatval($amCheckChildExist->school->school_latitude) : 0.0;
                        $result[$key1]['school_long'] = isset($amCheckChildExist->school->school_longitude) ? floatval($amCheckChildExist->school->school_longitude) : 0.0;
                        $tripStart = current($ArrResponse[$key1]);
                        $tripEnd = end($ArrResponse[$key1]);
                        /*$result[$key1]['trip_start'] = $tripStart['time']; //$getCheckTripDetail->start_time
                        $result[$key1]['trip_end'] = $tripEnd['time']; //$getCheckTripDetail->end_time
                        */
                        $result[$key1]['trip_start'] = $trips->timeZoneConvert(date('Y-m-d H:i:s', strtotime($getCheckTripDetail->start_time)), DEFAULT_TIME_ZONE, $userTimezone, $format = "h:i:s A"); 
                        $result[$key1]['trip_end'] = $trips->timeZoneConvert(date('Y-m-d H:i:s', strtotime($getCheckTripDetail->end_time)), DEFAULT_TIME_ZONE, $userTimezone, $format = "h:i:s A"); 
                        $result[$key1]['total_time'] = (int)(strtotime($result[$key1]['trip_end']) - strtotime($result[$key1]['trip_start']));

                        $result[$key1]['locations'] = $ArrResponse[$key1];
                    }
                } else {
                    $isSuccess = true;
                    $message = TRIP_DETAILS_NOT_FOUND;
                }
            } else {
                $isSuccess = false;
                $message = ROUTE_NOT_FOUND;
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }


    public function checkTripIsOngoing(Request $request) {
        $ArrResponse["tripOngoin"] = false;
        try {
            if(!isset($request->trip_id) || empty($request->trip_id) || !isset($request->trip_type) || !in_array($request->trip_type, ["0","1"] )) {
                throw new Exception("Invalid parameters.");
            }
            
            $tripModel = Trip::where(["trip_id"  => $request->trip_id, "trip_type"  => $request->trip_type, "driver_id"  => Auth::guard("api")->user()->id])->first();
            
            if(!isset($tripModel->trip_id) && empty($tripModel->trip_id)){
                throw new Exception(ROUTE_NOT_FOUND);
            }

            if ($tripModel->trip_status == 0) {
                $ArrResponse["tripOngoin"] = true;   
            }
            
            return Response::json(["success" => true, "msg" => "Trip found successfully.", "result" => (object) $ArrResponse]);
        } catch(Exception $e) {
            return Response::json(["success" => false, "msg" => $e->getMessage(), "result" => (object) $ArrResponse]);
        }
    }
}
