<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB, Validator, Response, Auth;
use App\User, App\UserToken, App\Vehicle, App\School, App\Page, App\Suggestion, App\Route, App\Child, App\FcmNotification;

class AuthController extends \App\Http\Controllers\API\AuthController {
    public function __construct() {
        parent::__construct();
    }
    
}
