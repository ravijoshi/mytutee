<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Route, App\SystemConfig, App\Child, App\Trip, App\TripDetail, App\NotificationDetail, App\FcmNotification;
use DB, Response, Auth;

class TripController extends \App\Http\Controllers\API\TripController {
	public function __construct() {
        parent::__construct();
    }
}
