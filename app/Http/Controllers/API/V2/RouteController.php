<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator, Response, Auth, DB;
use App\User, App\Vehicle, App\SchoolList, App\Route, App\School, App\Child, App\RouteType, App\RouteDetail;
use Carbon\Carbon;

class RouteController extends \App\Http\Controllers\API\RouteController {
	public function __construct() {
        parent::__construct();
    }
}
