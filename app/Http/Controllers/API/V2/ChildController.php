<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Response, Auth;
use App\Route, App\Child, App\RouteType, App\RouteDetail, App\SystemConfig, App\NotificationDetail, App\FcmNotification, App\Package;

class ChildController extends \App\Http\Controllers\API\ChildController {

    public function __construct() {
        parent::__construct();
    }
	/**
     * Get Child Details API
     	updates : sbscription plan added in response
     */
    public function getChildList(Request $request) {
        $isSuccess = true;
        $message = 'Child Details';
        $response = [];
        $data = [];

        $user = Auth::guard("api")->user();
        if (isset($user->id)) {
            $childrenModel = Child::where('parent_id', $user->id)
                    ->with(
                        [
                            'school', 
                            'route' => function($query){
                                $query->with(
                                    [
                                        'typeHs' => function ($select) {
                                            $select->with('driver');
                                        },
                                        'typeSh'
                                    ]
                                );
                            }
                        ])
                    ->where("child_status" ,1)
                    ->get();
                
            if ($childrenModel->count()) {
                foreach ($childrenModel->toArray() as $key => $child) {
                    
                    $data[$key]['id']    = strval($child['child_id']);
                    $data[$key]['fname'] = $child['child_fname'];
                    $data[$key]['mname'] = $child['child_mname'];
                    $data[$key]['lname'] = $child['child_lname'];
                    $data[$key]['is_free_trial']    = $child['is_free_trial'];
                    $data[$key]['is_subscribed']    = $child['subscribed'];
                    $data[$key]['subscribed_upto']  = $child['subscription_upto'];
                    $data[$key]['school_id']        = strval($child['school']['school_map_id']);
                    $data[$key]['school_name']      = $child['school']['school_name'];
                    $data[$key]['school_address']   = $child['school']['school_address'];
                    $data[$key]['pickup_lat']       = floatval($child['pickup_lat']);
                    $data[$key]['pickup_long']      = floatval($child['pickup_long']);
                    $data[$key]['pickup_address']   = $child['pickup_address'];
                    $data[$key]['route_code']       = '';
                    $data[$key]['driver_name']      = '';
                    $data[$key]['driver_phone']     = '';
                    $data[$key]['route_start_time'] = (int) 0;
                    $data[$key]['route_end_time']   = (int) 0;
                    $data[$key]['sh_start_time'] = (int) 0;
                    $data[$key]['sh_end_time']   = (int) 0;

                    if(isset($child['route']) && $child['route']) {
                        $data[$key]['route_code']     = $child['route']['route_code'];
                        
                        if(isset($child['route']['type_hs']) && $child['route']['type_hs'])  {
                            $data[$key]['route_start_time'] = intval(strtotime($child['route']['type_hs']['start_time']));
                            $data[$key]['route_end_time']   = intval(strtotime($child['route']['type_hs']['end_time']));
                         
                            if(isset($child['route']['type_hs']['driver']) && $child['route']['type_hs']['driver']) {
                                $data[$key]['driver_name']   = $child['route']['type_hs']['driver']['driver_name'];
                                $data[$key]['driver_phone']  = $child['route']['type_hs']['driver']['user_phone'];
                            }
                        }
                        if(isset($child['route']['type_sh']) && $child['route']['type_sh'])  {
                            $data[$key]['sh_start_time'] = intval($child['route']['type_sh']['sh_start_time']);
                            $data[$key]['sh_end_time']   = intval($child['route']['type_sh']['sh_end_time']);
                        }
                    }
                }
            } else {
                $message = RECORD_NOT_FOUND;
            }
            // get subscription packages 
            $packageModel =  new Package();
            $response['child_list'] = $data;
            $response['subscription_plan'] = $packageModel->getPackages();
        } else {
            $isSuccess = false;
            $message = USER_NOT_FOUND;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $response]);
    }


    /**
     * Add/Update Child Details API
     */
    public function alterChildDetails(Request $request) {
        $isSuccess =  TRUE;
        $message = '';
        $result = (object) [];
        $request->parent_id =  Auth::guard("api")->user()->id;

        if(!isset($request->route_code) || (isset($request->route_code) && !$request->route_code) ) {
            $message =  "Invalid input.";
            $isSuccess =  FALSE;
        } else {

            $routeExist = Route::where('route_code', strtoupper($request->route_code))->where("route_status" ,1)->first();

            if(!isset($routeExist->route_id) && empty($routeExist)) {
                $message = ROUTE_NOT_FOUND;
                $isSuccess =  FALSE;
            } else {
                if(isset($request->child_id) && $request->child_id){
                    // edit child
                    $childModel = Child::where(['child_id'=> $request->child_id, "parent_id" => Auth::guard("api")->user()->id])->first();
                    if(!isset($childModel->child_id) && empty($childModel)) {
                        $message =  CHILD_NOT_FOUND;
                        $isSuccess =  FALSE;
                    } else {
                        // check route code updated or not
                        $message = 'Child Details updated successfully.';
                        if($childModel->route_id !=  $routeExist->route_id) {
                            // check route have remain capacity.
                            $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" => $routeExist->route_id  ])->get();
                            if($routeCapacity->count()){
                                $childModel =  $childModel->saveChild($childModel,$routeExist, $request);
                                // send notification to driver.
                                $childModel->childAddedOnRoute($childModel, $routeExist);
                            } else {
                                $isSuccess = FALSE;
                                $message = "The route code doesn't have more space available.";
                            }
                        } else {
                            $childModel =  $childModel->saveChild($childModel,$routeExist, $request);
                        }
                    }
                } else {

                    // check route have remain capacity.
                    $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" => $routeExist->route_id  ])->get();
                    if($routeCapacity->count()){
                        // add new child
                        $childModel =  new Child();
                        $childModel =  $childModel->saveChild($childModel,$routeExist, $request);
                        $message = 'Child Details inserted successfully.';
                        // send notification to driver.
                        $childModel->childAddedOnRoute($childModel, $routeExist);
                    } else {
                        $isSuccess = FALSE;
                        $message = "The route code doesn't have more space available.";
                    }
                }
            }
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }
}
