<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Suggestion;
use Response, Auth;

class SuggestionController extends \App\Http\Controllers\API\SuggestionController {
	public function __construct() {
        parent::__construct();
    }
}
