<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle, App\Route, App\RouteType, App\Trip, App\TripDetail;
use Response, Auth;

class VehicleController extends \App\Http\Controllers\API\VehicleController {
    public function __construct() {
        parent::__construct();
    }
}
