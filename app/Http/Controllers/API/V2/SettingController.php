<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth, Exception, Validator;

class SettingController extends  \App\Http\Controllers\API\CommonController {
    public function __construct() {
        parent::__construct();
    }

    public function index(){
    	$response = [];
        $isSuccess = true;
        $message = '';
        try{
            $response['nearby_distance'] = Auth::guard("api")->user()->nearby_distance;
            $response['distance_options'] = \App\User::getDistanceOptions();
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }

        return response()->json(["success" => $isSuccess, "msg" => $message, "result" => (object) $response]);
    }


    public function update(Request $request){
    	$response = [];
        $isSuccess = true;
        $message = '';
        try{
        	$rules = [
                'nearby_distance' => 'required|in:'.\App\User::DISTANCE_100.','.\App\User::DISTANCE_200.','.\App\User::DISTANCE_300.','.\App\User::DISTANCE_400.','.\App\User::DISTANCE_500.','.\App\User::DISTANCE_1000.'',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                throw new Exception("Invalid input.");
            }
            $user = Auth::guard("api")->user();
            $user->nearby_distance =  $request->nearby_distance;
            $user->save();
            
            $message =  "Settings updated successfully.";
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }

        return response()->json(["success" => $isSuccess, "msg" => $message, "result" => (object) $response]);
    }
}