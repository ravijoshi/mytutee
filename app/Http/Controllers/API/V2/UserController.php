<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User, App\Route, App\Vehicle, App\Child, App\Trip;
use Response, Validator, Auth, DB, Hash, Exception;

class UserController extends \App\Http\Controllers\API\UserController {
   	public function __construct() {
        parent::__construct();
    }

    /**
     * Get Parent Dashboard API
     */
    public function parentDashboard(Request $request) {
        $isSuccess = true;
        $message = 'Parent Dashboard';
        $childData = [];
        $result = [];
        try{
            $parentUser = Auth::guard("api")->user();
            if ($parentUser->user_role == User::USER_ROLE_PARENT) {

                $childLists = Child::where('parent_id',$parentUser->id)
                        ->where("child_status" ,1)
                        ->with(
                            [
                            'school', 
                            'route' => function($query){
                                $query->with(
                                    [
                                    'typeSh' => function($select){
                                        $select->with('driver');
                                    }, 
                                    'typeHs' => function($select){
                                        $select->with('driver');
                                    },
                                    'trip'
                                ]);
                            }
                            ])
                        ->get();

                
                if ($childLists->count()) {
                    $result['is_child_available'] = true;
                    foreach ($childLists->toArray() as $key => $_child) {
                        $tripStatus = false;
                        $childData[$key]['child_id'] 	= strval($_child['child_id']);
                        $childData[$key]['child_fname'] = $_child['child_fname'];
                        $childData[$key]['child_mname'] = $_child['child_mname'];
                        $childData[$key]['child_lname'] = $_child['child_lname'];
                        $childData[$key]['pickup_lat'] 	= floatval($_child['pickup_lat']);
                        $childData[$key]['pickup_long'] = floatval($_child['pickup_long']);
                        $childData[$key]['pickup_address'] 	= $_child['pickup_address'];
                        $childData[$key]['is_free_trial'] 	= $_child['is_free_trial'];
                        $childData[$key]['is_subscribed']   = $_child['subscribed'];
                        $childData[$key]['subscribed_upto'] = $_child['subscription_upto'];

                        $childData[$key]['school_name']     = NULL;
                        $childData[$key]['school_address']  = NULL;
                        $childData[$key]['school_lat']      = NULL;
                        $childData[$key]['school_long']     = NULL;
                        
                        if(isset($_child['school']) && $_child['school']) {
                            $childData[$key]['school_name']     = isset($_child['school']['school_name']) ? $_child['school']['school_name'] : NULL;
                            $childData[$key]['school_address']  = isset($_child['school']['school_address']) ? $_child['school']['school_address'] : NULL;
                            $childData[$key]['school_lat']      = isset($_child['school']['school_latitude']) ? floatval($_child['school']['school_latitude']) : NULL;
                            $childData[$key]['school_long']     = isset($_child['school']['school_longitude']) ? floatval($_child['school']['school_longitude']) : NULL;
                        }
                        
                        $childData[$key]['route_id']   = (int) 0;
                        $childData[$key]['route_code'] = NULL;
                        $childData[$key]['route_start_time'] = NULL;
                        $childData[$key]['route_end_time']   = NULL;
                        // HS
                        $childData[$key]['driver_name_hs']     = NULL;
                        $childData[$key]['driver_phone_hs']    = NULL;
                        // SH
                        $childData[$key]['driver_name_sh']     = NULL;
                        $childData[$key]['driver_phone_sh']    = NULL;
                        $childData[$key]['sh_start_time']      = (int) 0;
                        $childData[$key]['sh_end_time']        = (int) 0;
                        
                        if (isset($_child['route']) && $_child['route']) {
                            
                            $childData[$key]['route_id'] = (int) (isset($_child['route']['route_id']) ? $_child['route']['route_id'] : 0);
                            $childData[$key]['route_code'] = isset($_child['route']['route_code']) ? $_child['route']['route_code'] : NULL;
                            
                            if ( isset($_child['route']['trip']) && $_child['route']['trip'] && isset($_child['route']['trip']['trip_status']) && $_child['route']['trip']['trip_status'] != 1) {
                                $tripStatus = true;
                            }

                            
                           // HS
                           if(isset($_child['route']['type_hs']) && $_child['route']['type_hs'] && isset($_child['route']['type_hs']['driver']) && $_child['route']['type_hs']['driver']) {
                                $childData[$key]['driver_name_hs']     = $_child['route']['type_hs']['driver']['driver_name'];
                                $childData[$key]['driver_phone_hs']    = $_child['route']['type_hs']['driver']['user_phone'];
                                $childData[$key]['route_start_time'] = isset($_child['route']['type_hs']['start_time']) ? intval(strtotime($_child['route']['type_hs']['start_time'])) : NULL;
                                $childData[$key]['route_end_time'] = isset($_child['route']['type_hs']['end_time']) ? intval(strtotime($_child['route']['type_hs']['end_time'])) :  NULL;
                            }

                            // SH
                            if(isset($_child['route']['type_sh']) && $_child['route']['type_sh'] && isset($_child['route']['type_sh']['driver']) && $_child['route']['type_sh']['driver']) {
                                $childData[$key]['driver_name_sh']     = $_child['route']['type_sh']['driver']['driver_name'];
                                $childData[$key]['driver_phone_sh']    = $_child['route']['type_sh']['driver']['user_phone'];
                                $childData[$key]['sh_start_time'] = intval($_child['route']['type_sh']['sh_start_time']);
                                $childData[$key]['sh_end_time'] = intval($_child['route']['type_sh']['sh_end_time']);
                            }
                           
                        }
                        $childData[$key]['is_trip_ongoing'] = $tripStatus;
                    }
                    $result['child_data'] = $childData;
                } else {
                    $message = RECORD_NOT_FOUND;
                    $result['is_child_available'] = false;
                    $result['child_data'] = [];
                }
            } else {
                $isSuccess = false;
                $message = USER_NOT_FOUND;
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }


    public function driverDashboard(Request $request) {
        $routeData = [];
        $isSuccess = true;
        $message = RECORD_NOT_FOUND;
        $result = [];
        try{
            $amGetCurrentUser = Auth::guard("api")->user();
            if($amGetCurrentUser->user_role == User::USER_ROLE_DRIVER) {

                $routeLists = Route::whereHas('driverRoute' , function ($query) {
                                $query->where('driver_id', Auth::guard("api")->user()->id);
                            })
                        ->where("route_status" ,1)
                        ->select( 'route_id','route_code','school_id','is_roud_trip')
                        ->with([
                                'school' => function ($query) {
                                    $query->select('id', 'school_name', 'school_map_id', 'school_address', 'school_latitude', 'school_longitude');
                                },
                                'driverRoute' => function ($query) {
                                    $query->select('*', DB::raw('UNIX_TIMESTAMP(start_time) as start_time'),DB::raw('UNIX_TIMESTAMP(end_time) as end_time'));
                                    $query->where('driver_id', Auth::guard("api")->user()->id);
                                    $query->with('driver');
                                },
                                'typeSh'
                            ]
                        )
                    ->get();
                
                $vehicleData = Vehicle::where('driver_id', Auth::guard("api")->user()->id)->first();

                $result['is_veh_available'] = false;
                $result['is_route_available'] = false;
                $result['grand_total'] = $this->getMonthlyEarnings(Auth::guard("api")->user());
                if (!empty($vehicleData)) {
                    $result['is_veh_available'] = true;
                }

                if ($routeLists->count()) {
                    $result['is_route_available'] = true;
                    $message = "Driver Dashboard";
                    $isSuccess = true;

                    foreach ($routeLists->toArray() as $key => $_route) {
                        $routeData[$key]['route_id']    = strval($_route['route_id']);
                        $routeData[$key]['route_type']  = $_route['route_type'];

                        $routeData[$key]['total']  = 0;
                        if($_route['route_type'] != TRIP_TYPE_SCHOOL_HOME)
                            $routeData[$key]['total']  = $this->getMonthlyEarnings(Auth::guard("api")->user(), $_route['route_id']);

                        $routeData[$key]['route_code']  = $_route['route_code'];
                        $routeData[$key]['driver_id']   = strval($_route['driver_route']['driver_id']);
                        $routeData[$key]['start_lat']   = (float) $_route['driver_route']['start_loc_lat'];
                        $routeData[$key]['start_long']  = (float) $_route['driver_route']['start_loc_long'];
                        $routeData[$key]['start_time']  = (int)$_route['driver_route']['start_time'];
                        $routeData[$key]['end_time']    = (int) $_route['driver_route']['end_time'];
                        $routeData[$key]['start_address'] = $_route['driver_route']['start_loc_address'];
                        
                        $routeData[$key]['sh_start_time']  = (int)$_route['type_sh']['sh_start_time'];
                        $routeData[$key]['sh_end_time']    = (int) $_route['type_sh']['sh_end_time'];
                        
                        $routeData[$key]['school_data']['id']   = NULL;
                        $routeData[$key]['school_data']['name'] = NULL;
                        $routeData[$key]['school_data']['lat']  = NULL;
                        $routeData[$key]['school_data']['long'] = NULL;
                        $routeData[$key]['school_data']['map_id']   = NULL;
                        $routeData[$key]['school_data']['address']  = NULL;

                        if(isset($_route['school']) && count($_route['school'])) {
                            $routeData[$key]['school_data']['id']   = strval($_route['school']['id']);
                            $routeData[$key]['school_data']['name'] = $_route['school']['school_name'];
                            $routeData[$key]['school_data']['lat']  = (float) $_route['school']['school_latitude'];
                            $routeData[$key]['school_data']['long'] = (float) $_route['school']['school_longitude'];
                            $routeData[$key]['school_data']['map_id']   = $_route['school']['school_map_id'];
                            $routeData[$key]['school_data']['address']  = $_route['school']['school_address'];
                        }
                    }
                } 
                $result['route_data'] = $routeData;
            } else {
                $message = USER_NOT_FOUND;
                $isSuccess = false;
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
            
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }
}
