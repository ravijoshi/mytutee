<?php
namespace App\Http\Controllers\API\V2;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Bank, App\UserBank, Auth, DB, Response, Validator;

class BankController extends \App\Http\Controllers\API\BankController {
	public function __construct() {
        parent::__construct();
    }
}