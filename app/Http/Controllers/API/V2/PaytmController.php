<?php
namespace App\Http\Controllers\API\V2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Paytm, App\Payment, App\Child, App\Package;
use Response, Auth;

class PaytmController extends \App\Http\Controllers\API\CommonController {
	
	public function __construct() {
        parent::__construct();
    }
	/*
		$checkSum = "";
		$paramList["MID"] = 'xxxxxxxxxxxxx'; //Provided by Paytm
		$paramList["ORDER_ID"] = 'ORDER0000001'; //unique OrderId for every request
		$paramList["CUST_ID"] = 'CUST00001'; // unique customer identifier 
		$paramList["INDUSTRY_TYPE_ID"] = 'xxxxxxxxxxx'; //Provided by Paytm
		$paramList["CHANNEL_ID"] = 'WAP'; //Provided by Paytm
		$paramList["TXN_AMOUNT"] = '1.00'; // transaction amount
		$paramList["WEBSITE"] = 'xxxxxxxx';//Provided by Paytm
		$paramList["CALLBACK_URL"] = 'https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp';//Provided by Paytm
		$paramList["EMAIL"] = 'abc@gmail.com'; // customer email id
		$paramList["MOBILE_NO"] = '9999999999'; // customer 10 digit mobile no.
		$checkSum = getChecksumFromArray($paramList,PAYTM_MERCHANT_KEY);
		$paramList["CHECKSUMHASH"] = $checkSum;
	*/

	public function generateChecksum(Request $request){
		
		$isSuccess = FALSE;
        $message = 'Invalid Input';
        $response = [];
        $parentUser = Auth::guard("api")->user();
        
		if(isset($request->child_id) && !empty($request->child_id) && isset($request->package_id) && !empty($request->package_id)){

			$childModel =  Child::where(["parent_id" => $parentUser->id, "child_id" => $request->child_id])->first();
			$packageModel =  Package::where(["id" => $request->package_id, "status" => 1])->first();

			if(!empty($childModel) && isset($childModel->child_id) && !empty($packageModel) && isset($packageModel->id)) {
				
				$paytmModel  = new Paytm();
				$paytmProperties =  $paytmModel->getPaytmProperties();
				$paramList = array();


				// add data in payment table.
				$paymentModel =  new Payment();
				$paymentModel->child_id 	=  $childModel->child_id;
				$paymentModel->parent_id 	=  $childModel->parent_id;
				$paymentModel->package_id 	=  $packageModel->id;
				$paymentModel->package_days =  $packageModel->duration * SUBSCRIPTION_MONTH_DAYS;
				$paymentModel->package_price =  $packageModel->price;
				$paymentModel->type   		=  Payment::PAYMENT_TYPE_SBUSCRIBED;
				$paymentModel->payment_method   =  Payment::PAYMENT_METHOD_PAYTM;
				$paymentModel->payment_status   =  Payment::PAYMENT_STATUS_PENDING;
				$paymentModel->created_by   =  Auth::guard("api")->user()->id;
				$paymentModel->start_date 	=  date("Y-m-d H:i:s");
				$paymentModel->end_date   	=  date("Y-m-d H:i:s" , strtotime($paymentModel->start_date. ' + '.$paymentModel->package_days.' days'));
				
				$paymentModel->save();
				
				$paramList["MID"] 		= $paytmProperties['merchantId'];
				$paramList["ORDER_ID"] 	= $paymentModel->id;
				$paramList["CUST_ID"] 	= $childModel->child_id;
				$paramList["INDUSTRY_TYPE_ID"] = $paytmProperties['industryTypeId'];
				$paramList["CHANNEL_ID"] 	= $paytmProperties['channelId'];
				$paramList["TXN_AMOUNT"] 	= $packageModel->price;
				$paramList["WEBSITE"] 		= $paytmProperties['merchantWebsite'];
				$paramList["CALLBACK_URL"] 	= $paytmProperties['callbackUrl'];
				$paramList["EMAIL"] 		= $childModel->parent->email; // customer email id
				$paramList["MOBILE_NO"] 	= $childModel->parent->user_phone; // customer 10 digit mobile no.


				$response['orderId'] =  $paymentModel->id;
				$response['CHECKSUMHASH'] = $paytmModel->getChecksumFromArray($paramList,$paytmProperties['merchantKey']);

				$paymentModel->checksum =  $response['CHECKSUMHASH'];
				$paymentModel->save();
				$isSuccess = TRUE;
	        	$message = "Paytm checksum getnrated successfully.";
	        } else {
	        	$message = CHILD_NOT_FOUND;
	        }
		}

		return Response::json(["success" => $isSuccess, "msg" => $message, "result" => (object) $response]);
	}

	public function verifyChecksum(Request $request){
		
		$isSuccess = FALSE;
        $message = 'Invalid Input';
        $response = [];
        $parentUser = Auth::guard("api")->user();


        $paytmModel  = new Paytm();
		$paytmProperties =  $paytmModel->getPaytmProperties();

		$paramList["MID"] 		= $paytmProperties['merchantId'];
		$paramList["ORDER_ID"] 	= 2;
		$paramList["CUST_ID"] 	= 2;
		$paramList["INDUSTRY_TYPE_ID"] = $paytmProperties['industryTypeId'];
		$paramList["CHANNEL_ID"] 	= $paytmProperties['channelId'];
		$paramList["TXN_AMOUNT"] 	= 300;
		$paramList["WEBSITE"] 		= $paytmProperties['merchantWebsite'];
		$paramList["CALLBACK_URL"] 	= $paytmProperties['callbackUrl'];
		$paramList["EMAIL"] 		= "hardik@gmail.com"; // customer email id
		$paramList["MOBILE_NO"] 	= "9998887770"; // customer 10 digit mobile no.
		$paytmChecksum = "KTS48E03Qy966ipSRlQIMhabunTEaFWWnVDk1mGGqMUpNO4DLLyfQMLCY5v1GN3utaAYD0D5RM9gBxu9R8jpllZ2KL2P/fUrZm+zQqHIk5M=";
        
        $isValidChecksum = $paytmModel->verifychecksum_e($paramList, $paytmProperties['merchantKey'], $paytmChecksum); //will return TRUE or FALSE string.
        var_dump($isValidChecksum);die;
		if(isset($request->child_id) && !empty($request->child_id) && isset($request->package_id) && !empty($request->package_id)){

			$childModel =  Child::where(["parent_id" => $parentUser->id, "child_id" => $request->child_id])->first();
			$packageModel =  Package::where(["id" => $request->package_id, "status" => 1])->first();

			if(!empty($childModel) && isset($childModel->child_id) && !empty($packageModel) && isset($packageModel->id)) {
				
				$paymentModel  = Payment::find($paramList["ORDER_ID"]);
				if(isset($paymentModel->id) && $paymentModel->id){
					$paymentModel->payment_status   =  Payment::PAYMENT_STATUS_PENDING;
					$paymentModel->updated_by   =  Auth::guard("api")->user()->id;
					$paymentModel->save();
					
					$isSuccess = TRUE;
		        	$message = "Paytm checksum verified successfully.";
				} else {
					$message =  "Order id not found.";
				}
	        } else {
	        	$message = CHILD_NOT_FOUND;
	        }
		}

		return Response::json(["success" => $isSuccess, "msg" => $message, "result" => (object) $response]);
	}



}
