<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vehicle, App\Route, App\RouteType, App\Trip, App\TripDetail;
use Response, Auth;

class VehicleController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Vehicle Details API
     */
    public function getVehicle(Request $request) {
        $isSuccess = true;
        $amResponse = (object) [];
        $message = "Vehicle Details";
        $amGetCurrentUser = Auth::guard("api")->user();

        if (!empty($amGetCurrentUser)) {

            $modelVehicle = new Vehicle;
            $getVehicle = Vehicle::where('driver_id', $amGetCurrentUser->id)->where("vehicle_status" ,1)->first();
            if ($getVehicle) {
                if (!empty($getVehicle->vehicle_photo)) {
                    $vehicle_img = url('public/images/vehicles') . '/' . $getVehicle->vehicle_photo;
                } else {
                    $vehicle_img = url('public/images/vehicles') . '/no-vehicle.png';
                }
                if (count($getVehicle->toArray()) > 0) {
                    $amResponse = [
                        "veh_id" => strval($getVehicle->vehicle_id),
                        "veh_owner_name" => $getVehicle->vehicle_owner_name,
                        "veh_capacity" => $getVehicle->vehicle_capacity,
                        "veh_photo" => $vehicle_img,
                        "veh_reg_no" => $getVehicle->vehicle_reg_no,
                        "school_assigned_no" => $getVehicle->vehicle_school_id,
                    ];
                } else {
                    $message = 'Invalid Driver id';
                }
            } else {
                $message = VEHICLE_NOT_FOUND;
            }
        } else {
            $isSuccess = false;
            $message = USER_NOT_FOUND;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $amResponse]);
    }

    /**
     * Add / Update Vehicle API
     */
    public function alterVehicleDetails(Request $request) {
        $isSuccess = true;
        $message = '';
        $amResponse = (object) [];
        $amGetCurrentUser = Auth::guard("api")->user();
        if ($amGetCurrentUser) {
            $vehicleId = $request->vehicle_id;
            if ($vehicleId == '') {
                $modelVehicle = new Vehicle;
                if (!empty($request->file('veh_image'))) {
                    $vehicle_img = $this->uploadImage($request->file('veh_image'), VEHICLE_IMAGE_FOLDER);
                } else {
                    $vehicle_img = 'no-vehicle.png';
                }
                $modelVehicle->driver_id = $amGetCurrentUser->id;
                $modelVehicle->vehicle_owner_name = !empty($request->veh_owner_name) ? $request->veh_owner_name : "";
                $modelVehicle->vehicle_capacity = !empty($request->veh_capacity) ? $request->veh_capacity : "";
                $modelVehicle->vehicle_photo = $vehicle_img;
                $modelVehicle->vehicle_reg_no = !empty($request->veh_reg_no) ? $request->veh_reg_no : "";
                $modelVehicle->vehicle_school_id = !empty($request->school_assigned_no) ? $request->school_assigned_no : "";
                $vehicle = $modelVehicle->save();
                if ($vehicle) {
                    if (!empty($modelVehicle->vehicle_photo)) {
                        $vehicle_img_url = url('public/images/vehicles') . '/' . $modelVehicle->vehicle_photo;
                    } else {
                        $vehicle_img_url = url('public/images/vehicles') . '/no-vehicle.png';
                    }
                    $amResponse = [
                        "veh_id" => strval($modelVehicle->vehicle_id),
                        "driver_id" => strval($modelVehicle->driver_id),
                        "veh_owner_name" => $modelVehicle->vehicle_owner_name,
                        "veh_capacity" => $modelVehicle->vehicle_capacity,
                        "veh_image" => $vehicle_img_url,
                        "veh_reg_no" => $modelVehicle->vehicle_reg_no,
                        "school_assigned_no" => $modelVehicle->vehicle_school_id,
                    ];
                    $message = 'Vehicle added Sucessfully';
                } else {
                    $isSuccess = false;
                    $message = VEHICLE_NOT_FOUND;
                }
            } else {
                $amCheckVehicleExist = Vehicle::where('vehicle_id', $request->vehicle_id)->first();
                if (!empty($amCheckVehicleExist)) {
                    if ($request->image_changed == "true") {
                        if (!empty($request->file('veh_image'))) {
                            $vehicle_img = $this->uploadImage($request->file('veh_image'), VEHICLE_IMAGE_FOLDER);
                        } else {
                            $vehicle_img = 'no-vehicle.png';
                        }
                    } else {
                        $vehicle_img = $amCheckVehicleExist->vehicle_photo;
                    }
                    $updatedVehDetails = Vehicle::where('vehicle_id', $request->vehicle_id)
                            ->update([
                        'driver_id' => $amGetCurrentUser->id,
                        'vehicle_owner_name' => $request->veh_owner_name,
                        'vehicle_capacity' => $request->veh_capacity,
                        'vehicle_photo' => $vehicle_img,
                        'vehicle_reg_no' => $request->veh_reg_no,
                        'vehicle_school_id' => $request->school_assigned_no,
                    ]);
                    if ($updatedVehDetails) {
                        $getVehicle = Vehicle::find($request->vehicle_id);
                        if (!empty($getVehicle->vehicle_photo)) {
                            $vehicle_img = url('public/images/vehicles') . '/' . $getVehicle->vehicle_photo;
                        } else {
                            $vehicle_img = url('public/images/vehicles') . '/no-vehicle.png';
                        }
                        $amResponse = [
                            "veh_id" => strval($request->vehicle_id),
                            "driver_id" => strval($amGetCurrentUser->id),
                            "veh_owner_name" => $request->veh_owner_name,
                            "veh_capacity" => $request->veh_capacity,
                            'veh_image' => $vehicle_img,
                            "veh_reg_no" => $request->veh_reg_no,
                            "school_assigned_no" => $request->school_assigned_no,
                        ];
                        $message = "Vehicle updated Sucessfully";
                    } else {
                        $message = "Vehicle not updated, please try again";
                    }
                } else {
                    $isSuccess = false;
                    $message = VEHICLE_NOT_FOUND;
                }
            }
        } else {
            $isSuccess = false;
            $message = USER_NOT_FOUND;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $amResponse]);
    }

    // track vihicle location for parent app.
    public function trackVehicleLocation(Request $request) {
        $isSuccess = true;
        $message = '';
        $amResponse = (object) [];
        $result = [];
        if(!isset($request->route_code) || empty($request->route_code)) {
            $isSuccess = false;
            $message = "Invalid input.";
        } else {
            $amCheckRouteExist = Route::with('school')->where('route_code', $request->route_code)->first();
            if (isset($amCheckRouteExist->route_id) && !empty($amCheckRouteExist->route_id)) {
                $amCheckTripDetails = Trip::where('route_id', $amCheckRouteExist->route_id)
                        ->where('trip_status', 0)
                        ->first();

                // default value for time tracker.
                $result['time_to_reach_school'] = (int) 0;
                if ($amCheckTripDetails) {
                    $message = 'Trip Ongoing';
                    $result['is_tracking_available'] = true;
                    $result['trip_type'] = (int)$amCheckTripDetails->trip_type;

                    $getLastTrackingLocation = TripDetail::where('trip_id', $amCheckTripDetails->trip_id)->latest()->first();

                    if (isset($getLastTrackingLocation->trip_id) && !empty($getLastTrackingLocation->trip_id)) {
                        $amResponse = [
                            "lat" => floatval($getLastTrackingLocation->trip_lat),
                            "long" => floatval($getLastTrackingLocation->trip_long),
                            "time" => intval($getLastTrackingLocation->trip_time),
                            "accuracy" => floatval($getLastTrackingLocation->trip_accuracy),
                            'bearing' => floatval($getLastTrackingLocation->trip_bearing),
                        ];
                        
                        $result['time_to_reach_school'] = (int) $getLastTrackingLocation->time_to_reach_school;
                        $result['location'] = $amResponse;
                    } else {
                        $message = TRIP_DETAILS_NOT_FOUND;
                        $result['location'] = (object)$amResponse;
                    }
                } else {
                    $isSuccess = true;
                    $message = "No Trip is going on right now";
                    $result['is_tracking_available'] = false;
                    $result['trip_type'] = (int) TRIP_TYPE_HOME_SCHOOL;
                    $result['location'] = (object)$amResponse;
                }
            } else {
                $isSuccess = false;
                $message = ROUTE_NOT_FOUND;
            }
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }


    //This function is used to get all child details in driver route  id
    public function getChildrenInVehicle(Request $request) {
        $isSuccess = true;
        $message = '';
        $data = (object) [];
        $user = Auth::guard("api")->user();

        if(!isset($request->route_id) || empty($request->route_id)) {
            $isSuccess = false;
            $message = "Invalid input.";
        } else {

            $route =  RouteType::with(
                [
                    'routeChildren' => function ($q){
                        $q->with(['child']);
                    },
                    'vehicle'
                ]
                )->where(["route_id" => $request->route_id,  'driver_id' =>  Auth::guard("api")->user()->id])
            ->first();

            if(!isset($route->vehicle) && empty($route->vehicle)) {
                $isSuccess = false;
                $message = "Vehicle details not found for this route driver.";
            } else {
                $data->children = [];
                
                if(isset($route->routeChildren)  && $route->routeChildren->count()){

                    foreach ($route->routeChildren as $children) {
                        if(isset($children->child->child_id) && $children->child->child_id){

                            $children->child->activate_upto = (!empty($children->child->activate_date) && !empty($children->child->free_trial_days)) ?  strtotime($children->child->activate_date. ' + '.$children->child->free_trial_days.' days') : NULL;
                            $data->children[] =  array_except($children->child->toArray(),["route_id","child_fname","child_mname","child_lname","school_id","parent_id","driver_id","activate_date","free_trial_days","created_at","updated_at","deleted_at","child_status","package_id","is_subscribed","subscription_days","subscribed_at","subscribed_upto","subscribed_at"]);
                        }
                    }
                }

                $data->vehicle_capacity =  $route->vehicle->vehicle_capacity;
                $isSuccess = true;
                $message = "Vehicle details found successfully.";
            }
        }

        return response()->json(["success" => $isSuccess, "msg" => $message, "result" => $data]);
    }
    
}
