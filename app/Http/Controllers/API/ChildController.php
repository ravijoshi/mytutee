<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Response, Auth;
use App\Route,  App\Child, App\RouteType, App\RouteDetail,  App\SystemConfig, App\NotificationDetail, App\FcmNotification;

class ChildController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Child Details API
     */
    public function getChildList(Request $request) {
        $isSuccess = true;
        $message = 'Child Details';
        $response = [];
        $amGetCurrentUser = Auth::guard("api")->user();
        if (!empty($amGetCurrentUser)) {
            $amGetChildLists = Child::where('parent_id', $amGetCurrentUser->id)
                    ->with(
                        [
                            'school', 
                            'route' => function($query){
                                $query->with(
                                    [
                                        'typeHs' => function ($select) {
                                            $select->with('driver');
                                        }
                                    ]
                                );
                            }
                        ])
                    ->where("child_status" ,1)
                    ->get();
            if ($amGetChildLists->count()) {
                foreach ($amGetChildLists->toArray() as $key => $amGetChildList) {
                    $response[$key]['id'] = strval($amGetChildList['child_id']);
                    $response[$key]['fname'] = $amGetChildList['child_fname'];
                    $response[$key]['mname'] = $amGetChildList['child_mname'];
                    $response[$key]['lname'] = $amGetChildList['child_lname'];
                    $response[$key]['free_trial_activate'] = $amGetChildList['free_trial_activate'];
                    $response[$key]['activate_upto'] = (int)($amGetChildList['activate_date'] && $amGetChildList['free_trial_days']) ?  strtotime($amGetChildList['activate_date']. ' + '.$amGetChildList['free_trial_days'].' days') : NULL;

                    $response[$key]['school_id']     = strval($amGetChildList['school']['school_map_id']);
                    $response[$key]['school_name']   = $amGetChildList['school']['school_name'];
                    $response[$key]['school_address'] = $amGetChildList['school']['school_address'];
                    $response[$key]['pickup_lat']    = floatval($amGetChildList['pickup_lat']);
                    $response[$key]['pickup_long']   = floatval($amGetChildList['pickup_long']);
                    $response[$key]['pickup_address'] = $amGetChildList['pickup_address'];
                    $response[$key]['route_code']    = '';
                    $response[$key]['driver_name']   = '';
                    $response[$key]['driver_phone']  = '';
                    $response[$key]['route_start_time'] = (int) 0;
                    $response[$key]['route_end_time'] = (int) 0;

                    if(isset($amGetChildList['route']) && $amGetChildList['route']) {
                        $response[$key]['route_code']     = $amGetChildList['route']['route_code'];
                        
                        if(isset($amGetChildList['route']['type_hs']) && $amGetChildList['route']['type_hs'])  {
                            $response[$key]['route_start_time'] = intval(strtotime($amGetChildList['route']['type_hs']['start_time']));
                            $response[$key]['route_end_time'] = intval(strtotime($amGetChildList['route']['type_hs']['end_time']));
                         
                            if(isset($amGetChildList['route']['type_hs']['driver']) && $amGetChildList['route']['type_hs']['driver']) {
                                $response[$key]['driver_name']   = $amGetChildList['route']['type_hs']['driver']['driver_name'];
                                $response[$key]['driver_phone']  = $amGetChildList['route']['type_hs']['driver']['user_phone'];
                            }
                        }
                    }
                    
                }
            } else {
                $message = RECORD_NOT_FOUND;
            }
        } else {
            $isSuccess = false;
            $message = USER_NOT_FOUND;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $response]);
    }

    /**
     * Add/Update Child Details API
     */
    public function alterChildDetails(Request $request) {
        $isSuccess;
        $message = '';
        $ArrResponse = (object) [];
        $amDriverId = $request->driver_id;
        $childId = $request->child_id;
        $amGetCurrentUser = Auth::guard("api")->user();
        $parentId = $amGetCurrentUser->id;
        if ($childId == '') {
            $amGetRouteCode = Route::where('route_code', $request->route_code)->first();
            if (isset($amGetRouteCode->route_id) && $amGetRouteCode->route_id) {
                // check route have remain capacity.
                $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" => $amGetRouteCode->route_id  ])->get();
                if(!empty($routeCapacity) && $routeCapacity->count()){
                    
                    $modelChild = new Child;
                    $modelChild->child_fname = $request->fname;
                    $modelChild->child_mname = $request->mname;
                    $modelChild->child_lname = $request->lname;
                    $modelChild->pickup_lat = $request->pickup_lat;
                    $modelChild->pickup_long = $request->pickup_long;
                    $modelChild->pickup_address = $request->pickup_address;
                    $modelChild->parent_id = strval($parentId);
                    $modelChild->school_id = strval($amGetRouteCode->school_id);
                    $modelChild->route_id = strval($amGetRouteCode->route_id);
                    $modelChild->driver_id = strval($amGetRouteCode->driver_id);
                    $modelChild->save();


                    // update child id in route details.
                    $updateRouteDetail = RouteDetail::where(['child_id' => $modelChild->child_id])->update(['child_id'=> 0]);
                    $routeDetail = RouteDetail::where(['child_id' => 0, "route_id" =>$amGetRouteCode->route_id  ])->first();
                    if(isset($routeDetail->route_id) && $routeDetail->route_id){
                        $routeDetail->child_id =  $modelChild->child_id;
                        $routeDetail->save();
                    }

                    $isSuccess = true;
                    $message = 'Child Details inserted successfully.';
                } else {
                    $isSuccess = false;
                    $message = "The route code doesn't have more space available.";
                }
            } else {
                $isSuccess = false;
                $message = ROUTE_NOT_FOUND;
            }
        } else {
            $modelChild = Child::where('child_id', $request->child_id)->first();
            if (isset($modelChild->child_id) && !empty($modelChild)) {
                $amGetRouteCode = Route::where('route_code', $request->route_code)->first();
                
                if (isset($amGetRouteCode->route_id) && $amGetRouteCode->route_id) {
                    
                    // check route code change or not
                    if($modelChild->route_id !=  $amGetRouteCode->route_id) {
                        // check route have remain capacity.
                        $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" => $amGetRouteCode->route_id  ])->get();
                        if(empty($routeCapacity) && !$routeCapacity->count()){
                            $isSuccess = false;
                            $message = "The route code doesn't have more space available.";
                        } else {
                            $modelChild->child_fname = $request->fname;
                            $modelChild->child_mname = $request->mname;
                            $modelChild->child_lname = $request->lname;
                            $modelChild->pickup_lat = $request->pickup_lat;
                            $modelChild->pickup_long = $request->pickup_long;
                            $modelChild->pickup_address = $request->pickup_address;
                            $modelChild->school_id = $amGetRouteCode->school_id;
                            $modelChild->driver_id = $amGetRouteCode->driver_id;
                            $modelChild->route_id = $amGetRouteCode->route_id;
                            $modelChild->save();
                            
                            // update child id in route details.
                            $updateRouteDetail = RouteDetail::where(['child_id' => $modelChild->child_id])->update(['child_id'=> 0]);
                            $routeDetail = RouteDetail::where(['child_id' => 0, "route_id" =>$amGetRouteCode->route_id  ])->first();
                            if(isset($routeDetail->route_id) && $routeDetail->route_id){
                                $routeDetail->child_id =  $modelChild->child_id;
                                $routeDetail->save();
                            }

                            $isSuccess = true;
                            $message = 'Child Details updated successfully.';
                        }

                    } else {
                        // check route have remain capacity.
                        $routeCapacity = RouteDetail::where(['child_id' => 0, "route_id" => $amGetRouteCode->route_id  ])->get();
                        if(empty($routeCapacity) && !$routeCapacity->count()){
                            $isSuccess = false;
                            $message = "The route code doesn't have more space available.";
                        } else {
                            $modelChild->child_fname = $request->fname;
                            $modelChild->child_mname = $request->mname;
                            $modelChild->child_lname = $request->lname;
                            $modelChild->pickup_lat = $request->pickup_lat;
                            $modelChild->pickup_long = $request->pickup_long;
                            $modelChild->pickup_address = $request->pickup_address;
                            $modelChild->school_id = $amGetRouteCode->school_id;
                            $modelChild->driver_id = $amGetRouteCode->driver_id;
                            $modelChild->route_id = $amGetRouteCode->route_id;
                            $modelChild->save();
                            
                            // update child id in route details.
                            $updateRouteDetail = RouteDetail::where(['child_id' => $modelChild->child_id])->update(['child_id'=> 0]);
                            $routeDetail = RouteDetail::where(['child_id' => 0, "route_id" =>$amGetRouteCode->route_id  ])->first();
                            if(isset($routeDetail->route_id) && $routeDetail->route_id){
                                $routeDetail->child_id =  $modelChild->child_id;
                                $routeDetail->save();
                            }

                            $isSuccess = true;
                            $message = 'Child Details updated successfully.';
                        }
                    }
                } else {
                    $isSuccess = false;
                    $message = ROUTE_NOT_FOUND;
                }
            } else {
                $isSuccess = false;
                $message = CHILD_NOT_FOUND;
            }
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Add/Update Child Details API
     */
    public function deleteChild(Request $request) {
        $isSuccess;
        $message = '';
        $ArrResponse = (object) [];

        $amCheckChildExist = Child::where('child_id', $request->child_id)
                ->first();
        if (!empty($amCheckChildExist)) {
            $updatedChildDetails = $amCheckChildExist->delete();
            if ($updatedChildDetails) {
                $isSuccess = true;
                $message = 'Child Details deleted successfully.';
            } else {
                $isSuccess = false;
                $message = "Child Details are not deleted. Please try again.";
            }
        } else {
            $isSuccess = false;
            $message = CHILD_NOT_FOUND;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    // This Funstion is used to update activat free trial
    public function activateFreeTrial(){
        $input = Input::all();
        $isSuccess = true;
        $message = '';
        $data = (object) [];

        if((!isset($input['route_id']) || empty($input['route_id'])) || (!isset($input['child_id']) || empty($input['child_id'])) ) {
            $isSuccess = false;
            $message = "Invalid input.";
        } else {

            $childModel =  Child::where(["route_id" => $input['route_id'],  'child_id' =>  $input['child_id'],  'free_trial_activate' =>  Child::FREE_TRIAL_STATUS_NOT_ACTIVATED])->first();
            
            $routeDetail = RouteDetail::where(["route_id" => $input['route_id'],  'child_id' =>  $input['child_id'], "route_code_available" => FREE_TRIL_AVAILABLE_YES ])->first();

            if(empty($childModel) && !$childModel ) {
                $isSuccess = false;
                $message = "You have already subscribed free trial subscriptions.";
            } else {
                
                $isSuccess =  false;
                $message = "Free trial limit exceeded for this route.";

                if(!empty($routeDetail) && isset($routeDetail->route_id)) {
                    $routeDetail->child_id =  $childModel->child_id;
                    $routeDetail->route_code_available =  FREE_TRIL_AVAILABLE_NO;
                    $routeDetail->save();

                    $childModel->free_trial_days =  DEFAULT_FREE_TRIL_DAYS;

                    $systemConfig =  SystemConfig::where("access_key", "free_trial_activation_days")->first();
                    
                    if(!empty($systemConfig) && isset($systemConfig->id) ){
                        $childModel->free_trial_days =  $systemConfig->value ? $systemConfig->value : DEFAULT_FREE_TRIL_DAYS;
                    }
                    
                    $childModel->free_trial_activate =  Child::FREE_TRIAL_STATUS_ACTIVATED;
                    $childModel->activate_date =  date("Y-m-d H:i:s");
                    $childModel->save();
                    $isSuccess = true;
                    $message = "You have successfully subscribed for free trial subscription.";
                }
            }
        }

        return response()->json(["success" => $isSuccess, "msg" => $message, "result" => $data]);
    }

    // This Funstion is used to send notification to driver that child is not coming today
    public function childNotComing(){
        $input = Input::all();
        $isSuccess = false;
        $message = '';
        $result = (object) [];

        if((!isset($input['route_id']) || empty($input['route_id'])) || (!isset($input['child_id']) || empty($input['child_id'])) ) {
            $message = "Invalid input.";
        } else {
            $message = CHILD_NOT_FOUND;
            
            $childModel =  Child::with(
                [
                    'route'=> function($select){
                        $select->with(
                            [
                                'typeHs' => function ($select){
                                    $select->with(
                                        [
                                            'driver' => function($select){
                                                $select->with(['userToken']);
                                            }
                                        ]
                                    );
                                }
                            ]
                        );
                    }
                ]
            )->where(
                [
                    "route_id" => $input['route_id'],  
                    'child_id' =>  $input['child_id'],
                    'parent_id' =>  Auth::guard('api')->user()->id
                ]
            )->first();

            if(!empty($childModel) && $childModel ) {
                $isSuccess = true;
                $deviceTokens = [];
                
                if(isset($childModel->route->typeHs->driver->userToken) &&  $childModel->route->typeHs->driver->userToken->count() ){

                    foreach ($childModel->route->typeHs->driver->userToken as $key => $data) {
                        if ($data->device_token != '')
                            $deviceTokens[] = $data->device_token;
                    }
                    // \Log::info("device token",['childNotComing' => $deviceTokens]);
                    $data = [
                        'msg_type' => CHILD_NOT_COMING,
                        'msg_text' => 'Child Not Coming',
                        'child_id' => strval($childModel->child_id),
                        'child_fname' => $childModel->child_fname,
                        'child_mname' => $childModel->child_mname,
                        'child_lname' => $childModel->child_lname,
                        'pickup_lat' => floatval($childModel->pickup_lat),
                        'pickup_long' => floatval($childModel->pickup_long),
                        'route_code' => $childModel->route->route_code,
                    ];

                    $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                    $notifications = new NotificationDetail;
                    $notifications->driver_id = $childModel->route->typeHs->driver->id;
                    $notifications->route_id  = $childModel->route->route_id;
                    $notifications->child_id  = $childModel->child_id;
                    $notifications->parent_id = $childModel->parent_id;
                    $notifications->trip_id = 0;
                    $notifications->trip_nearby = 1;
                    $notifications->trip_end = 1;
                    
                    if ($sendPushNotification->success >= 1) {
                        $notifications->trip_start = 1; // notification sent
                    } else {
                        $notifications->trip_start = 0; // notification not sent
                    }
                    $notifications->save();
                    // \Log::info("sendPushNotification",['childNotComing' => $sendPushNotification]);
                }

                $message = "Notification successfully send to driver.";
            }
        }
        return response()->json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }


}
