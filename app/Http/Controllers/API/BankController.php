<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Bank, App\UserBank, Auth, DB, Response, Validator;

class BankController extends CommonController {

    protected $_isSuccess = FALSE;
    protected $_input = [];
    protected $_message;
    protected $_response = [];

    public function __construct() {
        parent::__construct();
    }
    public function getBankDetail() {

        $this->_message = BANK_ERROR;

        $this->_response =  (object)$this->_response;
        $this->_response->bank_list =  (object) [];
        $this->_response->user_bank_details = (object) [];

        $bank_list=  Bank::getBankOptions();
        $user = Auth::guard("api")->user();

        if ( $bank_list->count()) {
            $this->_response->bank_list = $bank_list; 

            if (isset($user->bank) && $user->bank->count() ){
                $this->_response->user_bank_details = $user->bank;             
            }
            $this->_isSuccess = TRUE;
            $this->_message = BANK_FOUND;
        }

        return Response::json(["success" => $this->_isSuccess, "msg" => $this->_message, "result" => $this->_response]);
    }
    
    public function updateBankDetails() {

        $this->_input = Input::all();

        if (isset($this->_input['id']) && $this->_input['id']) {
            $userBank =  UserBank::find($this->_input['id']);
            $this->_message = USER_BANK_UPDATED;
        } else {
            $userBank = new UserBank();
            $this->_message = USER_BANK_CEATED;
        }

        $userBank->user_id =  Auth::guard("api")->user()->id;
        $userBank->bank_id =  $this->_input['bank_id'];
        $userBank->account_number =  $this->_input['account_number'];
        $userBank->ifsc_code    =  $this->_input['ifsc_code'];
        $userBank->bank_address =  !empty($this->_input['bank_address']) ? $this->_input['bank_address'] : NULL;

        $userBank->save();

        $this->_isSuccess = TRUE;

        return Response::json(["success" => $this->_isSuccess, "msg" => $this->_message, "result" => (object)$this->_response]);
    }  
}

