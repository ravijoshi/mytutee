<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\User, App\SystemConfig;
use Response;
use Auth;

class PageController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Contact data API
     */
    public function contactus(Request $request) {
        $isSuccess = true;
        $message = "Contact Us Details";
        $ArrResponse = (object) [];

        $pageModel = Page::where('access_key', "contact_us")->first();
        if (!empty($pageModel) && isset($pageModel->page_content) && $pageModel->page_content) {
            $contactUsAddress =  SystemConfig::where("access_key", "contact_us_address")->first();
            $contactUsEmail =  SystemConfig::where("access_key", "contact_us_email")->first();
            $contactUsPhone =  SystemConfig::where("access_key", "contact_us_phone_number")->first();

            if (!empty($contactUsAddress) && !empty($contactUsEmail) && !empty($contactUsPhone)) {
                $content =  str_replace('{{address}}', $contactUsAddress->value, $pageModel->page_content);
                $content =  str_replace('{{email}}', $contactUsEmail->value, $content);
                $content =  str_replace('{{phone_no}}', $contactUsPhone->value, $content);

                $ArrResponse = [
                    "admin_phone" => $contactUsPhone->value,
                    "admin_email" => $contactUsEmail->value,
                    "content" => $content
                ];
            } else {
                $isSuccess = false;
                $message = "Something wrong. Please try again";
            }
        } else {
            $isSuccess = false;
            $message = "Contact Us Details not found";
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

}
