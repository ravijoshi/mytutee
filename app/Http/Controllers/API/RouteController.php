<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator, Response, Auth, DB;
use App\User, App\Vehicle, App\SchoolList, App\Route, App\School, App\Child, App\RouteType, App\RouteDetail;
use Carbon\Carbon, Exception;


class RouteController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get Route List API
     */
    public function getRouteList(Request $request) {
        $isSuccess;
        $message = 'Route List';
        $result = [];
        try{
            $amGetCurrentUser = Auth::guard("api")->user();
            if (empty($amGetCurrentUser)) {
                throw new Exception(USER_NOT_FOUND);
            }

            $routes = Route::whereHas('driverRoute' , function ($query) use($amGetCurrentUser){
                            $query->where('driver_id', $amGetCurrentUser->id);
                        })
                    ->where("route_status" ,1)
                    ->select( 'route_id','route_code','school_id','is_roud_trip')
                    ->with([
                            'school' => function ($query) {
                                $query->select('id', 'school_name', 'school_map_id', 'school_address', 'school_latitude', 'school_longitude');
                            },
                            'driverRoute' => function ($query) use($amGetCurrentUser){
                                $query->select('*', DB::raw('UNIX_TIMESTAMP(start_time) as start_time'),DB::raw('UNIX_TIMESTAMP(end_time) as end_time'));
                                $query->where('driver_id', $amGetCurrentUser->id);
                                $query->with('driver');
                            },
                            'typeSh'/* => function($query){
                                $query->with('driver');
                            },*/
                        ]
                    );
            $routes = $routes->get();
            if ($routes->count()) {
                $isSuccess = true;
                
                foreach ($routes->toArray() as $key => $_route) {
                    $result[$key]['route_id']       = $_route['route_id'];
                    $result[$key]['route_type']     = $_route['route_type'];
                    $result[$key]['route_code']     = $_route['route_code'];
                    $result[$key]['driver_id']      = $_route['driver_route']['driver_id'];
                    $result[$key]['driver_name']    = $_route['driver_route']['driver']['driver_name'];
                    $result[$key]['driver_no']      = $_route['driver_route']['driver']['user_phone'];
                    $result[$key]['start_lat']      = floatval($_route['driver_route']['start_loc_lat']);
                    $result[$key]['start_long']     = floatval($_route['driver_route']['start_loc_long']);
                    $result[$key]['start_address']  = $_route['driver_route']['start_loc_address'];
                    $result[$key]['start_time']     = intval($_route['driver_route']['start_time']);
                    $result[$key]['end_time']       = intval($_route['driver_route']['end_time']);

                    $result[$key]['sh_start_time']     = intval($_route['type_sh']['sh_start_time']);
                    $result[$key]['sh_end_time']       = intval($_route['type_sh']['sh_end_time']);

                    $result[$key]['school_data']['id']      = $_route['school']['id'];
                    $result[$key]['school_data']['name']    = $_route['school']['school_name'];
                    $result[$key]['school_data']['address'] = $_route['school']['school_address'];
                    $result[$key]['school_data']['lat']     = floatval($_route['school']['school_latitude']);
                    $result[$key]['school_data']['long']    = floatval($_route['school']['school_longitude']);
                    $result[$key]['school_data']['map_id']  = $_route['school']['school_map_id'];
                }
            } else {
                $isSuccess = true;
                $message = RECORD_NOT_FOUND;
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

    /**
     * Add Route Details API
     route_type --> 0 HS , 1 SH , 2 =BOTH
     */
    public function create() {
        $input = Input::all();
        $isSuccess;
        $message = '';
        $result = (object) [];
        $amRouteId = 1;

        try{
            if($input['route_type'] == TRIP_TYPE_SCHOOL_HOME ){
                throw new Exception("You can't add School to Home route.");
            }


            $input['start_time'] =   Carbon::createFromTimestamp($input['start_time'])->toDateTimeString(); 
            $input['end_time'] =  Carbon::createFromTimestamp($input['end_time'])->toDateTimeString(); 

            $lastRouteDetail = Route::latest()->first();
            if (!empty($lastRouteDetail)) {
                $amRouteId = ($lastRouteDetail->route_id) + 1;
            }

            $driverId = Auth::guard("api")->user()->id;
            $routeCode = 'RO' . $amRouteId . 'DR' . $driverId;
                
            $schoolModel = School::find($input['school_id']);
            if (empty($schoolModel)) {
                throw new Exception(SCHOOL_NOT_FOUND);
            }

            $modelRoute = new Route;
            $modelRoute->route_code = $routeCode;
            if($input['route_type'] == TRIP_TYPE_ROUND ){
                $modelRoute->is_roud_trip = IS_ROUND_TRIP_YES;
            } else {
                $modelRoute->is_roud_trip = IS_ROUND_TRIP_NO;
            }

            $modelRoute->school_id = $schoolModel->id;
            $modelRoute->end_loc_lat = !empty($schoolModel) ?  $schoolModel->school_latitude :  NULL;
            $modelRoute->end_loc_long = !empty($schoolModel) ?  $schoolModel->school_longitude :  NULL;
            

            $routeSave = $modelRoute->save();

            $input['driver_id'] = $driverId;
            $input['sh_driver_id'] = $driverId;
            $input['end_loc_lat']  = !empty($schoolModel) ?  $schoolModel->school_latitude :  NULL;
            $input['end_loc_long'] = !empty($schoolModel) ?  $schoolModel->school_longitude :  NULL;
            $input['end_loc_address'] = !empty($schoolModel) ?  $schoolModel->school_address :  NULL;

            if ($routeSave) {

                // save route type
                $routeType =  new RouteType();
                $routeType->saveRouteType($modelRoute, $input);

                if(isset($input['sh_start_time']) && !empty($input['sh_start_time'])){
                    $input['start_time'] =   Carbon::createFromTimestamp($input['sh_start_time'])->toDateTimeString(); 
                }
                if(isset($input['sh_end_time']) && !empty($input['sh_end_time'])){
                    $input['end_time'] =   Carbon::createFromTimestamp($input['sh_end_time'])->toDateTimeString(); 
                }
                $routeType->saveRouteType($modelRoute, $input, false);

                $vehicleModel = Vehicle::where('driver_id', $input['driver_id'])
                        ->where('vehicle_status', 1)
                        ->first();
                
                if (!empty($vehicleModel)) {
                    $count = $vehicleModel->vehicle_capacity;
                    for ($i = 0; $i < $count; $i++) {
                        
                        $randomCode = strtoupper(bin2hex(random_bytes(3))); // 6 digits
                        
                        $modelRouteDetail = new RouteDetail;
                        $modelRouteDetail->route_id = $modelRoute->route_id;
                        $modelRouteDetail->route_promo_code = $randomCode;
                        $modelRouteDetail->save();
                    }
                }

                $isSuccess = true;
                $message = 'Route Details inserted successfully.';
            } else {
                $isSuccess = false;
                $message = "Route Details are not inserted. Please try again.";
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

    /**
     * Update Route Details API
     route_type --> 0 HS , 1 SH , 2 =BOTH
     */
    public function update() {
        $input = Input::all();
        $isSuccess;
        $message = '';
        $result = (object) [];

        try{
            $routeModel = Route::where('route_id', $input['route_id'])->first();
            if (empty($routeModel)) {
                throw new Exception(ROUTE_NOT_FOUND);
            }


            if($input['route_type'] == TRIP_TYPE_SCHOOL_HOME &&   $routeModel->is_roud_trip == IS_ROUND_TRIP_YES){
                throw new Exception("You can't add School to Home route. In Round trip route.");
            }
            $driverId = Auth::guard("api")->user()->id;
            $input['start_time'] =   Carbon::createFromTimestamp($input['start_time'])->toDateTimeString(); 
            $input['end_time'] =  Carbon::createFromTimestamp($input['end_time'])->toDateTimeString(); 

            $schoolModel = School::find($input['school_id']);
            if (empty($schoolModel)) {
                throw new Exception(SCHOOL_NOT_FOUND);
            }
            $routeModel->is_roud_trip = IS_ROUND_TRIP_NO;
            if($input['route_type'] == TRIP_TYPE_ROUND ) {
                $routeModel->is_roud_trip = IS_ROUND_TRIP_YES;
            }
            $routeModel->school_id = $schoolModel->id;
            $routeModel->end_loc_lat = !empty($schoolModel) ?  $schoolModel->school_latitude :  NULL;
            $routeModel->end_loc_long = !empty($schoolModel) ?  $schoolModel->school_longitude :  NULL;
            
            $input['driver_id'] = $driverId;
            $input['sh_driver_id'] = $driverId;
            $input['end_loc_lat']  = !empty($schoolModel) ?  $schoolModel->school_latitude :  NULL;
            $input['end_loc_long'] = !empty($schoolModel) ?  $schoolModel->school_longitude :  NULL;
            $input['end_loc_address'] = !empty($schoolModel) ?  $schoolModel->school_address :  NULL;

            $updatedRouteDetails =  $routeModel->save();

            if ($updatedRouteDetails) {
                // save route type
                $routeType =  new RouteType();
                if($input['route_type'] == TRIP_TYPE_ROUND ) {
                    $routeType->saveRouteType($routeModel, $input);
                    if(isset($input['sh_start_time']) && !empty($input['sh_start_time'])){
                        $input['start_time'] =   Carbon::createFromTimestamp($input['sh_start_time'])->toDateTimeString(); 
                    }
                    if(isset($input['sh_end_time']) && !empty($input['sh_end_time'])){
                        $input['end_time'] =   Carbon::createFromTimestamp($input['sh_end_time'])->toDateTimeString(); 
                    }
                    $routeType->saveRouteType($routeModel, $input, false);
                } else if($input['route_type'] == TRIP_TYPE_HOME_SCHOOL ) {
                    $routeType->saveRouteType($routeModel, $input);
                } else if($input['route_type'] == TRIP_TYPE_SCHOOL_HOME ) {
                    if(isset($input['sh_start_time']) && !empty($input['sh_start_time'])){
                        $input['start_time'] =   Carbon::createFromTimestamp($input['sh_start_time'])->toDateTimeString(); 
                    }
                    if(isset($input['sh_end_time']) && !empty($input['sh_end_time'])){
                        $input['end_time'] =   Carbon::createFromTimestamp($input['sh_end_time'])->toDateTimeString(); 
                    }
                    $routeType->saveRouteType($routeModel, $input, false);
                }

                $isSuccess = true;
                $message = 'Route Details updated successfully.';
            } else {
                $isSuccess = false;
                $message = "Route Details are not updated. Please try again.";
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

    // This function is used to update School to home driver details.

    public function joinRoute() {
        $input = Input::all();
        $isSuccess;
        $message = '';
        $result = (object) [];
        
        try{
            $routeModel = Route::where('route_code', strtoupper($input['route_code']))->where("route_status" ,1)->first();

            if (isset($routeModel->route_id) && !empty($routeModel->route_id) && !empty($routeModel)) {
                if($routeModel->is_roud_trip == IS_ROUND_TRIP_YES){
                    $isSuccess = false;
                    $message = "You can't Join In Round trip route.";
                } else {
                    $routeTypeModel = RouteType::where(["route_id" => $routeModel->route_id, "route_type" => ROUTE_TYPE_SCHOOL_TO_HOME ])->first();
                    
                    if(!empty($routeTypeModel)) {
                        $routeTypeModel->driver_id = Auth::guard("api")->user()->id;
                        $routeTypeModel->save();

                        $routeModel->is_roud_trip == IS_ROUND_TRIP_NO;
                        $routeModel->save();

                        $isSuccess = true;
                        $message = "You have successfully join route : ". strtoupper($input['route_code']);
                    }else {
                        $isSuccess = false;
                        $message = ROUTE_NOT_FOUND;    
                    }
                }
            } else {
                $isSuccess = false;
                $message = ROUTE_NOT_FOUND;
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

    /**
     * Delete Route Details API
     */
    public function deleteRouteDetails(Request $request) {
        $isSuccess;
        $message = '';
        $ArrResponse = (object) [];

        try{
            if(!isset($request->route_code) || empty($request->route_code)) {
                return Response::json(["success" => false, "msg" => "Invalid input.", "result" => $ArrResponse]);
            }
            $amCheckRouteExist = Route::where('route_code', strtoupper($request->route_code))->where("route_status" ,1)->first();
            $amGetChildLists = Child::where('route_id', $amCheckRouteExist->route_id)->where("child_status" ,1)->get();
            if ($amGetChildLists->count()) {
                $totalChilds = count($amGetChildLists);
                $isSuccess = false;
                $message = "Route not deleted as childs are assiged with this route";
            } else {
                if (!empty($amCheckRouteExist)) {
                    $updatedRouteDetails = $amCheckRouteExist->delete();
                    if ($updatedRouteDetails) {
                        $isSuccess = true;
                        $message = 'Route Details deleted successfully.';
                        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
                    } else {
                        $isSuccess = false;
                        $message = "Route Details are not deleted. Please try again.";
                    }
                } else {
                    $isSuccess = false;
                    $message = RECORD_NOT_FOUND;
                }
            }
        } catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }


    /**
     * Get Route Details API
     */
    public function getRouteDetails(Request $request) {
        $isSuccess = true;
        $message = 'Route Details';
        $result = [];
        try{
            $select = Route::select( 'route_id','route_code','school_id','is_roud_trip')
                        ->where('route_code', strtoupper($request->route_code))
                        ->where("route_status" ,1)
                        ->with([
                                'school' => function ($query) {
                                    $query->select('id', 'school_name', 'school_map_id', 'school_address', 'school_latitude', 'school_longitude');
                                },
                                'typeHs' => function ($query){
                                    $query->select('*', DB::raw('UNIX_TIMESTAMP(start_time) as route_start_time'),DB::raw('UNIX_TIMESTAMP(end_time) as route_end_time'));
                                },
                                'typeSh'
                            ]
                        );
          
            $routeDetail = $select->first();

            if (!empty($routeDetail)) {
                $result['route_code'] =  $routeDetail->route_code;
                $result['route_type'] =  $routeDetail->route_type;
                $result['route_start_time'] =  isset($routeDetail->typeHs->route_start_time) ? $routeDetail->typeHs->route_start_time : NULL;
                $result['route_end_time'] =  isset($routeDetail->typeHs->route_end_time ) ? $routeDetail->typeHs->route_end_time : NULL;
                $result['driver_name']  =  isset($routeDetail->typeHs->driver->driver_name) ? $routeDetail->typeHs->driver->driver_name : NULL;
                $result['driver_phone'] =  isset($routeDetail->typeHs->driver->user_phone) ? $routeDetail->typeHs->driver->user_phone : NULL;

                $result['sh_start_time'] =  isset($routeDetail->typeSh->sh_start_time) ? $routeDetail->typeSh->sh_start_time : NULL;
                $result['sh_end_time'] =  isset($routeDetail->typeSh->sh_end_time ) ? $routeDetail->typeSh->sh_end_time : NULL;

                $result['school_name']  =  isset($routeDetail->school->school_name) ? $routeDetail->school->school_name : NULL;
                $result['school_address'] =  isset($routeDetail->school->school_address) ? $routeDetail->school->school_address : NULL;
                $result['school_id'] =  isset($routeDetail->school->school_map_id) ? $routeDetail->school->school_map_id : NULL;
            } else {
                $result = (object) [];
                $isSuccess = false;
                $message = ROUTE_NOT_FOUND;
            }
        }catch (Exception $e){
            $message = $e->getMessage();
            $isSuccess = false;
        }

        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

}
