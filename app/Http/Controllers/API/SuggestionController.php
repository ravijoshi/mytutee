<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Suggestion;
use Response;
use Auth;

class SuggestionController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Add Suggestion API
     */
    public function suggestions(Request $request) {
        $isSuccess = true;
        $message = "";
        $ArrResponse = (object) [];
        $amGetCurrentUser = Auth::guard("api")->user();
        if (!empty($amGetCurrentUser)) {
            $modelSuggestion = new Suggestion;
            $modelSuggestion->parent_id = $amGetCurrentUser->id;
            $modelSuggestion->suggestion_msg = $request->suggestion_msg;
            $suggestionSave = $modelSuggestion->save();

            if ($suggestionSave) {
                $message = 'Suggestion uploaded successfully';
            } else {
                $isSuccess = false;
                $message = "Suggestions are not inserted. Please try again";
            }
        } else {
            $isSuccess = false;
            $message = USER_NOT_FOUND;
        }
        Return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

}
