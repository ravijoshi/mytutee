<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\User, App\Route, Carbon\Carbon;
use DB, Validator, Auth;

class CommonController extends Controller {

    protected $apiToken;
    protected $_imagePath;
    
    public function __construct() {
        $this->apiToken = uniqid(base64_encode(str_random(32)));
    }

    // This function is used to upload image on local server
    public function uploadImage($imgPath, $folderName) {
        $image = $imgPath;
        $image->getClientOriginalExtension();
        $userImagePath = time() . '.' . $image->getClientOriginalExtension();
        if ($folderName == 'users') {
            $this->_imagePath = 'images/users';
        } else {
            $this->_imagePath = 'images/vehicles';
        }

        $destinationPath = $this->getDirectory();

        $image->move($destinationPath, $userImagePath);
        if (!empty($userImagePath)) {
            $user_profile_img = $userImagePath;
        } else {
            $user_profile_img = '';
        }
        return $user_profile_img;
    }

    // This function is used to check user role.
    public static function checkUserRole($userRole) {
        if ($userRole == User::USER_ROLE_DRIVER_TEXT) {
            $user_role = User::USER_ROLE_DRIVER;
        } else if ($userRole == User::USER_ROLE_PARENT_TEXT) {
            $user_role = User::USER_ROLE_PARENT;
        } else {
            $user_role = '';
        }
        return $user_role;
    }

    // This function is used to set image path.
    public function setImagePath($imagePath = 'images/vehicles' ){
        $this->_imagePath =  $imagePath;
        return $this;
    }

    // This function is used to get directory path.
    public function getDirectory(){
        $this->isDirectoryExists();
        return public_path().DIRECTORY_SEPARATOR.$this->_imagePath;
    }

    // This function is used to check directory exists or not
    public function isDirectoryExists(){
        $path = public_path().DIRECTORY_SEPARATOR.$this->_imagePath;
        if(!is_dir($path)){
            mkdir($path,0777,true);
            chmod($path, 0777);
        }
        return true;
    }

    // This function is used to check file exists
    public function isFileExists($filename){
        if(file_exists($this->getDirectory().DIRECTORY_SEPARATOR.$filename)){
            return true;
        }
        return false;
    }

    //This function is used to remove existing image from server.
    public function removeFile($filename){
        if($this->isFileExists($filename)){
            unlink($this->getDirectory().DIRECTORY_SEPARATOR.$filename);
            return true;
        }
        return false;
    }

    // This function is used to get image url.
    public function getImageUrl($filename){
        if($this->isFileExists($filename));
            return url("public"."/".$this->_imagePath."/".$filename);

        return false;
    }


    // This Function is used to send message to parents.
    public function shareRouteCodeToParent() {
        $input = Input::all();
        $isSuccess = true;
        $message = '';
        $data = (object) [];
        
        if(!count($input['parents']) ||  empty($input['route_id'])) {
            $isSuccess = false;
            $message = "Invalid input.";
        } else {

            $route =  Route::find($input['route_id']);
            if(!empty($route)) {
                foreach ($input['parents'] as $parent) {
                }    
                $isSuccess = true;
                $message = "Route code successfully shared with selected parents.";
            } else{
                $isSuccess = false;
                $message = "Route id not found.";
            }
        }
        return response()->json(["success" => $isSuccess, "msg" => $message, "result" => $data]);
    }

    // This function is used to send/ veryfy OTP.
    public function sendOtp($phoneNo  , $otp ,$template = '' ,  $flag =  true){
        $user = new User();

        if($flag){
            // send otp
            $response = $user->setPhoneNo($phoneNo)->setOTP($otp)->setTemplate($template)->sendOTP($flag);
        } else {
            // verify otp
            $response = $user->setSessionId($phoneNo)->setOTP($otp)->sendOTP($flag);
        }
        return $response;
    }

    // get last date time of current month
    public function getCurrentMonthLastDateTime(){
        return User::getCurrentMonthLastDateTime();
    }

    // get last date  of current month
    public function getCurrentMonthLastDate(){
        return User::getCurrentMonthLastDate();
    }
    
    // get previous month last date time of current month
    public function getPreviousMonthLastDateTime(){
        return User::getPreviousMonthLastDateTime();
    }

    public function getMonthlyEarnings($user, $routeId = "") {
        return $user->getMothlyEarning($user->id, $routeId);
    }

}
