<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\School;
use App\User;
use Response;
use Auth;

class SchoolController extends CommonController {

    public function __construct() {
        parent::__construct();
    }
    /**
     * Get School List API
     */
    public function getSchoolList(Request $request) {
        $ArrResponse = [];
        $isSuccess = true;
        $message = "School List";
        $amGetSchoolLists = School::where("school_status" ,1)->get();
        if ($amGetSchoolLists->count()) {
            foreach ($amGetSchoolLists->toArray() as $key => $amGetSchoolList) {
                $ArrResponse[$key]['id'] = strval($amGetSchoolList['id']);
                $ArrResponse[$key]['name'] = $amGetSchoolList['school_name'];
                $ArrResponse[$key]['address'] = $amGetSchoolList['school_address'];
                $ArrResponse[$key]['lat'] = floatval($amGetSchoolList['school_latitude']);
                $ArrResponse[$key]['long'] = floatval($amGetSchoolList['school_longitude']);
                $ArrResponse[$key]['map_id'] = $amGetSchoolList['school_map_id'];
            }
        } else {
            $isSuccess = false;
            $message = SCHOOL_NOT_FOUND;
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse])
                        ->header('Cache-Control', 'max-age=60, public');
    }

}
