<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DB, Validator, Response, Auth, Exception;
use App\User, App\UserToken, App\Vehicle, App\School, App\Page, App\Suggestion, App\Route, App\Child, App\FcmNotification;
use Carbon\Carbon;

class AuthController extends CommonController {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Register API 
     */
    public function register(Request $request) {
        $result = (object) [];
        $isSuccess = true;
        $message = '';
        try
        {
            $rules = [
                'email' => 'required|email|unique:users,email,null,id,deleted_at,NULL',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $isSuccess = false;
                $message = "The email has already been taken.";

                // if email already exist and not complete registration process start.
                $user =  User::where(
                            [
                                "email" => $request->email, 
                                "user_otp_verification" => ''.User::USER_OTP_VERIFIED_FALSE.''
                            ])->first();

                if(isset($user->id) && $user->id){
                    $isSuccess = true;
                    $message = "User Registered Successfully.";

                    $request->profile_img = 'no-user.png';
                    if (!empty($request->file('user_image'))) {
                        $request->profile_img = $this->uploadImage($request->file('user_image'), USER_IMAGE_FOLDER);
                    }

                    $user =  $this->_registerUser($user, $request);
                    
                    // send otp
                    if(!empty($user->user_phone)){
                        $otpResponse =  $this->sendOtp($user->user_phone, $user->user_otp_token, User::USER_REGISTRATION_OTP_TEMPLATE );
                        if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                            $user->otp_session_id =  $otpResponse['Details'];
                            $user->save();
                        } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error") {
                            throw new Exception($otpResponse['Details']);
                        }
                    }

                    $result = [
                        "user_id" => strval($user->id),
                        "fname" => $user->user_fname,
                        "lname" => $user->user_lname,
                        "email" => $user->email,
                        "phone" => $user->user_phone,
                        "user_image" => !empty($user->user_image) ? $this->setImagePath('images/users')->getImageUrl($user->user_image) : $this->setImagePath('images/users')->getImageUrl("no-user.png"),
                        "access_token" => "",
                        "phone_required" => !empty($user->user_phone) ? false : true,
                        "otp_verified" => $user->user_otp_verification == User::USER_OTP_VERIFIED_FALSE ? false : true,
                        "otp" => $user->user_otp_token,
                    ];
                // if email already exist and not complete registration process end.
                } else {
                    $isSuccess = false;
                    throw new Exception($message);
                }

            } else {
                $modelUser = new User;
                $request->profile_img = 'no-user.png';
                if (!empty($request->file('user_image'))) {
                    $request->profile_img = $this->uploadImage($request->file('user_image'), USER_IMAGE_FOLDER);
                }

                if($request->user_role == User::USER_ROLE_DRIVER_TEXT || $request->user_role == User::USER_ROLE_PARENT_TEXT) {
                    $modelUser =  $this->_registerUser($modelUser,$request);
                    
                    // send otp
                    $otpResponse =  $this->sendOtp($modelUser->user_phone, $modelUser->user_otp_token, User::USER_REGISTRATION_OTP_TEMPLATE );
                    if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                        $modelUser->otp_session_id =  $otpResponse['Details'];
                        $modelUser->save();
                    } else {
                        throw new Exception($otpResponse['Details']);
                    }
                    
                    $result = [
                        "user_id" => strval($modelUser->id),
                        "fname" => $modelUser->user_fname,
                        "lname" => $modelUser->user_lname,
                        "email" => $modelUser->email,
                        "phone" => $modelUser->user_phone,
                        "user_image" => !empty($modelUser->user_image) ? $this->setImagePath('images/users')->getImageUrl($modelUser->user_image) : $this->setImagePath('images/users')->getImageUrl("no-user.png"),
                        "access_token" => $modelUser->api_token,
                        "phone_required" => false,
                        "otp_verified" => false,
                        "otp" => $modelUser->user_otp_token,
                    ];
                    $message = "User Registered Successfully.";
                    
                } else {
                    $isSuccess = false;
                    throw new Exception(INVALID_USER_ROLE);
                }
            }
        } catch (Exception $e){
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $result]);
    }

    protected function _registerUser($userModel, $request){

        $userModel->user_role = User::USER_ROLE_PARENT;
        if ($request->user_role == User::USER_ROLE_DRIVER_TEXT) {
            $userModel->user_role = User::USER_ROLE_DRIVER;
        }
        $userModel->user_fname = !empty($request->fname) ? $request->fname : "";
        $userModel->user_lname = !empty($request->lname) ? $request->lname : "";
        $userModel->email      = !empty($request->email) ? $request->email : "";
        $userModel->user_phone = !empty($request->phone) ? $request->phone : "";
        $userModel->user_image = $request->profile_img;
        $userModel->password   =  !empty($request->password) ? Hash::make($request->password) : "";
        $userModel->api_token  = $this->apiToken;

        $userModel->user_otp_token = $userModel->generateOTP();
        $userModel->user_otp_verification = User::USER_OTP_VERIFIED_FALSE;
        $userModel->user_otp_timeout = $userModel->generateOTPTimeOut();

        $userModel->save();

        $modelUserToken = new UserToken;
        $modelUserToken->user_id = $userModel->id;
        $modelUserToken->access_token = $userModel->api_token;
        $modelUserToken->save();
        
        return $userModel;
    }
    
    /**
     * Login API
     */
    public function login(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try {
            $amCheckUserRole = $this->checkUserRole($request->user_role);
            $user = Auth::attempt(["email" => $request->email, "password" => $request->password, "user_role" => $amCheckUserRole]);
            if ($user) {
                $amGetCurrentUser = Auth::user();
                if($amGetCurrentUser->user_status != 1 ){
                    throw new Exception(ACCOUNT_STATUS_INACTIVE);
                }
                if ($amGetCurrentUser->user_otp_verification == User::USER_OTP_VERIFIED_TRUE) {
                    $amGetCurrentUser->api_token = $this->apiToken;
                    $amGetCurrentUser->save();

                    $modelUserToken = new UserToken;
                    $modelUserToken->user_id = $amGetCurrentUser->id;
                    $modelUserToken->access_token = $amGetCurrentUser->api_token;
                    $modelUserToken->save();

                    $ArrResponse = [
                        "user_id" => strval($amGetCurrentUser->id),
                        'fname' => $amGetCurrentUser->user_fname,
                        'lname' => $amGetCurrentUser->user_lname,
                        'email' => $amGetCurrentUser->email,
                        'phone' => $amGetCurrentUser->user_phone,
                        'user_image' => !empty($amGetCurrentUser->user_image) ? $this->setImagePath('images/users')->getImageUrl($amGetCurrentUser->user_image) : $this->setImagePath('images/users')->getImageUrl("no-user.png"),
                        'access_token' => $amGetCurrentUser->api_token,
                        "phone_required" => !empty($amGetCurrentUser->user_phone) ? false : true,
                        "otp_verified" => true,
                    ];
                    $message = 'User Login Successfully';
                } else {
                    $amGetCurrentUser->user_otp_token =  $amGetCurrentUser->generateOTP();
                    $amGetCurrentUser->user_otp_verification = User::USER_OTP_VERIFIED_FALSE;
                    $amGetCurrentUser->user_otp_timeout = $amGetCurrentUser->generateOTPTimeOut();
                    

                    $ArrResponse = [
                        "user_id" => strval($amGetCurrentUser->id),
                        "fname" => $amGetCurrentUser->user_fname,
                        "lname" => $amGetCurrentUser->user_lname,
                        "email" => $amGetCurrentUser->email,
                        "phone" => $amGetCurrentUser->user_phone,
                        "user_image" => !empty($amGetCurrentUser->user_image) ? $this->setImagePath('images/users')->getImageUrl($amGetCurrentUser->user_image) : $this->setImagePath('images/users')->getImageUrl("no-user.png"),
                        "access_token" => "",
                        "phone_required" => !empty($amGetCurrentUser->user_phone) ? false : true,
                        "otp_verified" => false,
                        "otp" => $amGetCurrentUser->user_otp_token,
                    ];
                    // send otp
                    $otpResponse =  $this->sendOtp($amGetCurrentUser->user_phone, $amGetCurrentUser->user_otp_token, User::USER_REGISTRATION_OTP_TEMPLATE );
                    if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                        $amGetCurrentUser->otp_session_id =  $otpResponse['Details'];
                    } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error"){
                        $isSuccess = false;
                        throw new Exception($otpResponse['Details']);
                    }

                    $amGetCurrentUser->api_token =  $this->apiToken;
                    $amGetCurrentUser->save();

                    $modelUserToken = new UserToken;
                    $modelUserToken->user_id = $amGetCurrentUser->id;
                    $modelUserToken->access_token = $amGetCurrentUser->api_token;
                    $modelUserToken->save();

                    $isSuccess = true;
                    $message = 'User Login Successfully';
                    
                }
            } else {
                $isSuccess = false;
                throw new Exception('Invalid Credentials');
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Login/Register with Google API
     */
    public function googleSigning(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            $amCheckUserRole = $this->checkUserRole($request->user_role);
            $amCheckGoogleIdExist = User::where('user_social_id', $request->google_id)->first();
            if ( !empty($amCheckGoogleIdExist) && isset($amCheckGoogleIdExist->id)) {
                
                if ($amCheckGoogleIdExist->user_role == $amCheckUserRole) {

                    $amCheckGoogleIdExist->user_fname = $request->fname;
                    $amCheckGoogleIdExist->user_lname = $request->lname;
                    $amCheckGoogleIdExist->user_image_url = $request->user_image;
                    $amCheckGoogleIdExist->api_token = $this->apiToken;
                    $amCheckGoogleIdExist->save();

                    $modelUserToken = new UserToken;
                    $modelUserToken->user_id = $amCheckGoogleIdExist->id;
                    $modelUserToken->access_token = $amCheckGoogleIdExist->api_token;
                    $modelUserToken->save();

                    $ArrResponse = [
                        "user_id" => strval($amCheckGoogleIdExist->id),
                        "fname" => $amCheckGoogleIdExist->user_fname,
                        "lname" => $amCheckGoogleIdExist->user_lname,
                        "email" => $amCheckGoogleIdExist->email,
                        "phone" => $amCheckGoogleIdExist->user_phone,
                        "access_token" => $amCheckGoogleIdExist->api_token,
                        "user_image" => !empty($amCheckGoogleIdExist->user_image_url) ? $amCheckGoogleIdExist->user_image_url : "",
                        "phone_required" => !empty($amCheckGoogleIdExist->user_phone) ? false : true,
                        "otp_verified" => $amCheckGoogleIdExist->user_otp_verification == User::USER_OTP_VERIFIED_FALSE ? false : true,
                    ];

                    $message = 'Login with google successfull';
                    if(!empty($amCheckGoogleIdExist->user_phone) && $amCheckGoogleIdExist->user_otp_verification == User::USER_OTP_VERIFIED_FALSE){
                        // send otp
                        $otpResponse =  $this->sendOtp($amCheckGoogleIdExist->user_phone, $amCheckGoogleIdExist->user_otp_token ,User::USER_REGISTRATION_OTP_TEMPLATE);
                        if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                            $amCheckGoogleIdExist->otp_session_id =  $otpResponse['Details'];
                            $amCheckGoogleIdExist->save();
                        } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error"){
                            $message = $otpResponse['Details'];
                        }
                    }
                    
                    $isSuccess = true;
                } else {
                    $isSuccess = false;
                    throw new Exception(INVALID_USER_ROLE);
                }
            } else {
                $amCheckEmailExist = User::where('email', $request->email)->first();
                if (!empty($amCheckEmailExist) && isset($amCheckEmailExist->id)) {
                   
                    $amCheckEmailExist->user_fname = $request->fname;
                    $amCheckEmailExist->user_lname = $request->lname;
                    $amCheckEmailExist->user_social_id = $request->google_id;
                    $amCheckEmailExist->user_image_url = $request->user_image;
                    $amCheckEmailExist->api_token = $this->apiToken;

                    $message = 'Registration with google successfull';
                    if(!empty($amCheckEmailExist->user_phone) && $amCheckEmailExist->user_otp_verification == User::USER_OTP_VERIFIED_FALSE){
                        // send otp
                        $amCheckEmailExist->user_otp_token =  $amCheckEmailExist->generateOTP();

                        $otpResponse =  $this->sendOtp($amCheckEmailExist->user_phone, $amCheckEmailExist->user_otp_token ,User::USER_REGISTRATION_OTP_TEMPLATE);
                        if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                            $amCheckEmailExist->otp_session_id =  $otpResponse['Details'];
                        } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error"){
                            $message = $otpResponse['Details'];
                        }

                    }
                    $amCheckEmailExist->save();

                    $modelUserToken = new UserToken;
                    $modelUserToken->user_id = $amCheckEmailExist->id;
                    $modelUserToken->access_token = $amCheckEmailExist->api_token;
                    $modelUserToken->save();

                    $amCheckEmailExist = User::where('email', $request->email)->first();
                    $ArrResponse = [
                        "user_id" => strval($amCheckEmailExist->id),
                        "fname" => $amCheckEmailExist->user_fname,
                        "lname" => $amCheckEmailExist->user_lname,
                        "email" => $amCheckEmailExist->email,
                        "access_token" => $amCheckEmailExist->api_token,
                        "user_image" => !empty($amCheckEmailExist->user_image_url) ? $amCheckEmailExist->user_image_url : "",
                        "phone_required" => !empty($amCheckEmailExist->user_phone) ? false : true,
                        "otp_verified" => $amCheckEmailExist->user_otp_verification == User::USER_OTP_VERIFIED_FALSE ? false : true,
                        "otp" => $amCheckEmailExist->user_otp_token,
                    ];
                } else {
                    $modelUser = new User;
                    $user_profile_img = $request->user_image;
                    
                    if ($amCheckUserRole) {

                        $modelUser->user_role = $amCheckUserRole;
                        $modelUser->user_fname = !empty($request->fname) ? $request->fname : "";
                        $modelUser->user_lname = !empty($request->lname) ? $request->lname : "";
                        $modelUser->email = !empty($request->email) ? $request->email : "";
                        $modelUser->user_social_id = !empty($request->google_id) ? $request->google_id : "";
                        $modelUser->user_image_url = $user_profile_img;
                        $modelUser->api_token = $this->apiToken;
                        
                        $message = 'User registered with Google';
                        if(!empty($modelUser->user_phone) && $modelUser->user_otp_verification == User::USER_OTP_VERIFIED_FALSE){
                            // send otp
                            $modelUser->user_otp_token =  $modelUser->generateOTP();
                            $otpResponse =  $this->sendOtp($modelUser->user_phone, $modelUser->user_otp_token ,User::USER_REGISTRATION_OTP_TEMPLATE);
                            if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                                $modelUser->otp_session_id =  $otpResponse['Details'];
                            } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error"){
                                $message = $otpResponse['Details'];
                            }
                        }
                        $user = $modelUser->save();
                        if ($user) {

                            $modelUserToken = new UserToken;
                            $modelUserToken->user_id = $modelUser->id;
                            $modelUserToken->access_token = $modelUser->api_token;
                            $modelUserToken->save();

                            $ArrResponse = [
                                "user_id" => strval($modelUser->id),
                                "fname" => $modelUser->user_fname,
                                "lname" => $modelUser->user_lname,
                                "email" => $modelUser->email,
                                "user_image" => $modelUser->user_image_url,
                                "access_token" => $modelUser->api_token,
                                "phone_required" => true,
                                "otp_verified" => false,
                               // "api_token" => $modelUser['api_token'],
                               // "otp" => $modelUser['user_otp_token'],
                            ];
                            
                        } else {
                            $isSuccess = false;
                            throw new Exception('Registration failed, please try again');
                        }
                    } else {
                        $isSuccess = false;
                        throw new Exception(INVALID_USER_ROLE);
                    }
                }
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Opt Verification API
     */
    public function otpVerification(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            $amCheckUserRole = $this->checkUserRole($request->user_role);
            if ($amCheckUserRole) {
                $user = User::where('email', $request->email)
                        ->where('user_otp_token', $request->otp)
                        ->where('user_role', $amCheckUserRole)
                        ->first();
                if (!empty($user) && isset($user->id) && $user->id ) {
                    // verify otp
                    $otpResponse =  $this->sendOtp($user->otp_session_id, $request->otp, User::USER_REGISTRATION_OTP_TEMPLATE, false);

                    if(isset($otpResponse['Status']) && $otpResponse['Status'] != "Success"){
                        $isSuccess = false;
                        throw new Exception($otpResponse['Details']);
                    } else {
                        $user->user_otp_verification = User::USER_OTP_VERIFIED_TRUE;
                        $user->save();

                        $access_token = $user->api_token;
                        // otpVerification for reset password
                        if(isset($request->type) && !empty($request->type) && $request->type == OTP_VERIFICATION_TYPE_FORGOT_PASSWORD) {
                            $access_token =  "";
                        }
                        $ArrResponse = [
                            "access_token" => $access_token,
                        ];
                        $message = 'Otp Verified Successfully';
                    }
                } else {
                    throw new Exception('Otp not valid, please try again');
                }
            } else {
                throw new Exception(INVALID_USER_ROLE);
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Update User Mobile Number API
     */
    public function userSigningMobileUpdate(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            $amCheckUserRole = $this->checkUserRole($request->user_role);
            if ($amCheckUserRole) {
                $user = User::where('email', $request->email)
                       // ->where('user_status', '1')
                        ->where('user_role', $amCheckUserRole)
                       // ->where('user_social_id', $request->google_id)
                        ->first();
                if (isset($user->id) && $user->id ) {
                    $user->user_phone = $request->phone;

                    $user->user_otp_token = $user->generateOTP();
                    $user->user_otp_verification = User::USER_OTP_VERIFIED_FALSE;
                    $user->user_otp_timeout = $user->generateOTPTimeOut();

                    // send otp
                    $otpResponse =  $this->sendOtp($user->user_phone, $user->user_otp_token, User::USER_REGISTRATION_OTP_TEMPLATE);

                    if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                        $user->otp_session_id =  $otpResponse['Details'];
                    } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error"){
                        throw new Exception($otpResponse['Details']);
                    }

                    $user->save();

                    if (isset($otpResponse['Status']) && $otpResponse['Status'] == "Success") {
                        $ArrResponse = [
                            'otp' => $user->user_otp_token
                        ];
                        $message = 'Otp Sent Successfully';
                    } else {
                        $isSuccess = false;
                        throw new Exception('Mobile number not updated, Please try again');
                    }
                } else {
                    $isSuccess = false;
                    throw new Exception(USER_NOT_FOUND);
                }
            } else {
                throw new Exception(INVALID_USER_ROLE);
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Forgot Password API
     */
    public function forgotPassword(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            $amCheckUserRole = $this->checkUserRole($request->user_role);
            if ($amCheckUserRole) {
                $user = User::where('email', $request->email)
                       // ->where('user_status', '1')
                        ->where('user_role', $amCheckUserRole)
                        ->first();

                if (isset($user->id) && $user->id) {
                    if(empty($user->user_phone) || strlen($user->user_phone) < 8 )
                        throw new Exception("We didn't have your updated mobile number in system. Please contact Admin or Update your mobile number.");
                        
                    $user->user_otp_token =  $user->generateOTP();
                    // send otp
                    $otpResponse =  $this->sendOtp($user->user_phone, $user->user_otp_token, User::USER_RESET_PASSWORD_OTP_TEMPLATE);
                    if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Success"){
                        $user->otp_session_id =  $otpResponse['Details'];
                    } else if(isset($otpResponse['Status']) && $otpResponse['Status'] == "Error"){
                        throw new Exception($otpResponse['Details']);
                    }

                    $user->save();
                    
                    $ArrResponse = [
                        'otp' => $user->user_otp_token
                    ];
                    $message = 'Otp Sent Successfully';
                    
                } else {
                    throw new Exception(USER_NOT_FOUND);
                }
            } else {
                throw new Exception(INVALID_USER_ROLE);
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Reset Password API
     */
    public function resetPassword(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            $rules = [
                'password' => 'required:users,password',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                throw new Exception('Password is required');
            } else {
                $amCheckUserRole = $this->checkUserRole($request->user_role);
                if ($amCheckUserRole) {
                    $user = User::where('email', $request->email)
                           // ->where('user_status', '1')
                            ->where('user_role', $amCheckUserRole)
                            ->first();
                    if ($user) {
                        $user->password = Hash::make($request->password);
                        if ($user->save()) {
                            $message = 'Password changed Successfully';
                        } else {
                            $isSuccess = false;
                            throw new Exception('Password not changed. Please try again');
                        }
                    } else {
                        $isSuccess = false;
                        throw new Exception(USER_NOT_FOUND);
                    }
                } else {
                    $isSuccess = false;
                    throw new Exception(INVALID_USER_ROLE);
                }
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }

        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Logout API
     */
    public function postLogout(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            $amGetCurrentUser = Auth::guard("api")->user();
            if ($amGetCurrentUser) {
                $accessToken = $request->bearerToken();
                $removeUserDeviceToken = User::where('id', $amGetCurrentUser->id)->update(['api_token' => null]);
                $logoutUser = UserToken::where('access_token', $accessToken)->delete();
                if ($logoutUser) {
                    $message = 'User Logged out';
                } else {
                    throw new Exception('User not Logged out. Please try again');
                }
            } else {
                throw new Exception(USER_NOT_FOUND);
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Update Device Token API - called after user login successfully
     */
    public function updateDeviceToken(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        try{
            if(!isset($request->device_token) || (isset($request->device_token) && empty($request->device_token))){
                throw new Exception("Device token can't be empty.");
            } else {
                $tokenModel = UserToken::where('access_token', Auth::guard("api")->user()->api_token)->where("user_id", Auth::guard("api")->user()->id)->first();
                if (isset($tokenModel->id) && !empty($tokenModel->id)) {
                    $tokenModel->device_token = $request->device_token;
                    $tokenModel->save();

                    $tokenModel->deleteOldData();
                    $tokenModel->deleteOldDeviceToken();
                    $message = 'Device token updated Successfully';
                } else {
                    throw new Exception('Device token not updated, Please try again');
                }
            }
        } catch (Exception $e) {
            $isSuccess = false;
            $message =  $e->getMessage();
        }
        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

    /**
     * Make test API to send push notification
     * Used in startTrip, endTrip and trackVehicleLocation APIs.
     */
    public function sendPushNotificaion(Request $request) {
        $ArrResponse = (object) [];
        $isSuccess = true;
        $message = '';
        $recipients = [$request->device_token];
        $data = [
            'msg_type' => TRIP_STARTED,
            'msg_text' => 'Trip Started',
        ];
        $sendPushNotification = FcmNotification::sendPushNotification($recipients, $data);
        if ($sendPushNotification->success == 1) {
            $message = 'Notification sent';
        } else {
            $isSuccess = false;
            $message = "Notification is not sent";
        }

        return Response::json(["success" => $isSuccess, "msg" => $message, "result" => $ArrResponse]);
    }

}
