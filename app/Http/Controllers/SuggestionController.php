<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect, Mail;
use App\Suggestion , App\User;

class SuggestionController extends Controller
{
    public function index() {
        return View('pages/suggestion/index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'user_name',
            1 => 'suggestion_msg',
            2 => 'suggestion_stauts',
            3 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = Suggestion::orderBy($order, $dir);
        $select->select("suggestions.*",DB::raw('CONCAT(users.user_fname," ",users.user_lname) AS user_name'),DB::raw('SUBSTR(suggestion_msg, 1, 150) AS suggestion_msg'))
                ->leftJoin('users', 'users.id', '=', 'suggestions.parent_id');

        
        if (!empty($search) ) {
        
            $select = $select->where(function($query)use($search) {
                $query->where('suggestions.suggestion_msg', 'LIKE', "%{$search}%");
                $query->orWhere(DB::raw('CONCAT(users.user_fname," ",users.user_lname)'), 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $suggestions =  $select->offset($start)->limit($limit)->get();
        

        $data = array();
        if (!empty($suggestions)) {
            
            foreach ($suggestions as $_suggestion) {
                
                $nestedData['user_name'] = $_suggestion->user_name;
                $nestedData['suggestion_msg'] = $_suggestion->suggestion_msg;
                
                if ($_suggestion->suggestion_stauts == 1) {
                    $nestedData['suggestion_stauts'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_suggestion->suggestion_id, this);'>Read</button>";
                } else {
                    $nestedData['suggestion_stauts'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_suggestion->suggestion_id, this);'>Unread</button>";
                }
                $viewElement = $_suggestion->suggestion_id.',"'.$_suggestion->user_name.'"';
                $nestedData['action'] = "<button  title='View' class='btn btn-info btn-sm'  onclick='viewMessage(" . $viewElement . ");' ><span class='fa fa-eye'></span></button> <a href=".route("suggestion.reply")."/".$_suggestion->suggestion_id." class='btn btn-success btn-sm' title='Reply' > <span class='fa fa-reply'> </span> </a>";
                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function view($id = "") {
    	$message = "Suggestion found successfully.";
        $responseType = "success";
        $title ="";
        $content ="";
        if(empty($id) || !isset($id)) {
            $message = "Invalid Suggestion id.";
            $responseType = "failure";
        } else {
            $suggestion = Suggestion::find($id);

            if(!empty($suggestion)) {
                $content = $suggestion->suggestion_msg;
                $title = "Suggetion message from ".$suggestion->suggestion_msg;
            } else {
                $message = "Suggestion not found.";
                $responseType = "failure";
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "suggestion" =>$content,
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content =  "Read";
        if(empty($id) || !isset($id)) {
            $message = "Invalid Suggestion id.";
            $responseType = "failure";
        } else {
            $suggestion = Suggestion::find($id);

            if(empty($suggestion)) {
                $message = "Suggestion not found.";
                $responseType = "failure";
            } else {
                if ($suggestion->suggestion_stauts == 1) {
                    $suggestion->suggestion_stauts = 0;
                    $content =  "Unread";
                } else {
                    $suggestion->suggestion_stauts = 1;
                }
                $suggestion->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" =>  $content
        ]);
    }
    

    public function reply($id = "") {
        try{
            if(empty($id))
                throw new \Exception("Invalid id.");

            $suggestion =  Suggestion::find($id);

            if(empty($suggestion) && !count($suggestion))
                throw new \Exception("Suggestion not found.");
            
            return View('pages/suggestion/reply', compact('suggestion'));    
        } catch (\Exception $e) {
           return  back()->with('error', $e->getMessage());
        }
    }

    public function replyPost(Request $request) {
        $request->validate([
                'reply' => 'required',
            ]);
        try{
            if(empty($request->id))
                throw new \Exception("Invalid id.");

            $suggestion =  Suggestion::find($request->id);

            if(empty($suggestion) && !count($suggestion))
                throw new \Exception("Suggestion not found.");

            $mailtemplateModel = \App\Mailtemplate::where(["access_key" => "suggestion_reply","status" => 1])->first();
            
            if(empty($mailtemplateModel) && !$mailtemplateModel) {
                throw new \Exception("Mailtemplate not found.");
            }
            $mail_body = $mailtemplateModel->content;
            $mail_body = str_replace('{{name}}', $suggestion->parent->user_fname ." ".$suggestion->parent->user_lname, $mail_body);
            $mail_body = str_replace('{{suggestion_msg}}', $suggestion->suggestion_msg, $mail_body);
            $mail_body = str_replace('{{reply}}', "  <b>Reply:</b> ". $request->reply, $mail_body);
            $mail_body = str_replace('{{website_url}}', url("/"), $mail_body);
            $mail_body = str_replace('{{website_name}}',env('APP_NAME', 'My Tutee'), $mail_body);
            $mail_body = str_replace('{{emailLogo}}', asset('public/images/logo.png'), $mail_body);
            $mail_body = str_replace('{{logoUrl}}', asset('public/logo.png'), $mail_body);

            $mailData = [
                "from" => FROM_EMAIL,
                "from_title" => FROM_EMAIL_TITLE,
                "subject" => str_replace('{{website_name}}', env('APP_NAME', 'My Tutee') , $mailtemplateModel->subject) ,
                "to" => $suggestion->parent->email,
                "full_name" => $suggestion->parent->user_fname . ' ' . $suggestion->parent->user_lname,
                "mail_body" => $mail_body
                ];

            Mail::send('email.mailcontent', ['data' => $mailData], function ($m) use ($mailData) {
                $m->from($mailData['from'], $mailData['from_title']);

                $m->to($mailData['to'],$mailData['full_name'])->subject($mailData['subject']);
            });
            
            if( count(Mail::failures()) > 0 ) {
               throw new \Exception("Mail not send somthing went wrong.");
            }
            
            $suggestion->reply =  $request->reply;
            $suggestion->suggestion_stauts =  1;
            $suggestion->save();
            return redirect()->route('suggestion')->with("success", "Reply Mail sent successfully.");

        } catch (\Exception $e) {
           return  back()->with('error', $e->getMessage());
        }
    }
}
