<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect;
use App\Route , App\RouteType, App\RouteDetail, App\User, App\School, App\Vehicle;

class RouteController extends Controller
{
    public function index() {
        return View('pages/route/index');
    }

    public function grid(Request $request) {
        
        $columns = array(
            0 => 'route_code',
            1 => 'user_name',
            2 => 'school_name',
            3 => 'start_time',
            4 => 'end_time',
            5 => 'route_status',
            6 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = Route::orderBy($order, $dir);
        $select = $select->select('routes.route_id','routes.route_code','routes.route_status','school_id','RT.driver_id', DB::raw('DATE_FORMAT((RT.start_time), "%h:%i %p") AS start_time'),DB::raw('DATE_FORMAT((RT.end_time), "%h:%i %p") AS end_time'),'schools.school_name' , DB::raw('CONCAT(users.user_fname," ",users.user_lname) AS user_name')  );
        
        $select->join('route_types AS RT', function($join)
        {
            $join->on('RT.route_id', '=', 'routes.route_id');
            $join->where('RT.route_type', '=', ROUTE_TYPE_HOME_TO_SCHOOL);
        });

        $select->leftJoin('schools', 'schools.id', '=', 'routes.school_id');
        $select->leftJoin('users', 'users.id', '=', 'RT.driver_id');

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->where('routes.route_code', 'LIKE', "%{$search}%");
                $q->orWhere('RT.start_time', 'LIKE', "%{$search}%");
                $q->orWhere('RT.end_time', 'LIKE', "%{$search}%");
                $q->orWhere(DB::raw('CONCAT(users.user_fname," ",users.user_fname)'), 'like', "%{$search}%");
                $q->orWhere('schools.school_name', 'like', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $routes =  $select->offset($start)->limit($limit)->get();
      
        $data = array();
        if (!empty($routes)) {
            
            foreach ($routes as $_route) {
                $Editlink = route('route.edit', $_route->route_id);
                
                $nestedData['route_code'] =  "<a title='View Route' href='".route("holisticview")."?view=route&id=".$_route->route_id."'> ".$_route->route_code." </a>";
                $nestedData['user_name'] = "<a  title='View Driver' href='".route("holisticview")."?view=driver&id=".$_route->driver_id."'> ".$_route->user_name." </a>";
                $nestedData['school_name'] = "<a  title='View School' href='".route("holisticview")."?view=school&id=".$_route->school_id."'> ".$_route->school_name." </a>";
                $nestedData['start_time'] =  $_route->start_time;
                $nestedData['end_time'] =  $_route->end_time;
                

                if ($_route->route_status == 1) {
                    $nestedData['route_status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_route->route_id, this);'>Active</button>";
                } else {
                    $nestedData['route_status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_route->route_id, this);'>Inactive</button>";
                }
                $childs = \App\Child::where('route_id', $_route->route_id)->get();
                if($childs->count()){
                    $viewElement = 'alert("You can not able to delete this router. This Route is already used  by children."); return false;';
                } else {
                    $viewElement = $_route->route_id.',"'.$_route->route_code.'","'.FALSE.'"';    
                    $viewElement = "getDelConfirmation(" . $viewElement . ")";
                }
                
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>  <button type='button' class='btn btn-danger btn-sm' onclick='" . $viewElement . "'><i class='fa fa-trash'></i></button>";

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    public function create() {

        $driverOptions =  User::where('user_role',User::USER_ROLE_DRIVER)->select( DB::raw("CONCAT(user_fname,' ',user_lname) AS name"),'id')->pluck('name' , 'id');
        $schoolOptions =  School::pluck('school_name' , 'id');
        return view('pages/route/create', compact("driverOptions","schoolOptions"));
    }

    public function edit($id ="") {
    	if(empty($id) || !isset($id)) {
            return redirect()->route("route")->with("error","invalid Route id.");
        }

        
        $route = Route::find($id);
        
        if(empty($route))
        {
            return redirect()->route("route")->with("error","Route not found.");   
        }
        $driverOptions =  User::where('user_role',User::USER_ROLE_DRIVER)->select( DB::raw("CONCAT(user_fname,' ',user_lname) AS name"),'id')->pluck('name' , 'id');
        $schoolOptions =  School::pluck('school_name' , 'id');
        
        return view('pages.route.edit', compact('route',"driverOptions","schoolOptions"));
    }

    public function save(Request $request) {
        $id = $request->input('id');
        $is_roud_trip = $request->input('is_roud_trip');
        $msg = [
            "end_time.after" => "HS End time must greater than HS start time.",
            "sh_start_time.after" => "SH Start time must greater than HS end time.",
            "sh_end_time.after" => "SH End time must greater than SH start time."
        ];
        if (isset($id) && $id  && isset($is_roud_trip) && $is_roud_trip ) {
            $request->validate([
                'school_id' => 'required',
                'start_loc_address' => 'required',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|date_format:H:i|after:start_time',
                'sh_start_time' => 'required|date_format:H:i|after:end_time',
                'sh_end_time' => 'required|date_format:H:i|after:sh_start_time',
                'is_roud_trip' => 'required',
                'driver_id' => 'required',
                'route_status' => 'required|in:0,1',
                'id' => 'required',
            ],$msg);
        } else if (isset($is_roud_trip) && $is_roud_trip){
            $request->validate([
                'school_id' => 'required',
                'start_loc_address' => 'required',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|date_format:H:i|after:start_time',
                'sh_start_time' => 'required|date_format:H:i|after:end_time',
                'sh_end_time' => 'required|date_format:H:i|after:sh_start_time',
                'is_roud_trip' => 'required',
                'driver_id' => 'required',
                'route_status' => 'required|in:0,1',
            ],$msg);
         } else {
            $request->validate([
                'driver_id' => 'required',
                'sh_driver_id' => 'required',
                'school_id' => 'required',
                'start_loc_address' => 'required',
                'start_time' => 'required|date_format:H:i',
                'end_time' => 'required|date_format:H:i|after:start_time',
                'sh_start_time' => 'required|date_format:H:i|after:end_time',
                'sh_end_time' => 'required|date_format:H:i|after:sh_start_time',
                'route_status' => 'required|in:0,1',
            ],$msg);
         }
        
        $postData = $request->all(); 

        if (isset($request->id) && !empty($request->id)) {

            $route = Route::find($request->id);
            if(empty($route)) {
                return redirect()->route("route")->with("error","Route not found.");                
            }
            $message = "Route updated Sucessfully";

        } else {
            $route = new Route();
            $message = "Route added Sucessfully";
            $routeId = 1;
            $lastRuoteId = Route::latest()->first();
            
            if (!empty($lastRuoteId)) {
                $routeId = ($lastRuoteId->route_id) + 1;
            }
            $route->route_code = 'RO' . $routeId . 'DR' . $request->driver_id;
        }
        
        $schoolModel =  School::find($request->school_id);

        $route->is_roud_trip = IS_ROUND_TRIP_NO;
        if (isset($is_roud_trip) && $is_roud_trip){
            $route->is_roud_trip = IS_ROUND_TRIP_YES;
            $postData['sh_driver_id'] = $request->driver_id;
        }
        
        $route->school_id = $request->school_id;
        $route->end_loc_lat = !empty($schoolModel) ?  $schoolModel->school_latitude :  NULL;
        $route->end_loc_long = !empty($schoolModel) ?  $schoolModel->school_longitude :  NULL;
        
        $route->route_status = $request->route_status;
        $route->save();


        $postData['end_loc_lat']  = !empty($schoolModel) ?  $schoolModel->school_latitude :  NULL;
        $postData['end_loc_long'] = !empty($schoolModel) ?  $schoolModel->school_longitude :  NULL;
        $postData['end_loc_address'] = !empty($schoolModel) ?  $schoolModel->school_address :  NULL;
        // save route type
        $routeType =  new RouteType();
        
        $routeType->saveRouteType($route, $postData);
        $postData['start_time'] = $postData['sh_start_time'];
        $postData['end_time'] = $postData['sh_end_time'];
        $routeType->saveRouteType($route, $postData, false);

         if (!isset($request->id) && !$request->id) {
            $vehicleModel = Vehicle::where('driver_id', $request->driver_id)
                    ->where('vehicle_status', 1)
                    ->first();

            if (!empty($vehicleModel)) {
                $count = $vehicleModel->vehicle_capacity;
                for ($i = 0; $i < $count; $i++) {
                    
                    $randomCode = strtoupper(bin2hex(random_bytes(3))); // 6 digits
                    
                    $modelRouteDetail = new RouteDetail;
                    $modelRouteDetail->route_id = $route->route_id;
                    $modelRouteDetail->route_promo_code = $randomCode;
                    $modelRouteDetail->save();
                }
            }
        }

        return redirect()->route("route")->with("success",$message);
    }


    public function delete($id = "") {
        $message = "Route deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid route id.";
            $responseType = "failure";
        }

        $route = Route::find($id);
        
        if(empty($route))
        {
            $message = "Route not found.";
            $responseType = "failure";
        } else{

            $route->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Route id.";
            $responseType = "failure";
        } else {
            $route = Route::find($id);

            if(empty($route)) {
                $message = "Route not found.";
                $responseType = "failure";
            } else {
                if ($route->route_status == 1) {
                    $route->route_status = 0;
                    $content = "Inactive";
                } else {
                    $route->route_status = 1;
                }
                $route->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" =>$content,
        ]);
    }
    
}
