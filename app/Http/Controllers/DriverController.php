<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Validator, Hash, Redirect, DB;
use App\User , App\UserBank, App\Vehicle, App\RouteType, App\UserEarning;

class DriverController extends BaseController {

    public function index() {
        return View('pages/driver/index');
    }

    public function getDriverList(Request $request) {
        $columns = array(
            0 => 'user_name',
            1 => 'email',
            2 => 'user_phone',
            3 => 'earning',
            4 => 'user_status',
            5 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];

        /*if($order = "earning"){
            $order =  "UE.amount";
        }*/
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = User::with(['bank'])->withTrashed()->where('user_role',User::USER_ROLE_DRIVER)->select("users.*", DB::raw('CONCAT(user_fname," ",user_lname) AS user_name'), DB::raw("IF(users.deleted_at IS NOT NULL,1,0) as hard_delete"))->orderBy($order, $dir);

        

        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->orWhere(DB::raw('CONCAT(user_fname," ",user_lname)'), 'LIKE', "%{$search}%");
                $q->orWhere('email', 'LIKE', "%{$search}%");
                $q->orWhere('user_phone', 'LIKE', "%{$search}%");
                $q->orWhereHas("getUserEarning", function ($query) use ($search){
                    // $query->where( DB::raw('SUM(amount)'), "LIKE", "%{$search}%");
                    $query->having( \DB::raw('CONCAT("'.DEFAULT_CURRENCY_SYMBOL.'"," ",SUM(amount))'), "LIKE", "%{$search}%");
                });
            });
        }

        $totalFiltered = $totalData = $select->count();
        $drivers =  $select->offset($start)->limit($limit)->get();
        // echo "<pre>";
        // print_r($drivers->toArray());die;
        $data = array();
        if (!empty($drivers)) {
            
            
            foreach ($drivers as $_driver) {
                $Editlink = route('driver.edit', $_driver->id);
                
                $isHardDeleted =  false;
                if($_driver->hard_delete== 1)
                    $isHardDeleted =  true;
                $href =  $isHardDeleted ? "javascript:void(0);" :  route("holisticview")."?view=driver&id=".$_driver->id;
                $nestedData['user_name'] = "<a  title='View Driver' href='".$href."'>". $_driver->user_name ."</a>"  .'<span class="bg-danger"  title="Bank details not found." style="margin-left:10px !important"><i class="fa fa-check-circle" aria-hidden="true" style="color:red"  ></i> </span>';
                if(!empty($_driver->bank)){
                    $nestedData['user_name'] = "<a  title='View Driver' href='".$href."'>". $_driver->user_name ."</a>"   .'<span class="bg-success" title="'.$_driver->bank->bank_name .'   '. $_driver->bank->ifsc_code.'" style="margin-left:10px !important"><i class="fa fa-check-circle" aria-hidden="true" style="color:green"></i> </span>';
                }
                $nestedData['email'] = $_driver->email;
                $nestedData['user_phone'] = $_driver->user_phone;
                // $nestedData['earning'] = isset($_driver->totalEarning) ? $_driver->totalEarning : DEFAULT_CURRENCY_SYMBOL." ".number_format(0,2);
                $nestedData['earning'] = isset($_driver->getUserEarning->totalEarning) ? $_driver->getUserEarning->totalEarning : DEFAULT_CURRENCY_SYMBOL." ".number_format(0,2);

                $viewElement = $_driver->id.',"'.$_driver->user_name.'"';

                if($isHardDeleted){

                    $nestedData['action'] = "<button type='button' class='btn btn-warning btn-sm' title='Hard Delete' onclick='hardDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button> <button type='button' class='btn btn-warning btn-sm' title='Restore' onclick='restoreUser(" . $viewElement . ");'><i class='fa fa-undo'></i></button>";
                } else {
                    $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>";

                    $routes =  RouteType::where("driver_id" ,$_driver->id)->get();

                    $onclick = "getDelConfirmation(" . $viewElement . ")";
                    if ($routes->count()) {
                        $onclick = 'alert("You can not able to delete this driver. This Driver is mapped with routes."); return false;';
                    }
                    $nestedData['action'] .= " <button type='button' class='btn btn-danger btn-sm' title='Delete' onclick='".$onclick."'><i class='fa fa-trash'></i></button>";
                }


                $disabledAttr =  $isHardDeleted ? "disabled= 'disabled'" : "";
                if ($_driver->user_status == "1") {

                    $nestedData['user_status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_driver->id, this);' ".$disabledAttr.">Active</button>";
                } else {
                    $nestedData['user_status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_driver->id,this);' ".$disabledAttr.">Inactive</button>";
                }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function edit($id = "") {
        if(empty($id) || !isset($id)) {
            return redirect()->route("drivers")->with("error","invalid Driver user id.");
        }

        $driver = User::with('vehicle')->where(['user_role' => User::USER_ROLE_DRIVER, "id" => $id ])->first();
        if(empty($driver)) {
            return redirect()->route("drivers")->with("error","Driver user not found.");   
        }
        $totalEarning  = UserEarning::where(["type" => UserEarning::EARNING_TYPE_CREDIT,"user_id"=> $driver->id])->sum("amount");
        return view('pages.driver.edit', ['driver' => $driver, 'earning' => number_format($totalEarning,2)]);
    }

    public function delete($id = "") {
        $message = "Driver user deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid driver user id.";
            $responseType = "failure";
        }

        $driverUser = User::where(['user_role' => User::USER_ROLE_DRIVER, "id" => $id ])->first();
        
        if(empty($driverUser))
        {
            $message = "Driver user not found.";
            $responseType = "failure";
        } else{

            $driverUser->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function forcedelete($id = "") {
        $message = "Driver user deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid driver user id.";
            $responseType = "failure";
        }

        $driverUser = User::withTrashed()->where(['user_role' => User::USER_ROLE_DRIVER, "id" => $id ])->first();
        
        if(empty($driverUser)) {
            $message = "Driver user not found.";
            $responseType = "failure";
        } else{
            $driverUser->forcedelete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function restore($id = "") {
        $message = "Driver user restore successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid driver user id.";
            $responseType = "failure";
        }

        $driverUser = User::withTrashed()->where(['user_role' => User::USER_ROLE_DRIVER, "id" => $id ])->first();
        
        if(empty($driverUser)) {
            $message = "Driver user not found.";
            $responseType = "failure";
        } else {
            $checkEmailExist =  User::where(DB::raw("LOWER(email)"), strtolower($driverUser->email))->first();
            if(!empty($checkEmailExist) && isset($checkEmailExist->email)){
                $message = "Email already exist in system you can't restore this user.";
                $responseType = "failure";       
            } else{
                $driverUser->restore();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }
    public function create() {
        return view('pages/driver/create');
    }

    public function save(Request $request) {
        
        $validationMessage =  ["user_fname.required" => "The user first name field is required.","user_lname.required" => "The user last name field is required."];
        $id = $request->input('id');
        if (isset($id) && $id) {
            $request->validate([
                'user_fname' => 'required',
                'user_lname' => 'required',
                'email' => 'required|email|unique:users,email,'.$id.',id,deleted_at,NULL',
                'user_phone' => 'required',
                // 'user_role' => 'required|in:'.User::USER_ROLE_ADMIN.",".User::USER_ROLE_DRIVER.",".User::USER_ROLE_PARENT,
                'user_status' => 'required|in:0,1',
                'id' => 'required',
            ],$validationMessage);
        } else {
             $request->validate([
                'user_fname' => 'required',
                'user_lname' => 'required',
                'email' => 'required|email|unique:users,email,null,id,deleted_at,NULL',
                'user_phone' => 'required',
                // 'user_role' => 'required|in:'.User::USER_ROLE_ADMIN.",".User::USER_ROLE_DRIVER.",".User::USER_ROLE_PARENT,
                'user_status' => 'required|in:0,1',
                // 'id' => 'nullable',
            ],$validationMessage);
         }

        if (isset($request->id) && !empty($request->id)) {

            $driverUser = User::find($request->id);
            if(empty($driverUser))
            {
                return redirect()->route("drivers")->with("error","Driver details not found.");                
            }
            $message = "Driver details updated Sucessfully";
        } else {
            $driverUser = new User();
            $driverUser->user_otp_token = $driverUser->generateOTP();
            $driverUser->user_otp_verification = User::USER_OTP_VERIFIED_TRUE;
            $driverUser->user_otp_timeout = $driverUser->generateOTPTimeOut();
            $message = "Driver details added Sucessfully";
        }
        
        $driverUser->user_fname = $request->user_fname;
        $driverUser->user_lname = $request->user_lname;
        $driverUser->email = $request->email;
        $driverUser->user_phone = $request->user_phone;
        $driverUser->user_status = $request->user_status;
        $driverUser->user_role = User::USER_ROLE_DRIVER;
        $driverUser->save();

        if(isset($request->bank) && !empty($request->bank)){
            $bank =  UserBank::where("user_id" , $driverUser->id)->first();

            if(empty($bank)){
                $bank =  new UserBank();
            }

            $bank->user_id =  $driverUser->id;
            $bank->bank_id =  $request->bank_id;
            $bank->account_number =  $request->account_number;
            $bank->ifsc_code =  $request->ifsc_code;
            $bank->bank_address =  $request->bank_address ? $request->bank_address : NULL;

            $bank->save();
        }

        if(isset($request->vehicle) && !empty($request->vehicle)){
            $vehicle =  Vehicle::where("driver_id" , $driverUser->id)->first();

            if(empty($vehicle)){
                $vehicle =  new Vehicle();
            }

            $vehicle->driver_id =  $driverUser->id;
            $vehicle->vehicle_owner_name =  $request->vehicle_owner_name;
            $vehicle->vehicle_capacity =  $request->vehicle_capacity;
            $vehicle->vehicle_reg_no =  $request->vehicle_reg_no;
            $vehicle->vehicle_school_id =  $request->vehicle_school_id;
            $vehicle->vehicle_status = 1;

            $vehicle->save();
        }
        return redirect()->route("drivers")->with("success",$message);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Driver user id.";
            $responseType = "failure";
        } else {
            $driverUser = User::find($id);

            if(empty($driverUser)) {
                $message = "Driver user not found.";
                $responseType = "failure";
            } else {
                if ($driverUser->user_status == "1") {
                    $driverUser->user_status = "0";
                    $content = "Inactive";
                } else {
                    $driverUser->user_status = "1";
                }
                $driverUser->save();
            }

        }
        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content"=>$content 
        ]);
    }

}