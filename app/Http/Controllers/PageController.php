<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect;
use App\Page;

class PageController extends Controller
{
    public function index() {
        return View('pages/page/index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'page_name',
            1 => 'access_key',
            2 => 'page_status',
            3 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = Page::orderBy($order, $dir);

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->where('page_name', 'LIKE', "%{$search}%");
                $q->orWhere('access_key', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $pages =  $select->offset($start)->limit($limit)->get();
        

        $data = array();
        if (!empty($pages)) {
            
            foreach ($pages as $_page) {
                $Editlink = route('page.edit', $_page->page_id);
                
                $nestedData['page_name'] = $_page->page_name;
                $nestedData['access_key'] = $_page->access_key;
                
                if ($_page->page_status == "1") {
                    $nestedData['page_status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_page->page_id, this);'>Active</button>";
                } else {
                    $nestedData['page_status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_page->page_id, this);'>Inactive</button>";
                }
                $viewElement = $_page->page_id.',"'.$_page->page_name.'"';
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a> ";//" <button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function create() {
        return View('pages/page/create');
    }

    public function edit($id ="") {
    	if(empty($id) || !isset($id)) {
            return redirect()->route("page")->with("error","invalid Page id.");
        }

        $page = Page::find($id);
        if(empty($page))
        {
            return redirect()->route("page")->with("error","Page not found.");   
        }
        return view('pages.page.edit', compact('page'));
    }

    public function save(Request $request) {

        $id = $request->input('id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'page_name' => 'required',
                'access_key' => 'required|unique:pages,access_key,'.$id.',page_id,deleted_at,NULL',
                'page_content' => 'required',
                'page_status' => 'required|in:0,1',
                'id' => 'required',
            ]);
        } else {
             $input = $request->validate([
                'page_name' => 'required',
                'access_key' => 'required|unique:pages,access_key,null,page_id,deleted_at,NULL',
                'page_content' => 'required',
                'page_status' => 'required|in:0,1',
            ]);
         }
        
        if (isset($input['id']) && !empty($input['id'])) {

            $page = Page::find($input['id']);
            if(empty($page)) {
                return redirect()->route("page")->with("error","Page not found.");                
            }
            $message = "Page updated Sucessfully";
        } else {
            $page = new Page();
            $message = "Page added Sucessfully";
        }
        
        $page->page_name = $input["page_name"];
        $page->access_key = $input["access_key"];
        $page->page_content = $input["page_content"];
        $page->page_status = $input["page_status"];
        $page->save();

        return redirect()->route("page")->with("success",$message);
    }


    public function delete($id = "") {
        $message = "Page deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid page id.";
            $responseType = "failure";
        }

        $page = Page::find($id);
        
        if(empty($page))
        {
            $message = "Page not found.";
            $responseType = "failure";
        } else{

            $page->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Page id.";
            $responseType = "failure";
        } else {
            $page = Page::find($id);

            if(empty($page)) {
                $message = "Page not found.";
                $responseType = "failure";
            } else {
                if ($page->page_status == 1) {
                    $page->page_status = 0;
                    $content = "Inactive";
                } else {
                    $page->page_status = 1;
                }
                $page->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" =>$content,
        ]);
    }

    public function parentHelp() {
        $page = Page::where("access_key","parent_help")->first();
        $content =  isset($page->page_content) ? $page->page_content : "";
        return View('pages.page.cms' ,["content" => $content, "title" => "Help"]);
    }

    public function driverHelp() {
        $page = Page::where("access_key","driver_help")->first();
        $content =  isset($page->page_content) ? $page->page_content : "";
        return View('pages.page.cms',["content" => $content, "title" => "Help"]);
    }

    public function parentTerms() {
        $page = Page::where("access_key","parent_terms")->first();
        $content =  isset($page->page_content) ? $page->page_content : "";
        return View('pages.page.cms' ,["content" => $content, "title" => "Terms & Conditions"]);
    }

    public function driverTerms() {
        $page = Page::where("access_key","driver_terms")->first();
        $content =  isset($page->page_content) ? $page->page_content : "";
        return View('pages.page.cms',["content" => $content, "title" => "Terms & Conditions"]);
    }
    
}
