<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Validator;
use Hash;
use Redirect;
use DB;
use App\User;

class ParentController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function index() {
        return View('pages/parent/index');
    }

    public function getParentList(Request $request) {
        $columns = array(
            0 => 'user_name',
            1 => 'email',
            2 => 'user_phone',
            3 => 'user_status',
            4 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = User::withTrashed()->where('user_role',User::USER_ROLE_PARENT)->orderBy($order, $dir)->select("*", DB::raw('CONCAT(user_fname," ",user_lname) AS user_name'), DB::raw("IF(deleted_at IS NOT NULL,1,0) as hard_delete"));

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->orWhere(DB::raw('CONCAT(user_fname," ",user_lname)'), 'LIKE', "%{$search}%");
                $q->orWhere('email', 'LIKE', "%{$search}%");
                $q->orWhere('user_phone', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $parents =  $select->offset($start)->limit($limit)->get();

        $data = array();
        if (!empty($parents)) {
            
            foreach ($parents as $_parent) {
                
                $nestedData['user_name'] = $_parent->user_name;
                $nestedData['email'] = $_parent->email;
                $nestedData['user_phone'] = $_parent->user_phone;
                
                $isHardDeleted =  false;
                if($_parent->hard_delete== 1)
                    $isHardDeleted =  true;

                $disabledAttr =  $isHardDeleted ? "disabled= 'disabled'" : "";
                if ($_parent->user_status == "1") {
                    $nestedData['user_status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_parent->id, this);' ".$disabledAttr.">Active</button>";
                } else {
                    $nestedData['user_status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_parent->id, this);' ".$disabledAttr.">Inactive</button>";
                }

                $Editlink = route('parent.edit', $_parent->id);
                $childLink = route('children', $_parent->id);
                $viewElement = $_parent->id.',"'.$_parent->user_name.'"';

                if($isHardDeleted){
                    $nestedData['action'] = " <button type='button' class='btn btn-warning btn-sm' title='Hard Delete' onclick='hardDelConfirmation(" . $viewElement . ");'> <i class='fa fa-trash'></i></button> <button type='button' class='btn btn-warning btn-sm' title='Restore' onclick='restoreUser(" . $viewElement . ");'> <i class='fa fa-undo'></i></button>";
                } else {
                    $nestedData['action'] = "<a href='" . $childLink . "' target='_blank' title='View Children' class='btn btn-info btn-sm' ><span class='fa fa-'>View Children</span></a> ";
                    $nestedData['action'] .= "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>";
                    $nestedData['action'] .= " <button type='button' class='btn btn-danger btn-sm' title='Delete' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";
                }
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function edit($id = "") {
        if(empty($id) || !isset($id)) {
            return redirect()->route("parents")->with("error","invalid parent user id.");
        }

        $parent = User::where(['user_role' => User::USER_ROLE_PARENT, "id" => $id ])->first();
        if(empty($parent)) {
            return redirect()->route("parents")->with("error","Parent user not found.");   
        }
        return view('pages.parent.edit', compact('parent'));
    }

    public function delete($id = "") {
        $message = "Parent user deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid parent user id.";
            $responseType = "failure";
        }

        $parentUser = User::where(['user_role' => User::USER_ROLE_PARENT, "id" => $id ])->first();
        
        if(empty($parentUser)) {
            $message = "Parent user not found.";
            $responseType = "failure";
        } else{

            $parentUser->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);

    }

    public function forcedelete($id = "") {
        $message = "Parent user deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid parent user id.";
            $responseType = "failure";
        }

        $parentUser = User::withTrashed()->where(['user_role' => User::USER_ROLE_PARENT, "id" => $id ])->first();
        
        if(empty($parentUser)) {
            $message = "Parent user not found.";
            $responseType = "failure";
        } else{
            $parentUser->forcedelete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }
    
    public function restore($id = "") {
        $message = "Parent user restore successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid parent user id.";
            $responseType = "failure";
        }

        $parentUser = User::withTrashed()->where(['user_role' => User::USER_ROLE_PARENT, "id" => $id ])->first();
        
        if(empty($parentUser)) {
            $message = "Parent user not found.";
            $responseType = "failure";
        } else{
            $checkEmailExist =  User::where(DB::raw("LOWER(email)"), strtolower($parentUser->email))->first();
            if(!empty($checkEmailExist) && isset($checkEmailExist->email)){
                $message = "Email already exist in system you can't restore this user.";
                $responseType = "failure";       
            } else{
                $parentUser->restore();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);

    }

    public function create() {
        return view('pages/parent/create');
    }

    public function save(Request $request) {
        $validationMessage =  ["user_fname.required" => "The user first name field is required.","user_lname.required" => "The user last name field is required."];
        $id = $request->input('id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'user_fname' => 'required',
                'user_lname' => 'required',
                'email' => 'required|email|unique:users,email,'.$id.',id,deleted_at,NULL',
                'user_phone' => 'required',
                // 'user_role' => 'required|in:'.User::USER_ROLE_ADMIN.",".User::USER_ROLE_DRIVER.",".User::USER_ROLE_PARENT,
                'user_status' => 'required|in:0,1',
                'id' => 'required',
                'nearby_distance' => 'required|in:'.User::DISTANCE_100.','.User::DISTANCE_200.','.User::DISTANCE_300.','.User::DISTANCE_400.','.User::DISTANCE_500.','.User::DISTANCE_1000.'',
            ],$validationMessage);
        } else {
             $input = $request->validate([
                'user_fname' => 'required',
                'user_lname' => 'required',
                'email' => 'required|email|unique:users,email,null,id,deleted_at,NULL',
                'user_phone' => 'required',
                // 'user_role' => 'required|in:'.User::USER_ROLE_ADMIN.",".User::USER_ROLE_DRIVER.",".User::USER_ROLE_PARENT,
                'user_status' => 'required|in:0,1',
                'nearby_distance' => 'required|in:'.User::DISTANCE_100.','.User::DISTANCE_200.','.User::DISTANCE_300.','.User::DISTANCE_400.','.User::DISTANCE_500.','.User::DISTANCE_1000.'',
                // 'id' => 'nullable',
            ],$validationMessage);
         }

        if (isset($input['id']) && !empty($input['id'])) {

            $parentUser = User::find($input['id']);
            if(empty($parentUser))
            {
                return redirect()->route("parents")->with("error","Parent details not found.");                
            }
            $message = "Parent details updated Sucessfully";
        } else {
            $parentUser = new User();
            $message = "Parent details added Sucessfully";

            $parentUser->user_fname = $input["user_fname"];
            $parentUser->user_otp_token = $parentUser->generateOTP();
            $parentUser->user_otp_verification = User::USER_OTP_VERIFIED_TRUE;
            $parentUser->user_otp_timeout = $parentUser->generateOTPTimeOut();
        }
        
        $parentUser->user_fname = $input["user_fname"];
        $parentUser->user_lname = $input["user_lname"];
        $parentUser->email = $input["email"];
        $parentUser->user_phone = $input["user_phone"];
        $parentUser->nearby_distance = $input["nearby_distance"];
        $parentUser->user_status = $input["user_status"];
        $parentUser->user_role = User::USER_ROLE_PARENT;
        $parentUser->save();

        return redirect()->route("parents")->with("success",$message);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content =  "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Parent user id.";
            $responseType = "failure";
        } else {
            $parentUser = User::find($id);

            if(empty($parentUser)) {
                $message = "Parent user not found.";
                $responseType = "failure";
            } else {
                if ($parentUser->user_status == "1") {
                    $parentUser->user_status = "0";
                    $content = "Inactive";
                } else {
                    $parentUser->user_status = "1";
                }
                $parentUser->save();
            }

        }
        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" => $content
        ]);
    }

}