<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect;
use App\User, App\Route, App\Child, App\RouteDetail, App\RouteType, App\School;

class HolisticController extends Controller
{
    public function index(Request $request) {
        $viewType = $request->has('view') ? $request->input('view') : '';
        if (empty($viewType) || !in_array($viewType, ['route','driver','school', 'children'])) {
            return Redirect::back()->with("error", "Invalid view selected.");
        } else {

            $viewType = $request->input('view');
            $id = $request->input('id');
            $results;
            if($viewType == 'route'){
                $results = Route::with(
                                [
                                    'school',
                                    'typeSh' =>function ($select){
                                        $select->with(['driver']);
                                    },
                                    'typeHs' =>function ($select){
                                        $select->with(['driver']);
                                    },
                                    'routeChildren'
                                ])->where("route_id",  $id)->first();
            }else if($viewType == 'school'){
                $results = Route::with(
                                [
                                    'school',
                                    'typeSh' =>function ($select){
                                        $select->with(['driver']);
                                    },
                                    'typeHs' =>function ($select){
                                        $select->with(['driver']);
                                    },
                                    'routeChildren'
                                ])->where("school_id",  $id)->get();
            }else if($viewType == 'driver') {
                $results = RouteType::with(
                                [
                                    'route' => function ($select){
                                        $select->with(['routeChildren','school']);
                                    },
                                    'driver',
                                ])
                ->where("driver_id",  $id)
                ->groupBy("route_id")
                ->get();

            }else if($viewType == 'children'){
                $results = RouteDetail::with(
                                [
                                    'route' => function ($select){
                                        $select->with(
                                        [
                                            'typeSh' => function ($select){
                                                $select->with(['driver']);
                                            },
                                            'typeHs' =>function ($select){
                                                $select->with(['driver']);
                                            }
                                        ]);
                                    },
                                    'child' => function ($select){
                                            $select->with(['parent','school']);
                                        }
                                ])
                ->where("route_id",  $id)
                ->where("child_id", "!=",  0)
                ->get();
            }

            // echo "<pre>";
            // print_r($results->toArray());die;
            return view("pages/holisticview/view", ["view" => $viewType , "results" => $results]);
        }
    }
    
}
