<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect,Carbon\Carbon;
use App\User, App\Child, App\RouteType, App\UserEarning, App\Trip, App\SystemConfig, App\FcmNotification, App\NotificationDetail, App\CronjobLog;

class CronjobController extends Controller
{
    
    public function childrenArrivedAtSchool () {

        $currentDateTime =  date("Y-m-d H:i:s");
        $defaultMins = CHILDREN_ARRIVE_AT_SCHOOL;
        $systemConfig =  SystemConfig::where("access_key", "children_arrive_at_school")->first();

        if(!empty($systemConfig) && isset($systemConfig->id) ){
            $defaultMins = (int) $systemConfig->value ? $systemConfig->value : CHILDREN_ARRIVE_AT_SCHOOL;
        }
        
        $trips =  Trip::select("trip_id","route_id","driver_id")
            ->with(
            [
                'driverDetail'=> function($select) {
                    $select->with(['userToken' => function($select){
                        $select->select("device_token","user_id");
                    }]);
                },
                'route' => function ($select){
                    $select->select("route_id","route_code","school_id");
                    $select->with(
                        [
                            'school'=>function($select){
                                $select->select('id','school_name');
                            },
                            'typeHs' => function($select) {
                                $select->select("route_id","driver_id");
                                $select->with(['driver' => function($select) {
                                    $select->select("id",\DB::raw("CONCAT(user_fname,' ',user_lname) AS driver_name"));
                                }]);
                            },
                            'child' => function($select) {
                                $select->select("child_id","child_fname","child_mname","child_lname","parent_id","route_id");
                                $select->with(
                                    [
                                        'parent' => function($select){
                                            $select->select("id",\DB::raw("CONCAT(user_fname,' ',user_lname) AS parent_name"));
                                            $select->with(['userToken' => function($select){
                                                $select->select("device_token","user_id");
                                            }]);
                                        }
                                    ]
                                );
                            }
                        ]
                    );
                }
            ]
            )->where(
                [
                    "trip_type" => TRIP_TYPE_HOME_SCHOOL,
                    "is_notification_sent" => IS_NOTIFICATION_SENT_FALSE,
                    "is_nearby_school" => IS_NEARBY_SCHOOL_YES,
                    "trip_status" => 0,
                ]
            )
            ->where(DB::raw('DATE_SUB(nearby_school_at, INTERVAL '.$defaultMins.' MINUTE)') ,'<=', $currentDateTime)
            ->get();
        
        // $isNotificationSentToDriver = false;
        if($trips->count()){
            
            foreach ($trips as $_trip) {
                
                if(isset($_trip->route) && isset($_trip->route->child) && count($_trip->route->child)) {
                    foreach ($_trip->route->child as $key => $_child) {
                        
                        if(isset($_child->parent->id) && !empty($_child->parent->id) && isset($_child->parent->userToken) &&  count($_child->parent->userToken)) {

                            $deviceTokens = [];
                            foreach ($_child->parent->userToken as $key => $_userToken) {
                                if(isset($_userToken->device_token) && $_userToken->device_token) {
                                    $deviceTokens[] =  $_userToken->device_token;
                                }
                            }

                            if(count($deviceTokens)) {
                                $data = [
                                    'msg_type' => CHILD_ARRIVE_SCHOOL,
                                    'msg_text' => 'Your Child drop at school',
                                    'child_id' => strval($_child->child_id),
                                    'child_fname' => $_child->child_fname,
                                    'child_mname' => $_child->child_mname,
                                    'child_lname' => $_child->child_lname,
                                    'route_code'  => $_trip->route->route_code,
                                    'driver_name' => isset($_trip->route->typeHs->driver->id) ? $_trip->route->typeHs->driver->driver_name : '',
                                ];

                                $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                                $notifications = new NotificationDetail;
                                $notifications->driver_id = isset($_trip->route->typeHs->driver->id) ? $_trip->route->typeHs->driver->id : 0;
                                $notifications->route_id  = $_trip->route->route_id;
                                $notifications->child_id  = $_child->child_id;
                                $notifications->parent_id = $_child->parent_id;
                                $notifications->trip_id = $_trip->trip_id;
                                $notifications->trip_nearby = 1;
                                $notifications->trip_end = 1;
                                
                                if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                                    $notifications->trip_start = 1; // notification sent
                                } else {
                                    $notifications->trip_start = 0; // notification not sent
                                }
                                $notifications->save();
                                $cronjobLog = new CronjobLog();

                                $cronjobLog->notification_id = $notifications->id;
                                $cronjobLog->title = "childrenArrivedAtSchool ".json_encode($data);
                                $cronjobLog->save();
                            }
                        }
                    }
                }

                // send end trip notification to driver
                if(isset($_trip->driverDetail->id) && !empty($_trip->driverDetail->id) && isset($_trip->driverDetail->userToken) &&  count($_trip->driverDetail->userToken)) {
                   
                    $deviceTokens = [];
                    foreach ($_trip->driverDetail->userToken as $_userToken) {
                        if(isset($_userToken->device_token) && $_userToken->device_token) {
                            $deviceTokens[] =  $_userToken->device_token;
                        }
                    }
                    if(count($deviceTokens)) {
                        $pushData = [
                            'msg_type' => DRIVER_TRIP_END_REMINDER,
                            'msg_text' => "It's seems that you are reached the school. Please end your current trip.",
                            'route_code'  =>  isset($_trip->route->route_code) ? $_trip->route->route_code : "",
                            'route_id'  =>  isset($_trip->route->route_id) ? $_trip->route->route_id : "",
                            'school_name'  =>  isset($_trip->route->school->school_name) ? $_trip->route->school->school_name : "",
                            'driver_name' => isset($_trip->driverDetail->id) ? $_trip->driverDetail->driver_name : '',
                            'trip_type'     => TRIP_TYPE_HOME_SCHOOL
                        ];

                        $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $pushData);
                        $notifications = new NotificationDetail;
                        $notifications->driver_id = isset($_trip->driverDetail->id) ? $_trip->driverDetail->id : 0;
                        $notifications->route_id  = $_trip->route->route_id;
                        $notifications->child_id  = 0;
                        $notifications->parent_id = 0;
                        $notifications->trip_id = $_trip->trip_id;
                        $notifications->trip_nearby = 1;
                        $notifications->trip_end = 1;
                        
                        if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                            $notifications->trip_start = 1; // notification sent
                        } else {
                            $notifications->trip_start = 0; // notification not sent
                        }
                        $notifications->save();
                        $cronjobLog = new CronjobLog();
                        $cronjobLog->notification_id = $notifications->id;
                        $cronjobLog->title = "DRIVER_HS_TRIP_END_REMINDER ".json_encode($pushData);
                        $cronjobLog->save();
                    }
                }

                $tripModel =  Trip::where('trip_id', $_trip->trip_id)->first();
                $tripModel->is_notification_sent = IS_NOTIFICATION_SENT_TRUE;
                $tripModel->save();
            }
        }
    }


    public function endHSTripNotification () {
        $defaultMins = CHILDREN_ARRIVE_AT_SCHOOL;
        $remindMins =  SystemConfig::where("access_key", "children_arrive_at_school")->first();
        if(!empty($remindMins) && isset($remindMins->id) ){
            $defaultMins = (int) $remindMins->value ? $remindMins->value : CHILDREN_ARRIVE_AT_SCHOOL;
        }

        $systemConfig =  SystemConfig::where("access_key", "admin_time_zone")->first();
        $timezone =  isset($systemConfig->value) ? $systemConfig->value : DEFAULT_TIMEZONE;
        $currentTime =  User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", $timezone);
        $UTCHours = "+00:00";
        $timezoneHoursDiff = "+".User::getDateDiffInFormat(User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", "UTC"), $currentTime,"%H:%I");
        
        $trips =  Trip::select("trip_id","school_name","route_code",DB::raw("trips.route_id"),DB::raw("trips.driver_id"),DB::raw("childs.child_id"),'parent_id','child_fname','child_mname','child_lname', DB::raw("GROUP_CONCAT(device_token ORDER BY device_token ASC SEPARATOR ';') as device_token")/*,DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_ADD(route_types.end_time, INTERVAL ".$defaultMins." MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i') as ctime")*/)
            ->with(
            [
                'driverDetail'=> function($select) {
                    $select->select("id",DB::raw("CONCAT(user_fname,' ' ,user_lname) as driver_name"));
                    $select->with(['userToken' => function($select){
                        $select->select("device_token","user_id");
                    }]);
                },
            ]
            )
            ->join("routes" ,function($join) {
                $join->on("routes.route_id","=","trips.route_id");
                $join->whereNull("routes.deleted_at");
            })
            ->join("schools" ,function($join) {
                $join->on("routes.school_id","=","schools.id");
                $join->whereNull("schools.deleted_at");
            })
            ->join("childs" ,function($join) {
                $join->on("trips.route_id","=","childs.route_id");
                $join->whereNull("childs.deleted_at");
            })
            ->join("users" ,function($join) {
                $join->on("users.id","=","childs.parent_id");
                $join->whereNull("users.deleted_at");
            })
            ->leftJoin("user_tokens" ,function($join) {
                $join->on("user_tokens.user_id","=","users.id");
                $join->where("user_tokens.device_token","!=",NULL);
                $join->whereNull("user_tokens.deleted_at");
            })
            ->join("route_types" ,function($join){
                $join->on("route_types.route_id","=","trips.route_id");
                $join->whereNull("route_types.deleted_at");
            })
            ->where(
                [
                    "trip_type" => TRIP_TYPE_HOME_SCHOOL,
                    "trip_status" => 0,
                    "is_notification_sent" => IS_NOTIFICATION_SENT_FALSE,
                ]
            )
            ->where("route_types.route_type","=",ROUTE_TYPE_HOME_TO_SCHOOL)
            ->where(DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_ADD(route_types.end_time, INTERVAL ".$defaultMins." MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i')") ,'=', $currentTime)
            ->groupBy("childs.child_id")
            ->get();
        
        if($trips->count()) {

            $tripIdArr = [];
            foreach ($trips as $_trip) {
                // send end trip notification to driver
                if(!in_array($_trip->trip_id, $tripIdArr) && isset($_trip->driverDetail->id) && !empty($_trip->driverDetail->id) && isset($_trip->driverDetail->userToken) &&  count($_trip->driverDetail->userToken)) {
                   
                    $deviceTokens = [];
                    foreach ($_trip->driverDetail->userToken as $_userToken) {
                        if(isset($_userToken->device_token) && $_userToken->device_token) {
                            $deviceTokens[] =  $_userToken->device_token;
                        }
                    }
                    if(count($deviceTokens)) {
                        $pushData = [
                            'msg_type' => DRIVER_TRIP_END_REMINDER,
                            'msg_text' => "Your trip end time reached. Please end your current trip.",
                            'route_code'  =>  isset($_trip->route_code) ? $_trip->route_code : "",
                            'route_id'  =>  isset($_trip->route_id) ? $_trip->route_id : "",
                            'school_name'  =>  isset($_trip->school_name) ? $_trip->school_name : "",
                            'driver_name' => isset($_trip->driverDetail->id) ? $_trip->driverDetail->driver_name : '',
                            'trip_type'     => TRIP_TYPE_HOME_SCHOOL
                        ];

                        $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $pushData);
                        $notifications = new NotificationDetail;
                        $notifications->driver_id = isset($_trip->driverDetail->id) ? $_trip->driverDetail->id : 0;
                        $notifications->route_id  = $_trip->route_id;
                        $notifications->child_id  = 0;
                        $notifications->parent_id = 0;
                        $notifications->trip_id = $_trip->trip_id;
                        $notifications->trip_nearby = 1;
                        $notifications->trip_end = 1;
                        $notifications->notification_data = json_encode($pushData);
                        
                        if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                            $notifications->trip_start = 1; // notification sent
                        } else {
                            $notifications->trip_start = 0; // notification not sent
                            $notifications->fcm_response = json_encode($sendPushNotification);
                        }
                        $notifications->save();
                        $cronjobLog = new CronjobLog();
                        $cronjobLog->notification_id = $notifications->id;
                        $cronjobLog->title = "DRIVER_HS_TRIP_END_REMINDER ".json_encode($pushData);
                        $cronjobLog->save();
                    }
                }

                array_push($tripIdArr, $_trip->trip_id);
                // send notifications to parent for your child seems reached at school
                if(isset($_trip->child_id) && !empty($_trip->child_id) && isset($_trip->device_token) && !empty($_trip->device_token) ) {
                    $pushData = [
                        'msg_type' => CHILD_ARRIVE_SCHOOL,
                        'msg_text' => "Your Child drop at school.",
                        'child_id' => $_trip->child_id,
                        'child_fname' => $_trip->child_fname,
                        'child_mname' => $_trip->child_mname,
                        'child_lname' => $_trip->child_lname,
                        'parent_id' => !empty($_trip->parent_id) ? $_trip->parent_id : "",
                        'route_code'  =>  !empty($_trip->route_code) ? $_trip->route_code : "",
                        'driver_name' => isset($_trip->driverDetail->id) ? $_trip->driverDetail->driver_name : '',
                        'trip_type'   => TRIP_TYPE_HOME_SCHOOL
                    ];

                    $sendPushNotification = FcmNotification::sendPushNotification(explode(";", $_trip->device_token), $pushData);
                    $notifications = new NotificationDetail;
                    $notifications->driver_id = isset($_trip->driverDetail->id) ? $_trip->driverDetail->id : 0;
                    $notifications->route_id  = $_trip->route_id;
                    $notifications->child_id  = $_trip->child_id;
                    $notifications->parent_id = !empty($_trip->parent_id) ? $_trip->parent_id : 0;
                    $notifications->trip_id = $_trip->trip_id;
                    $notifications->trip_nearby = 1;
                    $notifications->trip_end = 1;
                    
                    if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                        $notifications->trip_start = 1; // notification sent
                    } else {
                        $notifications->trip_start = 0; // notification not sent
                    }
                    $notifications->save();
                    $cronjobLog = new CronjobLog();
                    $cronjobLog->notification_id = $notifications->id;
                    $cronjobLog->title = "CHILDREN_ARRIVE_AT_SCHOOL: ".json_encode($pushData);
                    $cronjobLog->save();
                }

                $tripModel =  Trip::where('trip_id', $_trip->trip_id)->first();
                $tripModel->is_notification_sent = IS_NOTIFICATION_SENT_TRUE;
                $tripModel->save();
            }
        }
    }

    public function endSHTripNotification () {
        $defaultMins = CHILDREN_ARRIVE_AT_SCHOOL;
        $remindMins =  SystemConfig::where("access_key", "children_arrive_at_school")->first();
        if(!empty($remindMins) && isset($remindMins->id) ){
            $defaultMins = (int) $remindMins->value ? $remindMins->value : CHILDREN_ARRIVE_AT_SCHOOL;
        }

        $systemConfig =  SystemConfig::where("access_key", "admin_time_zone")->first();
        $timezone =  isset($systemConfig->value) ? $systemConfig->value : DEFAULT_TIMEZONE;
        $currentTime =  User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", $timezone);
        $UTCHours = "+00:00";
        $timezoneHoursDiff = "+".User::getDateDiffInFormat(User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", "UTC"), $currentTime,"%H:%I");
        
        $trips =  Trip::select("trip_id","school_name","route_code",DB::raw("trips.route_id"),DB::raw("trips.driver_id"))
            ->with(
            [
                'driverDetail'=> function($select) {
                    $select->select("id",DB::raw("CONCAT(user_fname,' ' ,user_lname) as driver_name"));
                    $select->with(['userToken' => function($select){
                        $select->select("device_token","user_id");
                    }]);
                },
            ]
            )
            ->join("routes" ,"routes.route_id","=","trips.route_id")
            ->join("schools" ,"routes.school_id","=","schools.id")
            ->join("route_types" ,function($join){
                $join->on("route_types.route_id","=","trips.route_id");
            })
            ->where(
                [
                    "trip_type" => TRIP_TYPE_SCHOOL_HOME,
                    "trip_status" => 0,
                    "is_notification_sent" => IS_NOTIFICATION_SENT_FALSE,
                ]
            )
            ->where("route_types.route_type","=",ROUTE_TYPE_SCHOOL_TO_HOME)
            ->where(DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_ADD(route_types.end_time, INTERVAL ".$defaultMins." MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i')") ,'<=', $currentTime)
            ->get();

        if($trips->count()){
            foreach ($trips as $_trip) {
                // send end trip notification to driver
                if(isset($_trip->driverDetail->id) && !empty($_trip->driverDetail->id) && isset($_trip->driverDetail->userToken) &&  count($_trip->driverDetail->userToken)) {
                   
                    $deviceTokens = [];
                    foreach ($_trip->driverDetail->userToken as $_userToken) {
                        if(isset($_userToken->device_token) && $_userToken->device_token) {
                            $deviceTokens[] =  $_userToken->device_token;
                        }
                    }
                    if(count($deviceTokens)) {
                        $pushData = [
                            'msg_type' => DRIVER_TRIP_END_REMINDER,
                            'msg_text' => "Your trip end time reached. Please end your current trip.",
                            'route_code'  =>  isset($_trip->route_code) ? $_trip->route_code : "",
                            'route_id'  =>  isset($_trip->route_id) ? $_trip->route_id : "",
                            'school_name'  =>  isset($_trip->school_name) ? $_trip->school_name : "",
                            'driver_name' => isset($_trip->driverDetail->id) ? $_trip->driverDetail->driver_name : '',
                            'trip_type'     => TRIP_TYPE_SCHOOL_HOME
                        ];

                        $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $pushData);
                        $notifications = new NotificationDetail;
                        $notifications->driver_id = isset($_trip->driverDetail->id) ? $_trip->driverDetail->id : 0;
                        $notifications->route_id  = $_trip->route_id;
                        $notifications->child_id  = 0;
                        $notifications->parent_id = 0;
                        $notifications->trip_id = $_trip->trip_id;
                        $notifications->trip_nearby = 1;
                        $notifications->trip_end = 1;
                        $notifications->notification_data = json_encode($pushData);
                        
                        if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                            $notifications->trip_start = 1; // notification sent
                        } else {
                            $notifications->trip_start = 0; // notification not sent
                            $notifications->fcm_response = json_encode($sendPushNotification);
                        }
                        $notifications->save();
                        $cronjobLog = new CronjobLog();
                        $cronjobLog->notification_id = $notifications->id;
                        $cronjobLog->title = "DRIVER_SH_TRIP_END_REMINDER ".json_encode($pushData);
                        $cronjobLog->save();
                    }
                }

                $tripModel =  Trip::where('trip_id', $_trip->trip_id)->first();
                $tripModel->is_notification_sent = IS_NOTIFICATION_SENT_TRUE;
                $tripModel->save();
            }
        }
    }
    // this function is used to update free trial pack flag expired.
    public function expiredFreeTrailPack(){
        $endOfDay = User::getEndOfDay();
        
        $childrenModel  = Child::select("child_id", "free_trial_activate","activate_date")->where(['free_trial_activate' => IS_FREE_TRIL_ACTIVATED_YES ])
                        ->where(\DB::raw("DATE_ADD(activate_date, INTERVAL free_trial_days DAY)"), "<=", $endOfDay )
                        ->get();
       
        if($childrenModel->count()){
            foreach ($childrenModel as $child) {
                $child->free_trial_activate =  IS_FREE_TRIL_ACTIVATED_EXPIRED;
                $child->save();
                
                $cronjobLog = new CronjobLog();
                $cronjobLog->notification_id =  $child->child_id;
                $cronjobLog->title = "child id save in notification_id expiredFreeTrailPack : ".json_encode($child->toArray());
                $cronjobLog->save();
            }
        }
    }

    // this function is used to update subscription flag expired.
    public function expiredSubscriptionPack(){
       
        $currentDate =  User::getDateFromTimezone(date("Y-m-d H:i:s"));

        $childrenModel  = Child::select("child_id","is_subscribed", "subscribed_upto")->where(['is_subscribed' => IS_SUBSCRIBED_YES ])
                        ->where(\DB::raw("DATE(subscribed_upto)"), "=", $currentDate )
                        ->get();

        if($childrenModel->count()){
            foreach ($childrenModel as $child) {
                $child->is_subscribed =  IS_SUBSCRIBED_EXPIRED;
                $child->save();
                
                $cronjobLog = new CronjobLog();
                $cronjobLog->notification_id =  $child->child_id;
                $cronjobLog->title = "child id save in notification_id expiredSubscriptionPack : ".json_encode($child->toArray());
                $cronjobLog->save();
            }
        }
    }

    // This function is used to send reminder subscription expierd in days notification to parent
    public function sendReminderNotification(){
        $systemConfig =  SystemConfig::where("access_key", "subscription_expired_remind_before_days")->first();
        $endOfDay = User::getEndOfDay();
        $remindDays = isset($systemConfig->value) ? $systemConfig->value : SUBSCRIPTION_EXPIRED_REMIND_BEFORE_DAYS;
        
        $childrenModel  = Child::select("child_id","child_fname","child_mname","child_lname","route_id","parent_id","school_id","is_subscribed","subscribed_upto")
                        ->where(['is_subscribed' => IS_SUBSCRIBED_YES ])
                        ->where(\DB::raw("DATE_SUB(activate_date, INTERVAL ".$remindDays." DAY)"), "<=", $endOfDay )
                        ->with(
                            [
                                'parent' => function($query) {
                                        $query->select("id",\DB::raw("CONCAT(user_fname, ' ',user_lname) AS fullname"));
                                        $query->with(['userToken' => function($q){
                                            $q->select("user_id","device_token");
                                        }]);
                                    },
                                'route' => function($query) {
                                        $query->select("route_id","route_code");
                                        $query->with(
                                            [
                                                'typeHs' => function ($query) {
                                                    $query->select("id","route_id","driver_id");
                                                    $query->with(['driver' => function($q){
                                                        $q->select("id",\DB::raw("CONCAT(user_fname,' ',user_lname) AS driver_name"));
                                                    }]);
                                                }
                                            ]);
                                    }
                            ])
                        ->get();
        
        if($childrenModel->count()){
            foreach ($childrenModel as $child) {
                if(isset($child->parent->id) && !empty($child->parent->id) && isset($child->parent->userToken) &&  count($child->parent->userToken)) {

                    $deviceTokens = [];
                    foreach ($child->parent->userToken as $key => $_userToken) {
                        if(isset($_userToken->device_token) && $_userToken->device_token) {
                            $deviceTokens[] =  $_userToken->device_token;
                        }
                    }

                    if(count($deviceTokens)) {
                        $dayDiff = User::getDateDiffInDays($child->subscribed_upto,User::getCurrentDateTime());
                        $data = [
                            'msg_type' => SUBSCRIPTION_REMINDER,
                            'msg_text' => 'Your Subscription package expired in '. $dayDiff.' day',
                            'child_id' => strval($child->child_id),
                            'child_fname' => $child->child_fname,
                            'child_mname' => $child->child_mname,
                            'child_lname' => $child->child_lname,
                            'route_code'  => isset($child->route->route_code) ? $child->route->route_code : "",
                            'driver_name' => isset($child->route->typeHs->driver->id) ? $child->route->typeHs->driver->driver_name : "",
                            'subscribed_upto ' => (int)strtotime($child->subscribed_upto),
                            'remaining_days ' => (int)$dayDiff
                        ];

                        $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                        $notifications = new NotificationDetail;
                        $notifications->driver_id = isset($child->route->typeHs->driver->id) ? $child->route->typeHs->driver->id : 0;
                        $notifications->route_id  = $child->route_id;
                        $notifications->child_id  = $child->child_id;
                        $notifications->parent_id = $child->parent_id;
                        $notifications->trip_id = 0;
                        $notifications->trip_nearby = 1;
                        $notifications->trip_end = 1;
                        $notifications->notification_data = json_encode($data);
                        
                        if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                            $notifications->trip_start = 1; // notification sent
                        } else {
                            $notifications->trip_start = 0; // notification not sent
                            $notifications->fcm_response = json_encode($sendPushNotification);
                        }

                        $notifications->save();
                        $cronjobLog = new CronjobLog();

                        $cronjobLog->notification_id = $notifications->id;
                        $cronjobLog->title = "sendReminderNotification trip id always 0.".json_encode($data);
                        $cronjobLog->save();
                    }
                }                
            }
        }
    }

    // This function is used to send start HS trip remider notification to driver based on admin time zone set default asia/kolkata.
    public function startTripReminderNotification(){
        $systemConfig =  SystemConfig::where("access_key", "admin_time_zone")->first();
        $remindMins =  SystemConfig::where("access_key", "driver_start_trip_reminder")->first();

        $timezone =  isset($systemConfig->value) ? $systemConfig->value : DEFAULT_TIMEZONE;
        $mins =  isset($remindMins->value) ? $remindMins->value : TRIP_START_REMIND_BEFORE_MIN;
        $currentTime =  User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", $timezone);

        $UTCHours = "+00:00";
        $timezoneHoursDiff = "+".User::getDateDiffInFormat(User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", "UTC"), $currentTime,"%H:%I");
        
        $routes  = RouteType::select("id","route_id","driver_id", "start_time", "end_time")
                        ->where(['route_type' => ROUTE_TYPE_HOME_TO_SCHOOL])
                        ->where(DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_SUB(start_time, INTERVAL ".$mins." MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i')"), "=", $currentTime )
                        ->with(
                            [
                                'route' => function ($query){
                                    $query->select("route_code","route_id","school_id");
                                    $query->with(['school' => function($query){
                                        $query->select("school_name","id");
                                    }]);
                                },
                                'driver' => function($query){
                                    $query->select("id",\DB::raw("CONCAT(user_fname,' ',user_lname) AS driver_name"));
                                    $query->with(['userToken' =>function($query){
                                        $query->select("user_id","device_token");
                                    }]);
                                }
                            ])
                        ->get();
        
        if(count($routes)) {

            $currentTimestamp =  time(); // timestamp in utc time.
            foreach ($routes as $routeType) {
               
                if(isset($routeType->driver->id) && !empty($routeType->driver->id) && isset($routeType->driver->userToken) &&  $routeType->driver->userToken->count()) {
                    $deviceTokens = [];
                    foreach ($routeType->driver->userToken as $key => $_userToken) {
                        if(isset($_userToken->device_token) && $_userToken->device_token) {
                            $deviceTokens[] =  $_userToken->device_token;
                        }
                    }
                    $tripLength = User::getDateDiffInMinutes($routeType->start_time,$routeType->end_time);

                    if(count($deviceTokens)) {
                        $data = [
                            'msg_type'      => TRIP_START_REMINDER,
                            'route_id'      => isset($routeType->route->route_id) ? $routeType->route->route_id : "",
                            'route_code'    => isset($routeType->route->route_code) ? $routeType->route->route_code : "",
                            'driver_name'   => isset($routeType->driver->driver_name) ? $routeType->driver->driver_name : "",
                            'school_name'   => isset($routeType->route->school->school_name) ? $routeType->route->school->school_name : "",
                            'time'          => $currentTimestamp,
                            'trip_length'   => $tripLength,
                            'trip_type'     => TRIP_TYPE_HOME_SCHOOL,
                        ];

                        $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                        $notifications = new NotificationDetail;
                        $notifications->driver_id = isset($routeType->driver->id) ? $routeType->driver->id : 0;
                        $notifications->route_id  = $routeType->route->route_id;
                        $notifications->child_id  = 0;
                        $notifications->parent_id = 0;
                        $notifications->trip_id     = 0;
                        $notifications->trip_nearby = 1;
                        $notifications->trip_end    = 1;
                        
                       $notifications->notification_data = json_encode($data);
                        
                        if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                            $notifications->trip_start = 1; // notification sent
                        } else {
                            $notifications->trip_start = 0; // notification not sent
                            $notifications->fcm_response = json_encode($sendPushNotification);
                        }
                        
                        $notifications->save();
                        $cronjobLog = new CronjobLog();

                        $cronjobLog->notification_id = $notifications->id;
                        $cronjobLog->title = "startTripReminderNotification .".json_encode($data);
                        $cronjobLog->save();
                    }
                }                
            }
        }
    }
    // This function is used to send start SH trip remider notification to driver based on admin time zone set default asia/kolkata.
    public function startSHTripReminderNotification(){
        $systemConfig =  SystemConfig::where("access_key", "admin_time_zone")->first();
        $remindMins =  SystemConfig::where("access_key", "driver_start_trip_reminder")->first();

        $timezone =  isset($systemConfig->value) ? $systemConfig->value : DEFAULT_TIMEZONE;
        $mins =  isset($remindMins->value) ? $remindMins->value : TRIP_START_REMIND_BEFORE_MIN;
        $currentTime =  User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", $timezone);

        $UTCHours = "+00:00";
        $timezoneHoursDiff = "+".User::getDateDiffInFormat(User::getTimeFromTimezone(User::getCurrentDateTime(), "H:i", "UTC"), $currentTime,"%H:%I");
        
        $routes  = RouteType::select("id","route_id","driver_id", "start_time", "end_time")
                        ->where(['route_type' => ROUTE_TYPE_SCHOOL_TO_HOME])
                        ->where(DB::raw("DATE_FORMAT(TIME(CONVERT_TZ(DATE_SUB(start_time, INTERVAL ".$mins." MINUTE),'".$UTCHours."','".$timezoneHoursDiff."')),'%H:%i')"), "=", $currentTime )
                        ->with(
                            [
                                'route' => function ($query){
                                    $query->select("route_code","route_id","school_id");
                                    $query->with(['school' => function($query){
                                        $query->select("school_name","id");
                                    }]);
                                },
                                'driver' => function($query){
                                    $query->select("id",\DB::raw("CONCAT(user_fname,' ',user_lname) AS driver_name"));
                                    $query->with(['userToken' =>function($query){
                                        $query->select("user_id","device_token");
                                    }]);
                                }
                            ])
                        ->get();
        
        if(count($routes)) {

            $currentTimestamp =  time(); // timestamp in utc time.
            foreach ($routes as $routeType) {
               
                if(isset($routeType->driver->id) && !empty($routeType->driver->id) && isset($routeType->driver->userToken) &&  $routeType->driver->userToken->count()) {
                    $deviceTokens = [];
                    foreach ($routeType->driver->userToken as $key => $_userToken) {
                        if(isset($_userToken->device_token) && $_userToken->device_token) {
                            $deviceTokens[] =  $_userToken->device_token;
                        }
                    }
                    $tripLength = User::getDateDiffInMinutes($routeType->start_time,$routeType->end_time);

                    if(count($deviceTokens)) {
                        $data = [
                            'msg_type'      => TRIP_START_REMINDER,
                            'route_id'      => isset($routeType->route->route_id) ? $routeType->route->route_id : "",
                            'route_code'    => isset($routeType->route->route_code) ? $routeType->route->route_code : "",
                            'driver_name'   => isset($routeType->driver->driver_name) ? $routeType->driver->driver_name : "",
                            'school_name'   => isset($routeType->route->school->school_name) ? $routeType->route->school->school_name : "",
                            'time'          => $currentTimestamp,
                            'trip_length'   => $tripLength,
                            'trip_type'     => TRIP_TYPE_SCHOOL_HOME,
                        ];

                        $sendPushNotification = FcmNotification::sendPushNotification($deviceTokens, $data);
                        $notifications = new NotificationDetail;
                        $notifications->driver_id = isset($routeType->driver->id) ? $routeType->driver->id : 0;
                        $notifications->route_id  = $routeType->route->route_id;
                        $notifications->child_id  = 0;
                        $notifications->parent_id = 0;
                        $notifications->trip_id     = 0;
                        $notifications->trip_nearby = 1;
                        $notifications->trip_end    = 1;
                        
                        $notifications->notification_data = json_encode($data);
                        
                        if (isset($sendPushNotification->success) && $sendPushNotification->success >= 1) {
                            $notifications->trip_start = 1; // notification sent
                        } else {
                            $notifications->trip_start = 0; // notification not sent
                            $notifications->fcm_response = json_encode($sendPushNotification);
                        }
                        $notifications->save();
                        $cronjobLog = new CronjobLog();

                        $cronjobLog->notification_id = $notifications->id;
                        $cronjobLog->title = "startSHTripReminderNotification .".json_encode($data);
                        $cronjobLog->save();
                    }
                }                
            }
        }
    }

    // This function is used to save driver monthly earning.
    public function saveDriverMonthlyEarning() {
        $dateDiff =  User::getDateDiffInDays(User::getCurrentMonthLastDate(),User::getCurrentDate());
        if($dateDiff === 0) {
            $driverModel  = User::select("id",\DB::raw("CONCAT(user_fname,' ',user_lname) AS driver_name"))
                        ->with(["userRouteHs" => function($query){
                            $query->select("id","route_id","route_type","driver_id");
                        }])
                        ->where(['user_role' => User::USER_ROLE_DRIVER])
                        ->get();
            
            if($driverModel->count()) {
                foreach ($driverModel as $driver) {
                    
                    if(isset($driver->userRouteHs) && $driver->userRouteHs->count()) {

                        $userEarningModel =  new UserEarning();
                        $userEarningModel->user_id  =  $driver->id;
                        $userEarningModel->amount   =  $driver->getMothlyEarning($driver->id);
                        $userEarningModel->type     =  UserEarning::EARNING_TYPE_CREDIT;
                        $userEarningModel->save();

                        $cronjobLog = new CronjobLog();
                        $cronjobLog->notification_id =  $driver->id;
                        $cronjobLog->title = "saveDriverMonthlyEarning : ".json_encode($driver->toArray());
                        $cronjobLog->save();
                    }
                }
            }
        }
    }

    public function endTrip() {
        Trip::endTripBySystem(ROUTE_TYPE_HOME_TO_SCHOOL);
        Trip::endTripBySystem(ROUTE_TYPE_SCHOOL_TO_HOME);
    }
}