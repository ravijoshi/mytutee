<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect;
use App\Mailtemplate;

class MailtemplateController extends Controller
{
    public function index() {
        return View('pages/mailtemplate/index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'subject',
            1 => 'access_key',
            2 => 'status',
            3 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = Mailtemplate::orderBy($order, $dir);

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->where('subject', 'LIKE', "%{$search}%");
                $q->orWhere('access_key', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $templates =  $select->offset($start)->limit($limit)->get();
        

        $data = array();
        if (!empty($templates)) {
            
            foreach ($templates as $_template) {
                $Editlink = route('mailtemplate.edit', $_template->id);
                
                $nestedData['subject'] = $_template->subject;
                $nestedData['access_key'] = $_template->access_key;
                
                if ($_template->status == "1") {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_template->id,this);'>Active</button>";
                } else {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_template->id,this);'>Inactive</button>";
                }

                $viewElement = $_template->id.',"'.$_template->subject.'"';
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>"; /*"<button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";*/
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create() {
        return View('pages/mailtemplate/create');
    }

    public function edit($id ="") {
    	if(empty($id) || !isset($id)) {
            return redirect()->route("mailtemplate")->with("error","invalid Mailtemplate id.");
        }

        $mailtemplate = Mailtemplate::find($id);
        if(empty($mailtemplate))
        {
            return redirect()->route("mailtemplate")->with("error","Mailtemplate not found.");   
        }
        return view('pages.mailtemplate.edit', compact('mailtemplate'));
    }

    public function save(Request $request) {

        $id = $request->input('id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'subject' => 'required',
                'access_key' => 'required|unique:mailtemplate,access_key,'.$id.',id,deleted_at,NULL',
                'content' => 'required',
                'cc' => 'nullable|email',
                'bcc' => 'nullable|email',
                'status' => 'required|in:0,1',
                'id' => 'required',
            ]);
        } else {
             $input = $request->validate([
                'subject' => 'required',
                'access_key' => 'required|unique:mailtemplate,access_key,null,id,deleted_at,NULL',
                'content' => 'required',
                'cc' => 'nullable|email',
                'bcc' => 'nullable|email',
                'status' => 'required|in:0,1',
            ]);
         }

        if (isset($input['id']) && !empty($input['id'])) {

            $mailtemplate = Mailtemplate::find($input['id']);
            if(empty($mailtemplate)) {
                return redirect()->route("mailtemplate")->with("error","Mailtemplate not found.");                
            }
            $message = "Mailtemplate updated Sucessfully";
        } else {
            $mailtemplate = new Mailtemplate();
            $message = "Mailtemplate added Sucessfully";
        }
        
        $mailtemplate->subject = $input["subject"];
        $mailtemplate->access_key = $input["access_key"];
        $mailtemplate->content = $input["content"];
        $mailtemplate->cc = isset($input["cc"]) ? $input["cc"] : NULL;
        $mailtemplate->bcc = isset($input["bcc"]) ? $input["bcc"] : NULL;
        $mailtemplate->status = $input["status"];
        $mailtemplate->save();

        return redirect()->route("mailtemplate")->with("success",$message);
    }


    public function delete($id = "") {
        $message = "Mailtemplate deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid mailtemplate id.";
            $responseType = "failure";
        }

        $mailtemplate = Mailtemplate::find($id);
        
        if(empty($mailtemplate))
        {
            $message = "Mailtemplate not found.";
            $responseType = "failure";
        } else{

            $mailtemplate->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Mailtemplate id.";
            $responseType = "failure";
        } else {
            $mailtemplate = Mailtemplate::find($id);

            if(empty($mailtemplate)) {
                $message = "Mailtemplate not found.";
                $responseType = "failure";
            } else {
                if ($mailtemplate->status == 1) {
                    $mailtemplate->status = 0;
                    $content =  "Inactive";
                } else {
                    $mailtemplate->status = 1;
                }
                $mailtemplate->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" => $content
        ]);
    }
    
}
