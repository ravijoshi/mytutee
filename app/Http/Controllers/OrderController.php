<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Redirect;
use App\Payment;

class OrderController extends Controller
{
    public function index() {
        return view('pages.order.index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'parent_name',
            1 => 'child_name',
            2 => 'title',
            3 => 'price',
            4 => 'type',
            5 => 'payment_status',
            6 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        $select = Payment::select("*",
            DB::raw('CONCAT("'.DEFAULT_CURRENCY_SYMBOL.' ",package_price) AS price'), 
            DB::raw('IF(type = 1, "Subscribed", "Unsubscribed") AS type'), 
            DB::raw("CONCAT(U.user_fname, ' ',U.user_lname) as parent_name"), 
            DB::raw("CONCAT(C.child_fname, ' ',C.child_mname, ' ',C.child_lname) AS child_name"),
            DB::raw('
                CASE
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_INPROCESS.' THEN "'.Payment::PAYMENT_STATUS_INPROCESS_TEXT.'"
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_PAID.' THEN "'.Payment::PAYMENT_STATUS_PAID_TEXT.'"
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_CANCEL.' THEN "'.Payment::PAYMENT_STATUS_CANCEL_TEXT.'"
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_OTHER.' THEN "'.Payment::PAYMENT_STATUS_OTHER_TEXT.'"
                    ELSE "'.Payment::PAYMENT_STATUS_PENDING_TEXT.'"
                END
                 As payment_status'), 
            "P.title") 
        ->orderBy($order, $dir);
        
        $select->leftJoin("users as U", function($join){
            $join->on("U.id", "=","payment.parent_id");
        });
        $select->leftJoin("childs as C", function($join){
            $join->on("C.child_id", "=","payment.child_id");
        });
        $select->leftJoin("packege as P", function($join){
            $join->on("P.id", "=","payment.package_id");
        });

        if (!empty($search) ) {
            $select = $select->where(function($q)use($search) {
                $q->orWhere("package_price", 'LIKE', "%{$search}%");
                $q->orWhere(DB::raw('IF(type = 1, "Subscribed", "Unsubscribed")'), 'LIKE', "%{$search}%");
                $q->orWhere(\DB::raw("CONCAT(U.user_fname, ' ',U.user_lname)"), "LIKE", "%{$search}%");
                $q->orWhere(\DB::raw("CONCAT(C.child_fname, ' ',C.child_mname, ' ',C.child_lname)"), "LIKE", "%{$search}%");
                $q->orWhere(DB::raw('
                CASE
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_INPROCESS.' THEN "'.Payment::PAYMENT_STATUS_INPROCESS_TEXT.'"
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_PAID.' THEN "'.Payment::PAYMENT_STATUS_PAID_TEXT.'"
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_CANCEL.' THEN "'.Payment::PAYMENT_STATUS_CANCEL_TEXT.'"
                    WHEN payment_status = '.Payment::PAYMENT_STATUS_OTHER.' THEN "'.Payment::PAYMENT_STATUS_OTHER_TEXT.'"
                    ELSE "'.Payment::PAYMENT_STATUS_PENDING_TEXT.'"
                END
                 '), "LIKE", "%{$search}%");
                $q->orWhere("P.title", "LIKE", "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $orders =  $select->offset($start)->limit($limit)->get();

        $data = array();
        if (!empty($orders)) {
            
            foreach ($orders as $_order) {
                $viewLink = route('order.view', $_order->id);
                
                $nestedData['parent_name'] = isset($_order->parent_name) ? $_order->parent_name :  "";
                $nestedData['child_name'] = isset($_order->child_name) ? $_order->child_name :  "";
                $nestedData['title'] = isset($_order->title) ? $_order->title :  "";
                $nestedData['price'] = $_order->price ;
                $nestedData['type'] = $_order->type ;
                
                $nestedData['action'] = "<a href='" . $viewLink . "'  title='View' class='btn btn-info btn-sm' ><span class='fa fa-info'></span></a> ";
                    /*" <button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";*/
                $nestedData['payment_status'] = /*$_order->getStatusOptionsText*/($_order->payment_status);

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function view($id ="") {
    	if(empty($id) || !isset($id)) {
            return back()->with("error","invalid Order id.");
        }

        $order = Payment::find($id);
        if(empty($order))
        {
            return back()->with("error","Order not found.");   
        }
        return view('pages.order.view', compact('order'));
    }
    
}
