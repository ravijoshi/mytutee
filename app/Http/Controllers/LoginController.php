<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Validator, Auth, Hash, Redirect, DB;
use App\User, App\UserEarning, App\School, App\Payment;

class LoginController extends BaseController {

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function showLogin() {
        if (Auth::check()) {
            return Redirect('dashboard');
        } else {
            return view('pages/login/login');
        }
    }

    public function showDashboard() {

        $totalParentUser  = User::where("user_role",User::USER_ROLE_PARENT)->count();
        $totalDriverUser  = User::where("user_role",User::USER_ROLE_DRIVER)->count();
        $totalPayment  = UserEarning::where("type",UserEarning::EARNING_TYPE_CREDIT)->sum("amount");
        $totalEarning  = Payment::where(["type" => Payment::PAYMENT_TYPE_SBUSCRIBED, "payment_status" =>Payment::PAYMENT_STATUS_PAID])->sum("package_price");
        $totalSchool  = School::count();

        return view('layouts/default',compact('totalSchool','totalParentUser','totalDriverUser','totalPayment','totalEarning'));
    }

    public function doLogin() {
        $input = Input::all();
        $result = Auth::attempt(["email" => $input["email"], "password" => $input["password"], "user_role" => User::USER_ROLE_ADMIN]);
        if ($result) {
            return Redirect::intended('dashboard');
        } else {
            return redirect()->route('login')->with('error', 'Invalid Username or Password');
        }
    }

    public function forgotPassword(){
        if (Auth::check()) {
            return Redirect('dashboard');
        }
        return view('pages/login/forgotpwd');
    }

    public function resetPassword($token = "") {
        if (Auth::check()) {
            return Redirect('dashboard');
        }

        if(empty($token) || !isset($token)) {
            $message = "Reset password link expired.";
            $code = "error";

            return redirect("/")->with($code,$message);
        }

        $userModel = User::where("forgot_pwd_token", $token)->where("forgot_pwd_token","!=",NULL)->first();
        if(!empty($userModel) && $userModel) {
            return view('pages.login.resetpwd',compact("token"));
        }

        $message = "Reset password link expired.";
        $code = "error";
        return redirect("/")->with($code,$message);

    }

    public function resetPasswordPost(Request $request){
        $request->validate([
            'forgot_token' => 'required',
            'password' => 'required|string|min:6',
            'confirm_password' => 'required_with:password|same:password|min:6',
        ]);

        $code = 'error';
        $message  = "Reset password link expired.";
        $userModel = User::where("forgot_pwd_token", $request->forgot_token)->where("forgot_pwd_token","!=",NULL)->first();
        
        if(!empty($userModel) && $userModel){
            $userModel->password =  Hash::make($request->password);
            $userModel->forgot_pwd_token = NULL;
            $userModel->save();
            $code = 'success';
            $message  = "Password reset successfully. You can login now.";
        }

        return redirect("/")->with($code,$message);
    }
    
    public function doLogout() {
        Auth::logout();
        return Redirect('/');
    }

}
