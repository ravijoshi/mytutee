<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator ,Hash , DB, Auth, Redirect;
use App\User , App\Mailtemplate;

class UserController extends Controller
{
    public function index() {
    	$user = Auth::user();
        return View('pages/user/index', compact("user"));
    }

    public function edit() {
    	$user = Auth::user();
        return View('pages/user/edit', compact("user"));
    }


    // update profile
    public function update(Request $request) {

    	$user = Auth::user();
        $validationRule = ["user_fname.required" => "The user first name field is required.","user_lname.required" => "The user last name field is required."];
        $request->validate([
            'user_fname' => 'required',
            'user_lname' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id.',id,deleted_at,NULL',
            'user_phone' => 'required',
            // 'user_role' => 'required|in:'.User::USER_ROLE_ADMIN.",".User::USER_ROLE_DRIVER.",".User::USER_ROLE_PARENT,
            // 'user_status' => 'required|in:0,1',
            // 'id' => 'required',
        ],$validationRule);
        $user->user_fname 	= $request->user_fname;
        $user->user_lname 	= $request->user_lname;
        $user->email 		= $request->email;
        $user->user_phone 	= $request->user_phone;
        $user->save();

        return redirect()->route("profile")->with("success","Profile updated successfully.");
    }

    // change pwd
    public function changePassword() {
        return View('pages/user/password');
    }

    // change pwd post
    public function changePasswordPost(Request $request) {

        $request->validate([
            'old_password' => 'required',
            'password' => 'required|string|min:6',
            'confirm_password' => 'required_with:password|same:password|min:6',
        ]);

    	$user = Auth::user();
    	$message = "Old Password is not match.";
    	$code = "error";

        if (Hash::check($request->old_password, $user->password)) {
            $user->password =  Hash::make($request->password);
            $user->save();
            $code = "success";
            $message = "Password updated successfully.";
        }

        return redirect()->route("profile")->with($code,$message);
    }

    public function forgotPwd(Request $request) {
       
        $request->validate([
            'email' => 'required|email'
        ]);

		$code = "error";
		$message = "This email not linked with us.";

        $user = User::where(['email' => $request->email, "user_role" => User::USER_ROLE_ADMIN])->first();

        if(!empty($user) && isset($user->id)  ) {
            $mailtemplateModel = Mailtemplate::where(["access_key" => "forgot_password","status" => 1])->first();
            
            $code = "error";
	        $message = "Mail not sent somthing went wrong.";
            
            if(!empty($mailtemplateModel) && $mailtemplateModel) {

                $forgotToken = bin2hex(random_bytes(16)).str_random("10");
                $changePwdUrl = route('resetpassword') ."/". $forgotToken;
                $user->forgot_pwd_token =  $forgotToken;
                $user->save();
            
	            $mail_body = $mailtemplateModel->content;
	            $mail_body = str_replace('{{name}}', $user->first_name . ' ' . $user->last_name, $mail_body);
                $mail_body = str_replace('{{website_url}}', url("/"), $mail_body);
	            $mail_body = str_replace('{{website_name}}',env('APP_NAME', 'My Tutee'), $mail_body);
	            $mail_body = str_replace('{{changeUrl}}', $changePwdUrl, $mail_body);
	            $mail_body = str_replace('{{emailLogo}}', asset('public/images/logo.png'), $mail_body);
	            $mail_body = str_replace('{{logoUrl}}', asset('public/logo.png'), $mail_body);

	            $mailData = [
	                "from" => FROM_EMAIL,
	                "from_title" => FROM_EMAIL_TITLE,
	                "subject" => str_replace('{{website_name}}', env('APP_NAME', 'My Tutee') , $mailtemplateModel->subject) ,
	                "to" => $user->email,
	                "full_name" => $user->first_name . ' ' . $user->last_name,
	                "mail_body" => $mail_body
	                ];

	            $sendEmail = $user->sendForgotPasswordMail($mailData);

	            if($sendEmail){
	                $code = "success";
	                $message = "Reset Password mail sent to your mail address.";
	            }else{
	                $code = "error";
	                $message = "Mail not sent somthing went wrong.";
	            }
	        }
        }

        if($code == 'error')
        	return redirect()->route('forgotPassword')->with($code,$message);
        
        return redirect("/")->with($code,$message);
    }


    public function registerationPending(){
        return View('pages/user/registerPending');
    }

    public function registerationPendingList(Request $request){
        $columns = array(
            0 => 'user_name',
            1 => 'email',
            2 => 'user_phone',
            3 => 'user_type',
            4 => 'created_on',
            5 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = User::withTrashed()->where('user_otp_verification', User::USER_OTP_VERIFIED_FALSE)->orderBy($order, $dir)
                ->select("*", 
                    DB::raw('CONCAT(user_fname," ",user_lname) AS user_name'), 
                    DB::raw("IF(user_role = ".User::USER_ROLE_DRIVER.",'".User::USER_ROLE_DRIVER_TEXT."','".User::USER_ROLE_PARENT_TEXT."') as user_type"),
                    DB::raw("IF(deleted_at IS NOT NULL,1,0) as hard_delete"),
                    DB::raw('DATE_FORMAT(created_at,"%D %M %Y  %h:%m:%s%p" ) AS created_on')
                );

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->orWhere(DB::raw('CONCAT(user_fname," ",user_lname)'), 'LIKE', "%{$search}%");
                $q->orWhere('email', 'LIKE', "%{$search}%");
                $q->orWhere('user_phone', 'LIKE', "%{$search}%");
                $q->orWhere(DB::raw('DATE_FORMAT(created_at,"%D %M %Y  %h:%m:%s%p" )'), 'LIKE', "%{$search}%");
                $q->orWhere( DB::raw("IF(user_role = ".User::USER_ROLE_DRIVER.",'".User::USER_ROLE_DRIVER_TEXT."','".User::USER_ROLE_PARENT_TEXT."')"), 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $users =  $select->offset($start)->limit($limit)->get();

        $data = array();
        if (!empty($users)) {
            
            foreach ($users as $_user) {
                
                $nestedData['user_name'] = $_user->user_name;
                $nestedData['email'] = $_user->email;
                $nestedData['user_phone'] = $_user->user_phone;
                $nestedData['user_type'] = $_user->user_type;
                $nestedData['created_on'] = $_user->created_on;
                
                $viewElement = $_user->id.',"'.$_user->user_name.'"';

                
                $nestedData['action'] = " <button type='button' class='btn btn-danger btn-sm' title='Delete' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        return response()->Json($json_data);
    }

    public function forcedelete($id = "") {
        $message = "User deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid  user id.";
            $responseType = "failure";
        }

        $parentUser = User::where(["id" => $id ])->first();
        
        if(empty($parentUser)) {
            $message = "User not found.";
            $responseType = "failure";
        } else{
            $parentUser->forcedelete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);

    }
}
