<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator , DB, Auth, Redirect;
use App\Bank;

class BankController extends Controller
{
    public function index() {
        return View('pages/bank/index');
    }

    public function grid(Request $request) {
        $columns = array(
            0 => 'name',
            1 => 'status',
            2 => 'action',
        );
        
        $limit = $request->input('length');
        $start = $request->input('start');

        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');
        
        $select = Bank::orderBy($order, $dir);

        
        if (!empty($search) ) {
        
            $select = $select->where(function($q)use($search) {
                $q->where('name', 'LIKE', "%{$search}%");
            });
        }

        $totalFiltered = $totalData = $select->count();
        $banks =  $select->offset($start)->limit($limit)->get();
        

        $data = array();
        if (!empty($banks)) {
            
            foreach ($banks as $_bank) {
                $Editlink = route('bank.edit', $_bank->id);
                
                $nestedData['name'] = $_bank->name;
                
                if ($_bank->status == "1") {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-success btn-sm' onclick='ChangeStatus($_bank->id, this);'>Active</button>";
                } else {
                    $nestedData['status'] = "<button type='button' class='btn btn-block btn-danger btn-sm' onclick='ChangeStatus($_bank->id, this);'>Inactive</button>";
                }
                $viewElement = $_bank->id.',"'.$_bank->name.'"';
                $nestedData['action'] = "<a href='" . $Editlink . "'  title='Edit' class='btn btn-success btn-sm' ><span class='fa fa-edit'></span></a>  <button type='button' class='btn btn-danger btn-sm' onclick='getDelConfirmation(" . $viewElement . ");'><i class='fa fa-trash'></i></button>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        // echo json_encode($json_data);
        return response()->Json($json_data);
    }

    public function create() {
        return View('pages/bank/create');
    }

    public function edit($id ="") {
    	if(empty($id) || !isset($id)) {
            return redirect()->route("bank")->with("error","invalid Bank id.");
        }

        $bank = Bank::find($id);
        if(empty($bank))
        {
            return redirect()->route("bank")->with("error","Bank not found.");   
        }
        return view('pages.bank.edit', compact('bank'));
    }

    public function save(Request $request) {

        $id = $request->input('id');
        if (isset($id) && $id) {
            $input = $request->validate([
                'name' => 'required',
                'status' => 'required|in:0,1',
                'id' => 'required',
            ]);
        } else {
             $input = $request->validate([
                'name' => 'required',
                'status' => 'required|in:0,1',
            ]);
         }
        
        if (isset($input['id']) && !empty($input['id'])) {

            $bank = Bank::find($input['id']);
            if(empty($bank)) {
                return redirect()->route("bank")->with("error","Bank not found.");                
            }
            $message = "Bank updated Sucessfully";
        } else {
            $bank = new Bank();
            $message = "Bank added Sucessfully";
        }
        
        $bank->name = $input["name"];
        $bank->status = $input["status"];
        $bank->save();

        return redirect()->route("bank")->with("success",$message);
    }


    public function delete($id = "") {
        $message = "Bank deleted successfully.";
        $responseType = "success";

        if(empty($id) || !isset($id)) {
            $message = "Invalid bank id.";
            $responseType = "failure";
        }

        $bank = Bank::find($id);
        
        if(empty($bank))
        {
            $message = "Bank not found.";
            $responseType = "failure";
        } else{

            $bank->delete();
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message
        ]);
    }

    public function changeStatus($id = "") {

        $message = "Status updated successfully.";
        $responseType = "success";
        $content = "Active";
        
        if(empty($id) || !isset($id)) {
            $message = "Invalid Bank id.";
            $responseType = "failure";
        } else {
            $bank = Bank::find($id);

            if(empty($bank)) {
                $message = "Bank not found.";
                $responseType = "failure";
            } else {
                if ($bank->status == 1) {
                    $bank->status = 0;
                    $content = "Inactive";
                } else {
                    $bank->status = 1;
                }
                $bank->save();
            }
        }

        return response()->json([
            "responseType" =>$responseType,
            "message" =>$message,
            "content" =>$content,
        ]);
    }
    
}
