<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEarning extends Model {

    use SoftDeletes;
    
    protected $table = 'user_earning';
    protected $primaryKey = 'id';

    public $timestamps = true;
    const  EARNING_TYPE_CREDIT = 1 ;//plus
    const  EARNING_TYPE_DEBIT  = 2 ;//minus

    protected $fillable = [
        'user_id','amount', 'type'
    ];

}
