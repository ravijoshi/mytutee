<?php 
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\SystemConfig;
class Paytm {
   
   
    /*
    - Use _environment as 'PROD' if you wanted to do transaction in production environment else 'TEST' for doing transaction in testing environment.
    - Change the value of _merchantKey constant with details received from Paytm.
    - Change the value of _merchantId constant with details received from Paytm.
    - Change the value of _merchantWebsite constant with details received from Paytm.
    - Above details will be different for testing and production environment.
    */

    protected  $_merchantKey;
    protected  $_merchantId ;  //Change this constant's value with MID (Merchant ID) received from Paytm.
    protected  $_merchantWebsite ; //Change this constant's value with Website name received from Paytm.
    protected  $_environment;  // 0 = TEST 1 =PROD
    protected  $_callbackUrl;  //Provided by Paytm
    protected  $_channelId;    //Provided by Paytm
    protected  $_industryTypeId;    //Provided by Paytm
    protected  $_refundUrl  = '';
    protected  $_queryUrl;
    protected  $_queryNewUrl;
    protected  $_txnUrl;

    


    public function __construct(){

        $paytmEnvironment   =  SystemConfig::where("access_key", "PAYTM_ENVIRONMENT")->first();
        $paytmMerchantKey   =  SystemConfig::where("access_key", "PAYTM_MERCHANT_KEY")->first();
        $paytmMerchantId    =  SystemConfig::where("access_key", "PAYTM_MERCHANT_MID")->first();
        $paytmWebsite       =  SystemConfig::where("access_key", "PAYTM_MERCHANT_WEBSITE")->first();
        $paytmCallbackUrl   =  SystemConfig::where("access_key", "PAYTM_CALLBACK_URL")->first();
        $paytmChannelId     =  SystemConfig::where("access_key", "CHANNEL_ID")->first();
        $paytmIndustryTypeId   =  SystemConfig::where("access_key", "INDUSTRY_TYPE_ID")->first();
        
        $this->_merchantKey     =  $paytmMerchantKey->value;
        $this->_merchantId      =  $paytmMerchantId->value;
        $this->_merchantWebsite =  $paytmWebsite->value;
        $this->_environment     =  $paytmEnvironment->value;
        $this->_callbackUrl     =  $paytmCallbackUrl->value;
        $this->_channelId       =  $paytmChannelId->value;
        $this->_industryTypeId  =  $paytmIndustryTypeId->value;

        $this->_queryNewUrl= 'https://securegw-stage.paytm.in/merchant-status/getTxnStatus';
        $this->_txnUrl='https://securegw-stage.paytm.in/theia/processTransaction';
        // check paytm environment is test or production.
        if (isset($this->_environment) && $this->_environment == 1) {
            $this->_queryNewUrl='https://securegw.paytm.in/merchant-status/getTxnStatus';
            $this->_txnUrl='https://securegw.paytm.in/theia/processTransaction';
        }

        $this->_queryUrl =  $this->_queryNewUrl;
    }

    public function getPaytmProperties()
    {
        return  [
            'merchantKey'       => $this->_merchantKey,
            'merchantId'        => $this->_merchantId,
            'merchantWebsite'   => $this->_merchantWebsite,
            'industryTypeId'    => $this->_industryTypeId,
            'channelId'         => $this->_channelId,
            'callbackUrl'       => $this->_callbackUrl,
        ];
    }

    public function encrypt_e_openssl($input, $ky)
    {
        $iv = "@@@@&&&&####$$$$";
        $data = openssl_encrypt($input, "AES-128-CBC", $ky, 0, $iv);
        return $data;
    }

    public function decrypt_e_openssl($crypt, $ky)
    {
        $iv = "@@@@&&&&####$$$$";
        $data = openssl_decrypt($crypt, "AES-128-CBC", $ky, 0, $iv);
        return $data;
    }

    public function generateSalt_e($length)
    {
        $random = "";
        srand((double)microtime() * 1000000);
        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data.= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data.= "0FGH45OP89";
        for ($i = 0; $i < $length; $i++) {
            $random.= substr($data, (rand() % (strlen($data))) , 1);
        }

        return $random;
    }

    public function checkString_e($value)
    {
        $myvalue = ltrim($value);
        $myvalue = rtrim($myvalue);
        if ($myvalue == 'null') $myvalue = '';
        return $myvalue;
    }

    public function getChecksumFromArray($arrayList, $key, $sort = 1)
    {
        if ($sort != 0) {
            ksort($arrayList);
        }

        $str = $this->getArray2Str($arrayList);
        $salt = $this->generateSalt_e(4);
        $finalString = $str . "|" . $salt;
        $hash = hash("sha256", $finalString);
        $hashString = $hash . $salt;
        $checksum = $this->encrypt_e_openssl($hashString, $key);
        return $checksum;
    }

    public function getChecksumFromString($str, $key)
    {
        $salt = $this->generateSalt_e(4);
        $finalString = $str . "|" . $salt;
        $hash = hash("sha256", $finalString);
        $hashString = $hash . $salt;
        $checksum = $this->encrypt_e_openssl($hashString, $key);
        return $checksum;
    }

    public function verifychecksum_e($arrayList, $key, $checksumvalue)
    {
        $arrayList = $this->removeCheckSumParam($arrayList);
        ksort($arrayList);
        $str = $this->getArray2Str($arrayList);
        $paytm_hash = $this->decrypt_e_openssl($checksumvalue, $key);
        $salt = substr($paytm_hash, -4);
        $finalString = $str . "|" . $salt;
        $website_hash = hash("sha256", $finalString);
        $website_hash.= $salt;
        $validFlag = "FALSE";
        
        if ($website_hash == $paytm_hash) {
            $validFlag = "TRUE";
        } else {
            $validFlag = "FALSE";
        }

        return $validFlag;
    }

    public function verifychecksum_eFromStr($str, $key, $checksumvalue)
    {
        $paytm_hash = $this->decrypt_e_openssl($checksumvalue, $key);
        $salt = substr($paytm_hash, -4);
        $finalString = $str . "|" . $salt;
        $website_hash = hash("sha256", $finalString);
        $website_hash.= $salt;
        $validFlag = "FALSE";
        
        if ($website_hash == $paytm_hash) {
            $validFlag = "TRUE";
        } else {
            $validFlag = "FALSE";
        }

        return $validFlag;
    }

    public function getArray2Str($arrayList)
    {
        $paramStr = "";
        $flag = 1;
        foreach($arrayList as $key => $value) {
            if ($flag) {
                $paramStr.= $this->checkString_e($value);
                $flag = 0;
            } else {
                $paramStr.= " | " . $this->checkString_e($value);
            }
        }

        return $paramStr;
    }

    public function redirect2PG($paramList, $key)
    {
        $hashString = $this->getchecksumFromArray($paramList);
        $checksum = $this->encrypt_e_openssl($hashString, $key);
    }

    public function removeCheckSumParam($arrayList)
    {
        if (isset($arrayList["CHECKSUMHASH"])) {
            unset($arrayList["CHECKSUMHASH"]);
        }

        return $arrayList;
    }

    public function getTxnStatus($requestParamList)
    {
        return $this->callAPI($this->_queryNewUrl, $requestParamList);
    }

    public function initiateTxnRefund($requestParamList)
    {
        $CHECKSUM = $this->getChecksumFromArray($requestParamList, $this->_merchantKey, 0);
        $requestParamList["CHECKSUM"] = $CHECKSUM;
        return $this->callAPI($this->_refundUrl, $requestParamList);
    }

    public function callAPI($apiURL, $requestParamList)
    {
        $jsonResponse = "";
        $responseParamList = array();
        $JsonData = json_encode($requestParamList);
        $postData = 'JsonData=' . urlencode($JsonData);
        $ch = curl_init($apiURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content - Type : application / json',
            'Content - Length:
                ' . strlen($postData)
            ));

        $jsonResponse = curl_exec($ch);
        $responseParamList = json_decode($jsonResponse, true);
        return $responseParamList;
    }

}